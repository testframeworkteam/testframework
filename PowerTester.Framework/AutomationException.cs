﻿using System;

namespace PowerTester.Framework
{
    /// <summary>
    /// Exception for WebAutomation
    /// </summary>
    public class AutomationException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AutomationException"/> class.  
        /// </summary>
        public AutomationException()
            : base("Unknown automation related exception occured.")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AutomationException"/> class.  
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public AutomationException(string message)
            : base(message)
        {
        }
    }

    public class AutomationExceptionNotImplemented : AutomationException
    {
         /// <summary>
        /// Initializes a new instance of the <see cref="AutomationExceptionNotImplemented"/> class.  
        /// </summary>
        public AutomationExceptionNotImplemented()
            : base("This WebAutomation related function is not yet implemented!")
        {
        }
    }
}