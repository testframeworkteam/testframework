﻿using System;
using NUnit.Framework;

namespace PowerTester.Framework.BaseClasses
{
    [TestFixture]
    [Category(CategoryFull)]
    public class TestBase
    {
        public const string CategorySmoke = "Smoke";
        public const string CategoryNightRun = "NightRun";
        public const string CategoryFull = "FullTest";
        public const string CategoryPerformanceTest = "PerformanceTest";

        public const string EnvironmentTest = "Test";
        public const string EnvironmentIntegration = "Integration";
        public const string EnvironmentLocal = "Local";
        public const string EnvironmentInstalled = "Installed";

        [OneTimeSetUp]
        public virtual void OneTimeSetUp()
        {
        }

        [SetUp]
        public virtual void SetUp()
        {
            General.ClearOutputDitectory();
        }
        
        [TearDown]
        public virtual void TearDown()
        {
        }

        [OneTimeTearDown]
        public virtual void OneTimeTearDown()
        {
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Assembly)]
    public class ExcludeEnvironmentAttribute : PropertyAttribute
    {

        public ExcludeEnvironmentAttribute(string environments) : base(environments)
        {
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Assembly)]
    public class IncludeEnvironmentAttribute : PropertyAttribute
    {
        public IncludeEnvironmentAttribute(string environments): base(environments)
        {}

    }
}
