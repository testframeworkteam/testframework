﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Services.Protocols;
using System.Text.RegularExpressions;
using System.Xml.Schema;
using NUnit.Framework;

namespace PowerTester.Framework.BaseClasses
{
    public class WSTestBase : TestBase
    {
        public static bool ValidationResult { get; set; }

        public virtual void CheckQueryResponse(string query, string reference)
        {
            CheckQueryResponse(query, true, reference);
        }

        public virtual void CheckQueryResponse(string query, int errorNumber)
        {
            try
            {
                string sdmxResponse = ExecuteQuery(query);
                Logger.Info("sdmx response written to " + General.TestResultFolder);
                General.WriteToTestOutputDitectory("respose.xml", sdmxResponse);

                if (errorNumber != 0)
                {
                    TestExecuter.Assert.Assert(new AutomationException("Exception expected, bot not thrown."));
                }
            }
            catch (SoapException ex)
            {
                //// We got sopa exception. check for error number in it, and if it was expected or not.
                if (errorNumber == 0)
                {
                    TestExecuter.Assert.Info("Unexpected SoapException thrown...");
                    General.GetErrorCode(ex);
                    TestExecuter.Assert.Assert(ex);
                }
                else
                {
                    TestExecuter.Assert.Info("Validating SoapException...");

                    TestExecuter.Assert.AreEqual(errorNumber, General.GetErrorCode(ex), "Wrong error number in SoapException thrown.");
                }
            }
            catch (System.Net.WebException ex)
            {
                //// We got web exception. check for error number in it, and if it was expected or not.
                if (errorNumber == 0)
                {
                    TestExecuter.Assert.Info("Unexpected WebException thrown...");
                    General.GetErrorCode(ex);
                    TestExecuter.Assert.Assert(ex);
                }
                else
                {
                    TestExecuter.Assert.Info("Validating WebException...");
                    TestExecuter.Assert.AreEqual(errorNumber, General.GetErrorCode(ex), "Wrong error number in WebException thrown.");
                }
            }
        }

        public virtual void CheckQueryResponse(string query, bool validateXsd = true)
        {
            CheckQueryResponse(query, validateXsd, "");
        }

        public virtual void CheckQueryResponse(string query, bool validateXsd, string reference)
        {
            try
            {
                string sdmxResponse = ExecuteQuery(query);
                Logger.Info("sdmx response written to " + General.TestResultFolder);
                General.WriteToTestOutputDitectory("respose.xml", sdmxResponse);

                if (validateXsd)
                {
                    TestExecuter.Assert.Info("Validating the returned xml...");
                    ValidateResult(sdmxResponse);
                }

                if (!string.IsNullOrEmpty(reference))
                {
                    TestExecuter.Assert.Info("Comparing output response to reference response...");
                    CompareToReference(sdmxResponse, reference);
                }
            }
            catch (SoapException ex)
            {
                //// We got sopa exception. check for error number in it, and if it was expected or not.
                TestExecuter.Assert.Info("Unexpected SoapException thrown...");
                General.GetErrorCode(ex);
                TestExecuter.Assert.Assert(ex);
            }
            catch (System.Net.WebException ex)
            {
                //// We got web exception. check for error number in it, and if it was expected or not.
                TestExecuter.Assert.Info("Unexpected WebException thrown...");
                General.GetErrorCode(ex);
                TestExecuter.Assert.Assert(ex);
            }
        }

        public virtual void CheckQueryResponse(string query, bool validateXsd, List<string> contains = null, List<string> notContains = null, bool ignoreFormating = false)
        {
            try
            {
                string sdmxResponse = ExecuteQuery(query);
                Logger.Info("sdmx response written to " + General.TestResultFolder);
                General.WriteToTestOutputDitectory("response.xml", sdmxResponse);

                if (validateXsd)
                {
                    TestExecuter.Assert.Info("Validating the returned xml...");
                    ValidateResult(sdmxResponse);
                }

                if (contains != null)
                {
                    foreach (string pattern in contains)
                    {
                        if (ignoreFormating)
                        {
                            TestExecuter.Assert.Contains(Regex.Replace(pattern, @"\s+", ""), Regex.Replace(sdmxResponse, @"\s+", ""));
                        }
                        else
                        {
                            TestExecuter.Assert.Contains(pattern, sdmxResponse);
                        }
                    }
                }

                if (notContains != null)
                {
                    foreach (string pattern in notContains)
                    {
                        if (ignoreFormating)
                        {
                            TestExecuter.Assert.DoesNotContain(Regex.Replace(pattern, @"\s+", ""), Regex.Replace(sdmxResponse, @"\s+", ""));
                        }
                        else
                        {
                            TestExecuter.Assert.DoesNotContain(pattern, sdmxResponse);
                        }
                    }
                }
            }
            catch (System.Net.WebException ex)
            {
                //// We got web exception. check for error number in it, and if it was expected or not.
                TestExecuter.Assert.Info("Unexpected WebException thrown...");
                General.GetErrorCode(ex);
                TestExecuter.Assert.Assert(ex);
            }
        }


        public void SaveResultsToFile(string result)
        {
            // Save the query result 
            string returnedXmlPath = Path.Combine(General.TestResultFolder, string.Format(@"{0}\response.xml", General.TestResultFolder));
            TestExecuter.Assert.Info(string.Format(@"Response can be found here: {0}", returnedXmlPath));
            Directory.CreateDirectory(Path.GetDirectoryName(returnedXmlPath));
            File.WriteAllText(returnedXmlPath, result);
        }

        public virtual string ExecuteQuery(string xmlQuery)
        {
            throw new AutomationException(string.Format("Method Not implemented. Must be implemented in derived class"));
        }

        public virtual void CompareToReference(string response, string reference)
        {
            TestExecuter.Assert.AreEqual(response, reference, "The result does not equal to the reference.");
        }

        protected static void ValidationCallBack(Object sender, ValidationEventArgs args)
        {
            //Display the validation error.  This is only called on error
            ValidationResult = false; 
            Logger.Error(string.Format(@"Validation error: " + args.Message));
        }

        public virtual void ValidateResult(string results)
        {
            throw new AutomationException("Method not implemented. Must be implemented in derived class.");
        }
    }
}