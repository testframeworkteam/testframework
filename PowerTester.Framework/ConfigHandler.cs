﻿using System;
using System.Configuration;

namespace PowerTester.Framework
{
    public class ConfigHandler
    {
        private readonly string _configFilePath;
        private readonly ParameterProcessor ParameterProcessor;

        public ConfigHandler(string configFilePath)
        {
            _configFilePath = configFilePath;
            ParameterProcessor = new ParameterProcessor(_configFilePath);
        }

        /// <summary>
        /// Gives back a string value of configuration element.
        /// </summary>
        /// <param name="key">
        /// Key to configuration element.
        /// </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The get config parameter. </returns>
        public string GetConfigParameter(string key, string defaultValue = null)
        {
            string value = ParameterProcessor.GetParameter(key);
            if (value == null)
            {
                if (defaultValue == null)
                {
                    throw new ArgumentNullException(String.Format("Return value was null! Probably entered key: {0} not exist in config file?", key));
                }

                Logger.Warning(String.Format(@"Config key '{0}' not found in config file. Default value '{1}' is used instead.", key, defaultValue));
                return defaultValue;
            }

            return value;
        }

        /// <summary>
        /// Sets config values in config file
        /// </summary>
        /// <param name="configFilePath">path of config file</param>
        /// <param name="keyvaluepairs">KeyValueConfigurationCollection containing config keys and values.</param>
        public void SetConfigValues(string configFilePath, KeyValueConfigurationCollection keyvaluepairs)
        {
            Console.WriteLine(@"Setting config file:" + configFilePath);

            ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap {ExeConfigFilename = configFilePath};
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);
            foreach (KeyValueConfigurationElement element in keyvaluepairs)
            {
                if (config.AppSettings.Settings[element.Key] == null)
                {
                    config.AppSettings.Settings.Add(element.Key, element.Value);
                }
                else
                {
                    config.AppSettings.Settings[element.Key].Value = element.Value;
                }
            }
            config.Save(); 
        }
    }
}
