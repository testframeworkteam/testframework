﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using PowerTester.Framework.DBLogger.Interfaces;
using PowerTester.Framework.DBLogger.models.TestPerformance;

namespace PowerTester.Framework.DBLogger
{
    public class FsPerformanceDataProvider : IPerformanceDataProvider
    {
        public TestPerformances GetPerformancesForAllTestLatest()
        {
            TestPerformances testPerformances = new TestPerformances() {TestPerformance = new List<PerformanceResult>()};
            
            foreach (string testName in performanceTests())
            {
                testPerformances.TestPerformance.Add(GetPerformancesForTest(testName).TestPerformance.First());
            }

            return testPerformances;
        }

        public TestPerformances GetPerformancesForTest(string testName)
        {
            string testOutputPath = Path.Combine(General.TestResultRootFolder, string.Format(@"{0}.txt", testName));
            TestPerformances perfCounters = new TestPerformances {TestPerformance = new List<PerformanceResult>()};

            if (File.Exists(testOutputPath))
            {
                foreach (string line in File.ReadAllLines(testOutputPath).Where(l => !string.IsNullOrEmpty(l)))
                {
                    string[] splittedLine = line.Split('|');
                    perfCounters.TestPerformance.Add(new PerformanceResult
                    {
                        TestId = splittedLine[0],
                        Date = splittedLine[1],
                        Counter = double.Parse(splittedLine[2], CultureInfo.InvariantCulture),
                        Result = splittedLine[3]
                    });
                }
            }

            perfCounters.TestPerformance.Reverse();
            return perfCounters;
        }

        public bool WriteAndValidatePerformanceNumber(string testName, double performanceNumber)
        {
            double refPerfNumber = GetReferencePerformanceCounter(testName);

            if (refPerfNumber == 0 || performanceNumber < (refPerfNumber * 1.2))
            {
                WritePerformanceNumber(testName, performanceNumber, "Passed");
                return true;
            }

            WritePerformanceNumber(testName, performanceNumber, "Failed");

            return false;
        }

        public void WritePerformanceNumber(string testName, double performanceNumber, string result)
        {
            try
            {
                string testOutputPath = Path.Combine(General.TestResultFolder, string.Format(@"{0}\{1}.txt", General.TestResultRootFolder, testName));
                string performanceInfo = testName + "|" + DateTime.Now + "|" + performanceNumber + "|" + result;

                if (File.Exists(testOutputPath))
                {
                    File.AppendAllText(testOutputPath, performanceInfo + Environment.NewLine);
                }
                else
                {
                    File.WriteAllText(testOutputPath, performanceInfo + Environment.NewLine);
                }

                Logger.Info(string.Format(@"Performance info can be found here: {0}", testOutputPath));
            }
            catch (Exception)
            {
                Logger.Info(@"Response could not be written to output path.");
            }
        }

        public double GetReferencePerformanceCounter(string testName)
        {
            TestPerformances perfCounters = GetPerformancesForTest(testName);
            if (perfCounters.TestPerformance.Count > 100)
            {
                return perfCounters.TestPerformance.SkipWhile((val, index) => index < perfCounters.TestPerformance.Count() - 100).Average(x => x.Counter);
            }

            if (perfCounters.TestPerformance.Count == 0)
            {
                return 0;
            }

            return perfCounters.TestPerformance.Average(x => x.Counter);
        }

        public List<string> performanceTests()
        {
            List<string> performanceTests = new List<string>();
            foreach (string fileName in Directory.GetFiles(General.TestResultRootFolder).Where(f=>f.EndsWith(".txt")))
            {
                performanceTests.Add(new FileInfo(fileName).Name.Replace(".txt", ""));
            }
            return performanceTests;
        }
    }
}