﻿using System.Collections.Generic;
using PowerTester.Framework.DBLogger.models.TestPerformance;

namespace PowerTester.Framework.DBLogger.Interfaces
{
    public interface IPerformanceDataProvider
    {
        TestPerformances GetPerformancesForAllTestLatest();
        TestPerformances GetPerformancesForTest(string testName);
        bool WriteAndValidatePerformanceNumber(string testName, double performanceNumber);
        void WritePerformanceNumber(string testName, double performanceNumber, string result);
        double GetReferencePerformanceCounter(string testName);
        List<string> performanceTests();
    }
}
