﻿using PowerTester.Framework.DBLogger.models.BuildResults;
using PowerTester.Framework.DBLogger.models.Builds;
using PowerTester.Framework.DBLogger.models.TestHistory;
using Build = PowerTester.Framework.DBLogger.models.Builds.Build;
using TestOccurrence = PowerTester.Framework.DBLogger.models.TestOccurence.TestOccurrence;

namespace PowerTester.Framework.DBLogger.Interfaces
{
    public interface ITestDataProvider
    {
        Builds GetBuilds(string category, int numberOfTests, int startingIndex);
        
        Builds GetBuildsInfo(string category, int numberOfTests, int startingIndex);
       
        Build GetBuildInfo(int buildId);

        Results GetBuildResults(int buildId);

        TestOccurrence GetTestOccurenceDetails(string testId);

        TestHistory GetTestOccurenceHistory(string testName);

        TestHistory GetTestOccurenceHistory(int testId);
    }
}
