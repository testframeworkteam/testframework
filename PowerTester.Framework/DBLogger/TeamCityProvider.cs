﻿using System;
using System.IO;
using System.Reflection;
using PowerTester.Framework.DBLogger.Interfaces;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Deserializers;
using PowerTester.Framework.DBLogger.models.BuildResults;
using PowerTester.Framework.DBLogger.models.Builds;
using PowerTester.Framework.DBLogger.models.TestHistory;
using TestOccurrence = PowerTester.Framework.DBLogger.models.TestOccurence.TestOccurrence;


namespace PowerTester.Framework.DBLogger
{
    public class TeamCityProvider : ITestDataProvider
    {
        public static string BaseUrl ="http://vs-dots-bld-st1:8080/";
        //public static string RestUrl = BaseUrl + "httpAuth/app/rest/";

        private static JsonDeserializer Deserial = new JsonDeserializer();
        private static readonly HttpBasicAuthenticator auth = new HttpBasicAuthenticator("Dotstat_TestUser", "Dotstat_TestUser");

        public enum RestMethods
        {
            builds,
            buildTypes,
            testOccurrences,
        }

        public Builds GetBuilds(string category, int numberOfTests, int startingIndex)
        {
            //http://vs-dots-bld-st1:8080/httpAuth/app/rest/builds?count=100&start=100
            //IRestResponse response = CreateRestRequest(RestMethods.builds, "?count=" + numberOfTests + "&start=" + startingIndex);
            IRestResponse response = CreateRestRequest(RestMethods.buildTypes, "/id:" + category + "/builds?count=" + numberOfTests + "&start=" + startingIndex);
            var JSONObj = Deserial.Deserialize<Builds>(response);
            return JSONObj;
        }

        public Builds GetBuildsInfo(string category, int numberOfTests, int startingIndex)
        {
            Builds builds = GetBuilds(category, numberOfTests, startingIndex);
            for (int i =0; i < builds.count; i++)
            {
                builds.build[i] = GetBuildInfo(builds.build[i].id);
            }

            return builds;
        }

        public Build GetBuildInfo(int id)
        {
            //http://vs-dots-bld-st1:8080/httpAuth/app/rest/builds/id:4286
            //string result = CreateRestRequest(RestMethods.builds, "/id:" + id);

            IRestResponse response = CreateRestRequest(RestMethods.builds, "/id:" + id);
            Build build = Deserial.Deserialize<Build>(response);
            build.Started = ParseDatetime(build.startDate);
            build.Finished = ParseDatetime(build.finishDate);
            build.duration = build.Finished.Subtract(build.Started);
            return build;
        }

        public Results GetBuildResults(int id)
        {
            //http://vs-dots-bld-st1:8080/httpAuth/app/rest/testOccurrences?locator=build:(id:4392)
            IRestResponse response = CreateRestRequest(RestMethods.testOccurrences, "?locator=count:1000,start:0,build:(id:" + id + ")");
            var JSONObj = Deserial.Deserialize<Results>(response);
            return JSONObj;
        }

        public TestOccurrence GetTestOccurenceDetails(string testId)
        {
            //http://vs-dots-bld-st1:8080/httpAuth/app/rest/testOccurrences/id:6027,build:(id:4392)
            IRestResponse response = CreateRestRequest(RestMethods.testOccurrences, "/" + testId);
            var JSONObj = Deserial.Deserialize<TestOccurrence>(response);
            return JSONObj;
        }

        public TestHistory GetTestOccurenceHistory(int testId)
        {
            //http://vs-dots-bld-st1:8080/httpAuth/app/rest/testOccurrences?locator=test:(id:1618400791653829845)
            IRestResponse response = CreateRestRequest(RestMethods.testOccurrences, "?locator=test:(id:" + testId + ")");
            var JSONObj = Deserial.Deserialize<TestHistory>(response);
            return JSONObj;
        }

        public TestHistory GetTestOccurenceHistory(string name)
        {
            //http://vs-dots-bld-st1:8080/httpAuth/app/rest/testOccurrences?locator=test:(name:DotStat.Test.Functional.dll:%20DotStat.Test.Functional.DirectAccess.Tests.GetData.Verify_Endperiod_FullTime)
            IRestResponse response = CreateRestRequest(RestMethods.testOccurrences, "?locator=test:(name:" + name + ")");
            var JSONObj = Deserial.Deserialize<TestHistory>(response);
            return JSONObj;
        }

        private static IRestResponse CreateRestRequest(RestMethods restMethod, string locator)
        {
            string endPoint = "httpAuth/app/rest/" + restMethod + locator;
            RestClient client = new RestClient(BaseUrl);
            RestRequest restRequest = new RestRequest(endPoint, Method.GET);
             
            Logger.Debug(string.Format(@"Request Url: {0}", BaseUrl + endPoint));

            auth.Authenticate(client, restRequest);
            IRestResponse response = client.Execute(restRequest);

            string returnedJsonPath = Path.Combine(Environment.GetEnvironmentVariable("temp"),@"TeamcityResponse\response.txt");
            Directory.CreateDirectory(Path.GetDirectoryName(returnedJsonPath));
            Logger.Debug(string.Format(@"response saved to : {0}", returnedJsonPath));

            return response;
        }

        private static DateTime ParseDatetime(string dateTime)
        {
            DateTime dt = new DateTime();
            dt = dt.AddYears(int.Parse(dateTime.Substring(0, 4)) -1);
            dt = dt.AddMonths(int.Parse(dateTime.Substring(4, 2)) - 1);
            dt = dt.AddDays(int.Parse(dateTime.Substring(6, 2)) - 1);
            dt = dt.AddHours(int.Parse(dateTime.Substring(9, 2)) - 1);
            dt = dt.AddMinutes(int.Parse(dateTime.Substring(11, 2)) - 1);
            dt = dt.AddSeconds(int.Parse(dateTime.Substring(13, 2)) - 1);
            return dt;
        }
    }
}