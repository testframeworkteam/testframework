﻿using System.Collections.Generic;

namespace PowerTester.Framework.DBLogger.models.BuildResults
{
    public class Results
    {
        public int count { get; set; }
        public string href { get; set; }
        public List<TestOccurrence> testOccurrence { get; set; }
    }

    public class TestOccurrence
    {
        public string id { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public bool ignored { get; set; }
        public int duration { get; set; }
        public string href { get; set; }
    }
}
