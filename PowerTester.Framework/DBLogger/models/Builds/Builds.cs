﻿using System;
using System.Collections.Generic;

namespace PowerTester.Framework.DBLogger.models.Builds
{
    public class Builds
    {
        public int count { get; set; }
        public string href { get; set; }
        public string nextHref { get; set; }
        public List<Build> build { get; set; }
    }


    public class Build
    {
        public Build() 
        {
            testOccurrences = new TestOccurrences();
        }

        public int id { get; set; }
        public string buildTypeId { get; set; }
        public string nextHref { get; set; }
        public string status { get; set; }
        public string state { get; set; }
        public string href { get; set; }
        public string webUrl { get; set; }
        public string statusText { get; set; }
        public BuildType buildType { get; set; }
        public DateTime Started { get; set; }
        public DateTime Finished { get; set; }
        public string startDate { get; set; }
        public string finishDate { get; set; }
        public TimeSpan duration { get; set; }

        public TestOccurrences testOccurrences { get; set; }
    }

    public class TestOccurrences
    {
        private int _passed = 0;
        private int _failed = 0;
        private int _ignored = 0;
        public int count { get; set; }
        public string href { get; set; }
        public int passed 
        {
            get { return _passed; }
            set { _passed = value; }
        }

        public int failed
        {
            get { return _failed; }
            set { _failed = value; }
        }

        public int ignored
        {
            get { return _ignored; }
            set { _ignored = value; }
        }
        public int newFailed { get; set; }
    }

    public class BuildType
    {
        public string id { get; set; }
        public string name { get; set; }
        public string projectName { get; set; }
        public string projectId { get; set; }
        public string href { get; set; }
        public string webUrl { get; set; }
    }
}
