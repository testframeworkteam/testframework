﻿using System;

namespace PowerTester.Framework.DBLogger.models.TestOccurence
{
    public class TestOccurrence
    {
        public string id { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public int duration { get; set; }
        public string href { get; set; }
        public string details { get; set; }
        public Build build { get; set; }
        public Test test { get; set; }
    }

    public class Test
    {
        public Int64 id { get; set; }
        public string name { get; set; }
        public string href { get; set; }
    }

    public class Build
    {
        public int id { get; set; }
        public string buildTypeId { get; set; }
        public string number { get; set; }
        public string status { get; set; }
        public string state { get; set; }
        public string href { get; set; }
        public string webUrl { get; set; }
    }
}
