﻿using System;
using System.Collections.Generic;

namespace PowerTester.Framework.DBLogger.models.TestPerformance
{
    public class TestPerformances
    {
        public List<PerformanceResult> TestPerformance { get; set; }
    }

    public class PerformanceResult
    {
        public string TestId { get; set; }
        public string Date { get; set; }
        public double Counter { get; set; }
        public string Result { get; set; }
    }
}
