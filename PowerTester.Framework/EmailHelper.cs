﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using Microsoft.Exchange.WebServices.Data;
using EmailAddress = Microsoft.Exchange.WebServices.Data.EmailAddress;
using GetItemResponse = Microsoft.Exchange.WebServices.Data.GetItemResponse;

namespace PowerTester.Framework
{
    public class EmailHelper
    {
        readonly ExchangeService Service = new ExchangeService(ExchangeVersion.Exchange2010);

        public EmailHelper(string asmxUri, string username, string password, string domain)
        {
            Service.Credentials = new WebCredentials(username, password, domain);
            Service.Url = new Uri(asmxUri);
            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true; ;
        }
        
        public List<Email> GetMailsFromInbox()
        {
            Logger.Debug("looking for emails");
            
            FindItemsResults<Item> results = Service.FindItems(WellKnownFolderName.Inbox, new ItemView(1000));

            Logger.Debug(string.Format("Found {0} emails.", results.TotalCount));
            return GetEmailsFromResults(results);
        }

        public List<Email> GetMailsFromInboxAfterDate(DateTime startDateTime)
        {
            return GetMailsFromInboxAfterDate(startDateTime, string.Empty);
        }
        
        public List<Email> GetMailsFromInboxAfterDate(DateTime startDateTime, string subject)
        {
            Logger.Debug(string.Format("looking for emails after date {0} with subject {1}", startDateTime, subject));

            SearchFilter graterThenFilter = new SearchFilter.IsGreaterThanOrEqualTo(ItemSchema.DateTimeReceived, startDateTime);
            Folder folder = Folder.Bind(Service, WellKnownFolderName.Inbox);

            FindItemsResults<Item> results;

            if (string.IsNullOrWhiteSpace(subject))
            {
                results = folder.FindItems(graterThenFilter, new ItemView(1000));    
            }
            else
            {
                SearchFilter subjectContainsFilter = new SearchFilter.ContainsSubstring(ItemSchema.Subject, subject);
                SearchFilter filterCollection = new SearchFilter.SearchFilterCollection(LogicalOperator.And, graterThenFilter, subjectContainsFilter);
                results = folder.FindItems(filterCollection, new ItemView(1000));    
            }
            
            Logger.Debug(string.Format("Found {0} emails.", results.TotalCount));
            return GetEmailsFromResults(results); 
        }

        public List<Email> GetMailsFromInboxBeforeDate(DateTime endDateTime)
        {
            Logger.Debug(string.Format("looking for emails before date {0}", endDateTime));

            SearchFilter lessThenFilter = new SearchFilter.IsLessThanOrEqualTo(ItemSchema.DateTimeReceived, endDateTime);
            Folder folder = Folder.Bind(Service, WellKnownFolderName.Inbox);
            FindItemsResults<Item> results = folder.FindItems(lessThenFilter, new ItemView(1000));

            Logger.Debug(string.Format("Found {0} emails.", results.TotalCount));
            return GetEmailsFromResults(results);
        }

        public void DeleteOldEmails(int days)
        {
            Logger.Debug(string.Format("Deleting {0} day old emails", days));
            try
            {

                IEnumerable<ItemId> ids = GetMailsFromInboxBeforeDate(DateTime.Now.AddDays(-days)).Select(email => email.ItemId).ToList();

                if (ids.Any())
                {
                    WriteErrorResponsesToConsole(Service.DeleteItems(ids, DeleteMode.HardDelete, null, null));
                    Folder deletedItems = Folder.Bind(Service, WellKnownFolderName.DeletedItems);
                    deletedItems.Empty(DeleteMode.HardDelete, true);
                }

                Logger.Debug(string.Format("Deleted " + ids.Count() + " emails."));
            }
            catch (Exception ex)
            {
                Logger.Debug(string.Format("Emails could not be deleted. got exception:" + ex.Message));
            }
        }

        public void Delete(ItemId id)
        {
            Logger.Debug("Deleting Emails");

            WriteErrorResponsesToConsole(Service.DeleteItems(new List<ItemId>{id}, DeleteMode.MoveToDeletedItems, null, null));
           
            Logger.Debug(string.Format("Deleted 1 emails."));
        }
        
        public void DeleteAll()
        {
            Logger.Debug("Deleting Emails");
            IEnumerable<ItemId> ids = GetMailsFromInbox().Select(email => email.ItemId).ToList();

            if (ids.Any())
            {
                WriteErrorResponsesToConsole(Service.DeleteItems(ids, DeleteMode.MoveToDeletedItems, null, null));
            }

            Logger.Debug(string.Format("Deleted {0} emails.", ids.Count()));
        }

        private void WriteErrorResponsesToConsole(ServiceResponseCollection<ServiceResponse> responses)
        {
            if (responses.Any(r => r.Result == ServiceResult.Error))
            {
                foreach (ServiceResponse response in responses.Where(r => r.Result == ServiceResult.Error))
                {
                    Console.WriteLine("Error occured in email handling. ErrorCode: {0}, error message: {1}", response.ErrorCode, response.ErrorMessage);
                }
            }
        }

        private List<Email> GetEmailsFromResults(FindItemsResults<Item> results)
        {
            List<Email> returnResult = new List<Email>();
            if (results.Any())
            {
                ServiceResponseCollection<GetItemResponse> items =
                    Service.BindToItems(results.Select(item => item.Id),
                        new PropertySet(BasePropertySet.FirstClassProperties,
                            EmailMessageSchema.From,
                            EmailMessageSchema.ToRecipients));

                returnResult = items.Select(item =>
                {
                    return new Email()
                    {
                        ItemId = item.Item.Id,
                        Sender = ((EmailAddress) item.Item[EmailMessageSchema.From]).Address,
                        Receiver = ((EmailAddressCollection) item.Item[EmailMessageSchema.ToRecipients]).Select(recipient => recipient.Address).ToArray(),
                        Subject = item.Item.Subject,
                        Body = item.Item.Body.ToString(),
                    };
                }).ToList();
            }

            return returnResult;
        }

        public class Email
        {
            public ItemId ItemId { get; set; }

            public string Subject { get; set; }

            public string Body { get; set; }

            public string Sender { get; set; }

            public string[] Receiver { get; set; }

            public string Cc { get; set; }

            public string CreationDate { get; set; }
        }
    }
}