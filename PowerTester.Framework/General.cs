﻿using System;
using System.IO;
using System.Management;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Services.Protocols;
using PowerTester.Framework.UI.BaseClasses;
using NUnit.Framework;

namespace PowerTester.Framework
{
    /// <summary>
    /// The Delegate for bool delegate
    /// </summary>
    /// <returns>
    /// The result of booldelegate
    /// </returns>
    public delegate bool BoolDelegate();

    /// <summary>
    /// Enum for BrowserTypes
    /// </summary>
    public enum BrowserType
    {
        /// <summary>
        /// type for no browser
        /// </summary>
        None,

        /// <summary>
        /// type for the default browser
        /// </summary>
        Default,

        /// <summary>
        /// Type for internetexplorer
        /// </summary>
        InternetExplorer,

        /// <summary>
        /// Type for Firefox
        /// </summary>
        FireFox,

        /// <summary>
        /// Type for Chrome
        /// </summary>
        Chrome
    }
    
    /// <summary>
    /// Specify the OS type using tis well know name.
    /// </summary>
    [Flags]
    public enum OperatingSystem
    {
        /// <summary>
        /// Unknow OS.
        /// </summary>
        [System.ComponentModel.Description("Unknown")]
        Unknown = 1,

        /// <summary>
        /// Windows XP 32 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows XP 32bit")]
        WindowsXP32 = 2,

        /// <summary>
        /// Windows XP 64 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows XP 64bit")]
        WindowsXP64 = 4,

        /// <summary>
        /// Windows Vista 32 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows Vista 32bit")]
        WindowsVista32 = 8,

        /// <summary>
        /// Windows Vista 64 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows Vista 64bit")]
        WindowsVista64 = 16,

        /// <summary>
        /// Windows 7 32 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows 7 32bit")]
        Windows732 = 32,

        /// <summary>
        /// Windows 7 64 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows 7 64bit")]
        Windows764 = 64,

        /// <summary>
        /// Windows 2008 32 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows 2008 32bit")]
        Windows2K832 = 128,

        /// <summary>
        /// Windows 2008 64 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows 2008 64bit")]
        Windows2K864 = 256,

        /// <summary>
        /// Windows 2003 32 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows 2003 32bit")]
        Windows2K332 = 512,

        /// <summary>
        /// Windows 2003 64 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows 2003 64bit")]
        Windows2K364 = 1024,

        /// <summary>
        /// Grouped value for Windows XP 32 and 64 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows XP")]
        WindowsXP = 2048,

        /// <summary>
        /// Grouped value for Windows 2003 32 and 64 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows 2003")]
        Windows2K3 = 4096,

        /// <summary>
        /// Grouped value for Windows Vista 32 and 64 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows Vista")]
        WindowsVista = 8192,

        /// <summary>
        /// Grouped value for Windows 2008 32 and 64 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows 2008")]
        Windows2K8 = 16384,

        /// <summary>
        /// Grouped value for Windows 7 32 and 64 bit version.
        /// </summary>
        [System.ComponentModel.Description("Windows 7")]
        Windows7 = 32768,

        /// <summary>
        /// Grouped value for Windows XP based operating systems. (Windows XP and Windows 2003)
        /// </summary>
        [System.ComponentModel.Description("Windows Xp Base")]
        WindowsXPBase = 65536,

        /// <summary>
        /// Grouped value for Windows Vista based operating systems. (Windows Vista and Windows 2008)
        /// </summary>
        [System.ComponentModel.Description("Windows Vista Base")]
        WindowsVistaBase = 131072,

        /// <summary>
        /// Grouped value for Windows 7 based operating systems. (Windows 7 and Windows 2008 R2)
        /// </summary>
        [System.ComponentModel.Description("Windows 7 Base")]
        Windows7Base = 262144
    }

    /// <summary>
    /// Service pack enum
    /// </summary>
    public enum ServicePack
    {
        /// <summary>
        /// Enum for unknown
        /// </summary>
        [System.ComponentModel.Description("Unknown")]
        Unknown = 0,

        /// <summary>
        /// Enum for SP1
        /// </summary>
        [System.ComponentModel.Description("SP1")]
        SP1 = 1,

        /// <summary>
        /// Enum for SP2
        /// </summary>
        [System.ComponentModel.Description("SP2")]
        SP2 = 2,

        /// <summary>
        /// Enum for SP3
        /// </summary>
        [System.ComponentModel.Description("SP3")]
        SP3 = 3,

        /// <summary>
        /// Enum for SP4
        /// </summary>
        [System.ComponentModel.Description("SP4")]
        SP4 = 4
    }
        
    public static class General
    {
        private static string _testResultRootFolder;
        private static string _debug;
        private static string _environment;
        private static string _executionTime;
        private static string _browserType ;
        private static string _projectPath;

        // Get config values
        // Mandatory values
        public static string TestEnvironment
        {
            get
            {
                _environment = _environment ?? TestFrameworkConfigHandler.GetConfigParameter("Env");
                return _environment;
            }
        }

        public static string Debug
        {
            get
            {
                _debug = _debug ?? TestFrameworkConfigHandler.GetConfigParameter("Debug", "true");
                return _debug;
            }
        }

        public static int ExecuteTime
        {
            get
            {
                _executionTime = _executionTime ?? TestFrameworkConfigHandler.GetConfigParameter("FailedTestRepeatTime", "1");
                return int.Parse(_executionTime);
            }
        }

        public static BrowserType BrowserType
        {
            get
            {
                _browserType = _browserType ?? TestFrameworkConfigHandler.GetConfigParameter("Browser", BrowserType.InternetExplorer.ToString());
                return (BrowserType)Enum.Parse(typeof (BrowserType), _browserType);
            }
        }

        public static string TempDirectory
        {
            get { return Environment.GetEnvironmentVariable("Temp"); }
        }

        public static string ProjectPath
        {
            get
            {
                _projectPath = _projectPath ?? Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase.Substring(8));
                return _projectPath;
            }
        }

        public static string TestResultRootFolder
        {
            get
            {
                _testResultRootFolder = _testResultRootFolder ?? TestFrameworkConfigHandler.GetConfigParameter("TestResultsFolder", @"c:\TestResults");
                Directory.CreateDirectory(_testResultRootFolder);
                return _testResultRootFolder;
            }
        }

        public static string TestResultFolder
        {
            get
            {
                string path = Path.Combine(TestResultRootFolder, TestContext.CurrentContext.Test.FullName);
                Directory.CreateDirectory(path);
                return path;
            }
        }
        
        /// <summary>
        /// Waits until delegate becomes true
        /// </summary>
        /// <param name="booldelegate"> bool delegate</param>
        /// <returns>true if delegate returned true within timeout</returns>
        public static bool WaitFor(BoolDelegate booldelegate)
        {
            return WaitFor(booldelegate, UIAutomationSettings.WaitForTimeout);
        }

        /// <summary>
        /// Waits until delegate becomes true
        /// </summary>
        /// <param name="boolDelegate"> bool delegate</param>
        /// <param name="timeout">max timeout</param>
        /// <returns>true if delegate returned true within timeout</returns>
        public static bool WaitFor(BoolDelegate boolDelegate, int timeout)
        {
            Logger.Debug("Waitfor boolDelegate called.");
            DateTime startTime = DateTime.Now;

            do
            {
                try
                {
                    Console.Write(".");
                    if (boolDelegate.Invoke())
                    {
                        Logger.Debug("Waitfor boolDelegate return true.");
                        Console.WriteLine();
                        return true;
                    }
                }
                catch (Exception exception)
                {
                    // catch any exception here
                }

                Thread.Sleep(50);
            } while (startTime.AddSeconds(timeout) > DateTime.Now);

            Console.WriteLine();
            Logger.Debug("Waitfor boolDelegate ended false.");
            return false;
        }

        public static bool WaitForFileReady(string path, int timeout)
        {
            return WaitFor(() => !IsFileLocked(path), timeout);
        }
        
        private static bool IsFileLocked(string path)
        {
            var file = new FileInfo(path);
            FileStream stream = null;
            Console.WriteLine("IsFileLocked called");

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.Message);
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        /// <summary>
        /// Copy all the directories and files to a a target path.
        /// </summary>
        /// <param name="sourcePath">The source path.</param>
        /// <param name="destinationPath">The target path.</param>
        public static void CopyDirectory(string sourcePath, string destinationPath)
        {
            Directory.CreateDirectory(destinationPath);

            // Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(dirPath.Replace(sourcePath.TrimEnd('\\'), destinationPath.TrimEnd('\\')));
            }

            // Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
            {
                File.Copy(newPath, newPath.Replace(sourcePath.TrimEnd('\\'), destinationPath.TrimEnd('\\')), true);
            }
        }

        /// <summary>
        /// clears test output folder.
        /// </summary>
        public static void ClearOutputDitectory()
        {
            if (Directory.Exists(TestResultFolder))
            {
                foreach (string file in Directory.GetFiles(TestResultFolder))
                {
                    File.Delete(file);
                }
            }
            Logger.Info(string.Format(@"Output Directory cleared: {0}", TestResultFolder));
        }

        /// <summary>
        /// Write text to test output folder.
        /// </summary>
        /// <param name="filename">the file to be created.</param>
        /// <param name="text">the content of the file.</param>
        public static void WriteToTestOutputDitectory(string filename, string text)
        {
            try
            {
                string testOutputPath = Path.Combine(TestResultFolder, filename);
                File.WriteAllText(testOutputPath, text);
                Logger.Info(string.Format(@"Response can be found here: {0}", testOutputPath));
            }
            catch (Exception)
            {
                Logger.Info(@"Response could not be written to output path.");
            }            
        }

        /// <summary>
        /// Copy file to test output folder.
        /// </summary>
        /// <param name="sourceFile">the file to be copied.</param>
        public static void WriteToTestOutputDitectory(string sourceFile)
        {
            try
            {
                string testOutputPath = Path.Combine(TestResultFolder, new FileInfo(sourceFile).Name);
                File.Copy(sourceFile, testOutputPath);
                Logger.Info(string.Format(@"File can be found here: {0}", testOutputPath));
            }
            catch (Exception)
            {
                Logger.Info(@"File could not be written to output path.");
            }
        }

        /// <summary>
        /// Gets the operating system type.
        /// </summary>
        /// <example>
        /// Utils.OperatingSystem os = Utils.GetOperatingSystem();
        /// // When Windows 2K3 is the one we are looking for and the platform does not matter
        /// if (os.Contains(Utils.OperatingSystem.Windows2K3))
        /// {
        /// }
        /// // This one includes 2K3 and Xp on both platforms.
        /// if (os.Contains(Utils.OperatingSystem.WindowsXPBase))
        /// {
        /// }
        /// </example>
        /// <returns>
        /// The get operating system.
        /// </returns>
        public static OperatingSystem GetOperatingSystem()
        {
            return ParseEnvironmentInfo<OperatingSystem>(Environment.OSVersion.Platform, Environment.OSVersion.Version, string.Empty);
        }

        private static T ParseEnvironmentInfo<T>(PlatformID? platFormId, Version version, string value)
        {
            Type type = typeof(T);
            object returnValue = null;

            string pa = Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE");
            int? architecture = (string.IsNullOrEmpty(pa) || string.Compare(pa, 0, "x86", 0, 3, true) == 0) ? 32 : 64;

            #region Selecting Operating System

            if (type.Equals(typeof(OperatingSystem)))
            {
                returnValue = (object)OperatingSystem.Unknown;

                string osCaption = string.Empty;

                ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_OperatingSystem");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    osCaption = (string)queryObj["Caption"];
                    break;
                }

                if (string.IsNullOrEmpty(osCaption))
                {
                    return (T)returnValue;
                }

                if (platFormId == PlatformID.Win32NT)
                {
                    switch (version.Major)
                    {
                        case 5:

                            #region Windows XP

                            if (osCaption.ToLower().Contains("xp"))
                            {
                                if (architecture == 32)
                                {
                                    returnValue = (object)(OperatingSystem.WindowsXP32 | OperatingSystem.WindowsXP | OperatingSystem.WindowsXPBase);
                                }
                                else
                                {
                                    returnValue = (object)(OperatingSystem.WindowsXP64 | OperatingSystem.WindowsXP | OperatingSystem.WindowsXPBase);
                                }
                            }
                            else if (osCaption.ToLower().Contains("2003"))
                            {
                                if (architecture == 32)
                                {
                                    returnValue = (object)(OperatingSystem.Windows2K332 | OperatingSystem.Windows2K3 | OperatingSystem.WindowsXPBase);
                                }
                                else
                                {
                                    returnValue = (object)(OperatingSystem.Windows2K364 | OperatingSystem.Windows2K3 | OperatingSystem.WindowsXPBase);
                                }
                            }

                            #endregion

                            break;

                        case 6:

                            #region Vista and Windows 7

                            if (osCaption.ToLower().Contains("vista"))
                            {
                                if (architecture == 32)
                                {
                                    returnValue = (object)(OperatingSystem.WindowsVista32 | OperatingSystem.WindowsVista | OperatingSystem.WindowsVistaBase);
                                }
                                else
                                {
                                    returnValue = (object)(OperatingSystem.WindowsVista64 | OperatingSystem.WindowsVista | OperatingSystem.WindowsVistaBase);
                                }
                            }
                            else if (osCaption.ToLower().Contains("windows 7"))
                            {
                                if (architecture == 32)
                                {
                                    returnValue = (object)(OperatingSystem.Windows732 | OperatingSystem.Windows7 | OperatingSystem.Windows7Base);
                                }
                                else
                                {
                                    returnValue = (object)(OperatingSystem.Windows764 | OperatingSystem.Windows7 | OperatingSystem.Windows7Base);
                                }
                            }
                            else if (osCaption.ToLower().Contains("2008"))
                            {
                                if (architecture == 32)
                                {
                                    returnValue = (object)(OperatingSystem.Windows2K832 | OperatingSystem.Windows2K8 | OperatingSystem.WindowsVistaBase);
                                }
                                else
                                {
                                    returnValue = (object)(OperatingSystem.Windows2K864 | OperatingSystem.Windows2K8 | OperatingSystem.WindowsVistaBase);
                                }
                            }

                            #endregion

                            break;

                        default:

                            break;
                    }
                }
            }

            #endregion

            #region Selecting Service Pack

            if (type.Equals(typeof(ServicePack)))
            {
                returnValue = (object)ServicePack.Unknown;

                if (value.ToLower().Equals("service pack 1"))
                {
                    returnValue = (object)ServicePack.SP1;
                }
                else if (value.ToLower().Equals("service pack 2"))
                {
                    returnValue = (object)ServicePack.SP2;
                }
                else if (value.ToLower().Equals("service pack 3"))
                {
                    returnValue = (object)ServicePack.SP3;
                }
                else if (value.ToLower().Equals("service pack 4"))
                {
                    returnValue = (object)ServicePack.SP4;
                }
            }

            #endregion

            return (T)returnValue;
        }

        public static int GetErrorCode(System.Net.WebException ex)
        {
            return int.Parse(Regex.Match(ex.Message, @"\d+").Value);
        }

        public static int GetErrorCode(SoapException ex)
        {
            int errorNumber;
            var errorNode = ex.Detail.SelectSingleNode("//Error");
            if (errorNode.SelectSingleNode("//ErrorNumber") != null)
            {
                // NSI way of error number
                errorNumber = int.Parse(errorNode.SelectSingleNode("//ErrorNumber").InnerText);
            }
            else
            {
                // Dotstat way of error number
                errorNumber = int.Parse(errorNode.Attributes["Code"].Value);
            }


            Logger.Test(@"--------------------");
            Logger.Test(ex.Detail.InnerXml);
            Logger.Test(@"--------------------");

            return errorNumber;
        }
    }
}