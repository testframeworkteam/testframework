﻿using System;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;
using System.Runtime.ConstrainedExecution;
using System.Security;

namespace PowerTester.Framework
{
    public class ImpersonatorDomainLevel : IDisposable
    {
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
            int dwLogonType, int dwLogonProvider, out SafeTokenHandle phToken);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public extern static bool CloseHandle(IntPtr handle);

        internal SafeTokenHandle _safeTokenHandle;
        internal WindowsIdentity _newId;
        internal WindowsImpersonationContext _impersonatedUser;

        public ImpersonatorDomainLevel(string domainName, string userName, string userPassword)
        {
            Test(domainName, userName, userPassword);
        }

        public void Dispose()
        {
            UndoImpersonation();
        }

        // Test harness.
        // If you incorporate this code into a DLL, be sure to demand FullTrust.
        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        private void Test(string domainName, string userName, string userPassword)
        {
            try
            {
                const int LOGON32_PROVIDER_DEFAULT = 0;
                //This parameter causes LogonUser to create a primary token.
                const int LOGON32_LOGON_INTERACTIVE = 2;

                // Call LogonUser to obtain a handle to an access token.
                bool returnValue = LogonUser(userName, domainName, userPassword, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, out _safeTokenHandle);

                Console.WriteLine("LogonUser called.");

                if (false == returnValue)
                {
                    int ret = Marshal.GetLastWin32Error();
                    Console.WriteLine("LogonUser failed with error code : {0}", ret);
                    throw new System.ComponentModel.Win32Exception(ret);
                }

                Console.WriteLine("Did LogonUser Succeed? " + (returnValue ? "Yes" : "No"));
                Console.WriteLine("Value of Windows NT token: " + _safeTokenHandle);

                // Check the identity.
                Console.WriteLine("Before impersonation: " + WindowsIdentity.GetCurrent().Name);
                // Use the token handle returned by LogonUser.

                _newId = new WindowsIdentity(_safeTokenHandle.DangerousGetHandle());
                _impersonatedUser = _newId.Impersonate();

                // Check the identity.
                Console.WriteLine("After impersonation: " + WindowsIdentity.GetCurrent().Name);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred. " + ex.Message);
            }
        }

        private void UndoImpersonation()
        {
            if (_impersonatedUser != null)
            {
                _impersonatedUser.Dispose();
            }
            if (_newId != null)
            {
                _newId.Dispose();
            }
            if (_safeTokenHandle != null)
            {
                _safeTokenHandle.Dispose();
            }
            // Releasing the context object stops the impersonation
            // Check the identity.
            Console.WriteLine("After closing the context: " + WindowsIdentity.GetCurrent().Name);
        }
    }

    public sealed class SafeTokenHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
        private SafeTokenHandle()
            : base(true)
        {
        }

        [DllImport("kernel32.dll")]
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
        [SuppressUnmanagedCodeSecurity]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CloseHandle(IntPtr handle);

        protected override bool ReleaseHandle()
        {
            return CloseHandle(handle);
        }
    }
}