﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using log4net;
using log4net.Config;

namespace PowerTester.Framework
{
    /// <summary>
    /// The Log4NetLogger class
    /// </summary>
    public static class Logger
    {
        private static readonly ILog Log4NetLogger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly log4net.Core.Level TestLevel = new log4net.Core.Level(50000, "TEST");
        private static readonly string ProjectPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase.Substring(8));
        private static string _debug = "";

        /// <summary>
        /// Initializes static members of the <see cref="Framework.Logger"/> class.  
        /// </summary>
        static Logger()
        {
            _debug = TestFrameworkConfigHandler.GetConfigParameter("Debug", "true");

            LogManager.GetRepository().LevelMap.Add(TestLevel);

            XmlConfigurator.Configure(new FileInfo(Path.Combine(ProjectPath, @"log4net.config")));
        }
        
        /// <summary>
        /// Write test to log
        /// </summary>
        /// <param name="message">
        /// the message to log
        /// </param>
        /// <param name="objects">parameters to add to log</param>
        public static void Test(string message, params object[] objects)
        {
            if (objects.Length > 0)
            {
                Test(string.Format("{0} {1}", message, objects));
                return;
            }

            Log4NetLogger.Logger.Log(null, TestLevel, message, null);
        }

        /// <summary>
        /// Write debug to log
        /// </summary>
        /// <param name="message">
        /// The message to log
        /// </param>
        public static void Debug(string message)
        {
            if (bool.Parse(General.Debug))
            {
                ILog logger = LogManager.GetLogger(new StackFrame(1, true).GetMethod().DeclaringType);
                logger.Debug(message);
            }
        }

        /// <summary>
        /// Write info to log
        /// </summary>
        /// <param name="message">
        /// The message to log
        /// </param>
        public static void Info(string message)
        {
            ILog logger = LogManager.GetLogger(new StackFrame(1, true).GetMethod().DeclaringType);
            logger.Info(message);
        }

        /// <summary>
        /// Write warning to log
        /// </summary>
        /// <param name="message">
        /// The message to log
        /// </param>
        public static void Warning(string message)
        {
            ILog logger = LogManager.GetLogger(new StackFrame(1, true).GetMethod().DeclaringType);
            logger.Warn(message);
        }

        /// <summary>
        /// Write error to log 
        /// </summary>
        /// <param name="message">
        /// The message to log
        /// </param>
        /// <param name="exception">
        /// The exception
        /// </param>
        public static void Error(string message, Exception exception = null)
        {
            ILog logger = LogManager.GetLogger(new StackFrame(1, true).GetMethod().DeclaringType);
            if (exception == null)
            {
                logger.Error(message);
            }
            else
            {
                logger.Error(message, exception);
            }
        }
    }
}