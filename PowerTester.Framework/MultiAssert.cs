﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace PowerTester.Framework
{
    /// <summary>
    /// Delegate used by tests that execute code and
    /// capture any thrown exception.
    /// </summary>
    public delegate void TestDelegate();

    /// <summary>
    /// The TestAssert class
    /// </summary>
    public class MultiAssert
    {
        #region Private variables

        /// <summary>
        /// The logical name 
        /// </summary>
        private const string _LogicalName = "Test";

        /// <summary>
        /// Used to collect exceptions.
        /// </summary>
        private readonly List<Exception> _exceptionList = new List<Exception>();

        /// <summary>
        /// Used to collect screenshots.
        /// </summary>
        private readonly List<string> _screenshotList = new List<string>();

        private readonly string _methodBaseType;
        private readonly string _methodName;
        private readonly DateTime _startingTime;
        private int _counter = 0;
        private bool _endTestOnFailOnce = false;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the TestAssert class.
        /// </summary>
        public MultiAssert()
            : this(4)
        {
        }

        /// <summary>
        /// Initializes a new instance of the TestAssert class.
        /// </summary>
        /// <param name="level">the level in stacktrace</param>     
        public MultiAssert(int level)
        {
            EndTestOnFail = false;
            CreateScreenshot = false;
            level = level + 1;

            StackTrace st = new StackTrace();
            _startingTime = DateTime.Now;
            _methodBaseType = st.GetFrame(level).GetMethod().DeclaringType.ToString();
            _methodName = st.GetFrame(level).GetMethod().Name;

            string output = string.Format(
                "  STARTED: {0}.{1}",
                _methodBaseType,
                _methodName);
            Logger.Test(output);
        }
        #endregion

        #region Public properties

        /// <summary>
        /// Gets the counter.
        /// </summary>
        public int Counter
        {
            get { return _counter; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether CreateScreenshot.
        /// </summary>
        public bool CreateScreenshot { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether End Test On Fail.
        /// </summary>
        public bool EndTestOnFail { get; set; }

        #endregion

        #region Own Asserts
        /// <summary>
        /// Add exception to the exception list
        /// </summary>
        /// <param name="exception">Exception to be added to exception list</param>
        public void Assert(Exception exception)
        {
            Assert(exception, false);
        }

        /// <summary>
        /// Add exception to the exception list
        /// </summary>
        /// <param name="exception">Exception to be added to exception list</param>
        /// <param name="endTestOnFail">Set it true to end the test</param>
        public void Assert(Exception exception, bool endTestOnFail)
        {
            _counter++;
            if (endTestOnFail)
            {
                throw new Exception(exception.Message, exception);
            }

            if (EndTestOnFail)
            {
                _endTestOnFailOnce = false;
                throw new Exception(exception.Message, exception);
            }

            _exceptionList.Add(exception);
        }

        /// <summary>
        /// Assert that a string contains expected pattern.
        /// </summary>
        /// <param name="actual">Actual string to check.</param>
        /// <param name="expectedPatterns">the expceted patterns</param>
        /// <param name="unExpectedPatterns">the unexpected patterns</param>
        /// <param name="endOnFail">End test on failure if set to true.</param>
        public void ContainsPatterns(string actual,
            List<string> expectedPatterns,
            List<string> unExpectedPatterns,
            bool endOnFail = false)
        {
            ContainsPatterns(actual, expectedPatterns, unExpectedPatterns, "", endOnFail);
        }

        /// <summary>
        /// Assert that a string contains expected pattern.
        /// </summary>
        /// <param name="actual">Actual string to check.</param>
        /// <param name="expectedPatterns">the expceted patterns</param>
        /// <param name="unExpectedPatterns">the unexpected patterns</param>
        /// <param name="message">The error message.</param>
        /// <param name="endOnFail">End test on failure if set to true.</param>
        public void ContainsPatterns(string actual, List<string> expectedPatterns, List<string> unExpectedPatterns, string message, bool endOnFail = false)
        {
            if (expectedPatterns != null)
            {
                foreach (string pattern in expectedPatterns)
                {
                    Contains(pattern, actual, message, endOnFail);
                }
            }

            if (unExpectedPatterns != null)
            {
                foreach (string pattern in unExpectedPatterns)
                {
                    DoesNotContain(pattern, actual, message, endOnFail);
                }
            }
        }

        #endregion

        #region Public methods
        /// <summary>
        /// Overload of PrintTestResults used to print succeded test results in standaard output format
        /// to console and console.error.
        /// </summary>
        public void PrintTestResults(TimeSpan duration)
        {
            PrintTestResults(duration, null);
        }

        /// <summary>
        /// Clears List of collected exceptions.
        /// </summary>
        public void ClearExceptionList()
        {
            _counter = 0;
            _exceptionList.Clear();
        }

        /// <summary>
        /// Overload of PrintTestResults used to print results of a test. 
        /// Succeded results are printed if Exception and expectedException has the same type
        /// Failed result is printed if Exception and expectedException has different type        
        /// </summary>
        /// <param name="expectedException">type of expected exception in string format.</param>
        public void PrintTestResults(TimeSpan duration, string expectedException)
        {
            try
            {
                if (_exceptionList == null || _exceptionList.Count == 0)
                {
                    if (string.IsNullOrEmpty(expectedException))
                    {
                        PrintPassedResult(duration);
                    }
                    else
                    {
                        PrintFailedResult(duration);
                        throw new Exception(string.Concat("Exception was not raised but it was expected. Expected error: ", expectedException));
                    }
                }
                else if (_exceptionList.Count == 1)
                {
                    if (_exceptionList[0].GetType().ToString() == expectedException)
                    {
                        PrintPassedResult(duration);
                    }
                    else
                    {
                        Logger.Test(string.Format(@"Exception: {0}", _exceptionList[0]));
                        if (_screenshotList.Count > 0)
                        {
                            Logger.Test(string.Format(@"Screenshot is available: {0}", _screenshotList[0]));
                        }

                        PrintFailedResult(duration);
                        throw new Exception(_exceptionList[0].Message, _exceptionList[0]);
                    }
                }
                else
                {
                    for (int i = 0; i < _exceptionList.Count; i++)
                    {
                        Logger.Test(string.Format(@"Exception: {0}", _exceptionList[i]));
                        if (_screenshotList.Count > i)
                        {
                            Logger.Test(string.Format(@"Screenshot is available: {0}", _screenshotList[i]));
                        }
                    }

                    PrintFailedResult(duration);
                    throw new Exception(_exceptionList[0].Message, _exceptionList[0]);
                }
            }
            finally
            {
                _exceptionList.Clear();
                _counter = 0;
                Console.Error.WriteLine();
            }
        }

        /// <apiflag>No</apiflag>
        /// <summary>API:No 
        /// Prints an info
        /// </summary>
        /// <param name="message">formatted message</param>
        /// <param name="objects"> An array of objects to write using format</param>
        public void Info(string message, params object[] objects)
        {
            Logger.Test(string.Format("Info: {0}", message), objects);
        }

        /// <apiflag>No</apiflag>
        /// <summary>API:No 
        /// Prints an Error
        /// </summary>
        /// <param name="message">formatted message</param>
        /// <param name="objects"> An array of objects to write using format</param>
        public void Error(string message, params object[] objects)
        {
            Logger.Test(string.Format("Error: {0}", message), objects);
        }

        /// <apiflag>No</apiflag>
        /// <summary>API:No 
        /// Prints an Waring
        /// </summary>
        /// <param name="message">formatted message</param>
        /// <param name="objects"> An array of objects to write using format</param>
        public void Warning(string message, params object[] objects)
        {
            Logger.Test(string.Format("Warning: {0}", message), objects);
        }

        #endregion

        #region Dispose
        /// <summary>
        /// Dispose TestAssert
        /// </summary>
        public int Dispose()
        {
            DateTime endingTime = DateTime.Now;
            TimeSpan duration = endingTime - _startingTime;

            //string output = string.Format(
            //    "ENDED {0}.{1} (Duration : {2}:{3}:{4} {5} ms)",
            //    _methodBaseType,
            //    _methodName,
            //    duration.Hours,
            //    duration.Minutes,
            //    duration.Seconds,
            //    duration.Milliseconds);
            //Logger.Test(output);
            PrintTestResults(duration);
            return (int)duration.TotalMilliseconds;
        }
        #endregion

        #region Private Methods

        private void IncrementAssertCount()
        {
            ++_counter;
        }

        /// <apiflag>No</apiflag>
        /// <summary>API:No 
        /// Prints an info
        /// </summary>
        /// <param name="message">formatted message</param>
        /// <param name="endOnFail">Ends test if condition fails.</param>
        private void Assert(string message, bool endOnFail = false)
        {
            AssertionException exception = new AssertionException(message);
            _exceptionList.Add(exception);
            if (endOnFail)
            {
                throw exception;
            }
        }

        private void PrintPassedResult(TimeSpan duration)
        {
            Logger.Test(string.Format(@"  PASSED:     {0}.{1}  (Duration: {2} Asserts: {3})", _methodBaseType, _methodName, duration, _counter));
        }

        private void PrintFailedResult(TimeSpan duration)
        {
            Logger.Test(string.Format(@"  FAILED:     {0}.{1}  (Duration: {2} Asserts: {3})", _methodBaseType, _methodName, duration, _counter));
        }

        #endregion

        #region Nunit

        /// <summary>
        /// Asserts that a condition is true. If the condition is false the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary> 
        /// <param name="condition">The evaluated condition</param>
        /// <param name="message">The message to display if the condition is false</param>
        /// <param name="args">Arguments to be used in formatting the message</param>
        public void That(bool condition, string message, params object[] args)
        {
            That(condition, Is.True, message, args);
        }

        /// <summary>
        /// Asserts that a condition is true. If the condition is false the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary>
        /// <param name="condition">The evaluated condition</param>
        public void That(bool condition)
        {
            That(condition, Is.True, null, null);
        }

        /// <summary>
        /// Asserts that a condition is true. If the condition is false the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary> 
        /// <param name="condition">The evaluated condition</param>
        /// <param name="getExceptionMessage">A function to build the message included with the Exception</param>
        public void That(bool condition, Func<string> getExceptionMessage)
        {
            That(condition, Is.True, getExceptionMessage);
        }

        /// <summary>
        /// Asserts that a condition is true. If the condition is false the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary> 
        /// <param name="condition">A lambda that returns a Boolean</param>
        /// <param name="message">The message to display if the condition is false</param>
        /// <param name="args">Arguments to be used in formatting the message</param>
        public void That(Func<bool> condition, string message, params object[] args)
        {
            That(condition.Invoke(), Is.True, message, args);
        }

        /// <summary>
        /// Asserts that a condition is true. If the condition is false the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary>
        /// <param name="condition">A lambda that returns a Boolean</param>
        public void That(Func<bool> condition)
        {
            That(condition.Invoke(), Is.True, null, null);
        }

        /// <summary>
        /// Asserts that a condition is true. If the condition is false the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary> 
        /// <param name="condition">A lambda that returns a Boolean</param>
        /// <param name="getExceptionMessage">A function to build the message included with the Exception</param>
        public void That(Func<bool> condition, Func<string> getExceptionMessage)
        {
            That(condition.Invoke(), Is.True, getExceptionMessage);
        }

        /// <summary>
        /// Apply a constraint to an actual value, succeeding if the constraint
        /// is satisfied and throwing an assertion exception on failure.
        /// </summary>
        /// <typeparam name="TActual">The Type being compared.</typeparam>
        /// <param name="del">An ActualValueDelegate returning the value to be tested</param>
        /// <param name="expr">A Constraint expression to be applied</param>
        public void That<TActual>(ActualValueDelegate<TActual> del, IResolveConstraint expr)
        {
            That(del, expr.Resolve(), null, null);
        }

        /// <summary>
        /// Apply a constraint to an actual value, succeeding if the constraint
        /// is satisfied and throwing an assertion exception on failure.
        /// </summary>
        /// <typeparam name="TActual">The Type being compared.</typeparam>
        /// <param name="del">An ActualValueDelegate returning the value to be tested</param>
        /// <param name="expr">A Constraint expression to be applied</param>
        /// <param name="message">The message that will be displayed on failure</param>
        /// <param name="args">Arguments to be used in formatting the message</param>
        public void That<TActual>(ActualValueDelegate<TActual> del, IResolveConstraint expr, string message, params object[] args)
        {
            var constraint = expr.Resolve();

            IncrementAssertCount();
            var result = constraint.ApplyTo(del);
            if (!result.IsSuccess)
                _exceptionList.Add(new Exception(message + args));
        }

        /// <summary>
        /// Apply a constraint to an actual value, succeeding if the constraint
        /// is satisfied and throwing an assertion exception on failure.
        /// </summary>
        /// <typeparam name="TActual">The Type being compared.</typeparam>
        /// <param name="del">An ActualValueDelegate returning the value to be tested</param>
        /// <param name="expr">A Constraint expression to be applied</param>
        /// <param name="getExceptionMessage">A function to build the message included with the Exception</param>
        public void That<TActual>(
            ActualValueDelegate<TActual> del,
            IResolveConstraint expr,
            Func<string> getExceptionMessage)
        {
            var constraint = expr.Resolve();

            IncrementAssertCount();
            var result = constraint.ApplyTo(del);
            if (!result.IsSuccess)
                _exceptionList.Add(new Exception(getExceptionMessage()));
        }

        /// <summary>
        /// Asserts that the code represented by a delegate throws an exception
        /// that satisfies the constraint provided.
        /// </summary>
        /// <param name="code">A TestDelegate to be executed</param>
        /// <param name="constraint">A Constraint expression to be applied</param>
        public void That(TestDelegate code, IResolveConstraint constraint)
        {
            That(code, constraint, null, null);
        }

        /// <summary>
        /// Asserts that the code represented by a delegate throws an exception
        /// that satisfies the constraint provided.
        /// </summary>
        /// <param name="code">A TestDelegate to be executed</param>
        /// <param name="constraint">A Constraint expression to be applied</param>
        /// <param name="message">The message that will be displayed on failure</param>
        /// <param name="args">Arguments to be used in formatting the message</param>
        public void That(TestDelegate code, IResolveConstraint constraint, string message, params object[] args)
        {
            That((object)code, constraint, message, args);
        }

        /// <summary>
        /// Asserts that the code represented by a delegate throws an exception
        /// that satisfies the constraint provided.
        /// </summary>
        /// <param name="code">A TestDelegate to be executed</param>
        /// <param name="constraint">A Constraint expression to be applied</param>
        /// <param name="getExceptionMessage">A function to build the message included with the Exception</param>
        public void That(TestDelegate code, IResolveConstraint constraint, Func<string> getExceptionMessage)
        {
            That((object)code, constraint, getExceptionMessage);
        }

        /// <summary>
        /// Apply a constraint to an actual value, succeeding if the constraint
        /// is satisfied and throwing an assertion exception on failure.
        /// </summary>
        /// <typeparam name="TActual">The Type being compared.</typeparam>
        /// <param name="actual">The actual value to test</param>
        /// <param name="expression">A Constraint expression to be applied</param>
        public void That<TActual>(TActual actual, IResolveConstraint expression)
        {
            That(actual, expression, null, null);
        }

        /// <summary>
        /// Apply a constraint to an actual value, succeeding if the constraint
        /// is satisfied and throwing an assertion exception on failure.
        /// </summary>
        /// <typeparam name="TActual">The Type being compared.</typeparam>
        /// <param name="actual">The actual value to test</param>
        /// <param name="expression">A Constraint expression to be applied</param>
        /// <param name="message">The message that will be displayed on failure</param>
        /// <param name="args">Arguments to be used in formatting the message</param>
        public void That<TActual>(TActual actual, IResolveConstraint expression, string message, params object[] args)
        {
            var constraint = expression.Resolve();

            IncrementAssertCount();
            var result = constraint.ApplyTo(actual);
            if (!result.IsSuccess)
                _exceptionList.Add(new Exception(message + args));
        }

        /// <summary>
        /// Apply a constraint to an actual value, succeeding if the constraint
        /// is satisfied and throwing an assertion exception on failure.
        /// </summary>
        /// <typeparam name="TActual">The Type being compared.</typeparam>
        /// <param name="actual">The actual value to test</param>
        /// <param name="expression">A Constraint expression to be applied</param>
        /// <param name="getExceptionMessage">A function to build the message included with the Exception</param>
        public void That<TActual>(
            TActual actual,
            IResolveConstraint expression,
            Func<string> getExceptionMessage)
        {
            var constraint = expression.Resolve();

            IncrementAssertCount();
            var result = constraint.ApplyTo(actual);
            if (!result.IsSuccess)
                _exceptionList.Add(new Exception(getExceptionMessage()));
        }

        /// <summary>
        /// Apply a constraint to an actual value, succeeding if the constraint
        /// is satisfied and throwing an assertion exception on failure.
        /// Used as a synonym for That in rare cases where a private setter 
        /// causes a Visual Basic compilation error.
        /// </summary>
        /// <param name="actual">The actual value to test</param>
        /// <param name="expression">A Constraint expression to be applied</param>
        public void ByVal(object actual, IResolveConstraint expression)
        {
            That(actual, expression, null, null);
        }

        /// <summary>
        /// Apply a constraint to an actual value, succeeding if the constraint
        /// is satisfied and throwing an assertion exception on failure. 
        /// Used as a synonym for That in rare cases where a private setter 
        /// causes a Visual Basic compilation error.
        /// </summary>
        /// <remarks>
        /// This method is provided for use by VB developers needing to test
        /// the value of properties with private setters.
        /// </remarks>
        /// <param name="actual">The actual value to test</param>
        /// <param name="expression">A Constraint expression to be applied</param>
        /// <param name="message">The message that will be displayed on failure</param>
        /// <param name="args">Arguments to be used in formatting the message</param>
        public void ByVal(object actual, IResolveConstraint expression, string message, params object[] args)
        {
            That(actual, expression, message, args);
        }


        /// <summary>
        /// Verifies that the first int is greater than the second
        /// int. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Greater(int arg1, int arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first int is greater than the second
        /// int. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        public void Greater(int arg1, int arg2)
        {
            That(arg1, Is.GreaterThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is greater than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void Greater(uint arg1, uint arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>

        public void Greater(uint arg1, uint arg2)
        {
            That(arg1, Is.GreaterThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is greater than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Greater(long arg1, long arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        public void Greater(long arg1, long arg2)
        {
            That(arg1, Is.GreaterThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is greater than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void Greater(ulong arg1, ulong arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>

        public void Greater(ulong arg1, ulong arg2)
        {
            That(arg1, Is.GreaterThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is greater than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Greater(decimal arg1, decimal arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        public void Greater(decimal arg1, decimal arg2)
        {
            That(arg1, Is.GreaterThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is greater than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Greater(double arg1, double arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        public void Greater(double arg1, double arg2)
        {
            That(arg1, Is.GreaterThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is greater than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Greater(float arg1, float arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        public void Greater(float arg1, float arg2)
        {
            That(arg1, Is.GreaterThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is greater than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Greater(IComparable arg1, IComparable arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        public void Greater(IComparable arg1, IComparable arg2)
        {
            That(arg1, Is.GreaterThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Less(int arg1, int arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        public void Less(int arg1, int arg2)
        {
            That(arg1, Is.LessThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void Less(uint arg1, uint arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>

        public void Less(uint arg1, uint arg2)
        {
            That(arg1, Is.LessThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Less(long arg1, long arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        public void Less(long arg1, long arg2)
        {
            That(arg1, Is.LessThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void Less(ulong arg1, ulong arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>

        public void Less(ulong arg1, ulong arg2)
        {
            That(arg1, Is.LessThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Less(decimal arg1, decimal arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        public void Less(decimal arg1, decimal arg2)
        {
            That(arg1, Is.LessThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Less(double arg1, double arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        public void Less(double arg1, double arg2)
        {
            That(arg1, Is.LessThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Less(float arg1, float arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        public void Less(float arg1, float arg2)
        {
            That(arg1, Is.LessThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Less(IComparable arg1, IComparable arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThan(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        public void Less(IComparable arg1, IComparable arg2)
        {
            That(arg1, Is.LessThan(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void GreaterOrEqual(int arg1, int arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        public void GreaterOrEqual(int arg1, int arg2)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
        }
        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void GreaterOrEqual(uint arg1, uint arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>

        public void GreaterOrEqual(uint arg1, uint arg2)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
        }


        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void GreaterOrEqual(long arg1, long arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        public void GreaterOrEqual(long arg1, long arg2)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void GreaterOrEqual(ulong arg1, ulong arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>

        public void GreaterOrEqual(ulong arg1, ulong arg2)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void GreaterOrEqual(decimal arg1, decimal arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        public void GreaterOrEqual(decimal arg1, decimal arg2)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void GreaterOrEqual(double arg1, double arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        public void GreaterOrEqual(double arg1, double arg2)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
        }


        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void GreaterOrEqual(float arg1, float arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        public void GreaterOrEqual(float arg1, float arg2)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void GreaterOrEqual(IComparable arg1, IComparable arg2, string message, params object[] args)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is greater than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be greater</param>
        /// <param name="arg2">The second value, expected to be less</param>
        public void GreaterOrEqual(IComparable arg1, IComparable arg2)
        {
            That(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
        }


        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void LessOrEqual(int arg1, int arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        public void LessOrEqual(int arg1, int arg2)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void LessOrEqual(uint arg1, uint arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>

        public void LessOrEqual(uint arg1, uint arg2)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), null, null);
        }

        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void LessOrEqual(long arg1, long arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        public void LessOrEqual(long arg1, long arg2)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), null, null);
        }


        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void LessOrEqual(ulong arg1, ulong arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>

        public void LessOrEqual(ulong arg1, ulong arg2)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), null, null);
        }


        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void LessOrEqual(decimal arg1, decimal arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        public void LessOrEqual(decimal arg1, decimal arg2)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), null, null);
        }


        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void LessOrEqual(double arg1, double arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        public void LessOrEqual(double arg1, double arg2)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), null, null);
        }


        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void LessOrEqual(float arg1, float arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        public void LessOrEqual(float arg1, float arg2)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), null, null);
        }


        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void LessOrEqual(IComparable arg1, IComparable arg2, string message, params object[] args)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), message, args);
        }

        /// <summary>
        /// Verifies that the first value is less than or equal to the second
        /// value. If it is not, then an
        /// <see cref="AssertionException"/> is thrown. 
        /// </summary>
        /// <param name="arg1">The first value, expected to be less</param>
        /// <param name="arg2">The second value, expected to be greater</param>
        public void LessOrEqual(IComparable arg1, IComparable arg2)
        {
            That(arg1, Is.LessThanOrEqualTo(arg2), null, null);
        }


        /// <summary>
        /// Asserts that a condition is true. If the condition is false the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary>
        /// <param name="condition">The evaluated condition</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void True(bool? condition, string message, params object[] args)
        {
            That(condition, Is.True, message, args);
        }

        /// <summary>
        /// Asserts that a condition is true. If the condition is false the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary>
        /// <param name="condition">The evaluated condition</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void True(bool condition, string message, params object[] args)
        {
            That(condition, Is.True, message, args);
        }

        /// <summary>
        /// Asserts that a condition is true. If the condition is false the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary>
        /// <param name="condition">The evaluated condition</param>
        public void True(bool? condition)
        {
            That(condition, Is.True, null, null);
        }

        /// <summary>
        /// Asserts that a condition is true. If the condition is false the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary>
        /// <param name="condition">The evaluated condition</param>
        public void True(bool condition)
        {
            That(condition, Is.True, null, null);
        }

        /// <summary>
        /// Asserts that a condition is true. If the condition is false the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary>
        /// <param name="condition">The evaluated condition</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void IsTrue(bool? condition, string message, params object[] args)
        {
            That(condition, Is.True, message, args);
        }

        /// <summary>
        /// Asserts that a condition is true. If the condition is false the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary>
        /// <param name="condition">The evaluated condition</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void IsTrue(bool condition, string message, params object[] args)
        {
            That(condition, Is.True, message, args);
        }

        /// <summary>
        /// Asserts that a condition is true. If the condition is false the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary>
        /// <param name="condition">The evaluated condition</param>
        public void IsTrue(bool? condition)
        {
            That(condition, Is.True, null, null);
        }

        /// <summary>
        /// Asserts that a condition is true. If the condition is false the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary>
        /// <param name="condition">The evaluated condition</param>
        public void IsTrue(bool condition)
        {
            That(condition, Is.True, null, null);
        }


        /// <summary>
        /// Asserts that a condition is false. If the condition is true the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary> 
        /// <param name="condition">The evaluated condition</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void False(bool? condition, string message, params object[] args)
        {
            That(condition, Is.False, message, args);
        }

        /// <summary>
        /// Asserts that a condition is false. If the condition is true the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary> 
        /// <param name="condition">The evaluated condition</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void False(bool condition, string message, params object[] args)
        {
            That(condition, Is.False, message, args);
        }

        /// <summary>
        /// Asserts that a condition is false. If the condition is true the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary> 
        /// <param name="condition">The evaluated condition</param>
        public void False(bool? condition)
        {
            That(condition, Is.False, null, null);
        }

        /// <summary>
        /// Asserts that a condition is false. If the condition is true the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary> 
        /// <param name="condition">The evaluated condition</param>
        public void False(bool condition)
        {
            That(condition, Is.False, null, null);
        }

        /// <summary>
        /// Asserts that a condition is false. If the condition is true the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary> 
        /// <param name="condition">The evaluated condition</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void IsFalse(bool? condition, string message, params object[] args)
        {
            That(condition, Is.False, message, args);
        }

        /// <summary>
        /// Asserts that a condition is false. If the condition is true the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary> 
        /// <param name="condition">The evaluated condition</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void IsFalse(bool condition, string message, params object[] args)
        {
            That(condition, Is.False, message, args);
        }

        /// <summary>
        /// Asserts that a condition is false. If the condition is true the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary> 
        /// <param name="condition">The evaluated condition</param>
        public void IsFalse(bool? condition)
        {
            That(condition, Is.False, null, null);
        }

        /// <summary>
        /// Asserts that a condition is false. If the condition is true the method throws
        /// an <see cref="AssertionException"/>.
        /// </summary> 
        /// <param name="condition">The evaluated condition</param>
        public void IsFalse(bool condition)
        {
            That(condition, Is.False, null, null);
        }


        /// <summary>
        /// Verifies that the object that is passed in is not equal to <code>null</code>
        /// If the object is <code>null</code> then an <see cref="AssertionException"/>
        /// is thrown.
        /// </summary>
        /// <param name="anObject">The object that is to be tested</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void NotNull(object anObject, string message, params object[] args)
        {
            That(anObject, Is.Not.Null, message, args);
        }

        /// <summary>
        /// Verifies that the object that is passed in is not equal to <code>null</code>
        /// If the object is <code>null</code> then an <see cref="AssertionException"/>
        /// is thrown.
        /// </summary>
        /// <param name="anObject">The object that is to be tested</param>
        public void NotNull(object anObject)
        {
            That(anObject, Is.Not.Null, null, null);
        }

        /// <summary>
        /// Verifies that the object that is passed in is not equal to <code>null</code>
        /// If the object is <code>null</code> then an <see cref="AssertionException"/>
        /// is thrown.
        /// </summary>
        /// <param name="anObject">The object that is to be tested</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void IsNotNull(object anObject, string message, params object[] args)
        {
            That(anObject, Is.Not.Null, message, args);
        }

        /// <summary>
        /// Verifies that the object that is passed in is not equal to <code>null</code>
        /// If the object is <code>null</code> then an <see cref="AssertionException"/>
        /// is thrown.
        /// </summary>
        /// <param name="anObject">The object that is to be tested</param>
        public void IsNotNull(object anObject)
        {
            That(anObject, Is.Not.Null, null, null);
        }


        /// <summary>
        /// Verifies that the object that is passed in is equal to <code>null</code>
        /// If the object is not <code>null</code> then an <see cref="AssertionException"/>
        /// is thrown.
        /// </summary>
        /// <param name="anObject">The object that is to be tested</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Null(object anObject, string message, params object[] args)
        {
            That(anObject, Is.Null, message, args);
        }

        /// <summary>
        /// Verifies that the object that is passed in is equal to <code>null</code>
        /// If the object is not <code>null</code> then an <see cref="AssertionException"/>
        /// is thrown.
        /// </summary>
        /// <param name="anObject">The object that is to be tested</param>
        public void Null(object anObject)
        {
            That(anObject, Is.Null, null, null);
        }

        /// <summary>
        /// Verifies that the object that is passed in is equal to <code>null</code>
        /// If the object is not <code>null</code> then an <see cref="AssertionException"/>
        /// is thrown.
        /// </summary>
        /// <param name="anObject">The object that is to be tested</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void IsNull(object anObject, string message, params object[] args)
        {
            That(anObject, Is.Null, message, args);
        }

        /// <summary>
        /// Verifies that the object that is passed in is equal to <code>null</code>
        /// If the object is not <code>null</code> then an <see cref="AssertionException"/>
        /// is thrown.
        /// </summary>
        /// <param name="anObject">The object that is to be tested</param>
        public void IsNull(object anObject)
        {
            That(anObject, Is.Null, null, null);
        }


        /// <summary>
        /// Verifies that the double that is passed in is an <code>NaN</code> value.
        /// If the object is not <code>NaN</code> then an <see cref="AssertionException"/>
        /// is thrown.
        /// </summary>
        /// <param name="aDouble">The value that is to be tested</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void IsNaN(double aDouble, string message, params object[] args)
        {
            That(aDouble, Is.NaN, message, args);
        }

        /// <summary>
        /// Verifies that the double that is passed in is an <code>NaN</code> value.
        /// If the object is not <code>NaN</code> then an <see cref="AssertionException"/>
        /// is thrown.
        /// </summary>
        /// <param name="aDouble">The value that is to be tested</param>
        public void IsNaN(double aDouble)
        {
            That(aDouble, Is.NaN, null, null);
        }

        /// <summary>
        /// Verifies that the double that is passed in is an <code>NaN</code> value.
        /// If the object is not <code>NaN</code> then an <see cref="AssertionException"/>
        /// is thrown.
        /// </summary>
        /// <param name="aDouble">The value that is to be tested</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void IsNaN(double? aDouble, string message, params object[] args)
        {
            That(aDouble, Is.NaN, message, args);
        }

        /// <summary>
        /// Verifies that the double that is passed in is an <code>NaN</code> value.
        /// If the object is not <code>NaN</code> then an <see cref="AssertionException"/>
        /// is thrown.
        /// </summary>
        /// <param name="aDouble">The value that is to be tested</param>
        public void IsNaN(double? aDouble)
        {
            That(aDouble, Is.NaN, null, null);
        }


        /// <summary>
        /// Assert that a string is empty - that is equal to string.Empty
        /// </summary>
        /// <param name="aString">The string to be tested</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void IsEmpty(string aString, string message, params object[] args)
        {
            That(aString, new EmptyStringConstraint(), message, args);
        }

        /// <summary>
        /// Assert that a string is empty - that is equal to string.Empty
        /// </summary>
        /// <param name="aString">The string to be tested</param>
        public void IsEmpty(string aString)
        {
            That(aString, new EmptyStringConstraint(), null, null);
        }

        /// <summary>
        /// Assert that an array, list or other collection is empty
        /// </summary>
        /// <param name="collection">An array, list or other collection implementing ICollection</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void IsEmpty(IEnumerable collection, string message, params object[] args)
        {
            That(collection, new EmptyCollectionConstraint(), message, args);
        }

        /// <summary>
        /// Assert that an array, list or other collection is empty
        /// </summary>
        /// <param name="collection">An array, list or other collection implementing ICollection</param>
        public void IsEmpty(IEnumerable collection)
        {
            That(collection, new EmptyCollectionConstraint(), null, null);
        }


        /// <summary>
        /// Assert that a string is not empty - that is not equal to string.Empty
        /// </summary>
        /// <param name="aString">The string to be tested</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void IsNotEmpty(string aString, string message, params object[] args)
        {
            That(aString, Is.Not.Empty, message, args);
        }

        /// <summary>
        /// Assert that a string is not empty - that is not equal to string.Empty
        /// </summary>
        /// <param name="aString">The string to be tested</param>
        public void IsNotEmpty(string aString)
        {
            That(aString, Is.Not.Empty, null, null);
        }


        /// <summary>
        /// Assert that an array, list or other collection is not empty
        /// </summary>
        /// <param name="collection">An array, list or other collection implementing ICollection</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void IsNotEmpty(IEnumerable collection, string message, params object[] args)
        {
            That(collection, Is.Not.Empty, message, args);
        }

        /// <summary>
        /// Assert that an array, list or other collection is not empty
        /// </summary>
        /// <param name="collection">An array, list or other collection implementing ICollection</param>
        public void IsNotEmpty(IEnumerable collection)
        {
            That(collection, Is.Not.Empty, null, null);
        }


        /// <summary>
        /// Asserts that an int is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Zero(int actual)
        {
            That(actual, Is.Zero);
        }

        /// <summary>
        /// Asserts that an int is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Zero(int actual, string message, params object[] args)
        {
            That(actual, Is.Zero, message, args);
        }


        /// <summary>
        /// Asserts that an unsigned int is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>

        public void Zero(uint actual)
        {
            That(actual, Is.Zero);
        }

        /// <summary>
        /// Asserts that an unsigned int is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void Zero(uint actual, string message, params object[] args)
        {
            That(actual, Is.Zero, message, args);
        }

        /// <summary>
        /// Asserts that a Long is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Zero(long actual)
        {
            That(actual, Is.Zero);
        }

        /// <summary>
        /// Asserts that a Long is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Zero(long actual, string message, params object[] args)
        {
            That(actual, Is.Zero, message, args);
        }

        /// <summary>
        /// Asserts that an unsigned Long is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>

        public void Zero(ulong actual)
        {
            That(actual, Is.Zero);
        }

        /// <summary>
        /// Asserts that an unsigned Long is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void Zero(ulong actual, string message, params object[] args)
        {
            That(actual, Is.Zero, message, args);
        }


        /// <summary>
        /// Asserts that a decimal is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Zero(decimal actual)
        {
            That(actual, Is.Zero);
        }

        /// <summary>
        /// Asserts that a decimal is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Zero(decimal actual, string message, params object[] args)
        {
            That(actual, Is.Zero, message, args);
        }

        /// <summary>
        /// Asserts that a double is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Zero(double actual)
        {
            That(actual, Is.Zero);
        }

        /// <summary>
        /// Asserts that a double is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Zero(double actual, string message, params object[] args)
        {
            That(actual, Is.Zero, message, args);
        }


        /// <summary>
        /// Asserts that a float is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Zero(float actual)
        {
            That(actual, Is.Zero);
        }

        /// <summary>
        /// Asserts that a float is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Zero(float actual, string message, params object[] args)
        {
            That(actual, Is.Zero, message, args);
        }

        /// <summary>
        /// Asserts that an int is not zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void NotZero(int actual)
        {
            That(actual, Is.Not.Zero);
        }

        /// <summary>
        /// Asserts that an int is not zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void NotZero(int actual, string message, params object[] args)
        {
            That(actual, Is.Not.Zero, message, args);
        }


        /// <summary>
        /// Asserts that an unsigned int is not zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>

        public void NotZero(uint actual)
        {
            That(actual, Is.Not.Zero);
        }

        /// <summary>
        /// Asserts that an unsigned int is not zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void NotZero(uint actual, string message, params object[] args)
        {
            That(actual, Is.Not.Zero, message, args);
        }


        /// <summary>
        /// Asserts that a Long is not zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void NotZero(long actual)
        {
            That(actual, Is.Not.Zero);
        }

        /// <summary>
        /// Asserts that a Long is not zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void NotZero(long actual, string message, params object[] args)
        {
            That(actual, Is.Not.Zero, message, args);
        }

        /// <summary>
        /// Asserts that an unsigned Long is not zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>

        public void NotZero(ulong actual)
        {
            That(actual, Is.Not.Zero);
        }

        /// <summary>
        /// Asserts that an unsigned Long is not zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void NotZero(ulong actual, string message, params object[] args)
        {
            That(actual, Is.Not.Zero, message, args);
        }

        /// <summary>
        /// Asserts that a decimal is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void NotZero(decimal actual)
        {
            That(actual, Is.Not.Zero);
        }

        /// <summary>
        /// Asserts that a decimal is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void NotZero(decimal actual, string message, params object[] args)
        {
            That(actual, Is.Not.Zero, message, args);
        }


        /// <summary>
        /// Asserts that a double is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void NotZero(double actual)
        {
            That(actual, Is.Not.Zero);
        }

        /// <summary>
        /// Asserts that a double is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void NotZero(double actual, string message, params object[] args)
        {
            That(actual, Is.Not.Zero, message, args);
        }


        /// <summary>
        /// Asserts that a float is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void NotZero(float actual)
        {
            That(actual, Is.Not.Zero);
        }

        /// <summary>
        /// Asserts that a float is zero.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void NotZero(float actual, string message, params object[] args)
        {
            That(actual, Is.Not.Zero, message, args);
        }


        /// <summary>
        /// Asserts that an int is positive.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Positive(int actual)
        {
            That(actual, Is.Positive);
        }

        /// <summary>
        /// Asserts that an int is positive.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Positive(int actual, string message, params object[] args)
        {
            That(actual, Is.Positive, message, args);
        }


        /// <summary>
        /// Asserts that an unsigned int is positive.
        /// </summary>
        /// <param name="actual">The number to be examined</param>

        public void Positive(uint actual)
        {
            That(actual, Is.Positive);
        }

        /// <summary>
        /// Asserts that an unsigned int is positive.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void Positive(uint actual, string message, params object[] args)
        {
            That(actual, Is.Positive, message, args);
        }


        /// <summary>
        /// Asserts that a Long is positive.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Positive(long actual)
        {
            That(actual, Is.Positive);
        }

        /// <summary>
        /// Asserts that a Long is positive.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Positive(long actual, string message, params object[] args)
        {
            That(actual, Is.Positive, message, args);
        }


        /// <summary>
        /// Asserts that an unsigned Long is positive.
        /// </summary>
        /// <param name="actual">The number to be examined</param>

        public void Positive(ulong actual)
        {
            That(actual, Is.Positive);
        }

        /// <summary>
        /// Asserts that an unsigned Long is positive.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void Positive(ulong actual, string message, params object[] args)
        {
            That(actual, Is.Positive, message, args);
        }



        /// <summary>
        /// Asserts that a decimal is positive.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Positive(decimal actual)
        {
            That(actual, Is.Positive);
        }

        /// <summary>
        /// Asserts that a decimal is positive.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Positive(decimal actual, string message, params object[] args)
        {
            That(actual, Is.Positive, message, args);
        }



        /// <summary>
        /// Asserts that a double is positive.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Positive(double actual)
        {
            That(actual, Is.Positive);
        }

        /// <summary>
        /// Asserts that a double is positive.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Positive(double actual, string message, params object[] args)
        {
            That(actual, Is.Positive, message, args);
        }



        /// <summary>
        /// Asserts that a float is positive.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Positive(float actual)
        {
            That(actual, Is.Positive);
        }

        /// <summary>
        /// Asserts that a float is positive.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Positive(float actual, string message, params object[] args)
        {
            That(actual, Is.Positive, message, args);
        }


        /// <summary>
        /// Asserts that an int is negative.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Negative(int actual)
        {
            That(actual, Is.Negative);
        }

        /// <summary>
        /// Asserts that an int is negative.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Negative(int actual, string message, params object[] args)
        {
            That(actual, Is.Negative, message, args);
        }


        /// <summary>
        /// Asserts that an unsigned int is negative.
        /// </summary>
        /// <param name="actual">The number to be examined</param>

        public void Negative(uint actual)
        {
            That(actual, Is.Negative);
        }

        /// <summary>
        /// Asserts that an unsigned int is negative.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void Negative(uint actual, string message, params object[] args)
        {
            That(actual, Is.Negative, message, args);
        }


        /// <summary>
        /// Asserts that a Long is negative.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Negative(long actual)
        {
            That(actual, Is.Negative);
        }

        /// <summary>
        /// Asserts that a Long is negative.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Negative(long actual, string message, params object[] args)
        {
            That(actual, Is.Negative, message, args);
        }


        /// <summary>
        /// Asserts that an unsigned Long is negative.
        /// </summary>
        /// <param name="actual">The number to be examined</param>

        public void Negative(ulong actual)
        {
            That(actual, Is.Negative);
        }

        /// <summary>
        /// Asserts that an unsigned Long is negative.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>

        public void Negative(ulong actual, string message, params object[] args)
        {
            That(actual, Is.Negative, message, args);
        }


        /// <summary>
        /// Asserts that a decimal is negative.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Negative(decimal actual)
        {
            That(actual, Is.Negative);
        }

        /// <summary>
        /// Asserts that a decimal is negative.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Negative(decimal actual, string message, params object[] args)
        {
            That(actual, Is.Negative, message, args);
        }


        /// <summary>
        /// Asserts that a double is negative.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Negative(double actual)
        {
            That(actual, Is.Negative);
        }

        /// <summary>
        /// Asserts that a double is negative.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Negative(double actual, string message, params object[] args)
        {
            That(actual, Is.Negative, message, args);
        }

        /// <summary>
        /// Asserts that a float is negative.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        public void Negative(float actual)
        {
            That(actual, Is.Negative);
        }

        /// <summary>
        /// Asserts that a float is negative.
        /// </summary>
        /// <param name="actual">The number to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void Negative(float actual, string message, params object[] args)
        {
            That(actual, Is.Negative, message, args);
        }


        /// <summary>
        /// Verifies that two doubles are equal considering a delta. If the
        /// expected value is infinity then the delta value is ignored. If 
        /// they are not equal then an <see cref="AssertionException"/> is
        /// thrown.
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The actual value</param>
        /// <param name="delta">The maximum acceptable difference between the
        /// the expected and the actual</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void AreEqual(double expected, double actual, double delta, string message, params object[] args)
        {
            AssertDoublesAreEqual(expected, actual, delta, message, args);
        }

        /// <summary>
        /// Verifies that two doubles are equal considering a delta. If the
        /// expected value is infinity then the delta value is ignored. If 
        /// they are not equal then an <see cref="AssertionException"/> is
        /// thrown.
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The actual value</param>
        /// <param name="delta">The maximum acceptable difference between the
        /// the expected and the actual</param>
        public void AreEqual(double expected, double actual, double delta)
        {
            AssertDoublesAreEqual(expected, actual, delta, null, null);
        }

        /// <summary>
        /// Verifies that two doubles are equal considering a delta. If the
        /// expected value is infinity then the delta value is ignored. If 
        /// they are not equal then an <see cref="AssertionException"/> is
        /// thrown.
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The actual value</param>
        /// <param name="delta">The maximum acceptable difference between the
        /// the expected and the actual</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void AreEqual(double expected, double? actual, double delta, string message, params object[] args)
        {
            AssertDoublesAreEqual(expected, (double)actual, delta, message, args);
        }

        /// <summary>
        /// Verifies that two doubles are equal considering a delta. If the
        /// expected value is infinity then the delta value is ignored. If 
        /// they are not equal then an <see cref="AssertionException"/> is
        /// thrown.
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The actual value</param>
        /// <param name="delta">The maximum acceptable difference between the
        /// the expected and the actual</param>
        public void AreEqual(double expected, double? actual, double delta)
        {
            AssertDoublesAreEqual(expected, (double)actual, delta, null, null);
        }


        /// <summary>
        /// Verifies that two objects are equal.  Two objects are considered
        /// equal if both are null, or if both have the same value. NUnit
        /// has special semantics for some object types.
        /// If they are not equal an <see cref="AssertionException"/> is thrown.
        /// </summary>
        /// <param name="expected">The value that is expected</param>
        /// <param name="actual">The actual value</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void AreEqual(object expected, object actual, string message, params object[] args)
        {
            That(actual, Is.EqualTo(expected), message, args);
        }

        /// <summary>
        /// Verifies that two objects are equal.  Two objects are considered
        /// equal if both are null, or if both have the same value. NUnit
        /// has special semantics for some object types.
        /// If they are not equal an <see cref="AssertionException"/> is thrown.
        /// </summary>
        /// <param name="expected">The value that is expected</param>
        /// <param name="actual">The actual value</param>
        public void AreEqual(object expected, object actual)
        {
            That(actual, Is.EqualTo(expected), null, null);
        }

        /// <summary>
        /// Verifies that two objects are not equal.  Two objects are considered
        /// equal if both are null, or if both have the same value. NUnit
        /// has special semantics for some object types.
        /// If they are equal an <see cref="AssertionException"/> is thrown.
        /// </summary>
        /// <param name="expected">The value that is expected</param>
        /// <param name="actual">The actual value</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void AreNotEqual(object expected, object actual, string message, params object[] args)
        {
            That(actual, Is.Not.EqualTo(expected), message, args);
        }

        /// <summary>
        /// Verifies that two objects are not equal.  Two objects are considered
        /// equal if both are null, or if both have the same value. NUnit
        /// has special semantics for some object types.
        /// If they are equal an <see cref="AssertionException"/> is thrown.
        /// </summary>
        /// <param name="expected">The value that is expected</param>
        /// <param name="actual">The actual value</param>
        public void AreNotEqual(object expected, object actual)
        {
            That(actual, Is.Not.EqualTo(expected), null, null);
        }

        /// <summary>
        /// Asserts that two objects refer to the same object. If they
        /// are not the same an <see cref="AssertionException"/> is thrown.
        /// </summary>
        /// <param name="expected">The expected object</param>
        /// <param name="actual">The actual object</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void AreSame(object expected, object actual, string message, params object[] args)
        {
            That(actual, Is.SameAs(expected), message, args);
        }

        /// <summary>
        /// Asserts that two objects refer to the same object. If they
        /// are not the same an <see cref="AssertionException"/> is thrown.
        /// </summary>
        /// <param name="expected">The expected object</param>
        /// <param name="actual">The actual object</param>
        public void AreSame(object expected, object actual)
        {
            That(actual, Is.SameAs(expected), null, null);
        }


        /// <summary>
        /// Asserts that two objects do not refer to the same object. If they
        /// are the same an <see cref="AssertionException"/> is thrown.
        /// </summary>
        /// <param name="expected">The expected object</param>
        /// <param name="actual">The actual object</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        public void AreNotSame(object expected, object actual, string message, params object[] args)
        {
            That(actual, Is.Not.SameAs(expected), message, args);
        }

        /// <summary>
        /// Asserts that two objects do not refer to the same object. If they
        /// are the same an <see cref="AssertionException"/> is thrown.
        /// </summary>
        /// <param name="expected">The expected object</param>
        /// <param name="actual">The actual object</param>
        public void AreNotSame(object expected, object actual)
        {
            That(actual, Is.Not.SameAs(expected), null, null);
        }

        /// <summary>
        /// Helper for AreEqual(double expected, double actual, ...)
        /// allowing code generation to work consistently.
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The actual value</param>
        /// <param name="delta">The maximum acceptable difference between the
        /// the expected and the actual</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Array of objects to be used in formatting the message</param>
        protected void AssertDoublesAreEqual(double expected, double actual, double delta, string message, object[] args)
        {
            if (double.IsNaN(expected) || double.IsInfinity(expected))
                That(actual, Is.EqualTo(expected), message, args);
            else
                That(actual, Is.EqualTo(expected).Within(delta), message, args);
        }


        /// <summary>
        /// DO NOT USE! Use StringAreEqualIgnoringCase(...) or AreEqual(...) instead.
        /// The Equals method throws an InvalidOperationException. This is done
        /// to make sure there is no mistake by calling this function.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new bool Equals(object a, object b)
        {
            throw new InvalidOperationException("StringEquals should not be used. Use StringAreEqualIgnoringCase or AreEqual instead.");
        }

        /// <summary>
        /// DO NOT USE!
        /// The ReferenceEquals method throws an InvalidOperationException. This is done
        /// to make sure there is no mistake by calling this function.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new void ReferenceEquals(object a, object b)
        {
            throw new InvalidOperationException("StringReferenceEquals should not be used.");
        }

        /// <summary>
        /// Asserts that a string is found within another string.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The string to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Arguments used in formatting the message</param>
        public void Contains(string expected, string actual, string message, params object[] args)
        {
            That(actual, Does.Contain(expected), message, args);
        }

        /// <summary>
        /// Asserts that a string is found within another string.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The string to be examined</param>
        public void Contains(string expected, string actual)
        {
            Contains(expected, actual, string.Empty, null);
        }

        /// <summary>
        /// Asserts that a string is not found within another string.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The string to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Arguments used in formatting the message</param>
        public void DoesNotContain(string expected, string actual, string message, params object[] args)
        {
            That(actual, Does.Not.Contain(expected), message, args);
        }

        /// <summary>
        /// Asserts that a string is found within another string.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The string to be examined</param>
        public void DoesNotContain(string expected, string actual)
        {
            DoesNotContain(expected, actual, string.Empty, null);
        }

        /// <summary>
        /// Asserts that a string starts with another string.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The string to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Arguments used in formatting the message</param>
        public void StartsWith(string expected, string actual, string message, params object[] args)
        {
            That(actual, Does.StartWith(expected), message, args);
        }

        /// <summary>
        /// Asserts that a string starts with another string.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The string to be examined</param>
        public void StartsWith(string expected, string actual)
        {
            StartsWith(expected, actual, string.Empty, null);
        }

        /// <summary>
        /// Asserts that a string does not start with another string.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The string to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Arguments used in formatting the message</param>
        public void DoesNotStartWith(string expected, string actual, string message, params object[] args)
        {
            That(actual, Does.Not.StartWith(expected), message, args);
        }

        /// <summary>
        /// Asserts that a string does not start with another string.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The string to be examined</param>
        public void DoesNotStartWith(string expected, string actual)
        {
            DoesNotStartWith(expected, actual, string.Empty, null);
        }

        /// <summary>
        /// Asserts that a string ends with another string.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The string to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Arguments used in formatting the message</param>
        public void EndsWith(string expected, string actual, string message, params object[] args)
        {
            That(actual, Does.EndWith(expected), message, args);
        }

        /// <summary>
        /// Asserts that a string ends with another string.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The string to be examined</param>
        public void EndsWith(string expected, string actual)
        {
            EndsWith(expected, actual, string.Empty, null);
        }

        /// <summary>
        /// Asserts that a string does not end with another string.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The string to be examined</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Arguments used in formatting the message</param>
        public void DoesNotEndWith(string expected, string actual, string message, params object[] args)
        {
            That(actual, Does.Not.EndWith(expected), message, args);
        }

        /// <summary>
        /// Asserts that a string does not end with another string.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The string to be examined</param>
        public void DoesNotEndWith(string expected, string actual)
        {
            DoesNotEndWith(expected, actual, string.Empty, null);
        }

        /// <summary>
        /// Asserts that two strings are equal, without regard to case.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The actual string</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Arguments used in formatting the message</param>
        public void AreEqualIgnoringCase(string expected, string actual, string message, params object[] args)
        {
            That(actual, Is.EqualTo(expected).IgnoreCase, message, args);
        }

        /// <summary>
        /// Asserts that two strings are equal, without regard to case.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The actual string</param>
        public void AreEqualIgnoringCase(string expected, string actual)
        {
            AreEqualIgnoringCase(expected, actual, string.Empty, null);
        }

        /// <summary>
        /// Asserts that two strings are not equal, without regard to case.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The actual string</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Arguments used in formatting the message</param>
        public void AreNotEqualIgnoringCase(string expected, string actual, string message, params object[] args)
        {
            That(actual, Is.Not.EqualTo(expected).IgnoreCase, message, args);
        }

        /// <summary>
        /// Asserts that two strings are not equal, without regard to case.
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The actual string</param>
        public void AreNotEqualIgnoringCase(string expected, string actual)
        {
            AreNotEqualIgnoringCase(expected, actual, string.Empty, null);
        }

        /// <summary>
        /// Asserts that a string matches an expected regular expression pattern.
        /// </summary>
        /// <param name="pattern">The regex pattern to be matched</param>
        /// <param name="actual">The actual string</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Arguments used in formatting the message</param>
        public void IsMatch(string pattern, string actual, string message, params object[] args)
        {
            That(actual, Does.Match(pattern), message, args);
        }

        /// <summary>
        /// Asserts that a string matches an expected regular expression pattern.
        /// </summary>
        /// <param name="pattern">The regex pattern to be matched</param>
        /// <param name="actual">The actual string</param>
        public void IsMatch(string pattern, string actual)
        {
            IsMatch(pattern, actual, string.Empty, null);
        }

        /// <summary>
        /// Asserts that a string does not match an expected regular expression pattern.
        /// </summary>
        /// <param name="pattern">The regex pattern to be used</param>
        /// <param name="actual">The actual string</param>
        /// <param name="message">The message to display in case of failure</param>
        /// <param name="args">Arguments used in formatting the message</param>
        public void DoesNotMatch(string pattern, string actual, string message, params object[] args)
        {
            That(actual, Does.Not.Match(pattern), message, args);
        }

        /// <summary>
        /// Asserts that a string does not match an expected regular expression pattern.
        /// </summary>
        /// <param name="pattern">The regex pattern to be used</param>
        /// <param name="actual">The actual string</param>
        public void DoesNotMatch(string pattern, string actual)
        {
            DoesNotMatch(pattern, actual, string.Empty, null);
        }


        #endregion
    }
}