﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;

namespace PowerTester.Framework
{
    /// <summary>
    /// Class for restfull api realted helper methods
    /// </summary>
    public static class RestfullApiHelper
    {
        private static int _requestTimeout = 10000;
        public static CookieContainer CookieContainer = new CookieContainer();

        /// <summary>
        /// Timeout for rest api call
        /// </summary>
        public static int RequestTimeout
        {
            get { return _requestTimeout; }
            set { _requestTimeout = value; }
        }

        /// <summary>
        /// Http method types
        /// </summary>
        public enum CustomHttpMethod
        {
            Unknown = 0,
            Get = 1,
            Post = 2,
            Put = 3,
            DELETE = 4,
            Head = 5,
            Patch = 6,
            Options = 7,
        }

        /// <summary>
        /// Creates a web request for web api and returns the returned stream in string format
        /// </summary>
        /// <param name="serverAddress">The url (base address) of the web api.</param>
        /// <param name="endPoint">The endpoint for the web api.</param>
        /// <param name="method">The http method to call on the web api.</param>
        /// <param name="credentials">The credentials for the web api. May be null.</param>
        /// <param name="expectedResponseStatus">The expected http response status for the call.</param>
        /// <returns>The returned stream in string format.</returns>
        public static string CreateRequest(
            string serverAddress,
            string endPoint,
            CustomHttpMethod method, 
            NetworkCredential credentials,
            HttpStatusCode expectedResponseStatus = HttpStatusCode.OK)
        {
            return CreateRequest(serverAddress, endPoint, method, credentials, null, expectedResponseStatus, CookieContainer);
        }

        /// <summary>
        /// Creates a web request for web api and returns the returned stream in string format
        /// </summary>
        /// <param name="serverAddress">The url (base address) of the web api.</param>
        /// <param name="endPoint">The endpoint for the web api.</param>
        /// <param name="method">The http method to call on the web api.</param>
        /// <param name="credentials">The credentials for the web api. May be null.</param>
        /// <param name="headers">The Header key value names</param>
        /// <param name="expectedResponseStatus">The expected http response status for the call.</param>
        /// <returns>The returned stream in string format.</returns>
        public static string CreateRequest(
            string serverAddress,
            string endPoint,
            CustomHttpMethod method, 
            NetworkCredential credentials,
            NameValueCollection headers,
            HttpStatusCode expectedResponseStatus = HttpStatusCode.OK)
        {
            return CreateRequest(serverAddress, endPoint, method, credentials, headers, expectedResponseStatus, CookieContainer);
        }

        /// <summary>
        /// Creates a web request for web api and returns the returned stream in string format
        /// </summary>
        /// <param name="serverAddress">The url (base address) of the web api.</param>
        /// <param name="endPoint">The endpoint for the web api.</param>
        /// <param name="method">The http method to call on the web api.</param>
        /// <param name="credentials">The credentials for the web api. May be null.</param>
        /// <param name="headers">The Header key value names</param>
        /// <param name="expectedResponseStatus">The expected http response status for the call.</param>
        /// <param name="cookieStore"></param>
        /// <returns>The returned stream in string format.</returns>
        public static string CreateRequest(
            string serverAddress,
            string endPoint,
            CustomHttpMethod method,
            NetworkCredential credentials,
            NameValueCollection headers,
            HttpStatusCode expectedResponseStatus,
            CookieContainer cookieStore,
            string data = "",
            string authenticationType = "NTLM",
            string contentType = "text/plain; charset=utf-8",
            NameValueCollection expectedHeaders = null, 
            List<string> unExpectedHeaders = null)
        {
            string result = String.Empty;
            Uri requestTarget = FixUrl(serverAddress + "/" + endPoint);

            Logger.Info(String.Format(@"Request Url: {0}", requestTarget));
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create((requestTarget));
            if (credentials != null)
            {
                CredentialCache cs = new CredentialCache();
                cs.Add(requestTarget, authenticationType, credentials);
                request.Credentials = cs;
            }

            if (headers != null)
            {
                request.SetHeader(headers);
            }

            Logger.Info(String.Format(@"Request TimeOut: {0}", RequestTimeout));
            request.Timeout = RequestTimeout;

            Logger.Info(String.Format(@"Request Method: {0}", method.ToString().ToUpper()));
            request.Method = method.ToString();
            
            if (method == CustomHttpMethod.Post || method == CustomHttpMethod.Put)
            {
                Logger.Info(String.Format(@"Request contentType: {0}", contentType));
                request.ContentType = contentType;
            }

            request.CookieContainer = cookieStore;

            if (!string.IsNullOrEmpty(data))
            {
                var payload = Encoding.UTF8.GetBytes(data);
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(payload, 0, payload.Length);
                }
            }

            try
            {
                using (HttpWebResponse httpWebResponse = (HttpWebResponse) request.GetResponse())
                {
                    result = TryToReadResponeStream(httpWebResponse);

                    VerifyStatusCode(expectedResponseStatus, httpWebResponse.StatusCode);

                    verifyHeaders(httpWebResponse, expectedHeaders, unExpectedHeaders);
                }
            }
            catch (WebException webException)
            {
                if (webException.Response != null)
                {
                    using (StreamReader responseReader = new StreamReader(webException.Response.GetResponseStream()))
                    {
                        result = responseReader.ReadToEnd();
                    }

                    using (HttpWebResponse httpWebResponse = (HttpWebResponse) webException.Response)
                    {
                        VerifyStatusCode(expectedResponseStatus, httpWebResponse.StatusCode);
                    }
                }
                else
                {
                    throw new AutomationException(String.Format("Unexpected exception occured {0}", webException));
                }
            }

            return result;
        }

        private static string TryToReadResponeStream(HttpWebResponse httpWebResponse)
        {
            string result = string.Empty;

            try
            {
                Logger.Info(String.Format(@"Reading response stream"));
                using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                    General.WriteToTestOutputDitectory("response.txt", result);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return result;
        }

        private static void VerifyStatusCode(HttpStatusCode expectedStatuscode, HttpStatusCode returnedStatusCode)
        {
            Logger.Info(String.Format(@"Response Status: {0}", returnedStatusCode));
            if (expectedStatuscode != returnedStatusCode)
            {
                throw new AutomationException(
                    String.Format("Required response status is '{0}' not '{1}' !",
                        expectedStatuscode,
                        returnedStatusCode));
            }
        }

        private static void verifyHeaders(HttpWebResponse httpWebResponse, NameValueCollection expectedHeaders, List<string> unExpectedHeaders = null)
        {
            if (expectedHeaders != null)
            {
                foreach (string expectedKey in expectedHeaders.AllKeys)
                {
                    if (httpWebResponse.Headers.AllKeys.Any(k => k == expectedKey))
                    {
                        TestExecuter.Assert.AreEqual(expectedHeaders.GetValues(expectedKey), httpWebResponse.Headers.GetValues(expectedKey), "Response containsed wrong header value for key: " + expectedKey);
                    }
                    else
                    {
                        TestExecuter.Assert.IsTrue(httpWebResponse.Headers.AllKeys.Any(k => k == expectedKey), "Response did not contain expected header key: " + expectedKey);
                    }
                }
            }

            if (unExpectedHeaders != null)
            {
                foreach (string unExpectedKey in unExpectedHeaders)
                {
                    if (httpWebResponse.Headers.AllKeys.Any(k => k == unExpectedKey))
                    {
                        TestExecuter.Assert.IsFalse(httpWebResponse.Headers.AllKeys.Any(k => k == unExpectedKey), "Response contained unexpected header key: " + unExpectedKey);
                    }
                }
            }
        }

        /// <summary>
        /// Creates a web request for web api and returns the returned stream in string format
        /// </summary>
        /// <param name="serverAddress">The url (base address) of the web api.</param>
        /// <param name="endPoint">The endpoint for the web api.</param>
        /// <param name="payload">The payload to send</param>
        /// <param name="credentials">The credentials for the web api. May be null.</param>
        /// <param name="headers">The Header key value names</param>
        /// <param name="expectedResponseStatus">The expected http response status for the call.</param>
        /// <param name="cookieStore"></param>
        /// <returns>The returned stream in string format.</returns>
        public static string CreatePost(
            string serverAddress,
            string endPoint,
            string payload,
            NetworkCredential credentials,
            NameValueCollection headers,
            string contentType = "text/plain",
            HttpStatusCode expectedResponseStatus = HttpStatusCode.OK,
            CookieContainer cookieStore = null)
        {
            string result = String.Empty;
            string requestTarget = serverAddress + "/" + endPoint;

            Logger.Info(String.Format(@"Request Url: {0}", requestTarget));
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(FixUrl(requestTarget));

            if (credentials != null)
            {
                CredentialCache cs = new CredentialCache();
                cs.Add(new Uri(requestTarget), "NTLM", credentials);
                request.Credentials = cs;
            }

            request.SetHeader(headers);

            Logger.Info(String.Format(@"Request TimeOut: {0}", RequestTimeout));
            request.Timeout = RequestTimeout;

            Logger.Info(String.Format(@"Request Method: {0}", CustomHttpMethod.Post.ToString().ToUpper()));
            request.Method = CustomHttpMethod.Post.ToString();
            request.ContentType = contentType;

            request.CookieContainer = cookieStore;

            var data = Encoding.ASCII.GetBytes(payload);
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            try
            {
                using (HttpWebResponse httpWebResponse = (HttpWebResponse)request.GetResponse())
                {
                    Logger.Info(String.Format(@"Response Status: {0}", httpWebResponse.StatusCode));
                    using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        result = streamReader.ReadToEnd();
                        General.WriteToTestOutputDitectory("response.txt", result);
                    }

                    if (HttpStatusCode.OK != expectedResponseStatus)
                    {
                        throw new AutomationException(
                            String.Format("Required response status is '{0}' not '{1}' !",
                            expectedResponseStatus,
                            httpWebResponse.StatusCode));
                    }
                }
            }
            catch (WebException webException)
            {
                if (webException.Response != null)
                {
                    using (HttpWebResponse httpWebResponse = (HttpWebResponse)webException.Response)
                    {
                        Logger.Info(String.Format(@"Response Status: {0}", (int)httpWebResponse.StatusCode + " " + httpWebResponse.StatusCode));
                        if (expectedResponseStatus != httpWebResponse.StatusCode)
                        {
                            throw new AutomationException(
                                String.Format(
                                    "The response status is not the expected. Expected: {0}, got {1}",
                                    expectedResponseStatus,
                                    httpWebResponse.StatusCode));
                        }
                    }
                }
                else
                {
                    throw new AutomationException(String.Format("Unexpected exception occured {0}", webException));
                }
            }

            return result;
        }


        public static void SetHeader(this HttpWebRequest request, NameValueCollection keyValues)
        {

            if (keyValues != null)
            {
                foreach (string key in keyValues.AllKeys)
                {
                    if (key == "Accept")
                    {
                        request.Accept = keyValues.Get(key);
                    }
                    else if (request.Headers[key] != null)
                    {
                        request.Headers.Set(key, keyValues.Get(key));
                    }
                    else
                    {
                        request.Headers.Add(key, keyValues.Get(key));
                    }
                }
            }
        }

        /// <summary>
        /// Creates correct Uri from input string.
        /// </summary>
        /// <param name="inputUrl">The input uri in string format.</param>
        /// <returns>The uri in correct format.</returns>
        public static Uri FixUrl(string inputUrl)
        {
            MethodInfo getSyntax = typeof(UriParser).GetMethod("GetSyntax", BindingFlags.Static | BindingFlags.NonPublic);
            FieldInfo flagsField = typeof(UriParser).GetField("m_Flags", BindingFlags.Instance | BindingFlags.NonPublic);

            if (getSyntax != null && flagsField != null)
            {
                foreach (string scheme in new[] { "http", "https" })
                {
                    UriParser parser = (UriParser)getSyntax.Invoke(null, new object[] { scheme });

                    if (parser != null)
                    {
                        int flagsValue = (int)flagsField.GetValue(parser);
                    
                        if ((flagsValue & 0x1000000) != 0)
                        {
                            flagsField.SetValue(parser, flagsValue & ~0x1000000);
                        }
                    }
                }
            }

            return new Uri(inputUrl);
        }
    }
}
