﻿using System.Configuration;
using System.IO;

namespace PowerTester.Framework
{
    /// <summary>
    /// The ParameterProcessor class
    /// </summary>
    public class ParameterProcessor
    {
        #region Fields

        private readonly Configuration _config;
        private readonly string _environmentType;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ParameterProcessor"/> class.
        /// </summary>
        /// <param name="configFilePath">The config fileInfo path.</param>
        public ParameterProcessor(string configFilePath)
        {
            if (!File.Exists(configFilePath))
            {
                throw new FileNotFoundException("Config file not found on the path (" + configFilePath + ")!");
            }
            
            ConfigFilePath = configFilePath;

            ExeConfigurationFileMap exeConcigFileMap = new ExeConfigurationFileMap {ExeConfigFilename = configFilePath};

            _config = ConfigurationManager.OpenMappedExeConfiguration(exeConcigFileMap, ConfigurationUserLevel.None);
            _environmentType = GetParameter("Env");
        }

        #region Properties

        /// <summary>
        /// Gets or sets the config file path.
        /// </summary>
        /// <value>The config file's path.</value>
        public string ConfigFilePath { get; set; }

        #endregion

        #region GetParameters

        /// <summary>
        /// Gets the parameter.
        /// </summary>
        /// <param name="keyName">Name of the key.</param>
        /// <returns>The value of the key</returns>
        public string GetParameter(string keyName)
        {
            return GetParameter(keyName, null);
        }
     
        /// <summary>
        /// Gets the parameter.
        /// </summary>
        /// <typeparam name="T">The generic.</typeparam>
        /// <param name="keyName">Name of the key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The parameter.</returns>
        public string GetParameter(string keyName, string defaultValue)
        {
            AppSettingCollection appSettingsFromCustomSection = GetAppSettings();
            string configValue = null;

            if (appSettingsFromCustomSection != null && appSettingsFromCustomSection.ContainsKey(keyName))
            {
                configValue = appSettingsFromCustomSection[keyName].Value;
            }
            else
            {
                AppSettingsSection applicationSettings = _config.AppSettings;

                try
                {
                    foreach (string element in applicationSettings.Settings.AllKeys)
                    {
                        if (element.ToLower() == keyName.ToLower())
                        {
                            configValue = applicationSettings.Settings[element].Value;
                            break;
                        }
                    }
                }
                catch
                {
                    return defaultValue;
                }
            }

            if (configValue == null)
            {
                return defaultValue;
            }

            return configValue;
        }
        #endregion
        
        /// <summary>
        /// Gets the app settings.
        /// </summary>
        /// <returns>The App settings</returns>
        private AppSettingCollection GetAppSettings()
        {
            if (!string.IsNullOrEmpty(_environmentType))
            {
                ConfigurationSection section = _config.GetSection("environments");
                
                EnvironmentSection environments = (EnvironmentSection) section;

                int index = environments.Environments.IndexOf(_environmentType);
                if (index != -1)
                {
                    return environments.Environments[index].AppSettings;
                }
            }

            return null;
        }
    }
    /// <summary>
    /// Appsettings Collection Class
    /// </summary>
    public class AppSettingCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Configuration Element Collection Type
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        /// <summary>
        /// Gets the name used to identify this collection of elements in the configuration file when overridden in a derived class.
        /// </summary>
        /// <returns>
        /// The name of the collection; otherwise, an empty string. The default is an empty string.
        /// </returns>
        protected override string ElementName
        {
            get { return "add"; }
        }

        /// <summary>
        /// Indexer for AppsettingElement
        /// </summary>
        /// <param name="key">
        /// The Indexer of AppsettingElement
        /// </param>
        public new AppSettingElement this[string key]
        {
            get
            {
                if (IndexOf(key.ToLower()) < 0)
                {
                    return null;
                }

                return (AppSettingElement)BaseGet(key.ToLower());
            }
        }

        /// <summary>
        /// Indexer for Appsetting Element
        /// </summary>
        /// <param name="index">
        /// The index of AppsettingElement
        /// </param>
        public AppSettingElement this[int index]
        {
            get { return (AppSettingElement)BaseGet(index); }
        }

        /// <summary>
        /// Returns the index of the key
        /// </summary>
        /// <param name="key">
        /// The key to search for
        /// </param>
        /// <returns>
        /// The index of th ekey
        /// </returns>
        public int IndexOf(string key)
        {
            key = key.ToLower();

            for (int idx = 0; idx < Count; idx++)
            {
                if (this[idx].Key.ToLower() == key)
                {
                    return idx;
                }
            }

            return -1;
        }

        /// <summary>
        /// Return value indicating wether the key exists
        /// </summary>
        /// <param name="key">
        /// The key to check
        /// </param>
        /// <returns>
        /// Value indiacating if key exists 
        /// </returns>
        public bool ContainsKey(string key)
        {
            foreach (string appSettingKey in BaseGetAllKeys())
            {
                if (key.ToLower() == appSettingKey.ToLower())
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// When overridden in a derived class, creates a new <see cref="T:System.Configuration.ConfigurationElement"/>.
        /// </summary>
        /// <returns>
        /// A new <see cref="T:System.Configuration.ConfigurationElement"/>.
        /// </returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new AppSettingElement();
        }

        /// <summary>
        /// Adds a configuration element to the <see cref="T:System.Configuration.ConfigurationElementCollection"/>.
        /// </summary>
        /// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement"/> to add. </param>
        protected override void BaseAdd(ConfigurationElement element)
        {
            AppSettingElement appSettingElement = (AppSettingElement)element;
            appSettingElement.Key = appSettingElement.Key.ToLower();

            base.BaseAdd(appSettingElement);
        }

        /// <summary>
        /// Gets the element key for a specified configuration element when overridden in a derived class.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Object"/> that acts as the key for the specified <see cref="T:System.Configuration.ConfigurationElement"/>.
        /// </returns>
        /// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement"/> to return the key for. </param>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((AppSettingElement)element).Key;
        }
    }

    /// <summary>
    /// Appsetting element class
    /// </summary>
    public class AppSettingElement : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets the key for AppsettingElement
        /// </summary>
        [ConfigurationProperty("key", DefaultValue = "", IsRequired = true, IsKey = true)]
        public string Key
        {
            get { return (string)this["key"]; }
            set { this["key"] = value; }
        }

        /// <summary>
        /// Gets or sets the Value
        /// </summary>
        [ConfigurationProperty("value", DefaultValue = "", IsRequired = true, IsKey = false)]
        public string Value
        {
            get { return (string)this["value"]; }
            set { this["value"] = value; }
        }
    }

    /// <summary>
    /// The EnvironmentSection class
    /// </summary>
    public class EnvironmentSection : ConfigurationSection
    {
        /// <summary>
        /// Gets the Environment Collection
        /// </summary>
        [ConfigurationProperty("", IsDefaultCollection = true)]
        public EnvironmentCollection Environments
        {
            get { return (EnvironmentCollection)base[string.Empty]; }
        }
    }

    /// <summary>
    /// The EnvironmentCollection class
    /// </summary>
    public class EnvironmentCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Override for CollectionType
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        /// <summary>
        /// Override for ElementName
        /// </summary>
        protected override string ElementName
        {
            get { return "environment"; }
        }

        /// <summary>
        /// Gets the Environment element
        /// </summary>
        /// <returns>
        /// The Environment element
        /// </returns>
        public new EnvironmentElement this[string environment]
        {
            get
            {
                if (IndexOf(environment.ToLower()) < 0)
                {
                    return null;
                }

                return (EnvironmentElement)BaseGet(environment.ToLower());
            }
        }

        /// <summary>
        /// Gets the Environment element
        /// </summary>
        /// <param name="index">
        /// The indexer
        /// </param>
        /// <returns>
        /// The environment element
        /// </returns>
        public EnvironmentElement this[int index]
        {
            get { return (EnvironmentElement)BaseGet(index); }
        }

        /// <summary>
        /// Gets value indicating whether the key is present
        /// </summary>
        /// <param name="key">
        /// The key to serach
        /// </param>
        /// <returns>
        /// Value inidcating whether key exists
        /// </returns>
        public bool ContainsKey(string key)
        {
            foreach (string enviromentKey in BaseGetAllKeys())
            {
                if (key.ToLower() == enviromentKey.ToLower())
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the index of the environment
        /// </summary>
        /// <param name="environment">
        /// The environment
        /// </param>
        /// <returns>
        /// The index of environment
        /// </returns>
        public int IndexOf(string environment)
        {
            int index = -1;
            environment = environment.ToLower();

            for (int idx = 0; idx < Count; idx++)
            {
                if (this[idx].EnvironmentType.ToLower() == environment)
                {
                    index = idx;
                }
            }
            
            return index;
        }

        /// <summary>
        /// Override for CreateNewElement
        /// </summary>
        /// <returns>
        /// The Created element
        /// </returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new EnvironmentElement();
        }

        /// <summary>
        /// Override for GetElementKey
        /// </summary>
        /// <param name="element">
        /// The element
        /// </param>
        /// <returns>
        /// The key of computer
        /// </returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((EnvironmentElement)element).EnvironmentType.ToLower();
        }

        /// <summary>
        /// Everride for BaseAdd()
        /// </summary>
        /// <param name="element"> 
        /// The element
        /// </param>
        protected override void BaseAdd(ConfigurationElement element)
        {
            EnvironmentElement environmentElement = (EnvironmentElement)element;
            environmentElement.EnvironmentType = environmentElement.EnvironmentType.ToLower();

            foreach (object environment in BaseGetAllKeys())
            {
                if (environment.ToString() == environmentElement.EnvironmentType)
                {
                    base.BaseAdd(environmentElement);
                    return;
                }
            }

            base.BaseAdd(environmentElement);
        }
    }
    /// <summary>
    /// The EnvironmentElement class
    /// </summary>
    public class EnvironmentElement : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets the EnvironmentType
        /// </summary>
        [ConfigurationProperty("environmentType", IsRequired = true, IsKey = true)]
        public string EnvironmentType
        {
            get { return ((string)this["environmentType"]).ToLower(); }
            set { this["environmentType"] = value; }
        }

        /// <summary>
        /// Gets the AppSettings
        /// </summary>
        [ConfigurationProperty("appSettings", IsDefaultCollection = false)]
        public AppSettingCollection AppSettings
        {
            get { return (AppSettingCollection)base["appSettings"]; }
        }
    }
}
