﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using PowerTester.Framework.BaseClasses;
using PowerTester.Framework.DBLogger;
using PowerTester.Framework.DBLogger.Interfaces;

namespace PowerTester.Framework
{
    /// <summary>
    /// The TestMethod class
    /// </summary>
    public static class TestExecuter
    {
        private static MultiAssert _testAssert;

        /// <summary>
        /// Delegete for test execution
        /// </summary>
        public delegate void TestDelegate();

        #region Properties

        /// <summary>
        /// Gets the TestAssert instance
        /// </summary>
        public static MultiAssert Assert
        {
            get
            {
                if(_testAssert == null)
                {throw new AutomationException("Executer.Assert may only be used inside parameter of Executer.Execute(TestDelegate testDelegate).");}
                return _testAssert;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether CreateScreenshot.
        /// </summary>
        public static bool CreateScreenshot { get; set; }

        #endregion

        #region Public methods

        /// <summary>
        /// Test execution method
        /// </summary>
        /// <param name="testClass">
        /// The test class 
        /// </param>
        /// <param name="testDelegate">Test method delegate</param>
        public static void Execute(TestBase testClass, TestDelegate testDelegate)
        {
            Execute(testClass, testDelegate, string.Empty, string.Empty, string.Empty);
        }

        /// <summary>
        /// Test execution method
        /// </summary>
        /// <param name="testClass">
        /// The test class 
        /// </param>
        /// <param name="testDelegate">Test method delegate</param>
        /// <param name="username">User name for impersonation.</param>
        /// <param name="userDomain">User domain for impersonation.</param>
        /// <param name="userPass">User password for impersonation.</param>
        public static void Execute(TestBase testClass, TestDelegate testDelegate, string username, string userDomain, string userPass)
        {
            CheckIfTestIsIgnored();
            List<Exception> exceptionList = new List<Exception>();

            bool successful = CheckIfTestIsPerformanceTest()
                ? RunPerformanceTest(testClass, testDelegate, username, userDomain, userPass, ref exceptionList)
                : RunTest(testClass, testDelegate, username, userDomain, userPass, ref exceptionList);

            Logger.Test(@"--------------------------------------------------------");
            if (exceptionList.Count != 0)
            {
                for (int index = 0; index < exceptionList.Count; index++)
                {
                    var exception = exceptionList[index];
                    Logger.Test(string.Format(@"{0} Test run result: {1}", index + 1, exception.Message));
                    Logger.Test(@"--------------------------------------------------------");
                }
            }

            if (successful)
            {
                Logger.Test(string.Format(@"{0} Test run result: PASSED", exceptionList.Count + 1 ));
                Logger.Test(@"--------------------------------------------------------");
            }
            else
            {
                throw new Exception(exceptionList[exceptionList.Count - 1].Message, exceptionList[exceptionList.Count - 1]);
            }
        }

        private static bool RunTest(TestBase testClass, TestDelegate testDelegate, string username, string userDomain, string userPass, ref List<Exception> exceptionList)
        {
            int currentExecution = 0;
            bool successful = false;
            List<int> executionTimes = new List<int>();
            
            while (currentExecution < General.ExecuteTime && successful == false)
            {
                currentExecution++;

                try
                {
                    try
                    {
                        _testAssert = new MultiAssert { CreateScreenshot = CreateScreenshot };
                        if (!string.IsNullOrEmpty(username))
                        {
                            using (new Impersonator(username, userDomain, userPass))
                            {
                                testDelegate.Method.Invoke(testDelegate.Target, null);
                            }
                        }
                        else
                        {
                            testDelegate.Method.Invoke(testDelegate.Target, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        _testAssert.Assert(ex.InnerException);
                    }
                    finally
                    {
                        executionTimes.Add(_testAssert.Dispose());
                        successful = true;
                    }
                }
                catch (Exception ex)
                {
                    exceptionList.Add(ex);
                    testClass.OneTimeSetUp();
                    if (currentExecution < General.ExecuteTime)
                    {
                        testClass.SetUp();
                    }
                }
            }

            return successful;
        }

        private static bool RunPerformanceTest(TestBase testClass, TestDelegate testDelegate, string username, string userDomain, string userPass, ref List<Exception> exceptionList)
        {
            List<int> executionTimes = new List<int>();
            IPerformanceDataProvider PerformanceDataProvider = new FsPerformanceDataProvider();

            while (executionTimes.Count < 100 && exceptionList.Count < 10)
            {
                try
                {
                    try
                    {
                        _testAssert = new MultiAssert { CreateScreenshot = CreateScreenshot };
                        if (!string.IsNullOrEmpty(username))
                        {
                            using (new Impersonator(username, userDomain, userPass))
                            {
                                testDelegate.Method.Invoke(testDelegate.Target, null);
                            }
                        }
                        else
                        {
                            testDelegate.Method.Invoke(testDelegate.Target, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        _testAssert.Assert(ex.InnerException);
                    }
                    finally
                    {
                        executionTimes.Add(_testAssert.Dispose());
                    }
                }
                catch (Exception ex)
                {
                    exceptionList.Add(ex);
                    testClass.OneTimeSetUp();
                    testClass.SetUp();
                }
            }

            if (executionTimes.Count >= 100)
            {
                double currentPerfNumber = executionTimes.Average();
                if (PerformanceDataProvider.WriteAndValidatePerformanceNumber(TestContext.CurrentContext.Test.FullName, currentPerfNumber))
                {
                    return true;
                }

                exceptionList.Add(new AutomationException("Perfromance counter was higher then expected. Current performance counter: " + currentPerfNumber));
                return false;
            }

            return false;
        }

        private static void CheckIfTestIsIgnored()
        {
            if (TestContext.CurrentContext.Test.Properties["IncludeEnvironment"] != null && TestContext.CurrentContext.Test.Properties["IncludeEnvironment"].Any())
            {
                var environmentsList = (IList)TestContext.CurrentContext.Test.Properties["IncludeEnvironment"];
                if (!environmentsList.Contains(General.TestEnvironment))
                {
                    throw new IgnoreException(string.Format("The test is ignored in current environment: {0}.", General.TestEnvironment));
                }
            }

            if (TestContext.CurrentContext.Test.Properties["ExcludeEnvironment"] != null && TestContext.CurrentContext.Test.Properties["ExcludeEnvironment"].Any())
            {
                var environmentsList = (IList)TestContext.CurrentContext.Test.Properties["ExcludeEnvironment"];
                if (environmentsList.Contains(General.TestEnvironment))
                {
                    throw new IgnoreException(string.Format("The test is ignored in current environment: {0}.", General.TestEnvironment));
                }
            }
        }

        private static bool CheckIfTestIsPerformanceTest()
        {
            if (TestContext.CurrentContext.Test.Properties["_CATEGORIES"] != null && TestContext.CurrentContext.Test.Properties["_CATEGORIES"].Any())
            {
                return ((IList) TestContext.CurrentContext.Test.Properties["_CATEGORIES"]).Contains(TestBase.CategoryPerformanceTest);
            }

            return false;
        }

        #endregion
    }
}
