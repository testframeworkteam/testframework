﻿using System.Configuration;
using System.IO;
using System.Reflection;

namespace PowerTester.Framework
{
    public static class TestFrameworkConfigHandler
    {
        private static readonly ConfigHandler configHandler;

        static TestFrameworkConfigHandler()
        {
            configHandler = new ConfigHandler(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase.Substring(8)), "PowerTester.Framework.config"));
        }

        /// <summary>
        /// Gives back a string value of configuration element.
        /// </summary>
        /// <param name="key">
        /// Key to configuration element.
        /// </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The get config parameter. </returns>
        public static string GetConfigParameter(string key, string defaultValue = null)
        {
            return configHandler.GetConfigParameter(key, defaultValue);
        }

        /// <summary>
        /// Sets config values in config file
        /// </summary>
        /// <param name="configFilePath">path of config file</param>
        /// <param name="keyvaluepairs">KeyValueConfigurationCollection containing config keys and values.</param>
        public static void SetConfigValues(string configFilePath, KeyValueConfigurationCollection keyvaluepairs)
        {
            configHandler.SetConfigValues(configFilePath, keyvaluepairs);
        }
    }
}
