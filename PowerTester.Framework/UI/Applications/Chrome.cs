﻿using System;
using System.Windows.Automation;
using System.Windows.Input;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Controls;
using PowerTester.Framework.Web.Elements;
using PowerTester.Framework.Web.Interfaces;
using OpenQA.Selenium;

namespace PowerTester.Framework.UI.Applications
{
    public static class Chrome
    {
        private static Application _application = null;

        public static void SetApplication(Application application)
        {
            _application = application;
        }

        public static void SetApplication(int processId)
        {
            _application = Application.FindApplication(processId);
        }
        
        public class DownloadDialog : IDownloadDialog
        {
            /// <summary>
            /// Represents the Window window control of window
            /// </summary>
            public UIWindow Window
            {
                get
                {
                    UIWindow mainWindow = _application.FindUIWindow();
                    return mainWindow.FindUIControl<UIWindow>(FindBy.ClassName, "#32770", TreeScope.Children);
                }
            }

            /// <summary>
            /// Represents the Window window control of window
            /// </summary>
            public UIButton SaveButton
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "textSaveButton");
                }
            }

            /// <summary>
            /// Represents the Window window control of window
            /// </summary>
            public UIButton CancelButton
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "textCancel");
                }
            }

            /// <summary>
            /// Represents the Window window control of window
            /// </summary>
            public UIEdit FileNameEdit
            {
                get
                {
                    return Window.FindUIControl<UIEdit>(FindBy.Name, "textFileName");
                }
            }

            public override bool Exists
            {
                get { return Window.Exists; }
            }

            public override void Save(string fullPath)
            {
                FileNameEdit.Type(fullPath);
                Open();
            }

            public override void Cancel()
            {
                CancelButton.Click();
            }

            public override void Open()
            {
                SaveButton.Click();
            }
        }

        public class AlertDialog : IAlertDialog
        {
            public override bool Exists
            {
                get 
                {
                    try
                    {
                        IAlert alertDialog = WebAutEngine.Browser.SwitchToAlert();
                        bool result = !string.IsNullOrEmpty(alertDialog.Text);
                        WebAutEngine.Browser.SwitchToMainWindow();
                        return result;
                    }
                    catch (Exception)
                    {
                    }
                    return false;
                }
            }

            public override void Ok()
            {
                IAlert alertDialog = WebAutEngine.Browser.SwitchToAlert();
                alertDialog.Accept();
                WebAutEngine.Browser.SwitchToMainWindow();
            }
        }
        
        public class MessageDialog : IMessageDialog
        {
            public override bool Exists
            {
                get
                {
                    try
                    {
                        IAlert alertDialog = WebAutEngine.Browser.SwitchToAlert();
                        bool result = !string.IsNullOrEmpty(alertDialog.Text);
                        WebAutEngine.Browser.SwitchToMainWindow();
                        return result;
                    }
                    catch (Exception)
                    {
                    }
                    return false;
                }
            }

            public override void Ok()
            {
                IAlert alertDialog = WebAutEngine.Browser.SwitchToAlert();
                alertDialog.Accept();
                WebAutEngine.Browser.SwitchToMainWindow();
            }

            public override void Cancel()
            {
                IAlert alertDialog = WebAutEngine.Browser.SwitchToAlert();
                alertDialog.Dismiss();
                WebAutEngine.Browser.SwitchToMainWindow();
            }

            public override void Close()
            {
                IAlert alertDialog = WebAutEngine.Browser.SwitchToAlert();
                alertDialog.Dismiss();
                WebAutEngine.Browser.SwitchToMainWindow();
            }
        }

        public class OpenDialog : IOpenDialog
        {
            public override bool Exists
            {
                get { return _window.Exists; }
            }

            /// <summary>
            /// Represents the Window window control of window
            /// </summary>
            private UIWindow _window  = _application.FindUIWindow().FindUIControl<UIWindow>(FindBy.ClassName, "#32770", TreeScope.Subtree);

            /// <summary>
            /// Represents the Window window control of window
            /// </summary>
            public UIButton OpenButton
            {
                get
                {
                    return _window.FindUIControl<UIButton>(FindBy.Name, "Open");
                }
            }

            /// <summary>
            /// Represents the Window window control of window
            /// </summary>
            public UIButton CancelButton
            {
                get
                {
                    return _window.FindUIControl<UIButton>(FindBy.Name, "Cancel");
                }
            }

            /// <summary>
            /// Represents the Window window control of window
            /// </summary>
            public UIEdit FileNameTextBox
            {
                get
                {
                    return _window.FindUIControl<UIEdit>(FindBy.Name, "File name:");
                }
            }

            public override void Open(string fullPath)
            {
                FileNameTextBox.Type(fullPath);
                FileNameTextBox.SendKey(Key.Enter);
            }

            public override void Cancel()
            {
                CancelButton.Click();
            }

            public override void Open()
            {
                OpenButton.Click();
            }
        }
    }
}