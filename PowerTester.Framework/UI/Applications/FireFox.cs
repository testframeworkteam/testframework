﻿using System;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Controls;
using PowerTester.Framework.UI.Dialogs;
using PowerTester.Framework.Web.Elements;
using PowerTester.Framework.Web.Interfaces;

namespace PowerTester.Framework.UI.Applications
{
    public static class FireFox
    {
        private static Application _application = null;

        public static void SetApplication(Application application)
        {
            _application = application;
        }

        public static void SetApplication(int processId)
        {
            _application = Application.FindApplication(processId);
        }

        public static class ExtendedDownloadDialog
        {
            /// <summary>
            /// Represents the 'Save or Cancel' dialog window      
            /// </summary>
            public static UIWindow Window
            {
                get
                {
                    return _application.FindUIWindow(FindBy.ClassName, "MozillaDialogClass");
                }
            }

            /// <summary>
            /// Represents the Save File button control of the confirm dialog window
            /// </summary>
            public static UIRadioButton SaveFile
            {
                get
                {
                    return Window.FindUIControl<UIRadioButton>(FindBy.Name, "textConfirmSaveFileSave", TreeScope.Subtree);
                }
            }

            /// <summary>
            /// Represents the Cancel button control of the confirm dialog window
            /// </summary>
            public static UIButton Cancel
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "textConfirmSaveFileCancel");
                }
            }

            public static UIButton OK
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "textOK");
                }
            }
        }


        public static class SimpleDownloadDialog
        {
            /// <summary>
            /// Represents the 'Save or Cancel' dialog window      
            /// </summary>
            public static UIWindow Window
            {
                get
                {
                    return _application.FindUIWindow(FindBy.ClassName, "MozillaDialogClass");
                }
            }

            /// <summary>
            /// Represents the Save File button control of the confirm dialog window
            /// </summary>
            public static UIButton SaveFile
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "textConfirmSaveFileSave",
                        System.Windows.Automation.TreeScope.Children);
                }
            }

            /// <summary>
            /// Represents the Cancel button control of the confirm dialog window
            /// </summary>
            public static UIButton Cancel
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "textConfirmSaveFileCancel",
                        System.Windows.Automation.TreeScope.Children);
                }
            }
        }

        public static class DomainCredentialDialog
        {
            /// <summary>
            /// Represents the 'Save or Cancel' dialog window      
            /// </summary>
            public static UIWindow Window
            {
                get
                {
                    return _application.FindUIWindow(FindBy.ClassName, "MozillaDialogClass");
                }
            }

            /// <summary>
            /// Represents the Save File button control of the confirm dialog window
            /// </summary>
            public static UIButton Ok
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "textOK",
                        System.Windows.Automation.TreeScope.Children);
                }
            }

            /// <summary>
            /// Represents the Cancel button control of the confirm dialog window
            /// </summary>
            public static UIButton Cancel
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "textCancel",
                        System.Windows.Automation.TreeScope.Children);
                }
            }

            /// <summary>
            /// Represents the UserName UIEdit control of the confirm dialog window
            /// </summary>
            public static UIEdit UserName
            {
                get
                {
                    return Window.FindUIControls<UIEdit>()[1];
                }
            }

            /// <summary>
            /// Represents the UserPass UIEdit control of the confirm dialog window
            /// </summary>
            public static UIEdit UserPass
            {
                get
                {
                    return Window.FindUIControls<UIEdit>()[2];
                }
            }
        }

        public static class BrowserWindow
        {
            /// <summary>
            /// Represents the 'Save or Cancel' dialog window      
            /// </summary>
            public static UIWindow Window
            {
                get
                {
                    UIWindow window = _application.FindUIWindow(FindBy.ClassName, "MozillaWindowClass");
                    return window;
                }
            }

            public static UIEdit UrlText
            {
                get
                {
                    UIEdit url = Window.FindUIControl<UIEdit>(FindBy.Name, "Go to a Website", TreeScope.Descendants);
                    return url;
                }
            }
        }

        public class DownloadDialog : IDownloadDialog
        {
            public override bool Exists
            {
                get { throw new System.NotImplementedException(); }
            }

            public override void Save(string fullPath)
            {
            }

            public override void Cancel()
            {
                throw new System.NotImplementedException();
            }

            public override void Open()
            {
                throw new System.NotImplementedException();
            }
        }

        public class OpenDialog : IOpenDialog
        {
            public override bool Exists
            {
                get { throw new NotImplementedException(); }
            }

            public override void Open(string fullPath)
            {
                throw new NotImplementedException();
            }

            public override void Cancel()
            {
                throw new NotImplementedException();
            }

            public override void Open()
            {
                throw new NotImplementedException();
            }
        }

    }
}