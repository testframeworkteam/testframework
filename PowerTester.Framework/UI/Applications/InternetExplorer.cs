﻿using System.Windows.Automation;
using System.Windows.Input;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Controls;
using PowerTester.Framework.UI.Dialogs;
using PowerTester.Framework.Web.Interfaces;

namespace PowerTester.Framework.UI.Applications
{
    public static class InternetExplorer
    {
        private static Application _application = null;

        public static void SetApplication(Application application)
        {
            _application = application;
        }

        public static void SetApplication(int processId)
        {
            _application = Application.FindApplication(processId);
        }

        public static UIWindow MainWindow
        {
            get
            {
                return _application.FindUIWindow();
            }
        }

        #region Tabs

        private static UIPane tabPane
        {
            get
            {
                return MainWindow.FindUIControl<UIPane>(FindBy.ClassName, "DirectUIHWND");
            }
        }

        public static UIControlCollection<UIControl> Tabs
        {
            // these tabs don't support the IsSelectionItemPattern, so they can't be found as UITabItems
            get
            {
                UIControlCollection<UIControl> descendants =
                    tabPane.FindUIControls<UIControl>(System.Windows.Automation.TreeScope.Descendants);
                UIControlCollection<UIControl> tabItems = new UIControlCollection<UIControl>();

                foreach (UIControl control in descendants)
                {
                    if (control.ControlType == System.Windows.Automation.ControlType.TabItem)
                    {
                        tabItems.Add(control);
                    }
                }

                return tabItems;
            }
        }

        public static UIButton OpenNewTab
        {
            get
            {
                return tabPane.FindUIControl<UIButton>(FindBy.Name, "textNewTab");
            }
        }

        #endregion

        public static class BrowserWindow
        {
            /// <summary>
            /// Represents the 'Save or Cancel' dialog window      
            /// </summary>
            public static UIWindow Window
            {
                get
                {
                    UIWindow window = _application.FindUIWindow();
                    return window;
                }
            }

            public static UIEdit UrlText
            {
                get
                {
                    UIPane rebar = Window.FindUIControl<UIPane>(FindBy.ClassName, "ReBarWindow32");
                    UIEdit url = Window.FindUIControls<UIEdit>(TreeScope.Descendants)[0];
                    return url;
                }
            }
        }

        public static class ScriptingErrorDialog
        {
            /// <summary>
            /// Represents the main window of the dialog
            /// </summary>
            public static UIWindow Window
            {
                get
                {
                    return MainWindow.FindUIControl<UIWindow>(FindBy.ClassName, "Internet Explorer_TridentDlgFrame", TreeScope.Children);
                }
            }

            public static UIPane Pane
            {
                get
                {
                    return Window.FindUIControl<UIPane>(System.Windows.Automation.TreeScope.Children);
                }
            }

            private static UIControlCollection<UIButton> Buttons
            {
                get
                {
                    return Pane.FindUIControls<UIButton>(System.Windows.Automation.TreeScope.Children);
                }
            }

            public static UIButton CopyErrorDetails
            {
                get
                {
                    return Buttons[0];
                }
            }

            public static UIButton Close
            {
                get
                {
                    return Buttons[Buttons.Count - 1];
                }
            }
        }

        public static class IE9_DownloadDialog
        {
            public static UIWindow Window
            {
                get
                {
                    return UIDesktop.Instance.FindUIControl<UIWindow>(FindBy.Name, "Windows Internet Explorer",
                        TreeScope.Subtree);
                }
            }

            public static UIButton Cancel
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "Cancel");
                }
            }

            public static UIButton Open
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "Open");
                }
            }

            public static UIButton SaveAS
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "Save as");
                }
            }
        }

        public static class IE9_EmbeddedDownloadDialog
        {
            public static bool Exists
            {
                get { return NotificationBar.Exists; }
            }

            public static UIPane NotificationBar
            {
                get
                {
                    return MainWindow.FindUIControl<UIPane>(FindBy.ClassName, "Frame Notification Bar", TreeScope.Children);
                }
            }

            public static UIButton Cancel
            {
                get
                {
                    return NotificationBar.FindUIControl<UIButton>(FindBy.Name, "Cancel");
                }
            }

            public static UIButton Open
            {
                get
                {
                    return NotificationBar.FindUIControl<UIButton>(FindBy.Name, "Open");
                }
            }

            public static UIControl SaveMenu
            {
                get
                {
                    if (Open.Exists)
                    {
                        return NotificationBar.FindUIControls<UIControl>(TreeScope.Subtree)[5];
                    }

                    return NotificationBar.FindUIControls<UIControl>(TreeScope.Subtree)[4];
                }
            }
        }

        public static class IE9_EmbeddedDownloadDialogSaveMenu
        {
            public static UIMenuItem SaveAs
            {
                get
                {
                    IE9_EmbeddedDownloadDialog.SaveMenu.WaitFor();
                    IE9_EmbeddedDownloadDialog.SaveMenu.Click(() => UIDesktop.Instance.FindUIControl<UIMenu>().Exists);
                    return UIDesktop.Instance.FindUIControl<UIMenu>().GetMenuItemByName("Save as");
                }
            }
        }

        public static class IE9_EmbeddedDownloadCompleteDialog
        {
            public static UIWindow Window
            {
                get
                {
                    foreach (UIWindow window in UIDesktop.Instance.FindUIControls<UIWindow>(TreeScope.Children))
                    {
                        if (window.Name.Contains("Internet Explorer") &&
                            window.FindUIControl<UIStaticText>(FindBy.Name, "Notification bar Text")
                                .Value.Contains("download has completed"))
                        {
                            return window;
                        }

                    }
                    return null;
                }
            }

            public static UIButton Close
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "Close");
                }
            }
        }

        public static class IE9_TrustManagerDownloadingDialog
        {
            public static UIWindow Window
            {
                get
                {
                    foreach (UIWindow window in UIDesktop.Instance.FindUIControls<UIWindow>(TreeScope.Children))
                    {
                        if (window.Name.Contains("Downloading"))
                        {
                            return window;
                        }

                    }
                    return null;
                }
            }

            public static UIButton Cancel
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "Cancel");
                }
            }
        }

        public static class IE9_TrustManagerDownloadDialog
        {
            public static UIWindow Window
            {
                get
                {
                    foreach (UIWindow window in UIDesktop.Instance.FindUIControls<UIWindow>(TreeScope.Children))
                    {
                        if (window.Name.Contains("TrustManagerDialog"))
                        {
                            return window;
                        }
                    }

                    return null;
                }
            }

            public static UIButton Run
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "InstallOrRunButton");
                }
            }

            public static UIButton DontRun
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "DoNotInstallOrRunButton");
                }
            }
        }

        public class MessageDialog : IMessageDialog
        {
            public static UIWindow Window
            {
                get
                {
                    return UIDesktop.Instance.FindUIControl<UIWindow>(FindBy.Name, "Message from webpage", TreeScope.Descendants);
                }
            }

            public override bool Exists
            {
                get { return Window.Exists; }
            }

            public static UIButton OkButton
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "OK");
                }
            }

            public static UIButton CancelButton
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "Cancel");
                }
            }

            public static UIButton CloseButton
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "Close");
                }
            }

            public override void Ok()
            {
                OkButton.Click();
            }

            public override void Cancel()
            {
                CancelButton.Click();
            }

            public override void Close()
            {
                CloseButton.Click();
            }
        }

        public class AlertDialog : IAlertDialog
        {
            public static UIWindow Window
            {
                get
                {
                    return UIDesktop.Instance.FindUIControl<UIWindow>(FindBy.Name, "Message from webpage", TreeScope.Descendants);
                }
            }

            public override bool Exists
            {
                get { return Window.Exists; }
            }


            public static UIButton OkButton
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "OK");
                }
            }

            public override void Ok()
            {
                OkButton.Click();
            }
        }

        public class DownloadDialog : IDownloadDialog
        {
            public override bool Exists
            {
                get { return IE9_EmbeddedDownloadDialog.Exists; }
            }

            public override void Save(string fullPath)
            {
                Logger.Debug("Save method entered.");

                if (IE9_EmbeddedDownloadDialog.Exists)
                {
                    IE9_EmbeddedDownloadDialogSaveMenu.SaveAs.Click();

                    StandardWindowsDialogs.SaveAs.SaveFileAs(fullPath);

                    if (UIWaitFor.WaitFor(typeof (IE9_EmbeddedDownloadCompleteDialog), "Window", 20))
                    {
                        IE9_EmbeddedDownloadCompleteDialog.Close.Click();
                    }

                }
                else
                {
                    throw new AutomationException("Cannot find InternetExplorer9's download dialog");
                }
            }

            public override void Cancel()
            {
                throw new System.NotImplementedException();
            }

            public override void Open()
            {
                throw new System.NotImplementedException();
            }
        }

        public class OpenDialog : IOpenDialog
        {
            public override bool Exists
            {
                get { return _window.Exists; }
            }

            /// <summary>
            /// Represents the Window window control of window
            /// </summary>
            private UIWindow _window = _application.FindUIWindow().FindUIControl<UIWindow>(FindBy.ClassName, "#32770", TreeScope.Children);

            /// <summary>
            /// Represents the Window window control of window
            /// </summary>
            public UIButton OpenButton
            {
                get
                {
                    return _window.FindUIControl<UIButton>(FindBy.Name, "Open");
                }
            }

            /// <summary>
            /// Represents the Window window control of window
            /// </summary>
            public UIButton CancelButton
            {
                get
                {
                    return _window.FindUIControl<UIButton>(FindBy.Name, "Cancel");
                }
            }

            /// <summary>
            /// Represents the Window window control of window
            /// </summary>
            public UIEdit FileNameTextBox
            {
                get
                {
                    return _window.FindUIControl<UIEdit>(FindBy.Name, "File name:");
                }
            }

            public override void Open(string fullPath)
            {
                FileNameTextBox.Type(fullPath);
                FileNameTextBox.SendKey(Key.Enter);
            }

            public override void Cancel()
            {
                CancelButton.Click();
            }

            public override void Open()
            {
                OpenButton.Click();
            }
        }
    }
}