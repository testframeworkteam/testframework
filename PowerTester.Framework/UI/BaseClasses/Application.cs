using System;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Automation;
using System.Windows.Forms;
using PowerTester.Framework.UI.Controls;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.BaseClasses
{
    /// <summary>
    /// Represents a process as application.
    /// </summary>
    public class Application : UIControl, IApplicationDELETE
    {
        /// <summary>
        /// Initializes static members of the <see cref="Application"/> class. 
        /// </summary>
        static Application()
        {
        }

        #region ctor

        /// <summary>        
        /// Initializes a new instance of the Application class.
        /// </summary>
        /// <param name="handle">Handler of process</param>
        public Application(IntPtr handle) : base(UIFind.FindApplicationByHandle(handle.ToInt32()))
        {                        
        }

        /// <summary>
        /// Initializes a new instance of the Application class.
        /// </summary>
        /// <param name="title">Title of main window of process</param>
        public Application(string title) : base(UIFind.FindApplicationByTitle(title))
        {            
        }

        /// <summary>
        /// Initializes a new instance of the Application class.
        /// </summary>
        /// <param name="processID">Process ID of process </param>
        public Application(Int32 processID) : base(UIFind.FindApplicationByProcessId(processID))
        {            
        }

        /// <summary>
        /// Initializes a new instance of the Application class.
        /// </summary>
        /// <param name="rootElement">AutomationElement in the control tree.</param>
        public Application(AutomationElement rootElement) : base(rootElement)
        {
        }

        #endregion

        /// <summary>
        /// Gets the root automation element of Application.
        /// </summary>
        public AutomationElement Root
        {
            get { return Element; }
        }

        /// <summary>
        /// Returns an Application that satisfies the specified condition.
        /// </summary>
        /// <param name="handle">Handler of process</param>
        /// <returns>Application object</returns>
        public static Application FindApplication(IntPtr handle)
        {
            Logger.Debug("Application.FindApplication - Begin");

            Application application = new Application(UIFind.FindApplicationByHandle(handle.ToInt32()));

            Logger.Debug("Application.FindApplication - End");

            return application;
        }

        /// <summary>
        /// Returns an Application that satisfies the specified conditions.
        /// </summary>
        /// <param name="handle">
        /// The handle.
        /// </param>
        /// <param name="mainWindowControlType">
        /// Type of the main window control.
        /// </param>
        /// <returns>
        /// Application object
        /// </returns>
        /// <remarks>
        /// This method can be useful when more than one windows of the given process can be found under the root (desktop)
        /// </remarks>
        public static Application FindApplication(IntPtr handle, ControlType mainWindowControlType)
        {
            Logger.Debug("Application.FindApplication - Begin");

            Application application = new Application(UIFind.FindApplicationByHandle(handle.ToInt32(), mainWindowControlType));

            Logger.Debug("Application.FindApplication - End");

            return application;
        }

        /// <summary>
        /// Returns an Application that satisfies the specified condition.
        /// </summary>
        /// <param name="title">Title of main window of process</param>
        /// <returns>Application object</returns>
        public static Application FindApplication(string title)
        {
            Logger.Debug("Application.FindApplication - Begin");

            Application application = new Application(UIFind.FindApplicationByTitle(title));

            Logger.Debug("Application.FindApplication - End");

            return application; 
        }

        /// <summary>
        /// Returns an Application that satisfies the specified condition.
        /// </summary>
        /// <param name="processId">Process id of process</param>
        /// <returns>Application object</returns>
        public static Application FindApplication(int processId)
        {
            Logger.Debug("Application.FindApplication - Begin");

            Application application = new Application(UIFind.FindApplicationByProcessId(processId));

            Logger.Debug("Application.FindApplication - End");
            return application;
        }

        /// <summary>
        /// Returns an Application that satisfies the specified conditions.
        /// </summary>
        /// <param name="processId">The process id.</param>
        /// <param name="mainWindowControlType">Type of the main window control.</param>
        /// <returns>Application object</returns>
        /// <remarks>This method can be useful when more than one windows of the given process can be found under the root (desktop)</remarks>
        public static Application FindApplication(int processId, ControlType mainWindowControlType)
        {
            Logger.Debug("Application.FindApplication - Begin");

            Application application = new Application(UIFind.FindApplicationByProcessId(processId, mainWindowControlType));

            Logger.Debug("Application.FindApplication - End");

            return application;
        }

        /// <summary>
        /// Returns an Window that satisfies the specified condition.
        /// </summary>
        /// <param name="findBy">Type of finding mode</param>
        /// <param name="value">Value of finding mode</param>
        /// <returns>Application object</returns>
        public UIWindow FindUIWindow(FindBy findBy, string value)
        {
            Logger.Debug("Value: " + value + " FindBy: " + findBy + " ControlType: UIWindow TreeScope: Children, Application.FindUIWindow - Begin");

            UIWindow uiWindow = UIFind.FindUIWindow(findBy, Root, value);

            Logger.Debug("Value: " + value + " FindBy: " + findBy + " ControlType: UIWindow TreeScope: Children, Application.FindUIWindow - End");

            return uiWindow;
        }

        /// <summary>
        /// Returns the first Window of the Application
        /// </summary>
        /// <returns>The window</returns>
        public UIWindow FindUIWindow()
        {
            Logger.Debug("FindBy: Type ControlType: UIWindow TreeScope: Children, Application.FindUIWindow - Begin");

            UIWindow uiWindow = new UIWindow(UIFind.FindUIControlByType(Root, ControlType.Window).Element);

            Logger.Debug("FindBy: Type ControlType: UIWindow TreeScope: Children, Application.FindUIWindow - End");

            return uiWindow;
        }

        /// <summary>
        /// Finds the first UI Pane of the application.
        /// </summary>
        /// <returns>
        ///  The pane.
        /// </returns>
        public UIPane FindUIPane()
        {
            Logger.Debug("FindBy: Type ControlType: UIWindow TreeScope: Children, Application.FindUIWindow - Begin");

            UIPane uiPane = new UIPane(UIFind.FindUIControlByType(Root, ControlType.Pane).Element);

            Logger.Debug("FindBy: Type ControlType: UIWindow TreeScope: Children, Application.FindUIWindow - End");

            return uiPane;
        }

        /// <summary>
        /// Returns all UIWindow objects are connected to the Application.
        /// </summary>
        /// <returns>Collection of UIWindow objects</returns>
        public UIControlCollection<UIWindow> GetWindows()
        {
            throw new NotImplementedException();
        }
        
        public static void SetWindowStyle(IntPtr intPtr, FormWindowState windowStyle)
        {
            Win32.WINDOWPLACEMENT windowPlacement = Win32.WINDOWPLACEMENT.Default;            
            switch (windowStyle)
            {
                case FormWindowState.Minimized:
                    windowPlacement.showCmd = 2;
                    break;
                case FormWindowState.Normal:
                    windowPlacement.showCmd = 1;
                    windowPlacement.rcNormalPosition.Width = 640;
                    windowPlacement.rcNormalPosition.Height = 480;
                    windowPlacement.rcNormalPosition.X = 15;
                    windowPlacement.rcNormalPosition.Y = 15;
                    break;
                case FormWindowState.Maximized:
                    windowPlacement.showCmd = 3;
                    break;
            }
            Win32.SetWindowPlacement(intPtr, ref windowPlacement);
            //SetForegroundWindow(intPtr);
        }

        public static FormWindowState GetWindowStyle(IntPtr intPtr)
        {
            Win32.WINDOWPLACEMENT windowPlacement = Win32.WINDOWPLACEMENT.Default;
            Win32.GetWindowPlacement(intPtr, ref windowPlacement);

            switch (windowPlacement.showCmd)
            {
                case 2:
                    return FormWindowState.Minimized;
                    break;
                case 1:
                    return FormWindowState.Normal;
                    break;
                case 3:
                    return FormWindowState.Maximized;
                    break;
            }
            throw new Exception("Cannot determine window placement");

        }

        //const UInt32 SW_HIDE = 0;
        //const UInt32 SW_SHOWNORMAL = 1;
        //const UInt32 SW_NORMAL = 1;
        //const UInt32 SW_SHOWMINIMIZED = 2;
        //const UInt32 SW_SHOWMAXIMIZED = 3;
        //const UInt32 SW_MAXIMIZE = 3;
        //const UInt32 SW_SHOWNOACTIVATE = 4;
        //const UInt32 SW_SHOW = 5;
        //const UInt32 SW_MINIMIZE = 6;
        //const UInt32 SW_SHOWMINNOACTIVE = 7;
        //const UInt32 SW_SHOWNA = 8;
        //const UInt32 SW_RESTORE = 9;

    }
        
    
}
