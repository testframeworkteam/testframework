using System;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.BaseClasses
{
    /// <summary>
    /// This is the base class of automation execptions.
    /// </summary>
    [Serializable]
    public abstract class UIAutomationException : Exception
    {
        private IUIControl _control;

        /// <summary>
        /// Gets the ui control.
        /// </summary>
        /// <value>The control.</value>
        public IUIControl Control
        {
            get { return _control; }            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIAutomationException"/> class.
        /// </summary>
        /// <param name="ctrl">The CTRL.</param>
        /// <param name="message">The message.</param>
        public UIAutomationException(IUIControl ctrl, string message): base(message)
        {
            _control = ctrl;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIAutomationException"/> class.
        /// </summary>
        /// <param name="ctrl">The CTRL.</param>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public UIAutomationException(IUIControl ctrl, string message, Exception innerException)
            : base(message, innerException)
        {
            _control = ctrl;
        }
    }

    /// <summary>
    /// Represents an UIControl not found exception
    /// </summary>
    [Serializable]
    public class UIControlNotFoundException : UIAutomationException
    {
        public UIControlNotFoundException(IUIControl ctrl, string message)
            : base(ctrl, message)
        {
        }
    }

    /// <summary>
    /// Represents an UIAutomation Element not found exception
    /// </summary>
    [Serializable]
    public class UIAutomationElementNotFoundException : UIAutomationException
    {
        public UIAutomationElementNotFoundException(IUIControl ctrl, string message)
            : base(ctrl, message)
        {
        }
    }

    /// <summary>
    /// Represents an UIWindow not found exception
    /// </summary>
    [Serializable]
    public class UIWindowNotFoundException : UIAutomationException
    {
        public UIWindowNotFoundException(IUIControl ctrl, string message)
            : base(ctrl, message)
        {
        }
    }

    /// <summary>
    /// Represents an Application not found exception
    /// </summary>
    [Serializable]
    public class ApplicationNotFoundException : UIAutomationException
    {
        public ApplicationNotFoundException(IUIControl ctrl, string message)
            : base(ctrl, message)
        {
        }
    }

    /// <summary>
    /// Represents a Pattern not found exception
    /// </summary>
    [Serializable]
    public class PatternNotSupportedException : UIAutomationException
    {
        public PatternNotSupportedException(IUIControl ctrl, string message)
            : base(ctrl, message)
        {
        }
    }

    /// <summary>
    /// Represents a Special class reguired exception
    /// </summary>
    [Serializable]
    public class SpecialClassRequiredException : UIAutomationException
    {
        public SpecialClassRequiredException(IUIControl ctrl, string message)
            : base(ctrl, message)
        {
        }
    }

    /// <summary>
    /// Represents a Property value has not changed exception
    /// </summary>
    [Serializable]
    public class PropertyValueHasNotChangedException : UIAutomationException
    {
        public PropertyValueHasNotChangedException(IUIControl ctrl, string message)
            : base(ctrl, message)
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class UIClientSideProviderException : UIAutomationException
    {
        public UIClientSideProviderException(string message, Exception innerException)
            : base(null, message, innerException)
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class PatternIsNotAvailableException : UIAutomationException
    {
        public PatternIsNotAvailableException(string message, Exception innerException)
            : base(null, message, innerException)
        {
        }

        public PatternIsNotAvailableException(UIControl control, string patterName)
            : base(control, patterName)
        {            
        }
    }

}
