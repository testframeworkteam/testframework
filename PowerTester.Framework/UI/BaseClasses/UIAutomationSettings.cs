namespace PowerTester.Framework.UI.BaseClasses
{
    /// <summary>
    /// 
    /// </summary>
    public static class UIAutomationSettings
    {
        private static int _waitForApplication = int.Parse(TestFrameworkConfigHandler.GetConfigParameter("WaitForApplication", "0"));
        private static int _threadSleepTime = int.Parse(TestFrameworkConfigHandler.GetConfigParameter("WaitForThreadSleepTime", "200"));
        private static int _waitForDialog = int.Parse(TestFrameworkConfigHandler.GetConfigParameter("WaitForDialog", "0"));
        private static int _waitForUIControl = int.Parse(TestFrameworkConfigHandler.GetConfigParameter("WaitForUIControl", "0"));
        private static int _iterationWhenWatingForControl = int.Parse(TestFrameworkConfigHandler.GetConfigParameter("IterationWhenWatingForControl", "1"));
        private static int _waitForTimeout = int.Parse(TestFrameworkConfigHandler.GetConfigParameter("DefaultWaitForTimeout", "5"));

        public static int WaitForApplication
        {
            get { return _waitForApplication; }
            set { _waitForApplication = value; }
        }

        public static int ThreadSleepTime
        {
            get { return _threadSleepTime; }
            set { _threadSleepTime = value; }
        }

        public static int WaitForDialog
        {
            get { return _waitForDialog; }
            set { _waitForDialog = value; }
        }

        public static int WaitForUIControl
        {
            get { return _waitForUIControl; }
            set { _waitForUIControl = value; }
        }

        public static int IterationWhenWatingForControl
        {
            get { return _iterationWhenWatingForControl; }
            set { _iterationWhenWatingForControl = value; }
        }

        public static int WaitForTimeout
        {
            get { return _waitForTimeout; }
            set { _waitForTimeout = value; }
        }
    }
}
