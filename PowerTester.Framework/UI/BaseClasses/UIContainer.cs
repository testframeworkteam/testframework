using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Automation;
using PowerTester.Framework.UI.Controls;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.BaseClasses
{
    /// <summary>
    /// Represents a UIContainer class.
    /// </summary>
    public abstract class UIContainer : UIControl, IUIContainer
    {
        private AutomationElement root;
        
        /// <summary>
        /// Gets a value which indicates the root AutomationElement item.
        /// </summary>
        public AutomationElement Root
        {
            get { return root; }
            set { root = value; }
        }

        /// <summary>
        /// Initializes a new instace of the UIContainer class
        /// </summary>
        /// <param name="element">AutomationElement</param>
        public UIContainer(AutomationElement rootElement):base(rootElement)
        {
            root = rootElement;
        }

        #region Find Methods

        /// <summary>
        /// Tries to find the given type of UIControl
        /// </summary>
        /// <typeparam name="T">UIControl class e.g.: UIButton</typeparam>
        /// <param name="findBy">The find by.</param>
        /// <param name="value">The value.</param>
        /// <param name="scope">The scope.</param>
        /// <returns></returns>
        public T FindUIControl<T>(FindBy findBy, string value, TreeScope scope) where T : UIControl
        {
            UIControl uiControl = null;
            ControlType controlType = ControlType.Window;

            if (typeof(T).Equals(typeof(UIButton))) { controlType = ControlType.Button; }
            if (typeof(T).Equals(typeof(UIEdit))) { controlType = ControlType.Edit; }
            if (typeof(T).Equals(typeof(UICheckBox))) { controlType = ControlType.CheckBox; }
            if (typeof(T).Equals(typeof(UIListBox))) { controlType = ControlType.List; }
            if (typeof(T).Equals(typeof(UIComboBox))) { controlType = ControlType.ComboBox; }
            if (typeof(T).Equals(typeof(UIRadioButton))) { controlType = ControlType.RadioButton; }
            if (typeof(T).Equals(typeof(UISpinner))) { controlType = ControlType.Spinner; }
            if (typeof(T).Equals(typeof(UIStatusBar))) { controlType = ControlType.StatusBar; }
            if (typeof(T).Equals(typeof(UIScrollBar))) { controlType = ControlType.ScrollBar; }
            if (typeof(T).Equals(typeof(UITab))) { controlType = ControlType.Tab; }
            if (typeof(T).Equals(typeof(UITreeView))) { controlType = ControlType.Tree; }
            if (typeof(T).Equals(typeof(UIThumb))) { controlType = ControlType.Thumb; }
            if (typeof(T).Equals(typeof(UIMenu))) { controlType = ControlType.Menu; }
            if (typeof(T).Equals(typeof(UIMenuItem))) { controlType = ControlType.MenuItem; }
            if (typeof(T).Equals(typeof(UIGroup))) { controlType = ControlType.Group; }
            if (typeof(T).Equals(typeof(UIPane))) { controlType = ControlType.Pane; }
            if (typeof(T).Equals(typeof(UIDataGrid))) { controlType = ControlType.DataGrid; }
            if (typeof(T).Equals(typeof(UIStaticText))) { controlType = ControlType.Text; }
            if (typeof(T).Equals(typeof(UIDocument))) { controlType = ControlType.Document; }
            if (typeof(T).Equals(typeof(UIToolBar))) { controlType = ControlType.ToolBar; }
            if (typeof(T).Equals(typeof(UIProgressBar))) { controlType = ControlType.ProgressBar; }
            if (typeof(T).Equals(typeof(UIWindow))) { controlType = ControlType.Window; }

            if (typeof(T).Equals(typeof(UIControl)))
            {
                uiControl = UIFind.FindUIControl(findBy, Root, value);
            }
            else
            {
                switch (findBy)
                {
                    case FindBy.Name:
                        uiControl = FindUIControl(FindBy.Name, controlType, typeof(T), value, scope);                        
                        break;
                    case FindBy.ClassName:
                        uiControl = FindUIControl(FindBy.ClassName, controlType, typeof(T), value, scope);
                        break;
                    case FindBy.AutomationId:
                        uiControl = FindUIControl(FindBy.AutomationId, controlType, typeof(T), value, scope);
                        break;
                    case FindBy.Type:
                        uiControl = FindUIControl(FindBy.Type, controlType, typeof(T), string.Empty, scope);
                        break;
                }
            }
            
            return Activator.CreateInstance(typeof(T), uiControl.Element) as T;
        }

        /// <summary>
        /// Tries to find the given type of UIControl. The used Treescope is always Subtree.
        /// </summary>
        /// <typeparam name="T">UIControl class e.g.: UIButton</typeparam>
        /// <param name="findBy">The find by.</param>
        /// <param name="value">The value.</param>
        /// <param name="scope">The scope.</param>
        /// <returns></returns>
        public virtual T FindUIControl<T>(FindBy findBy, string value) where T : UIControl
        {
            return FindUIControl<T>(findBy, value, TreeScope.Subtree);
        }

        /// <summary>
        /// Tries to find all the control which have the given type. The used Treescope is always Subtree.
        /// </summary>
        /// <typeparam name="T">UIControl class e.g.: UIButton </typeparam>
        /// <returns></returns>
        public virtual T FindUIControl<T>()where T : UIControl
        {
            return FindUIControl<T>(FindBy.Type, string.Empty, TreeScope.Subtree);
        }

        /// <summary>
        /// Tries to find all the control which have the given type
        /// </summary>
        /// <typeparam name="T">UIControl class e.g.: UIButton</typeparam>
        /// <param name="scope">The scope of search</param>
        /// <returns></returns>
        public T FindUIControl<T>(TreeScope scope)where T: UIControl
        {
            return FindUIControl<T>(FindBy.Type, string.Empty, scope);
        }

        /// <summary>
        /// Tries to find all the given type controls of the container. When the TreeScope is not specified the method uses the TreeScope.Subtree value.
        /// </summary>
        /// <typeparam name="T">UIControl class e.g.: UIButton</typeparam>
        /// <returns></returns>
        public UIControlCollection<T> FindUIControls<T>() where T : UIControl
        {
            return FindUIControls<T>(TreeScope.Subtree);
        }

        /// <summary>
        /// Tries to find all the control which have the given type
        /// </summary>
        /// <typeparam name="T">UIControl class e.g.: UIButton</typeparam>
        /// <param name="scope">The find scope of tree</param>
        /// <returns></returns>
        public UIControlCollection<T> FindUIControls<T>(TreeScope scope) where T : UIControl
        {
            Condition[] conds = null;
            AutomationElementCollection automationElementCollection = null;

            #region Set Conditions

            if (typeof(T).Equals(typeof(UIButton)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Button), new PropertyCondition(AutomationElement.IsInvokePatternAvailableProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIEdit)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Edit), new PropertyCondition(AutomationElement.IsValuePatternAvailableProperty, true) };
            }

            if (typeof(T).Equals(typeof(UICheckBox)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.CheckBox), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIListBox)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.List), new PropertyCondition(AutomationElement.IsSelectionPatternAvailableProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIComboBox)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.ComboBox), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIRadioButton)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.RadioButton), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UISpinner)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Spinner), new PropertyCondition(AutomationElement.IsControlElementProperty, true)};
            }

            if (typeof(T).Equals(typeof(UIStatusBar)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.StatusBar), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIScrollBar)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.ScrollBar), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UITab)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Tab), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UITreeView)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Tree), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIThumb)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Thumb), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIMenu)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Menu), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIMenuItem)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.MenuItem), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIGroup)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Group), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIPane)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Pane), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIDataGrid)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.DataGrid), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIStaticText)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Text), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIDocument)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Document), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIToolBar)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.ToolBar), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIProgressBar)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.ProgressBar), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }

            if (typeof(T).Equals(typeof(UIWindow)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Window), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };
            }                        

            #endregion

            if (typeof(T).Equals(typeof(UIControl)))
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.IsContentElementProperty, true), new PropertyCondition(AutomationElement.IsControlElementProperty, true) };

                Logger.Debug("ControlType: " + typeof(T).Name + " TreeScope: " + scope + " UIContainer.FindUIControls - Begin");

                automationElementCollection = UIFind.FindAllElementsByOrConditions(Root, conds);

                Logger.Debug("ControlType: " + typeof(T).Name + " TreeScope: " + scope + " UIContainer.FindUIControls - End");
            }
            else
            {
                Logger.Debug("ControlType: " + typeof(T).Name + " TreeScope: " + scope + " UIContainer.FindUIControls - Begin");

                automationElementCollection = UIFind.FindAllElementsByAndConditions(Root, conds, scope);

                Logger.Debug("ControlType: " + typeof(T).Name + " TreeScope: " + scope + " UIContainer.FindUIControls - End");
            }


            UIControlCollection<T> uiControlCollection = new UIControlCollection<T>();

            Logger.Debug(string.Format("{0} controls were looked for...", typeof(T).Name));

            foreach (AutomationElement automationElement in automationElementCollection)
            {
                uiControlCollection.Add(Activator.CreateInstance(typeof(T), automationElement) as T);
                Logger.Debug(string.Format("  '{0}' control was found!", automationElement.Current.Name));

            }

            

            return uiControlCollection;
        }
        
        #endregion

        /// <summary>
        /// Tries to find the control based on the given parameters. If it does not find the control for the first
        /// attempt it waits a certain amount of seconds and tries to find the control again.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="findBy">The find by.</param>
        /// <param name="controlType">Type of the control.</param>
        /// <param name="uiType">Type of the UI class.</param>
        /// <param name="value">The value.</param>
        /// <param name="scope">The scope.</param>
        /// <returns></returns>
        protected virtual UIControl FindUIControl(FindBy findBy, ControlType controlType, Type uiType, string value, TreeScope scope)
        {
            UIControl uiControl = null;
            Condition[] conds = null;
            
            if (findBy == FindBy.Name)
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.NameProperty, value), new PropertyCondition(AutomationElement.ControlTypeProperty, controlType) };

                Logger.Debug(string.Format("The '{1}' {0} control was looked for by its name.", uiType.Name, value));
            }

            if (findBy == FindBy.AutomationId)
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.AutomationIdProperty, value), new PropertyCondition(AutomationElement.ControlTypeProperty, controlType) };

                Logger.Debug(string.Format("The '{1}' {0} control was looked for by its automation Id.", uiType.Name, value));
            }

            if (findBy == FindBy.ClassName)
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.ClassNameProperty, value), new PropertyCondition(AutomationElement.ControlTypeProperty, controlType) };

                Logger.Debug(string.Format("The '{1}' {0} control was looked for by its class name.", uiType.Name, value));
            }

            if (findBy == FindBy.Type)
            {
                conds = new Condition[] { new PropertyCondition(AutomationElement.IsControlElementProperty, true), new PropertyCondition(AutomationElement.ControlTypeProperty, controlType) };

                Logger.Debug(string.Format("The {0} control was looked for by its type.", uiType.Name));
            }

            Logger.Debug("Value: " + value + " FindBy: " + findBy + " ControlType: " + uiType.Name + " TreeScope: " + scope + " UIContainer.FindUIControl  - Begin");

            try
            {                    
                uiControl = new UIControl(UIFind.FindElementByAndConditions(Root, conds, scope));                    
            }
            catch (UIAutomationElementNotFoundException)
            {
                Thread.Sleep(UIAutomationSettings.WaitForUIControl);
            }

            Logger.Debug("Value: " + value + " FindBy: " + findBy + " ControlType: " + uiType.Name + " TreeScope: " + scope + " UIContainer.FindUIControl  - End");


            if (uiControl == null)
            {
                uiControl = new UIControl(null);

                Logger.Debug(string.Format("The '{1}' {0} control was not found!", uiType.Name, value));
            }
            else
            {
                if (value != "")
                {
                    Logger.Debug(string.Format("The '{1}' {0} control was found.", uiType.Name, value));                    
                }
                else
                {
                    Logger.Debug(string.Format("The {0} control was found.", uiType.Name));
                }                
            }

            return uiControl;
        }

        /// <summary>
        /// Returns a instance of UIWindow class that statisfies the specified condition.
        /// </summary>
        /// <param name="name">Title of UIWindow</param>
        /// <param name="time">Amount of time until it waits for UIWindow</param>
        /// <returns></returns>
        public UIWindow FindDialogByName(string name, int time)
        {
            int counter = 0;
            UIWindow form = null;

            while (counter <= UIAutomationSettings.WaitForDialog)
            {
                counter += UIAutomationSettings.ThreadSleepTime;
                Thread.Sleep(UIAutomationSettings.ThreadSleepTime);

                Condition[] conds = new Condition[2] { new PropertyCondition(AutomationElement.NameProperty, name), new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Window) };

                //form = new UIWindow(UIFind.FindElementByAndConditions(Root, conds, TreeScope.Subtree));

                form = FindUIControl<UIWindow>(FindBy.Name, name);

                if (form != null)
                {
                    break;
                }

            }

            return form;

        }

        /// <summary>
        /// Returns a instance of UIWindow class that statisfies the specified condition.
        /// </summary>
        /// <param name="name">Title of UIWindow</param>
        /// <returns></returns>
        public UIWindow FindDialogByName(string name)
        {
            return FindUIControl<UIWindow>(FindBy.Name, name);
        }
    }
}
