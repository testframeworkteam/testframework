using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Input;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.BaseClasses
{   
    /// <summary>
    /// Represents the Click event delegate
    /// </summary>
    /// <param name="sender">Automation element</param>
    /// <param name="clickedPoint">Clicked point</param>
    public delegate void AutomationEventClickControl(object sender, ClickEventArgs clickedPoint);

    /// <summary>
    /// Represents a base class for all UIControls.
    /// </summary>      
    [Serializable]
    public class UIControl : IUIControl
    {
        private static Dictionary<AutomationElement, AutomationEventClickControl> _clickedDelegates = new Dictionary<AutomationElement, AutomationEventClickControl>();
        private AutomationElement _element;        
    
        public UIControl(AutomationElement element)
        {
            _element = element;
        }

        #region Properties

        /// <summary>
        /// Gets the name of the underlying UI framework.
        /// </summary>
        public string FrameWorkID
        {
            get
            {
                return Element.GetCurrentPropertyValue(AutomationElement.FrameworkIdProperty) as string;
            }            
        }
        
        /// <summary>
        /// Gets a string containing the class name of the element as assigned by the control developer.
        /// </summary>
        public string ClassName
        {
            get
            {
                return _element.GetCurrentPropertyValue(AutomationElement.ClassNameProperty) as string;   
            }            
        }
        
        /// <summary>
        /// Gets the element that contains the text label for this element.
        /// </summary>
        public AutomationElement LabeledBy
        {
            get
            {
                return Element.GetCurrentPropertyValue(AutomationElement.LabeledByProperty) as AutomationElement;
            }
            
        }

        /// <summary>
        /// Gets a string containing the UI Automation identifier (ID) for the element.
        /// </summary>
        public string AutomationID
        {
            get
            {
                return _element.GetCurrentPropertyValue(AutomationElement.AutomationIdProperty) as string;   
            }            
        }
        
        /// <summary>
        /// Gets the name of the element.
        /// </summary>
        public string Name
        {
            get
            {
                try
                {
                    return _element.GetCurrentPropertyValue(AutomationElement.NameProperty) as string;
                }
                catch (Exception)
                {
                    return string.Empty;
                }
                
            }            
        }

        /// <summary>
        /// Gets the process identifier (ID) of this element.
        /// </summary>
        public int ProcessId
        {
            get
            {
                return Element.Current.ProcessId;
            }           
        }

        /// <summary>
        /// Identifies the property that contains the runtime identifier of the element.
        /// </summary>
        public int[] RuntimeId
        {
            get
            {
                return Element.GetCurrentPropertyValue(AutomationElement.RuntimeIdProperty) as int[];
            }
        }

        /// <summary>
        /// Gets the handle of the element's window.
        /// </summary>
        public int Hwnd
        {
            get
            {
                return (int)Element.GetCurrentPropertyValue(AutomationElement.NativeWindowHandleProperty);
            }
        }

        /// <summary>
        /// Gets the coordinates of the rectangle that completely encloses the element.
        /// </summary>
        public Rect BoundingRectangle
        {
            get
            {
                Rect boundingRect;

                object boundingRectNoDefault = _element.GetCurrentPropertyValue(AutomationElement.BoundingRectangleProperty, true);
                
                if (boundingRectNoDefault == AutomationElement.NotSupported)
                {
                    throw new System.NotImplementedException();
                }
                else
                {
                    boundingRect = (System.Windows.Rect)boundingRectNoDefault;
                }

                return boundingRect;
            }
            
        }

        /// <summary>
        /// Identifies the Culture property.
        /// </summary>
        public CultureInfo Culture
        {
            get
            {
                return Element.GetCurrentPropertyValue(AutomationElement.CultureProperty) as CultureInfo;
            }
        }

        /// <summary>
        /// Gets the help text associated with the element.
        /// </summary>
        public string HelpText
        {
            get
            {
                return Element.GetCurrentPropertyValue(AutomationElement.HelpTextProperty) as string;
            }            
        }

        /// <summary>
        /// Gets a value that indicates whether the UI Automation element is visible on the screen.
        /// </summary>
        public bool IsOffScreen
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsOffscreenProperty);
            }  
        }

        /// <summary>
        /// Gets the orientation of the control.
        /// </summary>
        public OrientationType Orientation
        {
            get
            {
                return (OrientationType)Element.GetCurrentPropertyValue(AutomationElement.OrientationProperty);
            }        
        }
        
        /// <summary>
        /// Gets the ControlType of element.
        /// </summary>
        public ControlType ControlType
        {
            get
            {
                return Element.GetCurrentPropertyValue(AutomationElement.ControlTypeProperty) as ControlType;
            }
        }

        /// <summary>
        /// Gets a value that indicates whether the element is viewed as a content.
        /// </summary>
        internal bool IsContentElement
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsContentElementProperty);
            }
        }
        
        /// <summary>
        /// Gets a value that indicates whether the element is viewed as a control.
        /// </summary>
        internal bool IsControlElement
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsControlElementProperty);
            }
        }

        /// <summary>
        /// This property is used to obtain information about items in a list, tree view, or data grid. For example, an item in a file directory view might be a "Document File" or a "Folder".
        /// </summary>
        public string ItemType
        {
            get
            {
                return Element.GetCurrentPropertyValue(AutomationElement.ItemTypeProperty) as string;
            }
        }

        /// <summary>
        /// This property enables a client to ascertain whether an element is conveying status about an item. For example, an item associated with a contact in a messaging application might be "Busy" or "Connected".
        /// </summary>
        public string ItemStatus
        {
            get
            {
                return Element.GetCurrentPropertyValue(AutomationElement.ItemStatusProperty) as string;
            }
        }

        /// <summary>
        ///  A localized description of the control type, such as "button".
        /// </summary>
        public string LocalizedControlType
        {
            get
            {
                return Element.GetCurrentPropertyValue(AutomationElement.LocalizedControlTypeProperty) as string;
            }
        }

        /// <summary>
        /// Gets a string containing the accelerator key combinations for the element.
        /// <remarks>
        /// Accelerator key combinations invoke an action. For example, CTRL + O is often used to invoke the File Open dialog box. UI Automation elements that have the accelerator key property set always implement the InvokePattern class.
        /// </remarks>
        /// </summary>        
        public string AcceleratorKey
        {
            get
            {
                return Element.GetCurrentPropertyValue(AutomationElement.AcceleratorKeyProperty) as string;
            }
        }

        /// <summary>
        /// Gets a string containing the access key character for the element.
        /// <remarks>
        /// An access key is a character in the text of a menu, menu item, or label of a control such as a button that activates the attached menu function. For example, the letter "O" is often used to invoke the Open file common dialog box from a File menu. UI Automation elements that have the access key property set always implement the InvokePattern class.
        /// </remarks>
        /// </summary>
        public string AccessKey
        {
            get
            {
                return Element.GetCurrentPropertyValue(AutomationElement.AccessKeyProperty) as string;
            }
        }

        /// <summary>
        /// Identifies the ClickablePoint property.
        /// <remarks>
        /// This identifier is used by UI Automation client applications. UI Automation providers should use the equivalent identifier in AutomationElementIdentifiers.
        /// An AutomationElement is not clickable if it is completely obscured by another window.
        /// Return values of the property are of type Point. The default value is a null reference (Nothing in Visual Basic).
        /// The value returned is in physical screen coordinates. 
        /// </remarks>
        /// </summary>
        public System.Windows.Point ClickablePoint
        {
            get
            {
                return (System.Windows.Point)Element.GetCurrentPropertyValue(AutomationElement.ClickablePointProperty);
            }
        }

        /// <summary>
        /// Gets a value that indicates whether the element has keyboard focus.
        /// <remarks>
        /// If an element within a container has focus, HasKeyboardFocus is also true for the container element.
        /// </remarks>
        /// </summary>
        public bool HasKeyboardFocus
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.HasKeyboardFocusProperty);
            }        
        }

        /// <summary>
        /// Gets a value that indicates whether the user interface (UI) item referenced by the UI Automation element is enabled.
        /// </summary>
        public bool IsEnabled
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsEnabledProperty);
            }           
        }

        /// <summary>
        /// Gets a value that indicates whether the UI Automation element can accept keyboard focus.
        /// </summary>
        public bool IsKeyboardFocusable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsKeyboardFocusableProperty);
            }            
        }

        /// <summary>
        /// Identifies the property that indicates whether DockPattern is available on this UI Automation element.
        /// </summary>
        internal bool IsDockPatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsDockPatternAvailableProperty);
            }
        }

        /// <summary>
        /// Identifies the property that indicates whether ExpandCollapsePattern is available on this UI Automation element.
        /// </summary>
        internal bool IsExpandCollapsePatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsExpandCollapsePatternAvailableProperty);
            }
        }
        
        /// <summary>
        /// Identifies the property that indicates whether GridItemPattern is available on this UI Automation element
        /// </summary>
        internal bool IsGridItemPatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsGridItemPatternAvailableProperty);
            }
        }

        /// <summary>
        /// Identifies the property that indicates whether GridPattern is available on this UI Automation element.
        /// </summary>
        internal bool IsGridPatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsGridPatternAvailableProperty);
            }
        }

        /// <summary>
        /// Identifies the property that indicates whether InvokePattern is available on this UI Automation element.
        /// </summary>
        internal bool IsInvokePatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsInvokePatternAvailableProperty);
            }
            
        }

        /// <summary>
        /// Identifies the property that indicates whether MultipleViewPattern is available on this UI Automation element.
        /// </summary>
        internal bool IsMultipleViewPatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsMultipleViewPatternAvailableProperty);
            }           
        }

        /// <summary>
        /// Identifies the property that indicates whether RangeValuePattern is available on this UI Automation element.
        /// </summary>
        internal bool IsRangeValuePatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsRangeValuePatternAvailableProperty);
            }         
        }

        /// <summary>
        /// Identifies the property that indicates whether ScrollItemPattern is available for this UI Automation element.
        /// </summary>
        internal bool IsScrollItemPatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsScrollItemPatternAvailableProperty);
            }          
        }

        /// <summary>
        /// Identifies the property that indicates whether ScrollPattern is available on this UI Automation element.
        /// </summary>
        internal bool IsScrollPatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsScrollPatternAvailableProperty);
            }            
        }
        
        /// <summary>
        /// Identifies the property that indicates whether SelectionItemPattern is available on this UI Automation element.
        /// </summary>
        internal bool IsSelectionItemPatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsSelectionItemPatternAvailableProperty);
            }     
        }

        /// <summary>
        /// Identifies the property that indicates whether SelectionPattern is available on this UI Automation element.
        /// </summary>
        internal bool IsSelectionPatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsSelectionPatternAvailableProperty);
            }            
        }

        /// <summary>
        /// Identifies the property that indicates whether the TableItemPattern is available on this UI Automation element.
        /// </summary>
        internal bool IsTableItemPatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsTableItemPatternAvailableProperty);
            }            
        }

        /// <summary>
        /// Identifies the property that indicates whether TablePattern is available on this UI Automation element.
        /// </summary>
        internal bool IsTablePatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsTablePatternAvailableProperty);
            }            
        }
        
        /// <summary>
        /// Identifies the property that indicates whether TextPattern is available on this UI Automation element.
        /// </summary>
        internal bool IsTextPatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsTextPatternAvailableProperty);
            }            
        }
        
        /// <summary>
        /// Identifies the property that indicates whether TogglePattern is available on this UI Automation element.
        /// </summary>
        internal bool IsTogglePatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsTogglePatternAvailableProperty);
            }            
        }

        /// <summary>
        /// Identifies the property that indicates whether TransformPattern is available on this UI Automation element.
        /// </summary>
        internal bool IsTransformPatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsWindowPatternAvailableProperty);
            }            
        }
        
        /// <summary>
        /// Identifies the property that indicates whether ValuePattern is available on this UI Automation element.
        /// </summary>
        internal bool IsValuePatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsValuePatternAvailableProperty);
            }            
        }

        /// <summary>
        /// Identifies the property that indicates whether WindowPattern is available on this UI Automation element.
        /// </summary>
        internal bool IsWindowPatternAvailable
        {
            get
            {
                return (bool)Element.GetCurrentPropertyValue(AutomationElement.IsWindowPatternAvailableProperty);
            }            
        }

        /// <summary>
        /// Gets the AutomationElement of UIControl.
        /// </summary>
        public AutomationElement Element
        {
            get
            {
                return _element;
            }            
        }
        
        /// <summary>
        /// Identifies the IsRequiredForForm property.
        /// </summary>
        public bool IsRequiredForForm
        {
            get
            {
                throw new System.NotImplementedException();
            }
        }  

        /// <summary>
        /// Identifies the Exists property.
        /// </summary>
        public bool Exists
        {
            get
            {
                try
                {
                    return (_element != null && _element.GetCurrentPropertyValue(AutomationElement.ClassNameProperty) != null);
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Identifies the Exists property.
        /// </summary>
        public bool WaitFor()
        {
            return WaitFor(UIAutomationSettings.WaitForTimeout);
        }

        /// <summary>
        /// Identifies the Exists property.
        /// </summary>
        public bool WaitFor(int timeout)
        {
            return General.WaitFor(() => Exists, timeout);
        }

        /// <summary>
        /// Gets the background color of the emelent
        /// </summary>
        /// <value>The background color</value>
        public Color BackgroundColor
        {
            get
            {
                Color backgroundColor = Color.Empty;

                Graphics g = Graphics.FromHwnd((IntPtr)_element.Current.NativeWindowHandle);
                IntPtr hDC = g.GetHdc();

                try
                {                    
                    backgroundColor = ColorTranslator.FromWin32(Win32.GetBkColor(hDC));
                }
                finally
                {
                    g.ReleaseHdc(hDC);
                    g.Dispose();
                }

                return backgroundColor;
            }
        }

        /// <summary>
        /// Gets the color of the text.
        /// </summary>
        /// <value>The color of the text.</value>
        public Color TextColor
        {
            get
            {
                Color textColor = Color.Empty;

                Graphics g = Graphics.FromHwnd((IntPtr)_element.Current.NativeWindowHandle);
                IntPtr hDC = g.GetHdc();

                try
                {
                    textColor = ColorTranslator.FromWin32(Win32.GetTextColor(hDC));
                }
                finally
                {
                    g.ReleaseHdc(hDC);
                    g.Dispose();
                }

                return textColor;
            }
        }

        #endregion 

        #region Methods

        /// <summary>
        /// Sets the focus on the UIControl.
        /// </summary>
        public void SetFocus()
        {
            _element.SetFocus();
        }

        /// <summary>
        /// It sets the property comes from.(Cashed or Current)
        /// </summary>        
        public void SetPropertySource(bool Cashed)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Click on element until the given delegate returns true
        /// </summary>     
        /// <param name="booldelegate">bool delegate</param>
        /// <param name="timeout">max timeout</param>
        /// <returns>true if the delegate returned true within timeout</returns>
        public bool Click(BoolDelegate booldelegate, int timeout = 5)
        {
            Logger.Debug("Click boolDelegate called.");
            DateTime startTime = DateTime.Now;

            Logger.Debug("Click boolDelegate invoking.");
            if (booldelegate.Invoke())
            {
                return true;
            }

            do
            {
                Logger.Debug("Click boolDelegate clicking.");
                if (Exists)
                {
                    Click();
                }

                Logger.Debug("Click boolDelegate invoking.");
                if (General.WaitFor(booldelegate.Invoke, 1))
                {
                    Logger.Debug("Click boolDelegate return true.");
                    return true;
                }

                Logger.Debug("Click boolDelegate retry.");
            } while (startTime.AddSeconds(timeout) > DateTime.Now);

            Logger.Debug("Click boolDelegate return false.");
            return false;
        }

        /// <summary>
        /// Clicks top left corner of UIControl
        /// </summary>        
        public void Click()
        {
            string name = Name;
            string type = GetType().Name;
            Rect rect = (Rect)(_element.GetCurrentPropertyValue(AutomationElement.BoundingRectangleProperty));            
            System.Windows.Point pt = new System.Windows.Point(rect.TopLeft.X + 5, rect.TopLeft.Y + 5);            
            Input.MoveToAndClick(pt);
            Thread.Sleep(100);
            OnClick(pt);

            Logger.Debug((name != string.Empty) ? string.Format("The '{0}' control was clicked!", name) : string.Format("The {0} control was clicked!", type));
        }
        
        /// <summary>
        /// Double Clicks top left corner of UIControl
        /// </summary>        
        public void DoubleClick()
        {
            Rect rect = (Rect)(_element.GetCurrentPropertyValue(AutomationElement.BoundingRectangleProperty));
            System.Windows.Point pt = new System.Windows.Point(rect.TopLeft.X + 5, rect.TopLeft.Y + 5);
            Input.MoveToAndClick(pt);
            Thread.Sleep(100);
            Input.MoveToAndClick(pt);
            Thread.Sleep(100);
            OnClick(pt);

            Logger.Debug((Name != string.Empty) ? string.Format("The '{0}' control was double clicked!", Name) : string.Format("The control was doubel clicked!"));

        }

        /// <summary>
        /// Clicks top left corner of UIControl using the specified mouse button
        /// </summary>
        /// <param name="useRightButton">If it is true it uses the right button</param>
        public void Click(bool useRightButton)
        {
            Rect rect = (Rect)(_element.GetCurrentPropertyValue(AutomationElement.BoundingRectangleProperty));
            System.Windows.Point pt = new System.Windows.Point(rect.TopLeft.X + 5, rect.TopLeft.Y + 5);
            Input.MoveToAndClick(pt, true);
            Thread.Sleep(100);
            OnClick(pt);

            Logger.Debug((Name != string.Empty) ? string.Format("The '{0}' control was clicked by right button!", Name) : string.Format("The control was clicked by right button!"));
        }

        /// <summary>
        /// Double Clicks top left corner of UIControl using the specified mouse button
        /// </summary>
        /// <param name="useRightButton">If it is true it uses the right button</param>
        public void DoubleClick(bool useRightButton)
        {
            Rect rect = (Rect)(_element.GetCurrentPropertyValue(AutomationElement.BoundingRectangleProperty));
            System.Windows.Point pt = new System.Windows.Point(rect.TopLeft.X + 5, rect.TopLeft.Y + 5);
            Input.MoveToAndClick(pt, true);
            Thread.Sleep(100);
            Input.MoveToAndClick(pt, true);
            Thread.Sleep(100);
            OnClick(pt);

            Logger.Debug((Name != string.Empty) ? string.Format("The '{0}' control was double clicked by right button!", Name) : string.Format("The control was double clicked by right button!"));
        }

        /// <summary>
        /// Clicks the UIControl at the specified point.
        /// </summary>
        /// <param name="pointToClick"></param>
        public void Click(System.Windows.Point pointToClick)
        {
            Rect rect = (Rect)(_element.GetCurrentPropertyValue(AutomationElement.BoundingRectangleProperty));
            System.Windows.Point pt = new System.Windows.Point(rect.TopLeft.X + pointToClick.X, rect.TopLeft.Y + pointToClick.Y);
            Input.MoveToAndClick(pt);
            Thread.Sleep(100);
            OnClick(pt);

            Logger.Debug((Name != string.Empty) ? string.Format("The '{0}' control was clicked!", Name) : string.Format("The control was clicked!"));
        }

        /// <summary>
        /// Double Clicks the UIControl at the specified point.
        /// </summary>
        /// <param name="pointToClick"></param>
        public void DoubleClick(System.Windows.Point pointToClick)
        {
            Rect rect = (Rect)(_element.GetCurrentPropertyValue(AutomationElement.BoundingRectangleProperty));
            System.Windows.Point pt = new System.Windows.Point(rect.TopLeft.X + pointToClick.X, rect.TopLeft.Y + pointToClick.Y);
            Input.MoveToAndClick(pt);
            Thread.Sleep(100);
            Input.MoveToAndClick(pt);
            Thread.Sleep(100);
            OnClick(pt);

            Logger.Debug((Name != string.Empty) ? string.Format("The '{0}' control was double clicked!", Name) : string.Format("The control was doubel clicked!"));
        }

        /// <summary>
        /// Clicks the UIControl at the specified point using the specified button
        /// </summary>
        /// <param name="pointToClick"></param>
        /// <param name="useRightButton">If it is true it uses the right button</param>
        public void Click(System.Windows.Point pointToClick, bool useRightButton)
        {
            Rect rect = (Rect)(_element.GetCurrentPropertyValue(AutomationElement.BoundingRectangleProperty));
            System.Windows.Point pt = new System.Windows.Point(rect.TopLeft.X + pointToClick.X, rect.TopLeft.Y + pointToClick.Y);
            Input.MoveToAndClick(pt, useRightButton);
            Thread.Sleep(100);
            OnClick(pt);

            Logger.Debug((Name != string.Empty) ? string.Format("The '{0}' control was clicked by right button!", Name) : string.Format("The control was clicked by right button!"));
        }

        /// <summary>
        /// Double Clicks the UIControl at the specified point using the specified button
        /// </summary>
        /// <param name="pointToClick"></param>
        /// <param name="useRightButton">If it is true it uses the right button</param>
        public void DoubleClick(System.Windows.Point pointToClick, bool useRightButton)
        {
            Rect rect = (Rect)(_element.GetCurrentPropertyValue(AutomationElement.BoundingRectangleProperty));
            System.Windows.Point pt = new System.Windows.Point(rect.TopLeft.X + pointToClick.X, rect.TopLeft.Y + pointToClick.Y);
            Input.MoveToAndClick(pt, useRightButton);
            Thread.Sleep(100);
            Input.MoveToAndClick(pt, useRightButton);
            Thread.Sleep(100);
            OnClick(pt);

            Logger.Debug((Name != string.Empty) ? string.Format("The '{0}' control was double clicked by right button!", Name) : string.Format("The control was doubel clicked by right button!"));
        }

        /// <summary>
        /// Methods is being invoked when the control is clicked
        /// </summary>        
        private void OnClick(System.Windows.Point clickedPoint)
        {
            ClickEventArgs clickEventArgs = new ClickEventArgs();

            foreach (KeyValuePair<AutomationElement, AutomationEventClickControl> kvp in _clickedDelegates)
            {
                if (kvp.Key == _element)
                {
                    clickEventArgs.ClickedPoint = clickedPoint;
                    kvp.Value.Invoke(_element, clickEventArgs);
                }
            }
        }

        /// <summary>
        /// Sends the specified key to the control
        /// </summary>
        /// <param name="key">The key.</param>
        public void SendKey(Key key)
        {
            _element.SetFocus();
            Input.SendKeyboardInput(key, true);
            Thread.Sleep(300);
            Input.SendKeyboardInput(key, false);
        }

        /// <summary>
        /// Sends the specified key to the control
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="setFocus">
        /// The set Focus.
        /// </param>
        public void SendKey(Key key, bool setFocus)
        {
            if (setFocus)
            {
                _element.SetFocus();
            }

            Input.SendKeyboardInput(key, true);
            Thread.Sleep(300);
            Input.SendKeyboardInput(key, false);
        }
        
        /// <summary>
        /// Sends the key and a functional key to the control (e.g.: CTRL+1)
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="functionKey">The function key. (ALT, CTRL, SHRIFT, ...)</param>
        public void SendKey(Key key, Key functionKey)
        {
            SendKey(key, functionKey, true);
        }

        /// <summary>
        /// Sends the key and a functional key to the control (e.g.: CTRL+1)
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="functionKey">
        /// The function key. (ALT, CTRL, SHRIFT, ...)
        /// </param>
        /// <param name="setFocus">
        /// The set Focus.
        /// </param>
        public void SendKey(Key key, Key functionKey, bool setFocus)
        {
            if (setFocus)
            {
                _element.SetFocus();
            }

            Input.SendKeyboardInput(functionKey, true);
            Thread.Sleep(300);
            Input.SendKeyboardInput(key, true);
            Thread.Sleep(300);
            Input.SendKeyboardInput(key, false);
            Input.SendKeyboardInput(functionKey, false);            
        }

        /// <summary>
        /// Moves the mouse to the UIControl.
        /// </summary>        
        public void MouseMoveTo()
        {
            Input.MoveTo(_element.GetClickablePoint());
        }

        /// <summary>
        /// Moves the mouse to the UIControl and click it.  The primary mouse button will be used
        /// this is usually the left button except if the mouse buttons are swaped.        
        /// </summary>        
        public void MouseMoveToAndClick()
        {
            Input.MoveToAndClick(_element.GetClickablePoint());
        }

        /// <summary>
        /// Waits for changing of specified property of UIControl.
        /// </summary>
        /// <param name="automationProperty">Automation property</param>
        /// <param name="value">Wait for value to be changed</param>
        /// <param name="timeOut">Period of time out</param>
        public bool WaitForPropertyChange(AutomationProperty automationProperty, object value, int timeOut)
        {
            bool hasChanged = false;
            int counter = 0;

            while (!hasChanged)
            {
                counter++;

                if (Element.GetCurrentPropertyValue(automationProperty).Equals(value))
                {
                    hasChanged = true;
                    break;
                }

                Thread.Sleep(UIAutomationSettings.ThreadSleepTime);

                if (counter >= timeOut) { break; }
            }

            return hasChanged;
        }

        /// <summary>
        /// Waits for changing of specified property of UIControl.
        /// </summary>
        /// <param name="automationProperty">Automation property</param>
        /// <param name="value">Wait for value to be changed</param>
        public bool WaitForPropertyChange(AutomationProperty automationProperty, object value)
        {
            return WaitForPropertyChange(automationProperty, value, 30);
        }

        #endregion

        #region Events

        /// <summary>
        /// Represents the Invoke event of UIControl class
        /// </summary>
        public event AutomationEventHandler Invoked
        {
            add
            {
                if (IsInvokePatternAvailable)
                {
                    System.Windows.Automation.Automation.AddAutomationEventHandler(InvokePattern.InvokedEvent, Element, TreeScope.Element, new AutomationEventHandler(value));
                }
                else
                {
                    throw new PatternNotSupportedException(this, "InvokePattern is not supported on this control !");
                }

            }
            remove
            {
                System.Windows.Automation.Automation.RemoveAutomationEventHandler(InvokePattern.InvokedEvent, Element, value);
            }

        }
        
        /// <summary>
        /// Represents the Click event of UIControl
        /// </summary>
        public event AutomationEventClickControl Clicked
        {
            add
            {
                _clickedDelegates.Add(_element, new AutomationEventClickControl(value));
            }
            remove
            {
                _clickedDelegates.Remove(_element);
            }
        }

        /// <summary>
        /// Represents IsEnabled property change event
        /// </summary>
        public event AutomationPropertyChangedEventHandler EnabledChanged
        {
            add
            {
                System.Windows.Automation.Automation.AddAutomationPropertyChangedEventHandler(_element,
                    TreeScope.Element, new AutomationPropertyChangedEventHandler(value), AutomationElement.IsEnabledProperty);
            }
            remove
            {                
                System.Windows.Automation.Automation.RemoveAutomationPropertyChangedEventHandler(_element, value);
            }
        }

        /// <summary>
        /// Represents IsOffScreen property change event
        /// </summary>
        public event AutomationPropertyChangedEventHandler OffScreenChanged
        {
            add
            {
                System.Windows.Automation.Automation.AddAutomationPropertyChangedEventHandler(_element,
                    TreeScope.Element, new AutomationPropertyChangedEventHandler(value), AutomationElement.IsOffscreenProperty);
            }
            remove
            {
                System.Windows.Automation.Automation.RemoveAutomationPropertyChangedEventHandler(_element, value);
            }
        }

        /// <summary>
        /// Identifies the event that is raised when the UI Automation tree structure is changed. 
        /// </summary>
        public event StructureChangedEventHandler StructureChangedEvent
        {
            add
            {
                System.Windows.Automation.Automation.AddStructureChangedEventHandler(_element, TreeScope.Element, new StructureChangedEventHandler(value));
            }
            remove
            {
                System.Windows.Automation.Automation.RemoveStructureChangedEventHandler(_element, value);
            }
        }

        #endregion

        #region IUIControl Members


        bool IUIControl.IsContentElement
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsControlElement
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsDockPatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsExpandCollapsePatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsGridItemPatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsGridPatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsInvokePatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsMultipleViewPatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsRangeValuePatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsScrollItemPatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsScrollPatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsSelectionItemPatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsSelectionPatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsTableItemPatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsTablePatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsTextPatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsTogglePatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsTransformPatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsValuePatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        bool IUIControl.IsWindowPatternAvailable
        {
            get { throw new NotImplementedException(); }
        }

        void IUIControl.OnClick(System.Windows.Point clickedPoint)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    /// <summary>
    /// Represents the ClickEventArgs class
    /// </summary>
    public class ClickEventArgs : EventArgs
    {
        private System.Windows.Point clickedPoint;

        /// <summary>
        /// Sets or gets the clicked point of UIControl
        /// </summary>
        public System.Windows.Point ClickedPoint
        {
            get { return clickedPoint; }
            set { clickedPoint = value; }
        }
    }
}
