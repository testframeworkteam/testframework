using System.Collections;

namespace PowerTester.Framework.UI.BaseClasses
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UIControlCollection<T>: CollectionBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="UIControlCollection&lt;T&gt;"/> class.
        /// </summary>
        public UIControlCollection() {}

        /// <summary>
        /// Adds the specified control.
        /// </summary>
        /// <param name="control">The control.</param>
        public virtual void Add(T control)
        {
            List.Add(control);
        }

        /// <summary>
        /// Gets or sets the <see cref="T"/> at the specified index.
        /// </summary>
        /// <value></value>
        public virtual T this[int Index]
        {
            get
            {
                return (T)List[Index];
            }
            set
            {
                List[Index] = value;
            }
        }
    
    }
}
