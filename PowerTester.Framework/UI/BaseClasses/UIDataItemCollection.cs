using System;
using System.Collections;
using PowerTester.Framework.UI.Controls;

namespace PowerTester.Framework.UI.BaseClasses
{
    /// <summary>
    /// Represents a collection of UIDataItem objects
    /// </summary>   
    public class UIDataItemCollection : CollectionBase
    {        
        /// <summary>
        /// Gets the <see cref="UIDataItem"/> at the specified index.
        /// </summary>
        /// <value></value>
        public virtual UIDataItem this[int index]
        {
            get
            {
                return (UIDataItem)List[index];
            }
        }

        /// <summary>
        /// Gets the <see cref="UIDataItem"/> with the specified name.
        /// </summary>
        /// <value></value>
        public virtual UIDataItem this[string name]
        {
            get
            {
                for (var i = 0; i <= List.Count; i++)
                {
                    if (((UIDataItem)List[i]).Name.ToLower() == name.ToLower())
                    {
                        return (UIDataItem)List[i];
                    }
                }

                throw new ArgumentOutOfRangeException(string.Format("The provided index ({0}) is out of range!", name));
            }
        }

        /// <summary>
        /// Adds the specified data item.
        /// </summary>
        /// <param name="dataItem">
        /// The data Item.
        /// </param>
        public virtual void Add(UIDataItem dataItem)
        {
            List.Add(dataItem);
        }

        /// <summary>
        /// Sorts the inner list based on the specified comparer.
        /// </summary>
        /// <param name="comparer">The comparer.</param>
        public virtual void Sort(IComparer comparer)
        {
            base.InnerList.Sort(comparer);
        }
    }
}

