using System;
using System.Threading;
using System.Windows.Automation;
using PowerTester.Framework.UI.Controls;

namespace PowerTester.Framework.UI.BaseClasses
{
    /// <summary>
    /// Provides functions for finding of UIControls
    /// </summary>
    public static class UIFind
    {
        /// <summary>
        /// Finds an UIControl which statisfies the conditions.
        /// </summary>
        /// <param name="findBy">Type of finding mode</param>
        /// <param name="value">Value of finding mode</param>
        /// <returns></returns>
        internal static UIControl FindUIControl(FindBy findBy, string value)
        {
            return FindUIControl(findBy, AutomationElement.RootElement, value, null);
        }

        /// <summary>
        /// Finds an UIControl which statisfies the conditions.
        /// </summary>
        /// <param name="findBy">Type of finding mode</param>
        /// <param name="controlType">ControlType</param>
        /// <returns></returns>
        internal static UIControl FindUIControl(FindBy findBy, ControlType controlType)
        {
            return FindUIControl(findBy, AutomationElement.RootElement, string.Empty, controlType);
        }

        /// <summary>
        /// Finds an UIControl which statisfies the conditions.
        /// </summary>
        /// <param name="findBy">Type of finding mode</param>
        /// <param name="root">rootElement</param>
        /// <param name="value">Value of finding mode</param>
        /// <returns></returns>
        public static UIControl FindUIControl(FindBy findBy, AutomationElement root, string value)
        {
            return FindUIControl(findBy, root, value, null);
        }
        
        /// <summary>
        /// Finds an UIControl which statisfies the conditions.
        /// </summary>
        /// <param name="findBy">Type of finding mode</param>
        /// <param name="root">rootElement</param>
        /// <param name="controlType">ControlType</param>
        /// <returns></returns>
        public static UIControl FindUIControl(FindBy findBy, AutomationElement root, ControlType controlType)
        {
            return FindUIControl(findBy, root, string.Empty, controlType);
        }
  
        /// <summary>
        /// Finds all UIControls whihc statisfy the conditions.
        /// </summary>
        /// <param name="rootElement">rootElement</param>
        /// <param name="name">Name of UIControl(s)</param>         
        public static UIControlCollection<UIControl> FindAllUIControlsByName(AutomationElement rootElement, string name)
        {
            return FindAllUIControlsByName(rootElement, name, TreeScope.Subtree);
        }

        /// <summary>
        /// Finds all UIControls whihc statisfy the conditions.
        /// </summary>
        /// <param name="rootElement">rootElement</param>
        /// <param name="name">Name of UIControl(s)</param>       
        /// <param name="scope">Scope in which the search is completed</param>
        public static UIControlCollection<UIControl> FindAllUIControlsByName(AutomationElement rootElement, string name, TreeScope scope)
        {
            UIControlCollection<UIControl> controlCollection = new UIControlCollection<UIControl>();
            AutomationElementCollection automationElementCollection = UIFind.FindAllElementsByName(rootElement, name, scope);

            foreach (AutomationElement automationElement in automationElementCollection)
            {
                controlCollection.Add(new UIControl(automationElement));
            }

            return controlCollection;
        }

        /// <summary>
        /// Finds all the UIControls which statisfy the conditions.
        /// </summary>
        /// <param name="rootElement">rootElement</param>
        /// <param name="type">ControlType</param>   
        public static UIControlCollection<UIControl> FindAllUIControlsByType(AutomationElement rootElement, ControlType type)
        {
            return FindAllUIControlsByType(rootElement, type, TreeScope.Subtree);
        }

        /// <summary>
        /// Finds all the UIControls which statisfy the conditions.
        /// </summary>
        /// <param name="rootElement">rootElement</param>
        /// <param name="type">ControlType</param>   
        /// <param name="scope">Scope in which the search is completed</param>
        public static UIControlCollection<UIControl> FindAllUIControlsByType(AutomationElement rootElement, ControlType type, TreeScope scope)
        {
            UIControlCollection<UIControl> controlCollection = new UIControlCollection<UIControl>();
            AutomationElementCollection automationElementCollection = UIFind.FindAllElementsByType(rootElement, type, scope);

            foreach (AutomationElement automationElement in automationElementCollection)
            {
                controlCollection.Add(new UIControl(automationElement));
            }

            return controlCollection;
        }

        /// <summary>
        /// Finds all the UIControls which statisfy the conditions.
        /// </summary>
        /// <param name="rootElement">rootElement</param>
        /// <param name="className">ClassName</param>                
        public static UIControlCollection<UIControl> FindAllUIControlsByClassName(AutomationElement rootElement, string className)
        {
            return FindAllUIControlsByClassName(rootElement, className, TreeScope.Subtree);
        }

        /// <summary>
        /// Finds all the UIControls which statisfy the conditions.
        /// </summary>
        /// <param name="rootElement">rootElement</param>
        /// <param name="className">ClassName</param>        
        /// <param name="scope">Scope in which the search is completed</param>
        public static UIControlCollection<UIControl> FindAllUIControlsByClassName(AutomationElement rootElement, string className, TreeScope scope)
        {
            UIControlCollection<UIControl> controlCollection = new UIControlCollection<UIControl>();
            AutomationElementCollection automationElementCollection = UIFind.FindAllElementsByClassName(rootElement, className, scope);

            foreach (AutomationElement automationElement in automationElementCollection)
            {
                controlCollection.Add(new UIControl(automationElement));
            }

            return controlCollection;
        }

        internal static UIWindow FindUIWindow(FindBy findBy, string value)
        {
            return FindUIWindow(findBy, AutomationElement.RootElement, value);
        }

        internal static UIWindow FindUIWindow(FindBy findBy, AutomationElement rootElement, string value)
        {
            UIWindow uiWindow = null;

            AutomationElement root = (rootElement == null) ? AutomationElement.RootElement : rootElement;

            switch (findBy)
            {
                case FindBy.Name:
                    uiWindow = FindUIWindowByName(root, value);
                    break;

                case FindBy.AutomationId:
                    uiWindow = FindUIWindowById(root, value);
                    break;

                case FindBy.ClassName:
                    uiWindow = FindUIWindowByClassName(root, value);
                    break;
            }

            return uiWindow;
        }
      
        internal static UIWindow FindUIWindowByName(AutomationElement rootElement, string name)
        {
            Condition[] conds = new Condition[2] { new PropertyCondition(AutomationElement.NameProperty, name), new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Window) };
            return new UIWindow(UIFind.FindElementByAndConditions(rootElement, conds, TreeScope.Element| TreeScope.Children));            
        }

        internal static UIWindow FindUIWindowByClassName(AutomationElement rootElement, string className)
        {
            Condition[] conds = new Condition[2] { new PropertyCondition(AutomationElement.ClassNameProperty, className), new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Window) };
            return new UIWindow(UIFind.FindElementByAndConditions(rootElement, conds, TreeScope.Element | TreeScope.Children));                                    
        }

        internal static UIWindow FindUIWindowById(AutomationElement rootElement, string id)
        {
            Condition[] conds = new Condition[2] { new PropertyCondition(AutomationElement.AutomationIdProperty, id), new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Window) };
            return new UIWindow(UIFind.FindElementByAndConditions(rootElement, conds, TreeScope.Element | TreeScope.Children));                        
        }

        internal static UIControl FindUIControl(FindBy findBy, AutomationElement rootElement, string value, ControlType controlType)
        {
            UIControl uiControl = null;
            AutomationElement root = (rootElement == null) ? AutomationElement.RootElement : rootElement;

            switch (findBy)
            {
                case FindBy.Name:
                    uiControl = FindUIControlByName(root, value);
                    break;

                case FindBy.AutomationId:
                    uiControl = FindUIControlByAutomationId(root, value);
                    break;

                case FindBy.ClassName:
                    uiControl = FindUIControlByClassName(root, value);
                    break;

                case FindBy.Type:
                    uiControl = FindUIControlByType(root, controlType);
                    break;
            }

            return uiControl;
        }

        internal static UIControl FindUIControlByName(AutomationElement rootElement, string name)
        {
            return new UIControl(UIFind.FindElementByName(rootElement, name));
        }

        internal static UIControl FindUIControlByType(AutomationElement rootElement, ControlType type)
        {
            return new UIControl(UIFind.FindElementByType(rootElement, type));
        }

        internal static UIControl FindUIControlByAutomationId(AutomationElement rootElement, string id)
        {
            return new UIControl(UIFind.FindElementById(rootElement, id));
        }

        internal static UIControl FindUIControlByClassName(AutomationElement rootElement, string className)
        {
            return new UIControl(UIFind.FindElementByClassName(rootElement, className));
        }

        internal static AutomationElement FindApplicationByHandle(Int32 handle)
        {            
            int sleepCounter = 0;
            AutomationElement automationElement = null;

            PropertyCondition conds = new PropertyCondition(AutomationElement.NativeWindowHandleProperty, handle);
            while (automationElement == null && (sleepCounter <= UIAutomationSettings.WaitForApplication))
            {
                sleepCounter += UIAutomationSettings.ThreadSleepTime;
                Thread.Sleep(UIAutomationSettings.ThreadSleepTime);
                automationElement = AutomationElement.RootElement.FindFirst(TreeScope.Children, conds);
            }            

            return automationElement;
        }


        internal static AutomationElement FindApplicationByHandle(Int32 handle, ControlType mainWindowControltype)
        {
            int sleepCounter = 0;
            AutomationElement automationElement = null;

            AndCondition andConds = new AndCondition(new PropertyCondition(AutomationElement.NativeWindowHandleProperty, handle), new PropertyCondition(AutomationElement.ControlTypeProperty, mainWindowControltype));

            while (automationElement == null && (sleepCounter <= UIAutomationSettings.WaitForApplication))
            {
                sleepCounter += UIAutomationSettings.ThreadSleepTime;
                Thread.Sleep(UIAutomationSettings.ThreadSleepTime);
                automationElement = AutomationElement.RootElement.FindFirst(TreeScope.Children, andConds);
            }

            return automationElement;
        }

        internal static AutomationElement FindApplicationByTitle(string title)
        {            
            int sleepCounter = 0;
            AutomationElement automationElement = null;
            
            PropertyCondition conds = new PropertyCondition(AutomationElement.NameProperty, title, PropertyConditionFlags.IgnoreCase);
            while (automationElement == null && (sleepCounter <= UIAutomationSettings.WaitForApplication))
            {
                sleepCounter += UIAutomationSettings.ThreadSleepTime;
                Thread.Sleep(UIAutomationSettings.ThreadSleepTime);
                automationElement = AutomationElement.RootElement.FindFirst(TreeScope.Children, conds);
            }            

            return automationElement;
        }

        internal static AutomationElement FindApplicationByProcessId(int processId)
        {            
            int sleepCounter = 0;
            AutomationElement automationElement = null;

            PropertyCondition conds = new PropertyCondition(AutomationElement.ProcessIdProperty, processId);
            while (automationElement == null && (sleepCounter <= UIAutomationSettings.WaitForApplication))
            {
                sleepCounter += UIAutomationSettings.ThreadSleepTime; 
                Thread.Sleep(UIAutomationSettings.ThreadSleepTime);
                automationElement = AutomationElement.RootElement.FindFirst(TreeScope.Children, conds);
            }            

            return automationElement;
        }

        internal static AutomationElement FindApplicationByProcessId(int processId, ControlType mainWindowControltype)
        {
            int sleepCounter = 0;
            AutomationElement automationElement = null;
            
            AndCondition andConds = new AndCondition(new PropertyCondition(AutomationElement.ProcessIdProperty, processId), new PropertyCondition(AutomationElement.ControlTypeProperty, mainWindowControltype));

            while (automationElement == null && (sleepCounter <= UIAutomationSettings.WaitForApplication))
            {
                sleepCounter += UIAutomationSettings.ThreadSleepTime;
                Thread.Sleep(UIAutomationSettings.ThreadSleepTime);
                automationElement = AutomationElement.RootElement.FindFirst(TreeScope.Children, andConds);
            }

            return automationElement;
        }

        internal static AutomationElement FindElementByName(AutomationElement rootElement, string name)
        {
            PropertyCondition conds = new PropertyCondition(AutomationElement.NameProperty, name, PropertyConditionFlags.IgnoreCase);            
            AutomationElement automationElement = rootElement.FindFirst(TreeScope.Subtree, conds);

            if (automationElement == null)
            {
                throw new UIAutomationElementNotFoundException(null, "Automation Element not found by Name!");
            }

            return automationElement;
        }

        internal static AutomationElement FindElementByClassName(AutomationElement rootElement, string className)
        {
            PropertyCondition conds = new PropertyCondition(AutomationElement.ClassNameProperty, className, PropertyConditionFlags.IgnoreCase);            
            AutomationElement automationElement = rootElement.FindFirst(TreeScope.Subtree, conds);

            if (automationElement == null)
            {
                throw new UIAutomationElementNotFoundException(null, "Automation Element not found by Class Name!");
            }

            return automationElement;
        }

        internal static AutomationElement FindElementById(AutomationElement rootElement, string id)
        {
            PropertyCondition conds = new PropertyCondition(AutomationElement.AutomationIdProperty, id);            
            AutomationElement automationElement = rootElement.FindFirst(TreeScope.Subtree, conds);
            
            if (automationElement == null)
            {
                throw new UIAutomationElementNotFoundException(null, "Automation Element not found by Automation Id!");
            }

            return automationElement;
        }

        internal static AutomationElement FindElementByType(AutomationElement rootElement, ControlType type)
        {
            PropertyCondition conds = new PropertyCondition(AutomationElement.ControlTypeProperty, type);            
            AutomationElement automationElement = rootElement.FindFirst(TreeScope.Subtree, conds);

            if (automationElement == null)
            {
                throw new UIAutomationElementNotFoundException(null, "Automation Element not found by Control Type!");
            }

            return automationElement;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootElement"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        internal static AutomationElementCollection FindAllElementsByName(AutomationElement rootElement, string name)
        {
            return FindAllElementsByName(rootElement, name, TreeScope.Subtree);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootElement"></param>
        /// <param name="name"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        internal static AutomationElementCollection FindAllElementsByName(AutomationElement rootElement, string name, TreeScope scope)
        {
            PropertyCondition conds = new PropertyCondition(AutomationElement.NameProperty, name, PropertyConditionFlags.IgnoreCase);
            AutomationElementCollection automationElementCollection = rootElement.FindAll(scope, conds);
            return automationElementCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootElement"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static AutomationElementCollection FindAllElementsByType(AutomationElement rootElement, ControlType type)
        {
            return FindAllElementsByType(rootElement, type, TreeScope.Subtree);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootElement"></param>
        /// <param name="type"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        internal static AutomationElementCollection FindAllElementsByType(AutomationElement rootElement, ControlType type, TreeScope scope)
        {
            PropertyCondition conds = new PropertyCondition(AutomationElement.ControlTypeProperty, type);            

            AutomationElementCollection automationElementCollection = rootElement.FindAll(scope, conds);

            if (automationElementCollection.Count == 0)
            {
                throw new UIAutomationElementNotFoundException(null, "Automation Elements not found by Type!");
            }

            return automationElementCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootElement"></param>
        /// <param name="className"></param>
        /// <returns></returns>
        internal static AutomationElementCollection FindAllElementsByClassName(AutomationElement rootElement, string className)
        {
            return FindAllElementsByClassName(rootElement, className, TreeScope.Subtree);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootElement"></param>
        /// <param name="className"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        internal static AutomationElementCollection FindAllElementsByClassName(AutomationElement rootElement, string className, TreeScope scope)
        {
            PropertyCondition conds = new PropertyCondition(AutomationElement.ClassNameProperty, className, PropertyConditionFlags.IgnoreCase);            
            AutomationElementCollection automationElementCollection = rootElement.FindAll(scope, conds);
            return automationElementCollection;
        }

        internal static AutomationElement FindElementByAndConditions(AutomationElement rootElement, Condition[] conditions, TreeScope scope)
        {
            AndCondition andConds = new AndCondition(conditions);            
            AutomationElement automationElement = rootElement.FindFirst(scope, andConds);

            if (automationElement == null)
            {
                throw new UIAutomationElementNotFoundException(null, "Automation Element not found by AndConditions!");
            }

            return automationElement;
        }

        internal static AutomationElement FindElementByOrConditions(AutomationElement rootElement, Condition[] conditions, TreeScope scope)
        {
            OrCondition orConds = new OrCondition(conditions);            
            AutomationElement automationElement = rootElement.FindFirst(scope, orConds);

            if (automationElement == null)
            {
                throw new UIAutomationElementNotFoundException(null, "Automation Element not found by OrConditions!");
            }

            return automationElement;

        }

        internal static AutomationElement FindElementByNotCondition(AutomationElement rootElement, Condition condition, TreeScope scope)
        {
            NotCondition notConds = new NotCondition(condition);                        
            AutomationElement automationElement = rootElement.FindFirst(scope, notConds);

            if (automationElement == null)
            {
                throw new UIAutomationElementNotFoundException(null, "Automation Element not found by NotCondition!");
            }

            return automationElement;
        }        

        /// <summary>
        /// Tries to find all the elements which statify the conditions. 
        /// </summary>
        /// <param name="rootElement">Root element in which of search</param>
        /// <param name="conditions">Conditions</param>        
        internal static AutomationElementCollection FindAllElementsByAndConditions(AutomationElement rootElement, Condition[] conditions)
        {
            return FindAllElementsByAndConditions(rootElement, conditions, TreeScope.Subtree);
        }

        /// <summary>
        /// Tries to find all the elements which statify the conditions. 
        /// </summary>
        /// <param name="rootElement">Root element in which of search</param>
        /// <param name="conditions">Conditions</param>        
        /// <param name="scope">Scope of search method</param>
        /// <returns></returns>
        internal static AutomationElementCollection FindAllElementsByAndConditions(AutomationElement rootElement, Condition[] conditions, TreeScope scope)
        {
            AndCondition andConds = new AndCondition(conditions);            
            AutomationElementCollection automationElementCollection = rootElement.FindAll(scope, andConds);
            return automationElementCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootElement"></param>
        /// <param name="conditions"></param>
        /// <returns></returns>
        internal static AutomationElementCollection FindAllElementsByOrConditions(AutomationElement rootElement, Condition[] conditions)
        {
            return FindAllElementsByOrConditions(rootElement, conditions, TreeScope.Subtree);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootElement"></param>
        /// <param name="conditions"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        internal static AutomationElementCollection FindAllElementsByOrConditions(AutomationElement rootElement, Condition[] conditions, TreeScope scope)
        {
            OrCondition orConds = new OrCondition(conditions);            
            AutomationElementCollection automationElementCollection = rootElement.FindAll(scope, orConds);            
            return automationElementCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootElement"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        internal static AutomationElementCollection FindAllElementsByNotCondition(AutomationElement rootElement, Condition condition)
        {
            return FindAllElementsByNotCondition(rootElement, condition, TreeScope.Subtree);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootElement"></param>
        /// <param name="condition"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        internal static AutomationElementCollection FindAllElementsByNotCondition(AutomationElement rootElement, Condition condition, TreeScope scope)
        {
            NotCondition notConds = new NotCondition(condition);            
            AutomationElementCollection automationElementCollection = rootElement.FindAll(scope, notConds);
            return automationElementCollection;
        }
    }

    /// <summary>
    /// Specifies basis value for finding method.
    /// </summary>
    public enum FindBy
    {
        /// <summary>Specifies that the findig should be based on the Name of UIControl</summary>
        Name,
        /// <summary>Specifies that the findig should be based on the AutomationId of UIControl</summary>
        AutomationId,
        /// <summary>Specifies that the findig should be based on the ClassName of UIControl</summary>
        ClassName,
        /// <summary>Specifies that the findig should be based on the ControlType of UIControl</summary>
        Type
    }

}
