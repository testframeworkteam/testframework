using System.Collections;
using PowerTester.Framework.UI.Controls;

namespace PowerTester.Framework.UI.BaseClasses
{
    /// <summary>
    /// Represents a collection of UIGridItem objects
    /// </summary>   
    public class UIGridItemCollection : CollectionBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="UIGridItemCollection"/> class.
        /// </summary>
        public UIGridItemCollection() { }


        /// <summary>
        /// Adds the specified header item.
        /// </summary>
        /// <param name="gridItem">The header item.</param>
        public virtual void Add(UIGridItem gridItem)
        {
            List.Add(gridItem);
        }


        /// <summary>
        /// Gets the <see cref="UIGridItem"/> at the specified index.
        /// </summary>
        /// <value></value>
        public virtual UIGridItem this[int Index]
        {
            get
            {
                return List[Index] as UIGridItem;
            }
        }


        /// <summary>
        /// Gets the <see cref="UIGridItem"/> with the specified name.
        /// </summary>
        /// <value></value>
        public virtual UIGridItem this[string Name]
        {
            get
            {
                object gridItem = null;

                for (int i = 0; i <= List.Count; i++)
                {
                    if (((UIGridItem)List[i]).Name.ToLower() == Name.ToLower())
                    {
                        gridItem = List[i];
                        break;
                    }
                }

                return (UIGridItem)gridItem;
            }
        }

        /// <summary>
        /// Sorts the inner list based on the specified comparer.
        /// </summary>
        /// <param name="comparer">The comparer.</param>
        public virtual void Sort(IComparer comparer)
        {
            base.InnerList.Sort(comparer);
        }
    }
}

