using System.Collections;
using PowerTester.Framework.UI.Controls;

namespace PowerTester.Framework.UI.BaseClasses
{
    /// <summary>
    /// Represents a collection of UIHeaderItem objects
    /// </summary>   
    public class UIHeaderItemCollection : CollectionBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UIHeaderItemCollection"/> class.
        /// </summary>
        public UIHeaderItemCollection() { }


        /// <summary>
        /// Adds the specified data item.
        /// </summary>
        /// <param name="gridItem">The data item.</param>
        public virtual void Add(UIHeaderItem headerItem)
        {
            List.Add(headerItem);
        }


        /// <summary>
        /// Gets the <see cref="UIHeaderItem"/> at the specified index.
        /// </summary>
        /// <value></value>
        public virtual UIHeaderItem this[int Index]
        {
            get
            {
                return List[Index] as UIHeaderItem;
            }
        }

        /// <summary>
        /// Gets the <see cref="UIHeaderItem"/> with the specified name.
        /// </summary>
        /// <value></value>
        public virtual UIHeaderItem this[string Name]
        {
            get
            {
                object headerItem = null;

                for (int i = 0; i <= List.Count; i++)
                {
                    if (((UIHeaderItem)List[i]).Name.ToLower() == Name.ToLower())
                    {
                        headerItem = List[i];
                        break;
                    }
                }

                return (UIHeaderItem)headerItem;
            }
        }

    }
}

