using System.Collections;
using PowerTester.Framework.UI.Controls;

namespace PowerTester.Framework.UI.BaseClasses
{
    /// <summary>
    /// Represents a collection of UIListItem objects
    /// </summary>   
    public class UIListItemCollection : CollectionBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="UIListItemCollection"/> class.
        /// </summary>
        public UIListItemCollection() { }

        /// <summary>
        /// Adds the specified list item.
        /// </summary>
        /// <param name="listItem">The list item.</param>
        public virtual void Add(UILisItem listItem)
        {
            List.Add(listItem);
        }

        /// <summary>
        /// Gets the <see cref="UILisItem"/> at the specified index.
        /// </summary>
        /// <value></value>
        public virtual UILisItem this[int Index]
        {
            get
            {
                return (UILisItem)List[Index];
            }
        }

        /// <summary>
        /// Gets the <see cref="UILisItem"/> with the specified name.
        /// </summary>
        /// <value></value>
        public virtual UILisItem this[string Name]
        {
            get
            {
                object listItem = null;

                for (int i = 0; i <= List.Count; i++)
                {
                    if (((UILisItem)List[i]).Name.ToLower() == Name.ToLower())
                    {
                        listItem = List[i];
                        break;
                    }
                }

                return (UILisItem)listItem;
            }
        }

    }
}

