using System.Collections;
using PowerTester.Framework.UI.Controls;

namespace PowerTester.Framework.UI.BaseClasses
{
    /// <summary>
    /// Represents a collection of UIMenuItem objects
    /// </summary>   
    public class UIMenuItemCollection : CollectionBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="UIMenuItemCollection"/> class.
        /// </summary>
        public UIMenuItemCollection() { }


        /// <summary>
        /// Adds the specified menu item.
        /// </summary>
        /// <param name="tabItem">The menu item.</param>
        public virtual void Add(UIMenuItem menuItem)
        {
            List.Add(menuItem);
        }

        /// <summary>
        /// Gets the <see cref="UIMenuItem"/> at the specified index.
        /// </summary>
        /// <value></value>
        public virtual UIMenuItem this[int Index]
        {
            get
            {
                return List[Index] as UIMenuItem;
            }
        }

        /// <summary>
        /// Gets the <see cref="UIMenuItem"/> with the specified name.
        /// </summary>
        /// <value></value>
        public virtual UIMenuItem this[string Name]
        {
            get
            {
                object menuItem = null;

                for (int i = 0; i <= List.Count; i++)
                {
                    if (((UIMenuItem)List[i]).Name.ToLower() == Name.ToLower())
                    {
                        menuItem = List[i];
                        break;
                    }
                }

                return (UIMenuItem)menuItem;
            }
        }

    }
}

