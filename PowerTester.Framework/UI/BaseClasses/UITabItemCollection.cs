using System.Collections;
using PowerTester.Framework.UI.Controls;

namespace PowerTester.Framework.UI.BaseClasses
{
    /// <summary>
    /// Represents a collection of UITabItem objects
    /// </summary>   
    public class UITabItemCollection : CollectionBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="UITabItemCollection"/> class.
        /// </summary>
        public UITabItemCollection() { }


        /// <summary>
        /// Adds the specified tab item.
        /// </summary>
        /// <param name="tabItem">The tab item.</param>
        public virtual void Add(UITabItem tabItem)
        {
            List.Add(tabItem);
        }

        /// <summary>
        /// Gets the <see cref="UITabItem"/> at the specified index.
        /// </summary>
        /// <value></value>
        public virtual UITabItem this[int Index]
        {
            get
            {
                return List[Index] as UITabItem;
            }
        }

        /// <summary>
        /// Gets the <see cref="UITabItem"/> with the specified name.
        /// </summary>
        /// <value></value>
        public virtual UITabItem this[string Name]
        {
            get
            {
                object tabItem = null;

                for (int i = 0; i <= List.Count; i++)
                {
                    if (((UITabItem)List[i]).Name.ToLower() == Name.ToLower())
                    {
                        tabItem = List[i];
                        break;
                    }
                }

                return (UITabItem)tabItem;
            }
        }

    }
}

