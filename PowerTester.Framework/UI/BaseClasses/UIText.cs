using System;
using System.Diagnostics;
using System.Windows.Automation;
using System.Windows.Input;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.BaseClasses
{
    /// <summary>
    /// Represents an UIText abstract class
    /// </summary>
    public abstract class UIText : UIControl, IUIText
    {
        private AutomationElement root;
        
        /// <summary>
        /// Initializes a new instance the UIText class
        /// </summary>
        /// <param name="rootElement"></param>
        public UIText(AutomationElement rootElement): base(rootElement)
        {
            root = rootElement;
        }

        /// <summary>
        /// Gets or sets a value of UIControl.
        /// </summary>
        public string Value
        {
            get
            {
                ValuePattern valuePattern = null;

                if (IsValuePatternAvailable)
                {
                    valuePattern = base.Element.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;
                }
                else
                {
                    throw new PatternNotSupportedException(this, "ValuePattern is not supported on this control.");
                }

                return valuePattern.Current.Value.ToString();
            }
            set
            {                
                if (IsValuePatternAvailable)
                {
                    ValuePattern valuePattern = base.Element.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;
                    valuePattern.SetValue(value);
                }
                else
                {
                    throw new PatternNotSupportedException(this, "ValuePattern is not supported on this control.");
                }

            }
        }


        /// <summary>
        /// Gets a value which indicates if the UIControl is enabled.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                ValuePattern valuePattern = null;

                if (IsValuePatternAvailable)
                {
                    valuePattern = Element.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;
                }
                else
                {
                    throw new PatternNotSupportedException(this, "ValuePattern is not supported on this control.");
                }
                
                return valuePattern.Current.IsReadOnly;
            }
        }

        /// <summary>
        /// Types the give text into the UIControl control
        /// </summary>
        /// <param name="text">Text to be typed</param>
        public void Type(string text)
        {
            try
            {
                SetFocus();
            }
            catch (InvalidOperationException) { }

            Input.SendUnicodeString(text, 1, 50);

            Logger.Debug(string.Format("The '{0}' text was typed into '{1}' control", text, GetType().Name));
        }

        /// <summary>
        /// Types the give text into the UIControl control
        /// </summary>
        /// <param name="text">Text to be typed</param>
        public void Type(string text, int sleepLength)
        {
            try
            {
                SetFocus();
            }
            catch (InvalidOperationException) { }

            Input.SendUnicodeString(text, 1, sleepLength);

            Logger.Debug(string.Format("The '{0}' text was typed into '{1}' control", text, GetType().Name));
        }

        /// <summary>
        /// Injects a unicode character as keyboard input into the system
        /// </summary>
        /// <param name="key">indicates the key to be pressed or released. Can be any unicode character</param>
        /// <param name="press">true to inject a key press, false to inject a key release</param>
        public void KeyBoardInput(Key key, bool press)
        {
            SetFocus();
            Input.SendKeyboardInput(key, press);
        }

        /// <summary>
        /// Returns the plain text of the text range.
        /// </summary>        
        public string GetText()
        {
            return GetText(-1);
        }

        /// <summary>
        /// Returns the plain text of the text range.
        /// </summary>
        /// <param name="maxLength">The maximum length of the string to return. Use -1 if no limit is required.</param>        
        public string GetText(int maxLength)
        {
            TextPattern textPattern = null;

            if (IsTextPatternAvailable)
            {
                textPattern = base.Element.GetCurrentPattern(TextPattern.Pattern) as TextPattern;
            }
            else
            {
                throw new PatternNotSupportedException(this, "TextPattern is not supported on this control.");
            }

            return textPattern.DocumentRange.GetText(maxLength);            
        }

    }
}
