﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace PowerTester.Framework.UI.BaseClasses
{
    public static class UIWaitFor
    {

        /// <summary>
        /// Waits for the given controls to become accessible.
        /// </summary>
        /// <param name="uiDefinitionTypePropertyPairs">The UI definition type and property pairs.</param>
        /// <returns>True if the control exists otherwise false</returns>
        public static bool WaitFor(UIDefinitionTypePropertyPair[] uiDefinitionTypePropertyPairs)
        {
            bool uiExist = true;

            foreach (UIDefinitionTypePropertyPair uiDefinitionTypePropertyPair in uiDefinitionTypePropertyPairs)
            {
                uiExist = WaitFor(uiDefinitionTypePropertyPair.UiDefinitionClassType, uiDefinitionTypePropertyPair.TimeOut, uiDefinitionTypePropertyPair.UiControlProperties) && uiExist;
            }

            return uiExist;
        }
                        
        /// <summary>
        /// Waits for the given controls to become accessible.
        /// </summary>
        /// <param name="uiDefinitionTypePropertyPairs">The UI definition type and property pairs.</param>
        /// <param name="timeOut">The time out value.</param>
        /// <returns>True if the control exists otherwise false</returns>
        public static bool WaitFor(List<UIDefinitionTypePropertyPair> uiDefinitionTypePropertyPairs, int timeOut)
        {
            bool uiExist = true;
            foreach (UIDefinitionTypePropertyPair uiDefinitionTypePropertyPair in uiDefinitionTypePropertyPairs)
            {
                uiExist = WaitFor(uiDefinitionTypePropertyPair.UiDefinitionClassType, timeOut, uiDefinitionTypePropertyPair.UiControlProperties) && uiExist;
            }
            return uiExist;
        }

        /// <summary>
        /// Waits for the given controls to become accessible.
        /// </summary>
        /// <param name="uiDefinitionClassType">Type of the UI definition class contaning the defined property.</param>
        /// <param name="uiControlProperties">The array of properties which are to give back an UIControl</param>
        /// <returns>True if the control exists otherwise false</returns>
        public static bool WaitFor(Type uiDefinitionClassType, params string[] uiControlProperties)
        {
            bool uiExist = true;
            foreach (string property in uiControlProperties)
            {
                uiExist = WaitFor(uiDefinitionClassType, property, 60) && uiExist;
            }
            return uiExist;
        }

        /// <summary>
        /// Waits for the given controls to become accessible.
        /// </summary>
        /// <param name="uiDefinitionClassType">Type of the UI definition class contaning the defined property.</param>
        /// <param name="timeOut">The value of time out.</param>
        /// <param name="uiControlProperties">The array of properties which are to give back an UIControl</param>
        /// <returns>True if the control exists otherwise false</returns>
        public static bool WaitFor(Type uiDefinitionClassType, int timeOut, params string[] uiControlProperties)
        {
            bool uiExist = true;
            foreach (string property in uiControlProperties)
            {
                uiExist = WaitFor(uiDefinitionClassType, property, timeOut) && uiExist;
            }

            return uiExist;
        }

        /// <summary>
        /// Waits for the given controls to become accessible.
        /// </summary>
        /// <param name="uiDefinitionClassType">Type of the UI definition class contaning the defined property.</param>
        /// <param name="timeOut">The time out.</param>
        /// <param name="hierarchycal">If the uiControlProperties hierarchycal, exits if an upper level UI is not presented (start the list with the most upper ui) </param>
        /// <param name="uiControlProperties">The array of properties which are to give back an UIControl</param>
        /// <returns>True if the control exists otherwise false</returns>
        public static bool WaitFor(Type uiDefinitionClassType, int timeOut, bool hierarchycal, params string[] uiControlProperties)
        {

            bool uiExist = true;
            foreach (string property in uiControlProperties)
            {
                uiExist = WaitFor(uiDefinitionClassType, property, timeOut) && uiExist;
                if (hierarchycal)
                {
                    if (!uiExist) { break; }
                }
            }

            return uiExist;
        }

        /// <summary>
        /// Waits for the given controls to become accessible.
        /// </summary>
        /// <param name="uiDefinitionClassType">Type of the UI definition class contaning the defined property.</param>
        /// <param name="uiControlProperty">The property is to give back an UIControl</param>
        /// <returns>True if the control exists otherwise false</returns>
        public static bool WaitFor(Type uiDefinitionClassType, string uiControlProperty)
        {
            return WaitFor(uiDefinitionClassType, uiControlProperty, 60);
        }

        /// <summary>
        /// Waits for the given controls to become accessible.
        /// </summary>
        /// <param name="uiDefinitionClassType">The UI definition class object.</param>
        /// <param name="uiControlProperty">The amount of time it waits for the control to become available.</param>
        /// <returns>True if the control exists otherwise false</returns>
        public static bool WaitFor(object uiDefinitionClassObject, string uiControlProperty)
        {
            if (uiDefinitionClassObject == null)
            {
                throw new ArgumentNullException("uiDefinitionClassObject");
            }

            return WaitFor(uiDefinitionClassObject, uiControlProperty, 60);
        }

        /// <summary>
        /// Waits for the given controls to become accessible.
        /// </summary>
        /// <param name="uiDefinitionClassType">Type of the UI definition class contaning the defined property.</param>
        /// <param name="uiControlProperty">The UI control property.</param>
        /// <param name="timeOut">The amount of time it waits for the control to become available.</param>
        /// <returns>True if the control exists otherwise false</returns>
        public static bool WaitFor(Type uiDefinitionClassType, string uiControlProperty, int timeOut)
        {            
            return WaitFor(uiDefinitionClassType, uiControlProperty, timeOut, null);
        }

        /// <summary>
        /// Waits for the given controls to become accessible.
        /// </summary>
        /// <param name="uiDefinitionClassObject">The UI definition class object.</param>
        /// <param name="uiControlProperty">The UI control property.</param>
        /// <param name="timeOut">The amount of time it waits for the control to become available.</param>
        /// <returns>True if the control exists otherwise false</returns>
        public static bool WaitFor(object uiDefinitionClassObject, string uiControlProperty, int timeOut)
        {
            if (uiDefinitionClassObject == null)
            {
                throw new ArgumentNullException("uiDefinitionClassObject");
            }

            return WaitFor(uiDefinitionClassObject.GetType(), uiControlProperty, timeOut, uiDefinitionClassObject);
        }

        /// <summary>
        /// Waits for any of the given controls using their class types. If any of them becomes accessible it returns a the type otherwise the return value is null!
        /// </summary>
        /// <param name="uiDefinitionTypePropertyPairs">WaitForAny to Find a specified dialog from a list</param>
        /// <returns>Type which contains the contarol became accessible</returns>
        public static Type WaitForAny(UIDefinitionTypePropertyPair[] uiDefinitionTypePropertyPairs, int search_timeOut)
        {                         
            Stopwatch timing = new Stopwatch();
            timing.Start();

            do
            {
                foreach (UIDefinitionTypePropertyPair uiDefinitionTypePropertyPair in uiDefinitionTypePropertyPairs)
                {
                    Type currentType = uiDefinitionTypePropertyPair.UiDefinitionClassType;

                    if (WaitFor(uiDefinitionTypePropertyPair.UiDefinitionClassType, uiDefinitionTypePropertyPair.TimeOut, true, uiDefinitionTypePropertyPair.UiControlProperties))
                    {
                        return currentType;
                    }
                }

            } while (search_timeOut > timing.Elapsed.Seconds);

            return null;
        }

        private static bool WaitFor(Type uiDefinitionClassType, string uiControlProperty, int timeOut, object uiDefinitionClassObject)
        {
            bool uiExist = false;
            bool propertyFound = false;

            #region Let's check whether the given property exists

            foreach (var property in uiDefinitionClassType.GetProperties())
            {
                if (property.Name.ToLower().Equals(uiControlProperty.ToLower()))
                {
                    propertyFound = true;
                    break;
                }
            }   

            if (!propertyFound)
            {
                throw new ArgumentException(string.Format("The {0} property does not exist!", uiControlProperty));
            }

            #endregion

            //Let's see whether the property is a UIControl or a descendant
            if (!(uiDefinitionClassType.GetProperty(uiControlProperty).PropertyType.IsSubclassOf(typeof(UIControl)) 
                || uiDefinitionClassType.GetProperty(uiControlProperty).PropertyType == typeof(UIControl)))
            {
                throw new ArgumentException("The return type of the specified property is not UIControl", uiControlProperty);
            }

            int iterationWhenWatingForControl = UIAutomationSettings.IterationWhenWatingForControl;
            int waitForUIControl = UIAutomationSettings.WaitForUIControl;

            UIAutomationSettings.IterationWhenWatingForControl = 0;
            UIAutomationSettings.WaitForUIControl = 0;

            try
            {
                int i = 0;
                DateTime start = DateTime.Now;

                while (start.AddSeconds(timeOut) > DateTime.Now)
                {
                    try
                    {
                        if ((uiDefinitionClassType.GetProperty(uiControlProperty).GetValue(uiDefinitionClassObject, null) != null)
                            && ((UIControl)uiDefinitionClassType.GetProperty(uiControlProperty).GetValue(uiDefinitionClassObject, null)).Exists
                            && ((UIControl)uiDefinitionClassType.GetProperty(uiControlProperty).GetValue(uiDefinitionClassObject, null)).IsEnabled)
                        {
                            uiExist = true;
                            break;
                        }
                    }
                    catch (System.Reflection.TargetInvocationException e) 
                    { 
                        if ((e.InnerException == null) || 
                            ((e.InnerException != null) && 
                            ((!e.InnerException.GetType().IsSubclassOf(typeof(UIAutomationException)) 
                            && (!e.InnerException.GetType().Equals(typeof(System.NullReferenceException))
                            && (!e.InnerException.GetType().Equals(typeof(System.ArgumentOutOfRangeException))))))))
                        {
                            throw e;
                        }                        
                    }

                    Thread.Sleep(200);
                    i++;
                }
            }            
            finally
            {
                UIAutomationSettings.IterationWhenWatingForControl = iterationWhenWatingForControl;
                UIAutomationSettings.WaitForUIControl = waitForUIControl;
            }

            return uiExist;
        }
    }

    [Serializable]
    public struct UIDefinitionTypePropertyPair
    {
        private Type _uiDefinitionClassType;
        private string[] _uiControlProperties;
        private int _timeOut;

        public int TimeOut
        {
            get { return _timeOut; }
        }

        /// <summary>
        /// Gets the type of the UI definition class.
        /// </summary>
        /// <value>The type of the UI definition class.</value>
        public Type UiDefinitionClassType
        {
            get { return _uiDefinitionClassType; }
        }

        /// <summary>
        /// Gets the UI control properties.
        /// </summary>
        /// <value>The UI control properties.</value>
        public string[] UiControlProperties
        {
            get { return _uiControlProperties; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIDefinitionTypePropertyPair"/> struct.
        /// </summary>
        /// <param name="uiDefinitionClassType">Type of the UI definition class.</param>
        /// <param name="uiControlProperty">The UI control property.</param>
        public UIDefinitionTypePropertyPair(Type uiDefinitionClassType, string[] uiControlProperty)
        {
            _uiDefinitionClassType = uiDefinitionClassType;
            _uiControlProperties = uiControlProperty;
            _timeOut = 60;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UIDefinitionTypePropertyPair"/> struct.
        /// </summary>
        /// <param name="uiDefinitionClassType">Type of the UI definition class.</param>
        /// <param name="uiControlProperty">The UI control property.</param>
        public UIDefinitionTypePropertyPair(Type uiDefinitionClassType, string[] uiControlProperty, int timeOut)
        {
            _uiDefinitionClassType = uiDefinitionClassType;
            _uiControlProperties = uiControlProperty;
            _timeOut = timeOut;
        }

    }

}
