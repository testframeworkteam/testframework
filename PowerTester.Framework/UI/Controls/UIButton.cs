using System;
using System.Diagnostics;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Button window control.
    /// </summary>
    [Serializable]
    public class UIButton : UIControl, IUIButton
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UIButton"/> class. 
        /// Initializes a new instance of UIButton class
        /// </summary>
        /// <param name="element">
        /// Automation element
        /// </param>
        public UIButton(AutomationElement element) : base(element)
        {
            if ((element != null) && (!IsInvokePatternAvailable))
            {
                throw new PatternNotSupportedException(this, "InvokePattern is not supported on this control!");
            }


            if ((element != null) && IsTogglePatternAvailable)
            {
            }
        }

        #region IUIButton Members

        /// <summary>
        /// Gets values that specify the ToggleState of UIButton control
        /// </summary>
        public ToggleState ToggleState
        {
            get
            {
                var togglePattern = Element.GetCurrentPattern(TogglePattern.Pattern) as TogglePattern;
                return togglePattern.Current.ToggleState;
            }
        }

        /// <summary>
        /// Sends a request to activate a control and initiate its single, unambiguous action. 
        /// </summary>
        public void Invoke()
        {
            var invoke = Element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
            invoke.Invoke();
        }

        /// <summary>
        /// Cycles through the toggle states of an UIButton. 
        /// </summary>
        public void Toggle()
        {
            var togglePattern = Element.GetCurrentPattern(TogglePattern.Pattern) as TogglePattern;
            togglePattern.Toggle();

            Logger.Debug(string.Format("The '{0}' UIButton was toggled.", Name));
        }

        /// <summary>
        /// Sets the toggle state of UIButton.
        /// </summary>
        /// <param name="state">
        /// Toggle state
        /// </param>
        public void SetToggleState(ToggleState state)
        {
            if ((ToggleState == ToggleState.On) && (state == ToggleState.Off))
            {
                Toggle();
            }

            if ((ToggleState == ToggleState.Off) && (state == ToggleState.On))
            {
                Toggle();
            }
        }

        /// <summary>
        /// Sets the toggle state of UIButton.
        /// </summary>
        /// <param name="toggled">
        /// Toggle state
        /// </param>
        public void SetToggleState(bool toggled)
        {
            if ((ToggleState == ToggleState.On) && (toggled == false))
            {
                Toggle();
            }

            if ((ToggleState == ToggleState.Off) && (toggled == true))
            {
                Toggle();
            }
        }

         /// <summary>
        /// Sets the toggle state of UIButton by clickong on it.
        /// </summary>
        /// <param name="toggled">
        /// Toggle state
        /// </param>
        public void SetToggleStateByMouseClick(ToggleState state)
        {
            if ((ToggleState == ToggleState.On) && (state == ToggleState.Off))
            {
                Click();
            }

            if ((ToggleState == ToggleState.Off) && (state == ToggleState.On))
            {
                Click();
            }
        }

        /// <summary>
        /// Sets the toggle state of UIButton by clickong on it.
        /// </summary>
        /// <param name="toggled">
        /// Toggle state
        /// </param>
        public void SetToggleStateByMouseClick(bool toggled)
        {
            if ((ToggleState == ToggleState.On) && (toggled == false))
            {
                Click();
            }

            if ((ToggleState == ToggleState.Off) && (toggled == true))
            {
                Click();
            }
        }




        #endregion
    }
}