using System;
using System.Diagnostics;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Check Box window control.
    /// </summary>
    public class UICheckBox : UIControl, IUICheckBox
    {
        /// <summary>
        /// Initializes a new instance of the UICheckBox class.
        /// </summary>
        /// <param name="element">Automation Element</param>
        public UICheckBox(AutomationElement element) : base(element)
        {
            if ((element != null) && (!IsTogglePatternAvailable))
            {
                throw new PatternNotSupportedException(this, "TogglePattern not supported on this control !");
            }
        }

        /// <summary>
        /// Gets values that specify the ToggleState of UICheckBox control
        /// </summary>
        public ToggleState ToggleState
        {
            get
            {
                var togglePattern = Element.GetCurrentPattern(TogglePattern.Pattern) as TogglePattern;
                return togglePattern.Current.ToggleState;
            }
        }

        /// <summary>
        /// Cycles through the toggle states of an UICheckBox. 
        /// </summary>
        public void Toggle()
        {
            var togglePattern = Element.GetCurrentPattern(TogglePattern.Pattern) as TogglePattern;
            
            togglePattern.Toggle();

            Logger.Debug(string.Format("The '{0}' UICheckBox was toggled.", Name));
        }

        /// <summary>
        /// Sets the toggle state of UICheckBox.
        /// </summary>
        /// <param name="state">Toggle state</param>
        public void SetToggleState(ToggleState state)
        {
            if ((ToggleState == ToggleState.On) && (state == ToggleState.Off))
            {
                Toggle();
            }

            if ((ToggleState == ToggleState.Off) && (state == ToggleState.On))
            {
                Toggle();
            }
        }

    }
}
