
using System.Windows.Automation;
using System.Windows.Input;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Combo Box window control.
    /// </summary>
    public class UIComboBox : UIControl, IUIComboBox
    {
        private readonly UIList list;

        /// <summary>
        /// Initializes a new instance of the UIComboBox class.
        /// </summary>
        /// <param name="element">
        /// Automation element
        /// </param>
        public UIComboBox(AutomationElement element) : base(element)
        {
            list = new UIList(UIFind.FindElementByType(Element, ControlType.List));
        }

        /// <summary>
        /// Gets the selected list item.
        /// </summary>
        /// <value>The selected list item.</value>
        public UILisItem SelectedListItem
        {
            get
            {
                var selectionPattern = Element.GetCurrentPattern(SelectionPattern.Pattern) as SelectionPattern;
                return new UILisItem(selectionPattern.Current.GetSelection()[0]);
            }
        }

        /// <summary>
        /// Gets the items of UIComboBox
        /// </summary>
        public UIListItemCollection ListItems
        {
            get { return list.ListItems; }
        }

        #region IUIComboBox Members

        /// <summary>
        /// Gets value of UIControl.
        /// </summary>
        public string Value
        {
            get
            {
                var valuePattern = Element.GetCurrentPattern(ValuePattern.Pattern) as ValuePattern;
                return valuePattern.Current.Value;
            }
        }

        /// <summary>
        /// Gets the Drop Down button of UIComboBox
        /// </summary>
        public UIButton DropDown
        {
            get { return new UIButton(UIFind.FindElementByType(Element, ControlType.Button)); }
        }

        /// <summary>
        /// Displays all child nodes, controls, or content of the control.
        /// </summary>
        public void Expand()
        {
            var expandCollapsePattern = Element.GetCurrentPattern(ExpandCollapsePattern.Pattern) as ExpandCollapsePattern;
            expandCollapsePattern.Expand();

            Logger.Debug(string.Format("The '{0}' UIComboBox was expanded.", Name));
        }

        /// <summary>
        /// Hides all nodes, controls, or content that are descendants of the control.
        /// </summary>
        public void Collapse()
        {
            var expandCollapsePattern = Element.GetCurrentPattern(ExpandCollapsePattern.Pattern) as ExpandCollapsePattern;
            expandCollapsePattern.Collapse();

            Logger.Debug(string.Format("The '{0}' UIComboBox was collapsed.", Name));
        }

        /// <summary>
        /// Types the give text into the UIControl control and hits the enter.
        /// </summary>
        /// <param name="text">
        /// Text to be typed
        /// </param>
        public void Type(string text)
        {
            SetFocus();
            Input.SendUnicodeString(text, 1, 50);
            Input.SendKeyboardInput(Key.Enter, true);

            Logger.Debug(string.Format("The '{0}' text was type in '{1}' UIComboBox.", text, Name));
        }

        /// <summary>
        /// Selects an UIListItem control by its name
        /// </summary>
        /// <param name="name">
        /// UIListIten control name
        /// </param>
        public void SelectItemByName(string name)
        {
            list.SelectItemByName(name);
        }

        #endregion
    }
}