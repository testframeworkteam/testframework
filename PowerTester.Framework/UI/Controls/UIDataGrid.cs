using System;
using System.Linq;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Data Grid window control.
    /// </summary>
    public class UIDataGrid : UIControl, IUIDataGrid
    {
        /// <summary>
        /// Initializes a new instance of the UIDataGrid class
        /// </summary>
        /// <param name="element">
        /// Automation element
        /// </param>
        public UIDataGrid(AutomationElement element) : base(element)
        {
        }

        /// <summary>
        /// Gets the header of the grid control
        /// </summary>
        /// <value>The header.</value>
        public UIHeader Header
        {
            get { return new UIHeader(UIFind.FindElementByType(Element, ControlType.Header)); }
        }

        /// <summary>
        /// Gets a collection of UIDataItems of UIDataGrid object.
        /// </summary>
        public UIDataItemCollection DataItems
        {
            get
            {
                var dataItemCollection = new UIDataItemCollection();

                try
                {
                    AutomationElementCollection automationElementCollection = UIFind.FindAllElementsByType(Element, ControlType.DataItem, TreeScope.Children);

                    foreach (AutomationElement automationElement in automationElementCollection)
                    {
                        dataItemCollection.Add(new UIDataItem(automationElement));
                    }
                }
                catch (UIAutomationElementNotFoundException)
                {
                }

                return dataItemCollection;
            }
        }

        /// <summary>
        /// Gets a collection of UIDataGrids of UIDataGrid object.
        /// </summary>
        public UIControlCollection<UIDataGrid> DataGrids
        {
            get
            {
                var dataGridCollection = new UIControlCollection<UIDataGrid>();

                try
                {
                    AutomationElementCollection automationElementCollection = UIFind.FindAllElementsByType(Element, ControlType.DataGrid, TreeScope.Children);

                    foreach (AutomationElement automationElement in automationElementCollection)
                    {
                        dataGridCollection.Add(new UIDataGrid(automationElement));
                    }
                }
                catch (UIAutomationElementNotFoundException)
                {
                }

                return dataGridCollection;
            }
        }

        /// <summary>
        /// Gets a collection of the selected UIDataItems of UIDataGrid object.
        /// </summary>
        /// <value>The selected data items.</value>
        public UIDataItemCollection SelectedDataItems
        {
            get
            {
                var selectedDataItemCollection = new UIDataItemCollection();
                
                foreach (UIDataItem dataItem in DataItems.Cast<UIDataItem>().Where(dataItem => dataItem.IsSelected))
                {
                    selectedDataItemCollection.Add(dataItem);
                }
                
                return selectedDataItemCollection;
            }
        }

        /// <summary>
        /// Selects all the items given in the list
        /// </summary>
        /// <param name="names">Name of items</param>
        public void SelectItemsByName(params string[] names)
        {
            SelectItemsByName(true, names);
        }

        /// <summary>
        /// Selects all the items given in the list
        /// </summary>
        /// <param name="deselectAllItemsBeforeSelection">if set to <c>true</c> [deselect all items before selection].</param>
        /// <param name="names">Name of items</param>
        public void SelectItemsByName(bool deselectAllItemsBeforeSelection, params string[] names)
        {
            if (deselectAllItemsBeforeSelection)
            {
                foreach (var dataitem in DataItems.Cast<UIDataItem>().Where(dataitem => dataitem.IsSelected))
                {
                    dataitem.RemoveFromSelection();
                }
            }

            foreach (var name in names)
            {
                DataItems[name].AddToSelection();
            }
        }

        /// <summary>
        /// Checks whether all of the provided items are selected.
        /// </summary>
        /// <param name="names">The name of the items to be verified.</param>
        /// <returns>True if all of the items are selected.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">If none of the provided values has a corresponding UIDataItem control!</exception>
        public bool AreItemsSelected(params string[] names)
        {
            var uiDataItems = DataItems.Cast<UIDataItem>().Where(uiDataItem =>
                                {
                                    return names.Where(name => name.ToLower().Equals(uiDataItem.Name.ToLower())).FirstOrDefault() != null;
                                });            

            if (uiDataItems == null)
            {
                throw new ArgumentOutOfRangeException(string.Format("None of the provided values ({0}) has a corresponding UIDataItem control!", names));
            }

            if (uiDataItems.Count() != names.Count())
            {
                var noutFoundNames = names.Where(name => uiDataItems.Where(uiDataItem => uiDataItem.Name.ToLower().Equals(name.ToLower())).FirstOrDefault() == null);
                throw new ArgumentOutOfRangeException(string.Format("The ({0}) value(s) do not have a corresponding UIDataItem control!", noutFoundNames.ToArray()));
            }

            return uiDataItems.Where(uiDataItem => !uiDataItem.IsSelected).FirstOrDefault() == null;
        }

        /// <summary>
        /// Checks whether all of the provided items are deselected.
        /// </summary>
        /// <param name="names">The name of the items to be verified.</param>
        /// <returns>True if all of the items are not selected.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">If none of the provided values has a corresponding UIDataItem control!</exception>
        public bool AreItemsDeselected(params string[] names)
        {
            var uiDataItems = DataItems.Cast<UIDataItem>().Where(uiDataItem =>
                                {
                                    return names.Where(name => name.ToLower().Equals(uiDataItem.Name.ToLower())).FirstOrDefault() != null;
                                });

            if (uiDataItems == null)
            {
                throw new ArgumentOutOfRangeException(string.Format("None of the provided values ({0}) has a corresponding UIDataItem control!", names));
            }

            if (uiDataItems.Count() != names.Count())
            {
                var noutFoundNames = names.Where(name => uiDataItems.Where(uiDataItem => uiDataItem.Name.ToLower().Equals(name.ToLower())).FirstOrDefault() == null);
                throw new ArgumentOutOfRangeException(string.Format("The ({0}) value(s) do not have a corresponding UIDataItem control!", noutFoundNames.ToArray()));
            }


            return uiDataItems.Where(uiDataItem => uiDataItem.IsSelected).FirstOrDefault() == null;
        }

        /// <summary>
        /// DeSelects all the items given in the list
        /// </summary>
        /// <param name="names">Name of the items to be deselected</param>        
        public void DeSelectItemsByName(params string[] names)
        {
            foreach (var name in names)
            {
                DataItems[name].RemoveFromSelection();
            }
        }

        /// <summary>
        /// Sets the scroll position in percent.
        /// </summary>
        /// <param name="horizontalPercent">
        /// The horizontal percent.
        /// </param>
        /// <param name="verticalPercent">
        /// The vertical percent.
        /// </param>
        /// <remarks>
        /// If the Scroll pattern is not supported then it throws and exception! 
        /// </remarks>
        public void SetScrollPercent(double horizontalPercent, double verticalPercent)
        {
            object scrollPattern = null;

            if (!Element.TryGetCurrentPattern(ScrollPattern.Pattern, out scrollPattern))
            {
                throw new PatternNotSupportedException(this, "ScrollPattern is not supported on this control !");
            }

            try
            {
                ((ScrollPattern)scrollPattern).SetScrollPercent(horizontalPercent, verticalPercent);
            }
            catch (InvalidOperationException)
            {
            }
        }

        /// <summary>
        /// Gets the scroll pattern information.
        /// </summary>
        /// <returns>
        /// Scroll pattern information
        /// </returns>
        public ScrollPattern.ScrollPatternInformation GetScrollPatternInformation()
        {
            object scrollPattern = null;

            if (!Element.TryGetCurrentPattern(ScrollPattern.Pattern, out scrollPattern))
            {
                throw new PatternNotSupportedException(this, "ScrollPattern is not supported on this control !");
            }

            return ((ScrollPattern)scrollPattern).Current;
        }
    }
}