using System;
using System.Diagnostics;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Data Item of Data Grid window control.
    /// </summary>
    public class UIDataItem : UIControl, IUIDataItem
    {
        /// <summary>
        /// Initializes a new instance of the UIDataItem class.
        /// </summary>
        /// <param name="element">Automation element</param>
        public UIDataItem(AutomationElement element) : base(element)
        {
            if ((element != null) && (!IsSelectionItemPatternAvailable))
            {
                throw new PatternNotSupportedException(this, "SelectionItemPattern is not supported on this control !");
            }
        }

        /// <summary>
        /// Gets a collection of UIGridItems of UIDataItem object.
        /// </summary>
        public UIGridItemCollection GridItems
        {
            get
            {
                var automationElementCollection = UIFind.FindAllElementsByType(Element, ControlType.Text);
                var gridItemCollection = new UIGridItemCollection();

                foreach (AutomationElement automationElement in automationElementCollection)
                {
                    gridItemCollection.Add(new UIGridItem(automationElement));
                }

                return gridItemCollection;
            }
        }

        /// <summary>
        /// Gets a value which shows whether the item is enabled or not.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                var selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
                return selectionItemPattern.Current.IsSelected;
            }
        }

        /// <summary>
        /// Selects the UIDataItem
        /// </summary>
        public void Select()
        {
            var selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
            selectionItemPattern.Select();

            Logger.Debug(string.Format("The '{0}' UIDataItem was selected.", Name));
        }

        /// <summary>
        /// Selects the UIDataItem and adds it to the selected list item collection
        /// </summary>
        public void AddToSelection()
        {
            var selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
            selectionItemPattern.AddToSelection();

            Logger.Debug(string.Format("The '{0}' UIDataItem was added to selection.", Name));
        }

        /// <summary>
        /// DeSelects the UIDataItem and removes it from the selected list item collection
        /// </summary>
        public void RemoveFromSelection()
        {
            var selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
            selectionItemPattern.RemoveFromSelection();

            Logger.Debug(string.Format("The '{0}' UIDataItem was removed from selection.", Name));
        }
    }
}
