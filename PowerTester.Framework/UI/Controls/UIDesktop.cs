
using System;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents the Desktop root object. It is a singleton. 
    /// </summary>
    /// <remarks>Find functions of the class just check the children element of the Desktop.</remarks>
    public sealed class UIDesktop : UIContainer, IUIDesktop
    {
        private static readonly UIDesktop _instance = new UIDesktop(AutomationElement.RootElement);

        /// <summary>
        /// Initializes a new instance of the <see cref="UIDesktop"/> class.
        /// </summary>
        /// <param name="element">The automation element.</param>
        private UIDesktop(AutomationElement element) : base(element)
        {
        }

        /// <summary>
        /// Gets the instance of the UIDesktop class
        /// </summary>
        public static UIDesktop Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        /// Registers a method that will handle focus-changed events. 
        /// </summary>
        /// <remarks>
        /// Focus-changed events are system-wide; you cannot set a narrower scope.
        /// </remarks>
        public event AutomationFocusChangedEventHandler FocusChanged
        {
            add
            {
                Automation.AddAutomationFocusChangedEventHandler(new AutomationFocusChangedEventHandler(value));
            }
            remove
            {
                Automation.RemoveAutomationFocusChangedEventHandler(value);
            }
        }

        /// <summary>
        /// Tries to find the given type of UIControl. The used Treescope is always Children.
        /// </summary>
        /// <typeparam name="T">UIControl class e.g.: UIButton</typeparam>
        /// <param name="findBy">The find by.</param>
        /// <param name="value">The value.</param>
        /// <param name="scope">The scope.</param>
        /// <returns></returns>
        public override T FindUIControl<T>(FindBy findBy, string value)
        {
            return FindUIControl<T>(findBy, value, TreeScope.Children);
        }

        /// <summary>
        /// Tries to find all the control which have the given type. The used Treescope is always Children.
        /// </summary>
        /// <typeparam name="T">UIControl class e.g.: UIButton </typeparam>
        /// <returns></returns>
        public override T FindUIControl<T>()
        {
            return FindUIControl<T>(FindBy.Type, string.Empty, TreeScope.Children);
        } 
    }
}
