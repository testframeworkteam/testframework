using System;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a UIDocument class
    /// </summary>
    public class UIDocument : UIText, IUIDocument
    {
        /// <summary>
        /// Initializes a new instace of UIDocument class
        /// </summary>
        /// <param name="element">Automation Element</param>
        public UIDocument(AutomationElement element): base(element)
        {
            if ((element != null) && (!IsTextPatternAvailable))
            {
                throw new PatternNotSupportedException(this, "TextPattern not supported on this control");
            }
        }

        /// <summary>
        /// Sets the scroll position in percent.
        /// </summary>
        /// <param name="horizontalPercent">The horizontal percent.</param>
        /// <param name="verticalPercent">The vertical percent.</param>
        /// <remarks>If the Scroll pattern is not supported then it throws and exception! </remarks>
        public void SetScrollPercent(double horizontalPercent, double verticalPercent)
        {
            object scrollPattern = null;

            if (!Element.TryGetCurrentPattern(ScrollPattern.Pattern, out scrollPattern))
            {
                throw new PatternNotSupportedException(this, "ScrollPattern is not supported on this control !");
            }

            try
            {
                ((ScrollPattern)scrollPattern).SetScrollPercent(horizontalPercent, verticalPercent);
            }
            catch (InvalidOperationException) { }
        }

        /// <summary>
        /// Gets the scroll pattern information.
        /// </summary>
        /// <returns></returns>
        public ScrollPattern.ScrollPatternInformation GetScrollPatternInformation()
        {
            object scrollPattern = null;

            if (!Element.TryGetCurrentPattern(ScrollPattern.Pattern, out scrollPattern))
            {
                throw new PatternNotSupportedException(this, "ScrollPattern is not supported on this control !");
            }

            return ((ScrollPattern)scrollPattern).Current;
        }
    }
}
