using System;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents an Edit window control.
    /// </summary>
    public class UIEdit : UIText, IUIEdit
    {        
        /// <summary>
        /// Initializes a new instance of the UIEdit class.
        /// </summary>
        /// <param name="element">Automation element</param>
        public UIEdit(AutomationElement element) : base(element)
        {
            if ((element != null) && (!(IsValuePatternAvailable || IsTextPatternAvailable)))
            {
                throw new PatternNotSupportedException(this, "ValuePattern or TextPattern is not supported on this control.");
            }
        }

        /// <summary>
        /// Gets a value indicating whether it is a password field.
        /// </summary>
        public bool IsPassword
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void TypeValidated(string text)
        {
            try
            {
                SetFocus();
            }
            catch (InvalidOperationException) { }

            KeyBoardInput(System.Windows.Input.Key.A, true);
            KeyBoardInput(System.Windows.Input.Key.Back, true);
            Type(text);

            Logger.Debug(string.Format("The '{0}' text was typed into '{1}' control", text, GetType().Name));     
        }
    }
}
