using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Grid Item window control.
    /// </summary>
    public class UIGridItem : UIControl, IUIGridItem
    {
        /// <summary>
        /// Initializes a new instance of the UIGridItem class
        /// </summary>
        /// <param name="element">Automation element</param>
        public UIGridItem(AutomationElement element) : base(element)
        {
            if ((element != null) && (!IsGridItemPatternAvailable))
            {
                throw new PatternNotSupportedException(this, "GridItemPattern is not supported on this control !");
            }

        }

        /// <summary>
        /// Gets the number of column
        /// </summary>
        public int Column
        {
            get
            {
                var gridItemPattern = Element.GetCurrentPattern(GridItemPattern.Pattern) as GridItemPattern;
                return gridItemPattern.Current.Column;
            }
        }

        /// <summary>
        /// Gets the number of row
        /// </summary>
        public int Row
        {
            get
            {
                var gridItemPattern = Element.GetCurrentPattern(GridItemPattern.Pattern) as GridItemPattern;
                return gridItemPattern.Current.Row;
            }
        }

        /// <summary>
        /// Gets the value of UIGridItem control.
        /// </summary>
        public string Value
        {
            get
            {
                return Name;
            }
        }
    }
}
