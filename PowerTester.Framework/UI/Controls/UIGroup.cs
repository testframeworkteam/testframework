using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Group window control.
    /// </summary>
    public class UIGroup : UIContainer, IUIGroup
    {
        /// <summary>
        /// Initializes a new instance of the UIGroup class
        /// </summary>
        /// <param name="element">This is the automation element.</param>
        public UIGroup(AutomationElement element) : base(element)
        {
        }
    }
}
