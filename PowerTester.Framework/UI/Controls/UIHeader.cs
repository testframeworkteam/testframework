
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a UIHeader window control.
    /// </summary>
    public class UIHeader : UIControl, IUIHeader
    {
        /// <summary>
        /// Initializes a new instance of the UIHeader class
        /// </summary>
        /// <param name="element">Automation element</param>
        public UIHeader(AutomationElement element) : base(element)
        {
        }

        /// <summary>
        /// Gets a collection of UIheaderItems of UIDataGrid object.
        /// </summary>
        public UIHeaderItemCollection HeaderItems
        {
            get
            {
                var headerItemCollection = new UIHeaderItemCollection();

                try
                {
                    var automationElementCollection = UIFind.FindAllElementsByType(Element, ControlType.HeaderItem, TreeScope.Children);

                    foreach (AutomationElement automationElement in automationElementCollection)
                    {
                        headerItemCollection.Add(new UIHeaderItem(automationElement));
                    }
                }
                catch (UIAutomationElementNotFoundException)
                {
                }

                return headerItemCollection;
            }
        }
    }
}
