using System;
using System.Diagnostics;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Header Item window control.
    /// </summary>
    public class UIHeaderItem : UIControl, IUIHeaderItem
    {
        /// <summary>
        /// Initializes a new instance of the UIHeaderItem class
        /// </summary>
        /// <param name="element">Automation element</param>
        public UIHeaderItem(AutomationElement element) : base(element)
        {
        }

        /// <summary>
        /// Sends a request to activate a control and initiate its single, unambiguous action. 
        /// </summary>
        public void Invoke()
        {
            if (!IsInvokePatternAvailable)
            {
                throw new PatternNotSupportedException(this, "InvokePattern is not supported on this control !");
            }

            var invokePattern = Element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
            invokePattern.Invoke();

            Logger.Debug(string.Format("The '{0}' UIHeaderItem was invoked.", Name));
        }
    }
}