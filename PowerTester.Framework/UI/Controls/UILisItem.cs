using System;
using System.Diagnostics;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls    
{   
    /// <summary>
    /// Represents a List Item window control.
    /// </summary>    
    public class UILisItem : UIControl, IUIListItem
    {
        /// <summary>
        /// Initializes a new instace of the UIListItem class
        /// </summary>
        /// <param name="element">AutomationElement</param>
        public UILisItem(AutomationElement element): base(element)
        {
            if ((element != null) && (!IsSelectionItemPatternAvailable))
            {
                throw new PatternNotSupportedException(this, "SelectionItemPattern is not supported on this control !");
            }

        }

        /// <summary>
        /// Gets a value that indicates whether an item is selected. 
        /// </summary>
        public bool IsSelected
        {
            get
            {
                SelectionItemPattern selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
                return selectionItemPattern.Current.IsSelected;                
            }

        }

        /// <summary>
        /// Gets the value of control.
        /// </summary>
        public string Value
        {
            get
            {
                return Name;
            }
        }

        /// <summary>
        /// Deselects any selected items and then selects the current element. 
        /// </summary>
        public void Select()
        {
            SelectionItemPattern selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
            selectionItemPattern.Select();

            Logger.Debug(string.Format("The '{0}' UIListItem was selected!", Value));
        }

        /// <summary>
        /// Adds the current element to the collection of selected items. 
        /// </summary>
        public void AddToSelection()
        {
            SelectionItemPattern selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
            selectionItemPattern.AddToSelection();

            Logger.Debug(string.Format("The '{0}' UIListItem was added to selection!", Value));
        }

        /// <summary>
        /// Removes the current element from the collection of selected items. 
        /// </summary>
        public void RemoveFromSelection()
        {
            SelectionItemPattern selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
            selectionItemPattern.RemoveFromSelection();

            Logger.Debug(string.Format("The '{0}' UIListItem was removed from selection!", Value));
        }

        /// <summary>
        /// Contains values that specify the ToggleState of checkable list item
        /// </summary>
        public ToggleState ToggleState
        {
            get
            {
                TogglePattern togglePattern = null; 

                if (IsTogglePatternAvailable)
                {
                    togglePattern = Element.GetCurrentPattern(TogglePattern.Pattern) as TogglePattern;
                }
                else
                {
                    throw new PatternNotSupportedException(this, "TooglePattern is not supported on this control !");
                }

                return togglePattern.Current.ToggleState;
            }
        }

        /// <summary>
        /// Cycles through the toggle states of a checkable list item. 
        /// </summary>
        public void Toggle()
        {
            TogglePattern togglePattern = null;

            if (IsTogglePatternAvailable)
            {
                togglePattern = Element.GetCurrentPattern(TogglePattern.Pattern) as TogglePattern;
                togglePattern.Toggle();

                Logger.Debug(string.Format("The '{0}' UIListItem was toggled.", Name));
            }
            else
            {
                throw new PatternNotSupportedException(this, "TooglePattern is not supported on this control !");
            }
        }

        /// <summary>
        /// Sets the toggle state of checkable list item. 
        /// </summary>
        /// <param name="State">ToggleState</param>
        public void SetToggleState(ToggleState State)
        {            
            if ((ToggleState == ToggleState.On) && (State == ToggleState.Off))
            {
                Toggle();
                if (State != ToggleState) { Toggle(); }
                
            }

            if ((ToggleState == ToggleState.Off) && (State == ToggleState.On))
            {
                Toggle();
                if (State != ToggleState) { Toggle(); }
            }
        }

    }
}
