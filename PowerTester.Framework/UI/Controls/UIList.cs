using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a List window control.
    /// </summary>
    internal class UIList: UIControl, IUIList
    {

        /// <summary>
        /// Initializes a new instace of the UIList class
        /// </summary>
        /// <param name="element">AutomationElement</param>       
        internal UIList(AutomationElement element) : base(element) 
        {
            if ((element != null) && (!IsSelectionPatternAvailable))
            {
                throw new PatternNotSupportedException(this, "SelectionPattern is not supported on this control.");
            }
        }

        /// <summary>
        /// Gets a collection of UIListItem controls
        /// </summary>
        internal UIListItemCollection ListItems
        {
            get
            {
                UIListItemCollection listItemCollection = new UIListItemCollection();

                try
                {
                    AutomationElementCollection automationElementCollection = UIFind.FindAllElementsByType(Element, ControlType.ListItem, TreeScope.Children);

                    foreach (AutomationElement automationElement in automationElementCollection)
                    {
                        listItemCollection.Add(new UILisItem(automationElement));
                    }
                }
                catch (UIAutomationElementNotFoundException) { }

                return listItemCollection;
            }
        }

        /// <summary>
        /// Gets the selected items.
        /// </summary>
        /// <value>The selected items.</value>
        internal UIListItemCollection SelectedListItems
        {
            get
            {
                UIListItemCollection listItemCollection = new UIListItemCollection();

                // AutomationElementCollection automationElementCollection = UIFind.FindAllElementsByType(Element, ControlType.ListItem, TreeScope.Children);

                SelectionPattern selectionPattern = Element.GetCurrentPattern(SelectionPattern.Pattern) as SelectionPattern;                

                foreach (AutomationElement automationElement in selectionPattern.Current.GetSelection())
                {                    
                    listItemCollection.Add(new UILisItem(automationElement));
                }

                return listItemCollection;
            }
        }

        /// <summary>
        /// Gets the checked items.
        /// </summary>
        /// <value>The checked items.</value>
        internal UIListItemCollection CheckedListItems
        {
            get
            {
                UIListItemCollection listItemCollection = new UIListItemCollection();                                
                
                foreach (UILisItem listItem in ListItems)
                {                                        
                    if ((listItem.IsTogglePatternAvailable) && (listItem.ToggleState == ToggleState.On))
                    {
                        listItemCollection.Add(listItem);
                    }
                }

                return listItemCollection;
            }
        }

        /// <summary>
        /// Selects an UIListItem control by its name
        /// </summary>
        /// <param name="name">UIListItem control name</param>
        public void SelectItemByName(string name)
        {
            Condition[] conds = new Condition[] { new PropertyCondition(AutomationElement.NameProperty, name), new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.ListItem) };
            UILisItem listItem = new UILisItem(UIFind.FindElementByAndConditions(Element, conds, TreeScope.Children));
            listItem.Select();            
        }
    }
}
