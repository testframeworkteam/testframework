using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a List Box window control.
    /// </summary>
    public class UIListBox : UIControl, IUIListBox
    {
        private UIList list;

        /// <summary>
        /// Initializes a new instace of the UIListBox class
        /// </summary>
        /// <param name="element">AutomationElement</param>
        public UIListBox(AutomationElement element):base(element)
        {
            if ((element != null) && (!IsSelectionPatternAvailable))
            {
                throw new PatternNotSupportedException(this, "SelectionPattern not supported ont his control !");
            }

            list = new UIList(UIFind.FindElementByType(Element, ControlType.List));
        }

        /// <summary>
        /// Gets a value which specifies whether the control supports the multiselect mode or not.
        /// </summary>
        public bool CanSelectMultiple
        {
            get
            {
                SelectionPattern selectionPattern = Element.GetCurrentPattern(SelectionPattern.Pattern) as SelectionPattern;
                return selectionPattern.Current.CanSelectMultiple;
            }
        }

        /// <summary>
        /// Gets a collection of selected UIListItem controls
        /// </summary>
        public UIListItemCollection SelectedItems
        {            
            get
            {           
                return list.SelectedListItems;
            }
            
        }

        /// <summary>
        /// Gets a collection of checked UIListItem controls
        /// </summary>
        public UIListItemCollection CheckedItems
        {
            get
            {
                return list.CheckedListItems;
            }

        }

        /// <summary>
        /// Gets a collection of UIListItem controls
        /// </summary>
        public UIListItemCollection ListItems
        {
            get
            {                                
                return list.ListItems;                
            }            
        }

        /// <summary>
        /// Selects an UIListItem control by its name
        /// </summary>
        /// <param name="name">UIListIten control name</param>
        public void SelectItemByName(string name)
        {
            list.SelectItemByName(name);
        }

    }
}
