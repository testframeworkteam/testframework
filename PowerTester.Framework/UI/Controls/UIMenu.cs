
using System.Threading;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Menu window control.
    /// </summary>
    public class UIMenu : UIControl, IUIMenu
    {
        /// <summary>
        /// Initializes a new instance of the UIMenu class
        /// </summary>
        /// <param name="element">Automation element</param>
        public UIMenu(AutomationElement element) : base(element)
        {
            if ((element != null) && (ControlType != ControlType.Menu))
            {
                throw new PatternNotSupportedException(this, "This is not a Menu Control!");
            }
        }

        /// <summary>
        /// Gets a collection of UIMenuItem controls
        /// </summary>
        /// <returns>UIMenuItem</returns>
        public UIMenuItemCollection MenuItems
        {
            get
            {
                var automationElementCollection = UIFind.FindAllElementsByType(Element, ControlType.MenuItem);
                var menuItemCollection = new UIMenuItemCollection();

                foreach (AutomationElement automationElement in automationElementCollection)
                {
                    menuItemCollection.Add(new UIMenuItem(automationElement));
                }

                return menuItemCollection;
            }
        }

        /// <summary>
        /// Returns the menu item based on its name
        /// </summary>
        /// <param name="menuItemPath">Contains the name of menu items by which the target control can be reached</param>
        /// <returns>The menu item</returns>
        public UIMenuItem GetMenuItemByName(params string[] menuItemPath)
        {
            var rootElement = Element;

            for (int i = 0; i < menuItemPath.Length; i++)
            {
                try
                {
                    var menuItem = new UIMenuItem(UIFind.FindElementByName(rootElement, menuItemPath[i]));

                    if (i != (menuItemPath.Length - 1))
                    {
                        menuItem.Expand(); 
                        Thread.Sleep(100);
                    }

                    rootElement = menuItem.Element;
                }
                catch (UIAutomationElementNotFoundException)
                {
                    rootElement = null;
                    break;
                }
            }

            return new UIMenuItem(rootElement);
        }
    }
}
