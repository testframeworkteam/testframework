using System;
using System.Diagnostics;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Menu Item window control.
    /// </summary>
    public class UIMenuItem : UIControl, IUIMenuItem
    {
        /// <summary>
        /// Initializes a new instace of the UIMenuItem class
        /// </summary>
        /// <param name="element">AutomationElement</param>
        public UIMenuItem(AutomationElement element): base(element)
        {
            if ((element != null) && (!((IsInvokePatternAvailable) || (IsSelectionPatternAvailable) || (IsTogglePatternAvailable) || (IsExpandCollapsePatternAvailable))))
            {
                throw new PatternNotSupportedException(this, "InvokePattern, SelectionPattern, ExpandCollapsePattern or TogglePattern is not supported on this control !");
            }
        }

        /// <summary>
        /// Sends a request to activate a control and initiate its single, unambiguous action. 
        /// </summary>
        public void Invoke()
        {
            InvokePattern invokePattern = Element.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
            invokePattern.Invoke();

            Logger.Debug(string.Format("The '{0}' UIMenuItem was invoked!", Name));
        }

        /// <summary>
        /// Gets a value which indicates whether the control is selected.
        /// </summary>
        /// <returns></returns>
        public bool IsSelected()
        {
            SelectionItemPattern selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
            return selectionItemPattern.Current.IsSelected;            
        }

        /// <summary>
        /// Deselects any selected items and then selects the current element. 
        /// </summary>
        public void Select()
        {
            SelectionItemPattern selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
            selectionItemPattern.Select();

            Logger.Debug(string.Format("The '{0}' UIMenuItem was selected!", Name));
        }

        /// <summary>
        /// Retrieves the toggle state of the control. 
        /// </summary>
        public ToggleState ToggleState
        {
            get
            {                
                TogglePattern togglePattern = Element.GetCurrentPattern(TogglePattern.Pattern) as TogglePattern;                
                return togglePattern.Current.ToggleState;
            }
        }

        /// <summary>
        /// Displays all child nodes, controls, or content of the UIMenuItem. 
        /// </summary>
        public void Expand()
        {
            ExpandCollapsePattern expandCollapsePattern = Element.GetCurrentPattern(ExpandCollapsePattern.Pattern) as ExpandCollapsePattern;
            expandCollapsePattern.Expand();

            Logger.Debug(string.Format("The '{0}' UIMenuItem was expanded!", Name));
        }

        /// <summary>
        /// Hides all descendant nodes, controls, or content of the UIMenuItem. 
        /// </summary>
        public void Collapse()
        {
            ExpandCollapsePattern expandCollapsePattern = Element.GetCurrentPattern(ExpandCollapsePattern.Pattern) as ExpandCollapsePattern;
            expandCollapsePattern.Collapse();

            Logger.Debug(string.Format("The '{0}' UIMenuItem was collapsed!", Name));
        }

        /// <summary>
        /// Sets the toggle state of control if the control supports this pattern.        
        /// </summary>
        /// <param name="State"></param>
        public void SetToggleState(ToggleState State)
        {
            ToggleState currentToggleState = ToggleState.Off;

            try
            {
                currentToggleState = ToggleState;
            }
            catch { }

            if ((currentToggleState == ToggleState.On) && (State == ToggleState.Off))
            {
                Click();
                return;
            }

            if ((currentToggleState == ToggleState.Off) && (State == ToggleState.On))
            {
                Click();
                return;
            }

            Logger.Debug(string.Format("The '{0}' UIMenuItem was toggled!", Name));
        }

        /// <summary>
        /// Gets a value which indicates whether the control supports the toggle state. Checkable menu item supports the TooglePattern.
        /// </summary>
        public bool IsTooglePatternAvailable
        {
            get
            {
                return IsTogglePatternAvailable;
            }
        }

    }
}
