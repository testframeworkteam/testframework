using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Pane window control.
    /// </summary>    
    public class UIPane:UIContainer, IUIPane
    {
        /// <summary>
        /// Initializes a new instace of the UIPane class
        /// </summary>
        /// <param name="element">AutomationElement</param>
        public UIPane(AutomationElement element):base (element)
        {

        }
    }
}
