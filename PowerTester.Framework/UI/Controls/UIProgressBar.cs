using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents the UIProgressBar control
    /// </summary>
    public class UIProgressBar:UIControl, IUIProgressBar
    {
        /// <summary>
        /// Initializes a new instace of the UIProgressBar class
        /// </summary>
        /// <param name="element">Automation element</param>
        public UIProgressBar(AutomationElement element) : base(element) 
        {
            if ((element != null) && (!IsRangeValuePatternAvailable))
            {
                throw new PatternNotSupportedException(this, "RangeValuePattern is not supported on this control !");
            }
        }

        /// <summary>
        /// Gets a value that specifies whether the control is read only or not.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                RangeValuePattern rangeValuePattern = base.Element.GetCurrentPattern(RangeValuePattern.Pattern) as RangeValuePattern;
                return rangeValuePattern.Current.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets the maximum range value supported by the UIProgressBar control
        /// </summary>
        public double Maximum
        {
            get
            {
                RangeValuePattern rangeValuePattern = base.Element.GetCurrentPattern(RangeValuePattern.Pattern) as RangeValuePattern;
                return rangeValuePattern.Current.Maximum;
            }
        }

        /// <summary>
        /// Gets the minimum range value supported by the UIProgressBar control
        /// </summary>
        public double Minimum
        {
            get
            {
                RangeValuePattern rangeValuePattern = base.Element.GetCurrentPattern(RangeValuePattern.Pattern) as RangeValuePattern;
                return rangeValuePattern.Current.Minimum;
            }
        }

        /// <summary>
        /// Gets the current value of UIProgressBar control.
        /// </summary>
        public double Value
        {
            get
            {
                RangeValuePattern rangeValuePattern = base.Element.GetCurrentPattern(RangeValuePattern.Pattern) as RangeValuePattern;
                return rangeValuePattern.Current.Value;
            }
        }

        /// <summary>
        /// Sets the value of the UIProgressBar control
        /// </summary>
        /// <param name="value">Value of control</param>
        public void SetValue(double value)
        {
            RangeValuePattern rangeValuePattern = base.Element.GetCurrentPattern(RangeValuePattern.Pattern) as RangeValuePattern;
            rangeValuePattern.SetValue(value);
        }
    }
}
