using System;
using System.Diagnostics;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Radio Button window control.
    /// </summary>
    public class UIRadioButton:UIControl, IUIRadioButton
    {
        /// <summary>
        /// Initializes a new instace of the UIRadioButton class
        /// </summary>
        /// <param name="element">AutomationElement</param>
        public UIRadioButton(AutomationElement element): base(element)
        {
            if ((element != null) && (!IsSelectionItemPatternAvailable))
            {
                throw new PatternNotSupportedException(this, "SelectionItemPattern is not supported on this control!");
            }
        }

        /// <summary>
        /// Gets a value which indicates whether the control is selected.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                SelectionItemPattern selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
                return selectionItemPattern.Current.IsSelected;
                
            }            
        }

        /// <summary>
        /// Deselects any selected items and then selects the current element. 
        /// </summary>        
        public void Select()
        {
            SelectionItemPattern selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
            
            selectionItemPattern.Select();

            Logger.Debug(string.Format("The '{0}' UIRadioButton was selected!", Name));
        }

    }
}
