using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represent a UIScrollBar control class
    /// </summary>
    public class UIScrollBar : UIControl, IUISrollbar
    {
        private const string BackBySmallAmountName = "SmallDecrement";
        private const string BackByLargeAmountName = "LargeDecrement";
        private const string ForwardBySmallAmountName = "SmallIncrement";
        private const string ForwardByLargeAmountName = "LargeIncrement";

        /// <summary>
        /// Initializes a new instance of UIScrollBar class.
        /// </summary>
        /// <param name="element"></param>
        public UIScrollBar(AutomationElement element): base(element) {}

        /// <summary>
        /// Represents the 'Back by small amount' button of UIScrollBar class
        /// </summary>
        public UIButton BackBySmallAmount
        {
            get
            {
                Condition[] conds = new Condition[2] { new PropertyCondition(AutomationElement.AutomationIdProperty, BackBySmallAmountName), new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Button) };
                return new UIButton(UIFind.FindElementByAndConditions(Element, conds, TreeScope.Children));
            }
        }

        /// <summary>
        /// Represents a 'Back by large amount' button of UIScrollBar class
        /// </summary>
        public UIButton BackByLargeAmount
        {
            get
            {
                Condition[] conds = new Condition[2] { new PropertyCondition(AutomationElement.AutomationIdProperty, BackByLargeAmountName), new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Button) };
                return new UIButton(UIFind.FindElementByAndConditions(Element, conds, TreeScope.Children));

            }
        }

        /// <summary>
        /// Represents a 'Forward by large amount' button of UIScrollBar class
        /// </summary>
        public UIButton ForwardByLargeAmount
        {
            get
            {
                Condition[] conds = new Condition[2] { new PropertyCondition(AutomationElement.AutomationIdProperty, ForwardByLargeAmountName), new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Button) };
                return new UIButton(UIFind.FindElementByAndConditions(Element, conds, TreeScope.Children));

            }
        }

        /// <summary>
        /// Represents a 'Forward by small amount' button of UIScrollBar class
        /// </summary>
        public UIButton ForwardBySmallAmount
        {
            get
            {
                Condition[] conds = new Condition[2] { new PropertyCondition(AutomationElement.AutomationIdProperty, ForwardBySmallAmountName), new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Button) };
                return new UIButton(UIFind.FindElementByAndConditions(Element, conds, TreeScope.Children));

            }
        }

    }
}
