using System;
using System.Diagnostics;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Spinner window control.
    /// </summary>
    public class UISpinner:UIControl, IUISpinner
    {
        private UIButton incrementButton = null;
        private UIButton decrementButton = null;

        /// <summary>
        /// Initializes a new instace of the UISpinner class
        /// </summary>
        /// <param name="element">AutomationElement</param>
        public UISpinner(AutomationElement element): base(element)
        {
            if ((element != null) && (!IsRangeValuePatternAvailable))
            {
                throw new PatternNotSupportedException(this, "RangeValuePattern is not supported on this control !");
            }

            if ((element != null) && (IsRangeValuePatternAvailable))
            {
                incrementButton = new UIButton(UIFind.FindElementById(Element, "SmallIncrement"));
                decrementButton = new UIButton(UIFind.FindElementById(Element, "SmallDecrement"));
            }
        }
        
        /// <summary>
        /// Represents the Increment button of UISpinner control
        /// </summary>
        public UIButton IncrementButton
        {
            get { return incrementButton; }
        }
        
        /// <summary>
        /// Represents the Decrement button of UISpinner control
        /// </summary>
        public UIButton DecrementButton
        {
            get { return decrementButton; }         
        }

        /// <summary>
        /// Gets or sets the value of control.
        /// </summary>
        public double Value
        {
            get
            {
                RangeValuePattern rangeValuePattern = Element.GetCurrentPattern(RangeValuePattern.Pattern) as RangeValuePattern;
                return rangeValuePattern.Current.Value;
            }
            set
            {
                RangeValuePattern rangeValuePattern = Element.GetCurrentPattern(RangeValuePattern.Pattern) as RangeValuePattern;
                rangeValuePattern.SetValue(value);
            }

        }

        /// <summary>
        /// Gets a value which indicates whether the control is read only.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                RangeValuePattern rangeValuePattern = Element.GetCurrentPattern(RangeValuePattern.Pattern) as RangeValuePattern;
                return rangeValuePattern.Current.IsReadOnly;
            }
        }

        /// <summary>
        /// Gets a value which indicates the minimum number of control.
        /// </summary>
        public double Minimum
        {
            get
            {
                RangeValuePattern rangeValuePattern = Element.GetCurrentPattern(RangeValuePattern.Pattern) as RangeValuePattern;
                return rangeValuePattern.Current.Minimum;
            }
        }

        /// <summary>
        /// Gets a value which indicates the maximum number of control.
        /// </summary>
        public double Maximum
        {
            get
            {
                RangeValuePattern rangeValuePattern = Element.GetCurrentPattern(RangeValuePattern.Pattern) as RangeValuePattern;
                return rangeValuePattern.Current.Maximum;
            }
        }

        /// <summary>
        /// Increases the value of control by step.
        /// </summary>
        public void Increment()
        {
            IncrementButton.Invoke();
            Logger.Debug(string.Format("The '{0}' UISpinner control was incremented!", GetType().Name));
        }

        /// <summary>
        /// Decreases the value of control by step.
        /// </summary>
        public void Decrement()
        {
            DecrementButton.Invoke();

            Logger.Debug(string.Format("The '{0}' UISpinner control was decremented!", GetType().Name));
        }
    }

}
