using System;
using System.Windows.Automation;
using System.Windows.Input;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Static Text window control.
    /// </summary>
    public class UIStaticText : UIText, IUIStaticText
    {
        /// <summary>
        /// Initializes a new instace of the UIStaticText class
        /// </summary>
        /// <param name="element">AutomationElement</param>
        public UIStaticText(AutomationElement element): base(element)
        {
            /*
            if ((element != null) && (ClassName != "Static"))
            {
                throw new SpecialClassRequiredException(this, "Static class is required on this control!");
            }
            */ 
        }

        /// <summary>
        /// Returns the text of control
        /// </summary> 
        public new string GetText()
        {
            return Name;
        }
        
        /// <summary>
        /// It was just intended to hide the method
        /// </summary> 
        private new string GetText(int maxLength)
        {
            return Name;
        }

        /// <summary>
        /// It was just intended to hide the method
        /// </summary>
        private new void KeyBoardInput(Key key, bool press) { }

        /// <summary>
        /// It was just intended to hide the method
        /// </summary>
        private new void Type(string text) { }

        /// <summary>
        /// It was just intended to hide the method
        /// </summary>
        private new string Value()
        {
            return string.Empty;
        }

        /// <summary>
        /// It was just intended to hide the method and it is always true
        /// </summary>
        public new bool IsReadOnly()
        {
            return true;
        }

    }
}
