using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Status Bar window control.
    /// </summary>
    public class UIStatusBar: UIContainer, IUIStatusBar
    {
        /// <summary>
        /// Initializes a new instace of the UIStatusBar class
        /// </summary>
        /// <param name="element">AutomationElement</param>
        public UIStatusBar(AutomationElement element): base(element)
        {

        }
    }
}
