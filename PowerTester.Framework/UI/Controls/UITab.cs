using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Tab window control.
    /// </summary>
    public class UITab : UIControl, IUITab
    {
        /// <summary>
        /// Initializes a new instace of the UITab class
        /// </summary>
        /// <param name="element">AutomationElement</param>
        public UITab(AutomationElement element): base(element) {}

        /// <summary>
        /// Gets the tabs of UITab control
        /// </summary>
        public UITabItemCollection TabItems
        {
            get
            {
                UITabItemCollection tabItemCollection = new UITabItemCollection();

                try
                {
                    AutomationElementCollection automationElementCollection = UIFind.FindAllElementsByType(Element, ControlType.TabItem);

                    if (automationElementCollection.Count != 0)
                    {
                        foreach (AutomationElement automationElement in automationElementCollection)
                        {
                            tabItemCollection.Add(new UITabItem(automationElement));
                        }
                    }
                }
                catch (UIAutomationElementNotFoundException) { }
                
                //This can be an alternative way for getting the handlers of tabItems
                if (tabItemCollection.Count == 0)
                {
                    {
                        SelectionPattern selectionPattern = Element.GetCurrentPattern(SelectionPattern.Pattern) as SelectionPattern;

                        foreach (AutomationElement automationElement in selectionPattern.Current.GetSelection())
                        {
                            tabItemCollection.Add(new UITabItem(automationElement));
                        }
                    }
                }
                
                return tabItemCollection;
            }
        }

        /// <summary>
        /// Selects a tab item which statisfies the condition
        /// </summary>
        /// <param name="name">Name of tab item</param>
        public UITabItem Select(string name)
        {
            UITabItem uiTabItem = null;

            foreach (UITabItem tabItem in TabItems)
            {                
                if (tabItem.Name.ToLower() == name.ToLower())
                {
                    tabItem.Select();
                    uiTabItem = tabItem;
                    break;
                }
            }

            return uiTabItem;
        }

    }
}
