using System;
using System.Diagnostics;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a UITabItem class
    /// </summary>
    public class UITabItem: UIContainer, IUITabItem
    {
        /// <summary>
        /// Initializes a new instance of the UITabItem class
        /// </summary>
        /// <param name="element"></param>
        public UITabItem(AutomationElement element) : base(element) 
        {
            if ((element != null) && (!IsSelectionItemPatternAvailable))
            {
                throw new PatternNotSupportedException(this, "SelectionItemPattern not supported on this control !");
            }
        }

        /// <summary>
        /// Deselects any selected items and then selects the current element. 
        /// </summary>
        public void Select()
        {
            SelectionItemPattern selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
            selectionItemPattern.Select();

            Logger.Debug(string.Format("The '{0}' UITabItem was selected!", Name));
        }
    }
}
