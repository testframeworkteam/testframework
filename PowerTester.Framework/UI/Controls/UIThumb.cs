using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represent a UIThumb control class
    /// </summary>
    public class UIThumb : UIContainer, IUIThumb
    {
        /// <summary>
        /// Initializes a new instance of UIThumb class.
        /// </summary>        
        public UIThumb(AutomationElement element): base(element) {}
    }
}
