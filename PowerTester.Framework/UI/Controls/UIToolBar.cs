using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represent a UIToolBar control class
    /// </summary>
    public class UIToolBar : UIContainer, IUIToolBar
    {
        /// <summary>
        /// Initializes a new instance of UIToolbar class.
        /// </summary>
        /// <param name="element"></param>
        public UIToolBar(AutomationElement element):base(element)
        {
        }
    }
}
