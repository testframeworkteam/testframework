using System;
using System.Diagnostics;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Tree Item window control
    /// </summary>
    public class UITreeItem : UIControl, IUITreeItem
    {
        /// <summary>
        /// Initializes a new instance of UITreeItem
        /// </summary>
        /// <param name="element">AutomationElement</param>
        public UITreeItem(AutomationElement element): base(element) {}

        /// <summary>
        /// Gets a value that indicates whether an item is selected. 
        /// </summary>
        public bool IsSelected()
        {
            SelectionItemPattern selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
            return selectionItemPattern.Current.IsSelected;
        }

        /// <summary>
        /// Deselects any selected items and then selects the current element. 
        /// </summary>
        public void Select()
        {
            SelectionItemPattern selectionItemPattern = Element.GetCurrentPattern(SelectionItemPattern.Pattern) as SelectionItemPattern;
            selectionItemPattern.Select();

            Logger.Debug(string.Format("The '{0}' UITreeItem was selected!", Name));

        }

        /// <summary>
        /// Displays all child nodes, controls, or content of the control.
        /// </summary>
        public void Expand()
        {
            ExpandCollapsePattern expandCollapsePattern = Element.GetCurrentPattern(ExpandCollapsePattern.Pattern) as ExpandCollapsePattern;
            expandCollapsePattern.Expand();

            Logger.Debug(string.Format("The '{0}' UITreeItem was expanded!", Name));
        }

        /// <summary>
        /// Hides all nodes, controls, or content that are descendants of the control.
        /// </summary>
        public void Collapse()
        {
            ExpandCollapsePattern expandCollapsePattern = Element.GetCurrentPattern(ExpandCollapsePattern.Pattern) as ExpandCollapsePattern;
            expandCollapsePattern.Collapse();

            Logger.Debug(string.Format("The '{0}' UITreeItem was collapsed!", Name));
        }        
    }
}
