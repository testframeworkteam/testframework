using System.Collections;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Tree window control.
    /// </summary>
    public class UITreeView : UIControl, IUITreeView
    {
        private UITreeViewItemNodeCollection treeViewItemNodeCollection = new UITreeViewItemNodeCollection();

        /// <summary>
        /// Gets the items of the tree view control
        /// </summary>
        public UITreeViewItemNodeCollection Nodes
        {
            get
            {
                treeViewItemNodeCollection.Clear();

                PropertyCondition cond = new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.TreeItem);
                AutomationElementCollection automationElementCollection = Element.FindAll(TreeScope.Children, cond);

                foreach (AutomationElement automationElement in automationElementCollection)
                {                    
                    UITreeViewItemNode treeItem = new UITreeViewItemNode(automationElement);
                    treeViewItemNodeCollection.Add(treeItem);
                }

                return treeViewItemNodeCollection;
            }
        }

        /// <summary>
        /// Initializes a new instace of the UITreeView class
        /// </summary>
        /// <param name="element">AutomationElement</param>
        public UITreeView(AutomationElement element): base(element)
        {            
            if ((element != null) && (ControlType != ControlType.Tree))
            {
                throw new PatternNotSupportedException(this, "TreeView pattern is not supported on this control!");
            }
        }
    }

    /// <summary>
    /// Represents a collection of UITreeItemNode objects
    /// </summary>   
    public class UITreeViewItemNodeCollection: CollectionBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="UITreeViewItemNodeCollection"/> class.
        /// </summary>
        public UITreeViewItemNodeCollection()
        {
        }

        /// <summary>
        /// Adds the specified tree item.
        /// </summary>
        /// <param name="treeItem">The tree item.</param>
        public virtual void Add(UITreeViewItemNode treeItem)
        {            
            List.Add(treeItem);
        }


        /// <summary>
        /// Gets the <see cref="UITreeViewItemNode"/> at the specified index.
        /// </summary>
        /// <value></value>
        public virtual UITreeViewItemNode this[int Index]
        {
            get
            {
                return (UITreeViewItemNode)List[Index];
            }
        }


        /// <summary>
        /// Gets the <see cref="UITreeViewItemNode"/> with the specified name.
        /// </summary>
        /// <value></value>
        public virtual UITreeViewItemNode this[string Name]
        {
            get
            {
                object treeItem = null;

                for (int i = 0; i <= List.Count; i++)
                {
                    if (((UITreeViewItemNode)List[i]).Name.ToLower() == Name.ToLower())                    
                    {
                        treeItem = List[i];
                        break;
                    }
                }

                return (UITreeViewItemNode)treeItem;
            }            
        }

        }


    /// <summary>
    /// Represent the UITreeViewItemNode class
    /// </summary>
    public class UITreeViewItemNode : UITreeItem
    {
        private UITreeViewItemNodeCollection childrenNodeCollection = new UITreeViewItemNodeCollection();
        private int currentChildNodeIndex;

        /// <summary>
        /// Specifies the current child node index
        /// </summary>
        public int CurrentChildNodeIndex
        {
            get { return currentChildNodeIndex; }            
        }

        /// <summary>
        /// Specifies the children nodes of this node
        /// </summary>
        public UITreeViewItemNodeCollection Nodes
        {
            get 
            {
                if (IsExpandCollapsePatternAvailable)
                {
                    ExpandCollapsePattern expandCollapsePattern = Element.GetCurrentPattern(ExpandCollapsePattern.Pattern) as ExpandCollapsePattern;
                    
                    if (expandCollapsePattern.Current.ExpandCollapseState == ExpandCollapseState.Collapsed)
                    {
                        expandCollapsePattern.Expand();
                    }
                }

                PropertyCondition cond = new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.TreeItem);
                AutomationElementCollection automationElementCollection = Element.FindAll(TreeScope.Children, cond);

                foreach (AutomationElement treeElement in automationElementCollection)
                {
                    UITreeViewItemNode nextNode = new UITreeViewItemNode(treeElement);

                    childrenNodeCollection.Add(nextNode);                    
                }
                
                return childrenNodeCollection; 
            }            
        }

        /// <summary>
        /// Specifies if the current node has children or not
        /// </summary>
        public bool HasChildren
        {
            get
            {
                return (childrenNodeCollection.Count == 0) ? false : true;
            }
        }
        
        /// <summary>
        /// Initializes a new instance of the UITreeViewItemNode class
        /// </summary>
        /// <param name="element"></param>
        public UITreeViewItemNode(AutomationElement element) : base(element) { }

        /// <summary>
        /// Gives back the first child element of the current node
        /// </summary>
        /// <returns></returns>
        public UITreeViewItemNode FirstChildNode()
        {
            UITreeViewItemNode childNode = null;

            if (HasChildren)
            {
                currentChildNodeIndex = 0;
                childNode = Nodes[currentChildNodeIndex];                
            }

            return childNode;
        }

        /// <summary>
        /// Gives back the last child node of the current node
        /// </summary>
        /// <returns></returns>
        public UITreeViewItemNode LastChildNode()
        {
            UITreeViewItemNode childNode = null;

            if (HasChildren)
            {
                currentChildNodeIndex = Nodes.Count - 1;
                childNode = Nodes[currentChildNodeIndex];                
            }

            return childNode;
        }

        /// <summary>
        /// Gives back the next child node of the current node according to the CurrentChildNodeIndex
        /// </summary>        
        public UITreeViewItemNode NextChildNode()
        {
            UITreeViewItemNode childNode = null;

            if ((HasChildren) && (currentChildNodeIndex + 1 <= Nodes.Count - 1))
            {
                currentChildNodeIndex++;
                childNode = Nodes[currentChildNodeIndex];
            }

            return childNode;
        }

        /// <summary>
        /// Gives back the previous child node of the current node according to the CurrentChildNodeIndex
        /// </summary>        
        public UITreeViewItemNode PreviosChildNode()
        {
            UITreeViewItemNode childNode = null;

            if ((HasChildren) && (currentChildNodeIndex - 1 >= 0))
            {
                currentChildNodeIndex--;
                childNode = Nodes[currentChildNodeIndex];
            }

            return childNode;
        }
    }

}
