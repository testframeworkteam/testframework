using System;
using System.Diagnostics;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.UI.Controls
{
    /// <summary>
    /// Represents a Window control.
    /// </summary>
    public class UIWindow : UIContainer, IUIWindow
    {        
        /// <summary>
        /// Initializes a new instance of the UIWindow class.
        /// </summary>
        /// <param name="rootElement">Root element of Application object</param>
        public UIWindow(AutomationElement rootElement):base(rootElement)
        {
            if ((rootElement != null) && (!IsWindowPatternAvailable))
            {
                throw new PatternNotSupportedException(this, "WindowPattern is not supported on this control!");
            }
        }

        /// <summary>
        /// Gets a value of AutomationElement.
        /// </summary>
        public new AutomationElement Root
        {
            get
            {
                return Element;
            }
        }

        /// <summary>
        /// Gets a value that specifies whether the window can be maximized.
        /// </summary>
        public bool CanMaximize
        {
            get                                        
            {
                WindowPattern windowPattern = Element.GetCurrentPattern(WindowPattern.Pattern) as WindowPattern;
                return windowPattern.Current.CanMaximize;
            }
        }

        /// <summary>
        /// Gets a value that specifies whether the window can be minimized.
        /// </summary>
        public bool CanMinimize
        {
            get
            {
                WindowPattern windowPattern = Element.GetCurrentPattern(WindowPattern.Pattern) as WindowPattern;
                return windowPattern.Current.CanMinimize;
            }
        }
        
        /// <summary>
        /// Gets a value that specifies whether the window is modal.
        /// </summary>
        public bool IsModal
        {
            get
            {
                WindowPattern windowPattern = Element.GetCurrentPattern(WindowPattern.Pattern) as WindowPattern;
                return windowPattern.Current.IsModal;
            }
        }
        
        /// <summary>
        /// Gets a value that specifies whether the window is the topmost element in the z-order.
        /// </summary>
        public bool IsTopmost
        {
            get
            {
                WindowPattern windowPattern = Element.GetCurrentPattern(WindowPattern.Pattern) as WindowPattern;
                return windowPattern.Current.IsTopmost;
            }
        }

        /// <summary>
        /// Gets the visual state of the window.
        /// </summary>
        public WindowVisualState WindowsVisualState
        {
            get
            {
                WindowPattern windowPattern = Element.GetCurrentPattern(WindowPattern.Pattern) as WindowPattern;
                return windowPattern.Current.WindowVisualState;
            }
        }

        /// <summary>
        /// Attempts to close the window.
        /// </summary>
        public void Close()
        {
            WindowPattern windowPattern = Element.GetCurrentPattern(WindowPattern.Pattern) as WindowPattern;
            
            windowPattern.Close();

            Logger.Debug(string.Format("The '{0}' UIWindow was closed!", Name));
        }

        /// <summary>
        /// Changes the visual state of the window.
        /// </summary>
        /// <param name="VisualState"></param>
        public void SetWindowVisualState(WindowVisualState VisualState)
        {
            WindowPattern windowPattern = Element.GetCurrentPattern(WindowPattern.Pattern) as WindowPattern;
            windowPattern.SetWindowVisualState(VisualState);

            Logger.Debug(string.Format("The window state of '{0}' UIWindow was changed to '{1}'!", Name, VisualState.ToString()));
        }

        /// <summary>
        /// Causes the calling code to block for the specified time or until the associated process enters an idle state, whichever completes first.
        /// </summary>
        /// <param name="milliseconds"></param>
        public void WaitForInputIdle(int milliseconds)
        {
            WindowPattern windowPattern = Element.GetCurrentPattern(WindowPattern.Pattern) as WindowPattern;
            windowPattern.WaitForInputIdle(milliseconds);
        }

        #region ITransformProvider

        /// <summary>
        /// Gets a value that specifies whether the control can be moved.
        /// </summary>        
        public bool CanMove
        {
            get
            {
                if (!IsTransformPatternAvailable)
                {
                    throw new PatternIsNotAvailableException(this, TransformPattern.Pattern.ProgrammaticName);
                }

                TransformPattern transformPattern = Element.GetCurrentPattern(TransformPattern.Pattern) as TransformPattern;
                return transformPattern.Current.CanMove;
            }
        }

        /// <summary>
        /// Gets a value that specifies whether the UI Automation element can be resized.
        /// </summary>
        public bool CanResize
        {
            get
            {
                if (!IsTransformPatternAvailable)
                {
                    throw new PatternIsNotAvailableException(this, TransformPattern.Pattern.ProgrammaticName);
                }

                TransformPattern transformPattern = Element.GetCurrentPattern(TransformPattern.Pattern) as TransformPattern;
                return transformPattern.Current.CanResize;
            }
        }

        /// <summary>
        /// Gets a value that specifies whether the control can be rotated.
        /// </summary>        
        public bool CanRotate
        {
            get
            {
                if (!IsTransformPatternAvailable)
                {
                    throw new PatternIsNotAvailableException(this, TransformPattern.Pattern.ProgrammaticName);
                }

                TransformPattern transformPattern = Element.GetCurrentPattern(TransformPattern.Pattern) as TransformPattern;
                return transformPattern.Current.CanRotate;
            }
        }

        /// <summary>
        /// Moves the control.
        /// </summary>
        /// <param name="x">Absolute screen coordinates of the left side of the control.</param>
        /// <param name="y">Absolute screen coordinates of the top of the control.</param>
        public void Move(double x, double y)
        {
            if (!IsTransformPatternAvailable)
            {
                throw new PatternIsNotAvailableException(this, TransformPattern.Pattern.ProgrammaticName);
            }

            TransformPattern transformPattern = Element.GetCurrentPattern(TransformPattern.Pattern) as TransformPattern;
            transformPattern.Move(x, y);
        }

        /// <summary>
        /// Resizes the control.
        /// </summary>
        /// <param name="width">The new width of the window, in pixels.</param>
        /// <param name="height">The new height of the window, in pixels.</param>
        public void Resize(double width, double height)
        {
            if (!IsTransformPatternAvailable)
            {
                throw new PatternIsNotAvailableException(this, TransformPattern.Pattern.ProgrammaticName);
            }

            TransformPattern transformPattern = Element.GetCurrentPattern(TransformPattern.Pattern) as TransformPattern;
            transformPattern.Resize(width, height);
        }


        /// <summary>
        /// Rotates the control.
        /// </summary>
        /// <param name="degrees">The number of degrees to rotate the control. A positive number rotates clockwise; a negative number rotates counterclockwise.</param>
        public void Rotate(double degrees)
        {
            if (!IsTransformPatternAvailable)
            {
                throw new PatternIsNotAvailableException(this, TransformPattern.Pattern.ProgrammaticName);
            }

            TransformPattern transformPattern = Element.GetCurrentPattern(TransformPattern.Pattern) as TransformPattern;
            transformPattern.Rotate(degrees);
        }

        #endregion
    }
}
