﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Windows.Automation;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Controls;

namespace PowerTester.Framework.UI.Dialogs
{
    public static class StandardWindowsDialogs
    {
        
        /// <summary>
        /// Represents the File Open dialog
        /// </summary>
        public static class OpenDialog
        {
            private static string _windowTitle = "Choose File to Upload";

            #region properties

            public static UIContainer RootElement
            {
                get
                {
                    return UIDesktop.Instance;
                }
            }

            /// <summary>
            /// Gets value indicating whether open dialog exists
            /// </summary>
            public static bool Exists
            {
                get
                {
                    return Window.Exists;
                }
            }

            /// <summary>
            /// Gets value indicating whether open dialog exists
            /// </summary>
            public static bool WaitFor()
            {
                return WaitFor(UIAutomationSettings.WaitForTimeout);
            }

            /// <summary>
            /// Gets value indicating whether open dialog exists
            /// </summary>
            public static bool WaitFor(int timeout)
            {
                return Window.WaitFor(timeout);
            }

            /// <summary>
            /// Gets File Open dialog
            /// </summary>
            public static UIWindow Window
            {
                get
                {
                    var window = RootElement.FindUIControl<UIWindow>(FindBy.Name, _windowTitle, TreeScope.Descendants);
                    return window;
                }
            }

            /// <summary>
            /// Gets the Open button control of window
            /// </summary>
            public static UIButton Open
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "textOpenButton");
                }
            }

            /// <summary>
            /// Gets the Open pane of browser dialog window
            /// </summary>
            public static UIPane PaneOpenButton
            {
                get
                {
                    return Window.FindUIControl<UIPane>(FindBy.Name, "textOpenButton");
                }
            }

            /// <summary>
            /// Gets the Cancel button control of window
            /// </summary>
            public static UIButton Cancel
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "textCancel");
                }
            }

            /// <summary>
            /// Gets the FileName edit control of window
            /// </summary>
            private static UIEdit FileName
            {
                get
                {
                    return Window.FindUIControls<UIEdit>()[0];
                }
            }

            private static UIComboBox FileNameWin7xx
            {
                get
                {
                    return Window.FindUIControls<UIComboBox>(TreeScope.Children)[0];
                }
            }
            
            #endregion

            #region Methods

            public static void OpenFile(string path, string windowTitle)
            {
                if (windowTitle != null)
                {
                    _windowTitle = windowTitle;
                }
                OpenFile(path);
            }

            /// <summary>
            /// Opens the given file by the dialog.
            /// </summary>
            /// <param name="path"></param>
            public static void OpenFile(string path)
            {
                ////UIAutomationSettings.ExecutionLog = true;
                ////UIAutomationSettings.TraceLog = true;

                UIWaitFor.WaitFor(typeof(OpenDialog), "Window");

                Win32.SetFocus(new IntPtr(Window.Hwnd));

                if (((int)OperatingSystem.WindowsXPBase & (int)General.GetOperatingSystem()) == 0)
                {
                    Win32.SetFocus(new IntPtr(FileNameWin7xx.Hwnd));
                }
                else
                {
                    Win32.SetFocus(new IntPtr(FileName.Hwnd));
                }

                Thread.Sleep(500);
                Input.SendUnicodeString(path, 1, 100);

                Input.SendKeyboardInput(System.Windows.Input.Key.End, true);
                Thread.Sleep(500);
                Input.SendKeyboardInput(System.Windows.Input.Key.End, false);
                Thread.Sleep(1000);
                Input.SendKeyboardInput(System.Windows.Input.Key.Enter, true);
                Thread.Sleep(500);
                Input.SendKeyboardInput(System.Windows.Input.Key.Enter, false);

                ////UIAutomationSettings.ExecutionLog = executionLog;
                ////UIAutomationSettings.TraceLog = traceLog;
            }

            public static void UploadFileFromBrowser(string path, string windowTitle)
            {
                if (windowTitle != null)
                {
                    _windowTitle = windowTitle;
                }
                UploadFileFromBrowser(path);
            }

            /// <summary>
            /// Opens the given file by the open dialog in browsers.
            /// </summary>
            /// <param name="path"></param>
            public static void UploadFileFromBrowser(string path)
            {
                UIWaitFor.WaitFor(typeof(OpenDialog), "Window");

                Win32.SetFocus(new IntPtr(Window.Hwnd));

                Win32.SetFocus(((int)OperatingSystem.WindowsXPBase & (int)General.GetOperatingSystem()) == 0
                                   ? new IntPtr(FileNameWin7xx.Hwnd)
                                   : new IntPtr(FileName.Hwnd));

                Thread.Sleep(500);
                Input.SendUnicodeString(path, 1, 100);
                PaneOpenButton.Click();
            }

            #endregion

        }

        /// <summary>
        /// Represents the File Open dialog
        /// </summary>
        public static class SaveDialog 
        {
            private const string WindowTitle = "File Download";
            private static UIContainer _rootElement = UIDesktop.Instance;


            #region properties

            public static bool Exists
            {
                get { return Window.Exists; }
            }

            public static bool WaitFor()
            {
                return WaitFor(UIAutomationSettings.WaitForTimeout);
            }

            public static bool WaitFor(int timeout)
            {
                return Window.WaitFor();
            }

            public static UIContainer RootElement
            {
                get
                {
                    return _rootElement;
                }
                set
                {
                    _rootElement = value;
                }
            }

            /// <summary>
            /// Gets File Open dialog
            /// </summary>
            public static UIWindow Window
            {
                get
                {
                    var window = _rootElement.FindUIControl<UIWindow>(FindBy.Name, WindowTitle, TreeScope.Subtree);
                    return window;
                }
            }

            /// <summary>
            /// Gets the Fidn button control of window
            /// </summary>
            public static UIButton Find
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "Find");
                }
            }

            /// <summary>
            /// Gets the Save pane of browser dialog window
            /// </summary>
            public static UIButton Save
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "Save");
                }
            }

            /// <summary>
            /// Gets the Cancel button control of window
            /// </summary>
            public static UIButton Cancel
            {
                get
                {
                    return Window.FindUIControl<UIButton>(FindBy.Name, "Cancel");
                }
            }


            #endregion

            #region Methods
            /// <summary>
            /// Method to open SaveAs dialog, and save the file
            /// </summary>
            /// <param name="path">the path to save the file on</param>
            public static void SaveFile(string path)
            {
                Save.WaitFor();
                if (Save.Click(() => SaveAs.WaitFor(), 15))
                {
                    SaveAs.SaveFileAs(path);
                }
            }
            #endregion
        }

        /// <summary>
        /// Represents the Save As dialog
        /// </summary>
        public static class SaveAs
        {
            private static string _windowTitle = "Save As";
            
            #region properties

            public static bool Exists
            {
                get { return Window.Exists; }
            }

            public static bool WaitFor()
            {
                return WaitFor(UIAutomationSettings.WaitForTimeout);
            }

            public static bool WaitFor(int timeout)
            {
                return Window.WaitFor();
            }

            /// <summary>
            /// Gets Save As dialog
            /// </summary>
            public static UIWindow Window
            {
                get
                {
                     UIWindow window = null;

                    try
                    {
                        window = UIDesktop.Instance.FindUIControl<UIWindow>(FindBy.Name, _windowTitle, TreeScope.Subtree);
                    }
                    catch
                    {
                    }

                    return window;
                }
            }

            /// <summary>
            /// Gets the Save button control of window
            /// </summary>
            public static UIButton Save
            {
                get { return Window.FindUIControl<UIButton>(FindBy.Name, "Save"); }
            }

            /// <summary>
            /// Gets the Cancel button control of window
            /// </summary>
            public static UIButton Cancel
            {
                get { return Window.FindUIControl<UIButton>(FindBy.Name, "Cancel"); }
            }

            /// <summary>
            /// Gets the FileName edit control of window
            /// </summary>
            public static UIEdit FileName
            {
                get
                {
                    if (((int) OperatingSystem.WindowsVista & (int)General.GetOperatingSystem()) != 0)
                    {
                        Logger.Debug(@"UIEdit find [0] element");
                        return Window.FindUIControls<UIEdit>(TreeScope.Subtree)[0];
                    }
                    else
                    {
                        Logger.Debug(@"UIEdit find by name: 'File name:'.");
                        return Window.FindUIControl<UIEdit>(FindBy.Name, @"File name:");
                    }
                }
            }

            #endregion

            #region Methods

            public static void SaveFileAs(string path, string windowTitle)
            {
                if (windowTitle != null)
                {
                    _windowTitle = windowTitle;
                }
                SaveFileAs(path);
            }

            /// <summary>
            /// Save as the given file by the dialog, replace is parameterized
            /// </summary>
            /// <param name="fullpath"></param>
            public static void SaveFileAs(string fullpath)
            {
                Debug.WriteLine("Waiting for Save as dialog and saving file...");
                if (!Window.WaitFor(10))
                {
                    throw new Exception("Save as dialog is not found");
                }

                Thread.Sleep(500);
                FileName.Type(fullpath);
                Thread.Sleep(200);

                Save.Click();
            }

            #endregion

            #region Dialogs

            /// <summary>
            /// Gets the replace existing file dialog.
            /// </summary>
            public static class Replace
            {
                public static UIWindow Window
                {
                    get
                    {
                        return SaveAs.Window.FindUIControl<UIWindow>(FindBy.Name, "textSaveAsDialogTitle", TreeScope.Children);
                    }
                }

                /// <summary>
                /// Gets the Yes button control of window
                /// </summary>
                public static UIButton Yes
                {
                    get { return Window.FindUIControl<UIButton>(FindBy.Name, "textYes"); }
                }

                /// <summary>
                /// Gets the No button control of window
                /// </summary>
                public static UIButton No
                {
                    get { return Window.FindUIControl<UIButton>(FindBy.Name, "textNo"); }
                }
            }

            #endregion

        }       
    }
}