﻿using System.Windows.Automation;

namespace PowerTester.Framework.UI.Interfaces
{
    
    public interface IUIButton
    {

        /// <summary>
        /// Cycles through the toggle states of an UIAutomationElement.
        /// </summary>
        void Toggle();

        /// <summary>
        /// Contains values that specify the ToggleState of UIButton control
        /// </summary>       
        ToggleState ToggleState {get;}

        /// <summary>
        /// Sends a request to activate a control and initiate its single, unambiguous action. 
        /// </summary>
        void Invoke();

        /// <summary>
        /// Sets the toggle state of UIButton.
        /// </summary>
        /// <param name="State">ToggleState</param>
        void SetToggleState(ToggleState State);

    }
}
