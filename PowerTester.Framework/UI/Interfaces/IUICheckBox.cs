﻿using System.Windows.Automation;

namespace PowerTester.Framework.UI.Interfaces
{

    public interface IUICheckBox
    {

        /// <summary>
        /// Cycles through the toggle states of an UIAutomationElement.
        /// </summary>
        void Toggle();

        /// <summary>
        /// Contains values that specify the ToggleState of UIButton control
        /// </summary>       
        ToggleState ToggleState { get; }

        /// <summary>
        /// Sets the toggle state of UIButton.
        /// </summary>
        /// <param name="State">ToggleState</param>
        void SetToggleState(ToggleState State);

    }
}
