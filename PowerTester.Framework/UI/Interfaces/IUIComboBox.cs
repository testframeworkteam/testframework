﻿using PowerTester.Framework.UI.Controls;

namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIComboBox
    {

        UIButton DropDown { get; }

        /// <summary>
        /// Displays all child nodes, controls, or content of the control.
        /// </summary>
        void Expand();

        /// <summary>
        /// Hides all nodes, controls, or content that are descendants of the control.
        /// </summary>
        void Collapse();

        /// <summary>
        /// Gets value of this instance.
        /// </summary>        
        string Value {get;}

        /// <summary>
        /// Types the give text into the control and hits the enter.
        /// </summary>
        void Type(string text);

        /// <summary>
        /// Selects an UIListItem control by its name
        /// </summary>        
        void SelectItemByName(string name);

    }
}
