﻿using System.Globalization;
using System.Windows;
using System.Windows.Input;

namespace PowerTester.Framework.UI.Interfaces
{    
    public interface IUIControl
    {
        #region Properties

        /// <summary>
        /// Gets the frame work ID.
        /// </summary>
        string FrameWorkID { get; }

        
        /// <summary>
        /// Gets the class name to be selected from in the query.
        /// </summary>
        string ClassName { get; }

        /// <summary>
        /// Gets a string containing the UI Automation identifier (ID) for the element.
        /// </summary>
        string AutomationID { get; }

        /// <summary>
        /// Gets or sets the name of the control.
        /// </summary>        
        string Name { get; }

        /// <summary>
        /// Gets the process identifier. 
        /// </summary>
        int ProcessId { get; }

        /// <summary>
        /// Identifies the property that contains the runtime identifier of the element.
        /// </summary>
        int[] RuntimeId { get; }

        /// <summary>
        /// Gets the handle of the message window. 
        /// </summary>
        int Hwnd { get; }

        /// <summary>
        /// Gets the bounding rectangle of this element. 
        /// </summary>
        Rect BoundingRectangle { get; }

        /// <summary>
        /// Culture. Represents the attribute in schema: culture 
        /// </summary>
        CultureInfo Culture { get; }

        /// <summary>
        /// Associated Help Text. 
        /// </summary>
        string HelpText { get; }

        /// <summary>
        /// Gets a value that indicates whether an element is off the screen. 
        /// </summary>
        bool IsOffScreen { get; }

        /// <summary>
        /// Gets a value that indicates whether this instance is content element.
        /// </summary>
        bool IsContentElement { get; }

        /// <summary>
        /// Gets a value that indicates whether this instance is control element.
        /// </summary>
        bool IsControlElement { get; }

        /// <summary>
        /// Item Type. Represents the attribute in schema: t 
        /// </summary>
        string ItemType { get; }

        /// <summary>
        /// Gets or sets a description of the status of an item within an element. 
        /// </summary>
        string ItemStatus { get; }

        /// <summary>
        /// Gets a description of the control type. 
        /// </summary>
        string LocalizedControlType { get; }

        /// <summary>
        /// Gets or sets the accelerator key for the specified element. 
        /// </summary>
        string AcceleratorKey { get; }

        /// <summary>
        /// Gets or sets the access key that allows you to quickly navigate to the Web server control.
        /// </summary>
        string AccessKey { get; }

        /// <summary>
        /// Identifies the ClickablePoint property.
        /// </summary>
        System.Windows.Point ClickablePoint { get; }

        /// <summary>
        /// Gets a value that indicates whether the element has keyboard focus. 
        /// </summary>
        bool HasKeyboardFocus { get; }

        /// <summary>
        /// Gets or sets a value that indicates whether the timer is running. 
        /// </summary>
        bool IsEnabled { get; }

        /// <summary>
        /// Gets a value that indicates whether the element can accept keyboard focus. 
        /// </summary>
        bool IsKeyboardFocusable { get; }

        /// <summary>
        /// Identifies the property that indicates whether DockPattern is available on this UI Automation element.
        /// </summary>
        bool IsDockPatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether ExpandCollapsePattern is available on this UI Automation element.
        /// </summary>
        bool IsExpandCollapsePatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether GridItemPattern is available on this UI Automation element
        /// </summary>
        bool IsGridItemPatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether GridPattern is available on this UI Automation element.
        /// </summary>
        bool IsGridPatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether InvokePattern is available on this UI Automation element.
        /// </summary>
        bool IsInvokePatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether MultipleViewPattern is available on this UI Automation element.
        /// </summary>
        bool IsMultipleViewPatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether RangeValuePattern is available on this UI Automation element.
        /// </summary>
        bool IsRangeValuePatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether ScrollItemPattern is available for this UI Automation element.
        /// </summary>
        bool IsScrollItemPatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether ScrollPattern is available on this UI Automation element.
        /// </summary>
        bool IsScrollPatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether SelectionItemPattern is available on this UI Automation element.
        /// </summary>
        bool IsSelectionItemPatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether SelectionPattern is available on this UI Automation element.
        /// </summary>
        bool IsSelectionPatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether the TableItemPattern is available on this UI Automation element.
        /// </summary>
        bool IsTableItemPatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether TablePattern is available on this UI Automation element.
        /// </summary>
        bool IsTablePatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether TextPattern is available on this UI Automation element.
        /// </summary>
        bool IsTextPatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether TogglePattern is available on this UI Automation element.
        /// </summary>
        bool IsTogglePatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether TransformPattern is available on this UI Automation element.
        /// </summary>
        bool IsTransformPatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether ValuePattern is available on this UI Automation element.
        /// </summary>
        bool IsValuePatternAvailable { get; }

        /// <summary>
        /// Identifies the property that indicates whether WindowPattern is available on this UI Automation element.
        /// </summary>
        bool IsWindowPatternAvailable { get; }

        /// <summary>
        /// Gets or sets a value that indicates whether the specified element is required to be filled out on a form. 
        /// </summary>
        bool IsRequiredForForm { get; }
        
        /// <summary>
        /// Determines whether the specified array contains elements that match the conditions defined by the specified predicate.
        /// </summary>
        bool Exists { get; }

        #endregion

        #region Methods

        /// <summary>
        /// /// Sets the focus on the UIControl.
        /// </summary>
        void SetFocus();

        /// <summary>
        /// It sets the property comes from.(Cashed or Current)
        /// </summary>
        /// <param name="Cashed">if set to <c>true</c> [cashed].</param>
        void SetPropertySource(bool Cashed);

        /// <summary> 
        /// Gets value indicating whether the element exists
        /// </summary>
        /// <param name="timeout">The Timeout</param>
        /// <returns>Gets value indicating whether the element exists</returns>
        bool WaitFor(int timeout);

        /// <summary>
        /// Clicks this instance.
        /// </summary>
        void Click();

        /// <summary>
        /// Double-clicks this instance.
        /// </summary>
        void DoubleClick();

        /// <summary>
        /// Clicks this instance by right mousebutton.
        /// </summary>
        /// <param name="useRightButton">if set to <c>true</c> [use right button].</param>
        void Click(bool useRightButton);

        /// <summary>
        /// Double-clicks this instance by right mousebutton.
        /// </summary>
        /// <param name="useRightButton">if set to <c>true</c> [use right button].</param>
        void DoubleClick(bool useRightButton);

        /// <summary>
        /// Clicks the specified point to click.
        /// </summary>
        /// <param name="pointToClick">The point to click.</param>
        void Click(System.Windows.Point pointToClick);

        /// <summary>
        /// Double-clicks the specified point to click.
        /// </summary>
        /// <param name="pointToClick">The point to click.</param>
        void DoubleClick(System.Windows.Point pointToClick);

        /// <summary>
        /// Clicks the specified point to click by right mousebutton.
        /// </summary>
        /// <param name="pointToClick">The point to click.</param>
        /// <param name="useRightButton">if set to <c>true</c> [use right button].</param>
        void Click(System.Windows.Point pointToClick, bool useRightButton);

        /// <summary>
        /// Double-clicks the specified point to click by right mousebutton.
        /// </summary>
        /// <param name="pointToClick">The point to click.</param>
        /// <param name="useRightButton">if set to <c>true</c> [use right button].</param>
        void DoubleClick(System.Windows.Point pointToClick, bool useRightButton);

        /// <summary>
        /// Methods is being invoked when the control is clicked
        /// </summary>
        /// <param name="clickedPoint">The clicked point.</param>
        void OnClick(System.Windows.Point clickedPoint);

        /// <summary>
        /// Sends the key and a functional key to the control (e.g.: CTRL+1)
        /// </summary>
        /// <param name="key">The key.</param>
        void SendKey(Key key);

        /// <summary>
        /// Sends the key and a functional key to the control (e.g.: CTRL+1)
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="setFocus"></param>
        void SendKey(Key key, bool setFocus);

        /// <summary>
        /// Sends the key and a functional key to the control (e.g.: CTRL+1)
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="functionKey">The function key.</param>
        void SendKey(Key key, Key functionKey);

        /// <summary>
        /// Sends the key and a functional key to the control (e.g.: CTRL+1)
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="functionKey">The function key.</param>
        /// <param name="setFocus"></param>
        void SendKey(Key key, Key functionKey, bool setFocus);

        /// <summary>
        /// Moves the mouse to this instance.
        /// </summary>
        void MouseMoveTo();

        /// <summary>
        /// Moves the mouse to this instance and clicks.
        /// </summary>
        void MouseMoveToAndClick();

        #endregion       
    }
}
