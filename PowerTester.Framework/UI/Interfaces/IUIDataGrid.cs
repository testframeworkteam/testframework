﻿namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIDataGrid
    {

        /// <summary>
        /// Selects all the items given in the list
        /// </summary>
        /// <param name="names"></param>
        void SelectItemsByName(params string[] names);

        /// <summary>
        /// DeSelects all the items given in the list
        /// </summary>
        /// <param name="names"></param>
        void DeSelectItemsByName(params string[] names);

        /// <summary>
        /// Sets the scroll position in percent.
        /// </summary>
        /// <param name="horizontalPercent">The horizontal percent.</param>
        /// <param name="verticalPercent">The vertical percent.</param>
        /// <remarks>If the Scroll pattern is not supported then it throws and exception! </remarks>
        void SetScrollPercent(double horizontalPercent, double verticalPercent);

    }
}
