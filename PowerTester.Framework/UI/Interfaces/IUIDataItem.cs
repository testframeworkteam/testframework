﻿
namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIDataItem
    {

        /// <summary>
        /// Gets a value which shows whether the item is enabled or not.
        /// </summary>
        bool IsSelected { get; }

        /// <summary>
        /// Selects the UIDataItem
        /// </summary>
        void Select();

        /// <summary>
        /// Selects the UIDataItem and adds it to the selected list item collection
        /// </summary>
        void AddToSelection();

        /// <summary>
        /// DeSelects the UIDataItem and removes it from the selected list item collection
        /// </summary>
        void RemoveFromSelection();

    }
}
