﻿namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIDocument
    {

        /// <summary>
        /// Sets the scroll position in percent.
        /// </summary>
        /// <param name="horizontalPercent">The horizontal percent.</param>
        /// <param name="verticalPercent">The vertical percent.</param>
        /// <remarks>If the Scroll pattern is not supported then it throws and exception! </remarks>
        void SetScrollPercent(double horizontalPercent, double verticalPercent);

    }
}
