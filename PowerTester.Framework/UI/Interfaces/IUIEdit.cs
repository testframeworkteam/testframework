﻿namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIEdit
    {
        /// <summary>
        /// Identifies the IsPassword property.
        /// </summary>
        bool IsPassword { get; }
    }
}
