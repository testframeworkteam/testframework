﻿namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIGridItem
    {
        /// <summary>
        /// Gets the number of column
        /// </summary>
        int Column { get; }

        /// <summary>
        /// Gets the number of row
        /// </summary>
        int Row { get; }

        /// <summary>
        /// Gets the value of UIGridItem control.
        /// </summary>
        string Value { get; }

    }
}
