﻿namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIHeaderItem
    {
        /// <summary>
        /// Sends a request to activate a control and initiate its single, unambiguous action. 
        /// </summary>
        void Invoke();

    }
}
