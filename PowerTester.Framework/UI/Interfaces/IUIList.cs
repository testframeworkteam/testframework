﻿namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIList
    {
        /// <summary>
        /// Selects an UIListItem control by its name
        /// </summary>
        /// <param name="name">UIListItem control name</param>
        void SelectItemByName(string name);
    }
}
