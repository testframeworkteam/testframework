﻿namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIListBox
    {

        /// <summary>
        /// Gets a value which specifies whether the control supports the multiselect mode or not.
        /// </summary>
        bool CanSelectMultiple { get; }

        /// <summary>
        /// Selects an UIListItem control by its name
        /// </summary>
        /// <param name="name">UIListIten control name</param>
        void SelectItemByName(string name);

    }
}
