﻿using System.Windows.Automation;

namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIListItem
    {

        /// <summary>
        /// Gets a value that indicates whether an item is selected. 
        /// </summary>
        bool IsSelected { get; }

        /// <summary>
        /// Gets the value of control.
        /// </summary>
        string Value { get; }

        /// <summary>
        /// Adds the current element to the collection of selected items. 
        /// </summary>
        void AddToSelection();

        /// <summary>
        /// Removes the current element from the collection of selected items. 
        /// </summary>
        void RemoveFromSelection();

        /// <summary>
        /// Contains values that specify the ToggleState of checkable list item
        /// </summary>
        ToggleState ToggleState { get; }

        /// <summary>
        /// Cycles through the toggle states of a checkable list item. 
        /// </summary>
        void Toggle();

        /// <summary>
        /// Sets the toggle state of checkable list item. 
        /// </summary>
        /// <param name="State">ToggleState</param>
        void SetToggleState(ToggleState State);
    }
}
