﻿using System.Windows.Automation;

namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIMenuItem
    {
        /// <summary>
        /// Sends a request to activate a control and initiate its single, unambiguous action. 
        /// </summary>
        void Invoke();

        /// <summary>
        /// Gets a value which indicates whether the control is selected.
        /// </summary>
        /// <returns></returns>
        bool IsSelected();

        /// <summary>
        /// Deselects any selected items and then selects the current element. 
        /// </summary>
        void Select();

        /// <summary>
        /// Retrieves the toggle state of the control. 
        /// </summary>
        ToggleState ToggleState { get; }

        /// <summary>
        /// Displays all child nodes, controls, or content of the UIMenuItem. 
        /// </summary>
        void Expand();

        /// <summary>
        /// Hides all descendant nodes, controls, or content of the UIMenuItem. 
        /// </summary>
        void Collapse();

        /// <summary>
        /// Sets the toggle state of control if the control supports this pattern.        
        /// </summary>
        /// <param name="State"></param>
        void SetToggleState(ToggleState State);

        /// <summary>
        /// Gets a value which indicates whether the control supports the toggle state. Checkable menu item supports the TooglePattern.
        /// </summary>
        bool IsTooglePatternAvailable { get; }


    }
}
