﻿namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIProgressBar
    {
        /// <summary>
        /// Gets a value that specifies whether the control is read only or not.
        /// </summary>
        bool IsReadOnly { get; }

        /// <summary>
        /// Gets the maximum range value supported by the UIProgressBar control
        /// </summary>
        double Maximum {get;}

        /// <summary>
        /// Gets the minimum range value supported by the UIProgressBar control
        /// </summary>
        double Minimum {get; }

        /// <summary>
        /// Gets the current value of UIProgressBar control.
        /// </summary>
        double Value { get; }

        /// <summary>
        /// Sets the value of the UIProgressBar control
        /// </summary>
        /// <param name="value">Value of control</param>
        void SetValue(double value);
    }
}
