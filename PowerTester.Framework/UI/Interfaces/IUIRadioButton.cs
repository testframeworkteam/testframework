﻿namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIRadioButton
    {
        /// <summary>
        /// Gets a value which indicates whether the control is selected.
        /// </summary>
        bool IsSelected {get; }

        /// <summary>
        /// Deselects any selected items and then selects the current element. 
        /// </summary>        
        void Select();

    }
}
