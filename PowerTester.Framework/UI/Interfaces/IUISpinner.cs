﻿namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUISpinner
    {
        /// <summary>
        /// Gets or sets the value of control.
        /// </summary>
        double Value {get;}

        /// <summary>
        /// Gets a value which indicates whether the control is read only.
        /// </summary>
        bool IsReadOnly {get;}
        
        /// <summary>
        /// Gets a value which indicates the minimum number of control.
        /// </summary>
        double Minimum {get;}

        /// <summary>
        /// Gets a value which indicates the maximum number of control.
        /// </summary>
        double Maximum {get;}

        /// <summary>
        /// Increases the value of control by step.
        /// </summary>
        void Increment();
        
        /// <summary>
        /// Decreases the value of control by step.
        /// </summary>
        void Decrement();        
    }
}
