﻿namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIStaticText
    {
        /// <summary>
        /// Returns the text of control
        /// </summary> 
        string GetText();

        /// <summary>
        /// It was just intended to hide the method and it is always true
        /// </summary>
        bool IsReadOnly();        
    }
}
