﻿namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUITabItem
    {
        /// <summary>
        /// Deselects any selected items and then selects the current element. 
        /// </summary>
        void Select();
    }
}
