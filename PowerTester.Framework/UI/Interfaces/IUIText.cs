﻿using System.Windows.Input;

namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIText
    {

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        string Value { get; set; }


        /// <summary>
        /// Gets a value indicating whether this instance is read only.
        /// </summary>
        bool IsReadOnly { get; }

        /// <summary>
        /// Types the specified text.
        /// </summary>
        void Type(string text);

        /// <summary>
        /// Keys the board input.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="press">if set to <c>true</c> [press].</param>
        void KeyBoardInput(Key key, bool press);

        /// <summary>
        /// Gets the text.
        /// </summary>        
        string GetText();

        /// <summary>
        /// Gets the text.
        /// </summary>
        /// <param name="maxLength">Length of the max.</param>
        /// <returns></returns>
        string GetText(int maxLength);

    }
}
