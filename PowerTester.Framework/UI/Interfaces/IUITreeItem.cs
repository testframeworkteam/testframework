﻿namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUITreeItem
    {
        /// <summary>
        /// Gets a value that indicates whether an item is selected. 
        /// </summary>
        bool IsSelected();
        
        /// <summary>
        /// Deselects any selected items and then selects the current element. 
        /// </summary>
        void Select();
        
        /// <summary>
        /// Displays all child nodes, controls, or content of the control.
        /// </summary>
        void Expand();
        
        /// <summary>
        /// Hides all nodes, controls, or content that are descendants of the control.
        /// </summary>
        void Collapse();
    }
}
