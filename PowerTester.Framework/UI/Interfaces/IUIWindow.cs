﻿using System.Windows.Automation;

namespace PowerTester.Framework.UI.Interfaces
{
    public interface IUIWindow
    {
        /// <summary>
        /// Gets a value that specifies whether the window can be maximized.
        /// </summary>
        bool CanMaximize { get; }

        /// <summary>
        /// Gets a value that specifies whether the window can be minimized.
        /// </summary>
        bool CanMinimize { get; }

        /// <summary>
        /// Gets a value that specifies whether the window is modal.
        /// </summary>
        bool IsModal { get; }

        /// <summary>
        /// Gets a value that specifies whether the window is the topmost element in the z-order.
        /// </summary>
        bool IsTopmost { get; }

        /// <summary>
        /// Gets the visual state of the window.
        /// </summary>
        WindowVisualState WindowsVisualState { get; }

        /// <summary>
        /// Attempts to close the window.
        /// </summary>
        void Close();
        

        /// <summary>
        /// Changes the visual state of the window.
        /// </summary>
        /// <param name="VisualState"></param>
        void SetWindowVisualState(WindowVisualState VisualState);
        
        /// <summary>
        /// Causes the calling code to block for the specified time or until the associated process enters an idle state, whichever completes first.
        /// </summary>
        /// <param name="milliseconds"></param>
        void WaitForInputIdle(int milliseconds);
        
    }
}
