﻿
using System;

namespace PowerTester.Framework.UI
{

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [Serializable]
    public struct PointDescriptor
    {
        private double _x;
        private double _y;

        public double X { get { return _x; } set { _x = value; } }
        public double Y { get { return _y; } set { _y = value; } }

        public PointDescriptor(double x, double y)
        {
            _x = x;
            _y = y;
        }
    }
}
