﻿
using System;
using System.Diagnostics.Contracts;

namespace PowerTester.Framework.UI
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [Serializable]
    public struct RectDescriptor
    {
        private double _x;
        private double _y;
        private double _width;
        private double _height;

        public double X
        {
            get { return _x; }
            set { _x = value; }
        }

        public double Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public double Width
        {
            get { return _width; }
            set { _width = value; }
        }

        public double Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public RectDescriptor(double x, double y, double width, double height)
        {
            Contract.Requires<ArgumentException>(width >= 0);
            Contract.Requires<ArgumentException>(height >= 0);

            _x = x;
            _y = y;
            _width = width;
            _height = height;
        }

        public override string ToString()
        {
            return string.Format("[RectDescriptor X={0} Y={1}, W={2}, H={3}]", X, Y, Width, Height);
        }
    }
}
