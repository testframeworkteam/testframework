﻿namespace PowerTester.Framework.UI
{
    public class UiFind
    {
        /// <summary>
        /// Specifies basic values for platform
        /// </summary>
        public enum PlatformType
        {
            Windows,
            Mac
        }

        /// <summary>
        /// Specifies basic value for finding method.
        /// </summary>
        public enum FindBy
        {
            Name,
            AutomationId,
            ClassName,
            Type,
            Attribute,
            None
        }

        /// <summary>
        /// Specifies basic value for finding method.
        /// </summary>
        public enum TreeScope
        {
            Ancestors,
            Children,
            Descendants,
            Element,
            Parent,
            Subtree
        }

        public enum ControlType
        {
            Button,
            Calendar,
            CheckBox,
            ComboBox,
            Control,
            Custom,
            DataGrid,
            DataItem,
            Document,
            Edit,
            Group,
            Header,
            HeaderItem,
            Hyperlink,
            Image,
            List,
            ListItem,
            Menu,
            MenuBar,
            MenuItem,
            Pane,
            ProgressBar,
            RadioButton,
            ScrollBar,
            Separator,
            Slider,
            Spinner,
            SplitButton,
            StatusBar,
            Tab,
            TabItem,
            Table,
            Text,
            Thumb,
            TitleBar,
            ToolBar,
            ToolTip,
            Tree,
            TreeItem,
            Window
        }
    }
}
