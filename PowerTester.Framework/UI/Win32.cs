using System;
using System.Runtime.InteropServices;
using System.Text;

namespace PowerTester.Framework.UI
{

    /// <summary>
    /// This class is used to PInvoke for win32 functionality.
    /// </summary>
    public class Win32
    {
        public static readonly IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);

        #region Native Function Declaration and Enumerations
  
        [Flags]
        public enum ProcessAccessTypes
        {
            PROCESS_TERMINATE = 0x00000001,
            PROCESS_CREATE_THREAD = 0x00000002,
            PROCESS_SET_SESSIONID = 0x00000004,
            PROCESS_VM_OPERATION = 0x00000008,
            PROCESS_VM_READ = 0x00000010,
            PROCESS_VM_WRITE = 0x00000020,
            PROCESS_DUP_HANDLE = 0x00000040,
            PROCESS_CREATE_PROCESS = 0x00000080,
            PROCESS_SET_QUOTA = 0x00000100,
            PROCESS_SET_INFORMATION = 0x00000200,
            PROCESS_QUERY_INFORMATION = 0x00000400,
            STANDARD_RIGHTS_REQUIRED = 0x000F0000,
            SYNCHRONIZE = 0x00100000,
            PROCESS_ALL_ACCESS = PROCESS_TERMINATE | PROCESS_CREATE_THREAD | PROCESS_SET_SESSIONID | PROCESS_VM_OPERATION |
              PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_DUP_HANDLE | PROCESS_CREATE_PROCESS | PROCESS_SET_QUOTA |
              PROCESS_SET_INFORMATION | PROCESS_QUERY_INFORMATION | STANDARD_RIGHTS_REQUIRED | SYNCHRONIZE
        }

        [Flags]
        public enum AccessProtectionFlags
        {
            PAGE_NOACCESS = 0x001,
            PAGE_READONLY = 0x002,
            PAGE_READWRITE = 0x004,
            PAGE_WRITECOPY = 0x008,
            PAGE_EXECUTE = 0x010,
            PAGE_EXECUTE_READ = 0x020,
            PAGE_EXECUTE_READWRITE = 0x040,
            PAGE_EXECUTE_WRITECOPY = 0x080,
            PAGE_GUARD = 0x100,
            PAGE_NOCACHE = 0x200,
            PAGE_WRITECOMBINE = 0x400
        }

        [Flags]
        public enum VirtualAllocExTypes
        {
            WRITE_WATCH_FLAG_RESET = 0x00000001, // Win98 only
            MEM_COMMIT = 0x00001000,
            MEM_RESERVE = 0x00002000,
            MEM_COMMIT_OR_RESERVE = 0x00003000,
            MEM_DECOMMIT = 0x00004000,
            MEM_RELEASE = 0x00008000,
            MEM_FREE = 0x00010000,
            MEM_PUBLIC = 0x00020000,
            MEM_MAPPED = 0x00040000,
            MEM_RESET = 0x00080000, // Win2K only
            MEM_TOP_DOWN = 0x00100000,
            MEM_WRITE_WATCH = 0x00200000, // Win98 only
            MEM_PHYSICAL = 0x00400000, // Win2K only
            //MEM_4MB_PAGES    = 0x80000000, // ??
            SEC_IMAGE = 0x01000000,
            MEM_IMAGE = SEC_IMAGE
        }

        public enum GWL
        {
            GWL_WNDPROC = (-4),
            GWL_HINSTANCE = (-6),
            GWL_HWNDPARENT = (-8),
            GWL_STYLE = (-16),
            GWL_EXSTYLE = (-20),
            GWL_USERDATA = (-21),
            GWL_ID = (-12)
        }

        [DllImport("user32.dll", SetLastError = true)]
        public static extern int SendInput(int nInputs, ref INPUT mi, int cbSize);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int MapVirtualKey(int nVirtKey, int nMapType);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int GetAsyncKeyState(int nVirtKey);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int GetKeyState(int nVirtKey);

        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern int GetKeyboardState(byte[] keystate);

        [DllImport("user32.dll", ExactSpelling = true, EntryPoint = "keybd_event", CharSet = CharSet.Auto)]
        public static extern void Keybd_event(byte vk, byte scan, int flags, int extrainfo);

        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern int SetKeyboardState(byte[] keystate);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(Win32.HWND hWnd, int nMsg, IntPtr wParam, IntPtr lParam);

        // Overload for WM_GETTEXT
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(Win32.HWND hWnd, int nMsg, IntPtr wParam, System.Text.StringBuilder lParam);

        [DllImport("user32.dll")]
        public static extern int GetSystemMetrics(int metric);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetWindowLong(Win32.HWND hWnd, int nIndex);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("gdi32.dll")]
        public static extern int GetBkColor(IntPtr hdc);

        [DllImport("gdi32.dll", SetLastError = false, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int GetTextColor(IntPtr hDC);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetScrollInfo(IntPtr hwnd, int fnBar, ref ScrollInfo lpsi);

        [DllImport("user32.dll")]
        public static extern int SetScrollInfo(IntPtr hwnd, int fnBar, [In] ref ScrollInfo lpsi, bool fRedraw);

        [DllImport("User32.dll")]
        public static extern int RegisterWindowMessage(string lpString);

        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        public static extern Int32 FindWindow(String lpClassName, String lpWindowName);

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, ref COPYDATASTRUCT lParam);

        [DllImport("User32.dll", EntryPoint = "PostMessage")]
        public static extern int PostMessage(IntPtr hWnd, int Msg, int wParam, ref COPYDATASTRUCT lParam);

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);        

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);        

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, String lParam);        

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, StringBuilder lParam);

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hwnd, int Msg, int wParam, ref TCITEM lParam);

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hwnd, int Msg, int wParam, ref CNmToolbar lParam);

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hwnd, int Msg, int wParam, ref CComboboxItemData lParam);

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hwnd, int Msg, int wParam, ref RECT lParam);

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hwnd, int Msg, int wParam, IntPtr lParam);

        [DllImport("User32.dll", EntryPoint = "PostMessage")]
        public static extern int PostMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [DllImport("User32.dll", EntryPoint = "SetForegroundWindow")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(ProcessAccessTypes dwDesiredAccess, [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, uint dwProcessId);

        [DllImport("kernel32.dll")]
        public static extern bool CloseHandle(IntPtr hHandle);

        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        public static extern IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress, uint dwSize, VirtualAllocExTypes flAllocationType, AccessProtectionFlags flProtect);

        [DllImport("kernel32.dll")]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, UIntPtr nSize, out IntPtr lpNumberOfBytesWritten);

        [DllImport("kernel32.dll")]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, UIntPtr nSize, IntPtr lpNumberOfBytesWritten);

        [DllImport("kernel32.dll")]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, UIntPtr nSize, IntPtr lpNumberOfBytesWritten);

        [DllImport("kernel32.dll")]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, ref TCITEM lpBuffer, int nSize, IntPtr lpNumberOfBytesWritten);
        
        [DllImport("kernel32.dll")]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, ref CComboboxItemData lpBuffer, int nSize, IntPtr lpNumberOfBytesWritten);

        [DllImport("kernel32.dll")]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, ref CComboboxItemData lpBuffer, int nSize, [Out] int lpNumberOfBytesWritten);

        [DllImport("kernel32.dll")]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, ref CControlRawData lpBuffer, int nSize, IntPtr lpNumberOfBytesWritten);
        
        [DllImport("kernel32.dll")]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, ref CToolbarButton lpBuffer, int nSize, IntPtr lpNumberOfBytesWritten);

        [DllImport("kernel32.dll")]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, ref RECT lpBuffer, int nSize, IntPtr lpNumberOfBytesWritten);

        [DllImport("kernel32.dll")]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, ref CMenuRawData lpBuffer, int nSize, IntPtr lpNumberOfBytesWritten);

        [DllImport("kernel32.dll")]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, ref CNmToolbar lpBuffer, int nSize, IntPtr lpNumberOfBytesWritten);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, [Out()] byte[] lpBuffer, int dwSize, out int lpNumberOfBytesRead);       

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, [Out(), MarshalAs(UnmanagedType.AsAny)] object lpBuffer, int dwSize, out int lpNumberOfBytesRead);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, int dwSize, out int lpNumberOfBytesRead);

        //[DllImport("kernel32.dll", SetLastError = true)]
        //public static unsafe extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, void* lpBuffer, int dwSize, out int lpNumberOfBytesRead);

        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        public static extern bool VirtualFreeEx(IntPtr hProcess, IntPtr lpAddress, UIntPtr dwSize, VirtualAllocExTypes dwFreeType);

        //[DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        //public static unsafe extern bool VirtualFreeEx(IntPtr hProcess, byte* pAddress, UIntPtr size, VirtualAllocExTypes freeType);

        [DllImport("User32.dll", EntryPoint = "GetDlgCtrlID")]
        public static extern int GetDlgCtrlID(IntPtr hwndCtl);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPlacement(IntPtr hWnd, [In] ref WINDOWPLACEMENT lpwndpl);

        [DllImport("user32.dll", EntryPoint = "GetWindowLong")]
        private static extern IntPtr GetWindowLongPtr32(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", EntryPoint = "GetWindowLongPtr")]
        private static extern IntPtr GetWindowLongPtr64(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, ExactSpelling = true, SetLastError = true)]
        public static extern bool GetWindowRect(IntPtr hWnd, ref RECT rect);

        [DllImport("user32.dll")]
        public static extern IntPtr GetMenu(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern IntPtr GetSubMenu(IntPtr hMenu, int nPos);

        [DllImport("user32.dll")]
        public static extern uint GetMenuState(IntPtr hMenu, uint uId, uint uFlags);

        [DllImport("user32.dll")]
        public static extern uint GetMenuItemID(IntPtr hMenu, int nPos);

        [DllImport("user32.dll")]
        public static extern int GetMenuString(IntPtr hMenu, uint uIDItem, [Out, MarshalAs(UnmanagedType.LPStr)] StringBuilder lpString, int nMaxCount, uint uFlag);

        [DllImport("user32.dll", ExactSpelling = true)]
        public static extern bool IsWindowVisible(IntPtr hWnd);
 
        [DllImport("user32.dll", CharSet=CharSet.Unicode, SetLastError=true)]
        public static extern IntPtr SendMessageTimeout(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, int flags, int uTimeout, out IntPtr pResult);        

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWindowEnabled(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern bool GetComboBoxInfo(IntPtr hWnd, ref COMBOBOXINFO pcbi);

        [DllImport("user32.dll")]
        public static extern IntPtr SetFocus(IntPtr hWnd);

        #endregion

        #region Message Constants

        /// <summary>
        /// SendInput related
        /// </summary>
        public const int VK_SHIFT = 0x10;
        public const int VK_CONTROL = 0x11;
        public const int VK_MENU = 0x12;

        public const int KEYEVENTF_EXTENDEDKEY = 0x0001;
        public const int KEYEVENTF_KEYUP = 0x0002;
        public const int KEYEVENTF_UNICODE = 0x0004;
        public const int KEYEVENTF_SCANCODE = 0x0008;

        public const int MOUSEEVENTF_VIRTUALDESK = 0x4000;


        /// <summary>
        /// GetSystemMetrics
        /// </summary>
        public const int SM_CXMAXTRACK = 59;
        public const int SM_CYMAXTRACK = 60;
        public const int SM_XVIRTUALSCREEN = 76;
        public const int SM_YVIRTUALSCREEN = 77;
        public const int SM_CXVIRTUALSCREEN = 78;
        public const int SM_CYVIRTUALSCREEN = 79;
        public const int SM_SWAPBUTTON = 23;


        /// <summary>
        /// Window style information
        /// </summary>
        public const int GWL_STYLE = -16;
        public const int GWL_EXSTYLE = -20;

        public const int ES_MULTILINE = 0x0004;
        public const int ES_AUTOVSCROLL = 0x0040;
        public const int ES_AUTOHSCROLL = 0x0080;

        public const int INPUT_MOUSE = 0;
        public const int INPUT_KEYBOARD = 1;

        public const UInt32 MN_GETHMENU = 0x01E1;

        public const UInt32 MF_BYCOMMAND = 0x00000000;
        public const UInt32 MF_BYPOSITION = 0x00000400;

        public const int SC_CLOSE = 0xF060;

        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        static readonly IntPtr HWND_TOP = new IntPtr(0);

        public const UInt32 SWP_NOSIZE = 0x0001;
        public const UInt32 SWP_NOMOVE = 0x0002;
        public const UInt32 SWP_NOZORDER = 0x0004;
        public const UInt32 SWP_NOREDRAW = 0x0008;
        public const UInt32 SWP_NOACTIVATE = 0x0010;
        public const UInt32 SWP_FRAMECHANGED = 0x0020;  /* The frame changed: send WM_NCCALCSIZE */
        public const UInt32 SWP_SHOWWINDOW = 0x0040;
        public const UInt32 SWP_HIDEWINDOW = 0x0080;
        public const UInt32 SWP_NOCOPYBITS = 0x0100;
        public const UInt32 SWP_NOOWNERZORDER = 0x0200;  /* Don't do owner Z ordering */
        public const UInt32 SWP_NOSENDCHANGING = 0x0400;  /* Don't send WM_WINDOWPOSCHANGING */

        public const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;

        /// <summary>
        /// Window Styles 
        /// </summary>
        public const UInt32 WS_OVERLAPPED = 0;
        public const UInt32 WS_POPUP = 0x80000000;
        public const UInt32 WS_CHILD = 0x40000000;
        public const UInt32 WS_MINIMIZE = 0x20000000;
        public const UInt32 WS_VISIBLE = 0x10000000;
        public const UInt32 WS_DISABLED = 0x8000000;
        public const UInt32 WS_CLIPSIBLINGS = 0x4000000;
        public const UInt32 WS_CLIPCHILDREN = 0x2000000;
        public const UInt32 WS_MAXIMIZE = 0x1000000;
        public const UInt32 WS_CAPTION = 0xC00000;      // WS_BORDER or WS_DLGFRAME  
        public const UInt32 WS_BORDER = 0x800000;
        public const UInt32 WS_DLGFRAME = 0x400000;
        public const UInt32 WS_VSCROLL = 0x200000;
        public const UInt32 WS_HSCROLL = 0x100000;
        public const UInt32 WS_SYSMENU = 0x80000;
        public const UInt32 WS_THICKFRAME = 0x40000;
        public const UInt32 WS_GROUP = 0x20000;
        public const UInt32 WS_TABSTOP = 0x10000;
        public const UInt32 WS_MINIMIZEBOX = 0x20000;
        public const UInt32 WS_MAXIMIZEBOX = 0x10000;
        public const UInt32 WS_TILED = WS_OVERLAPPED;
        public const UInt32 WS_ICONIC = WS_MINIMIZE;
        public const UInt32 WS_SIZEBOX = WS_THICKFRAME;
        
        /// <summary>
        /// Extended Window Styles 
        /// </summary>
        public const UInt32 WS_EX_DLGMODALFRAME = 0x0001;
        public const UInt32 WS_EX_NOPARENTNOTIFY = 0x0004;
        public const UInt32 WS_EX_TOPMOST = 0x0008;
        public const UInt32 WS_EX_ACCEPTFILES = 0x0010;
        public const UInt32 WS_EX_TRANSPARENT = 0x0020;
        public const UInt32 WS_EX_MDICHILD = 0x0040;
        public const UInt32 WS_EX_TOOLWINDOW = 0x0080;
        public const UInt32 WS_EX_WINDOWEDGE = 0x0100;
        public const UInt32 WS_EX_CLIENTEDGE = 0x0200;
        public const UInt32 WS_EX_CONTEXTHELP = 0x0400;
        public const UInt32 WS_EX_RIGHT = 0x1000;
        public const UInt32 WS_EX_LEFT = 0x0000;
        public const UInt32 WS_EX_RTLREADING = 0x2000;
        public const UInt32 WS_EX_LTRREADING = 0x0000;
        public const UInt32 WS_EX_LEFTSCROLLBAR = 0x4000;
        public const UInt32 WS_EX_RIGHTSCROLLBAR = 0x0000;
        public const UInt32 WS_EX_CONTROLPARENT = 0x10000;
        public const UInt32 WS_EX_STATICEDGE = 0x20000;
        public const UInt32 WS_EX_APPWINDOW = 0x40000;
        public const UInt32 WS_EX_OVERLAPPEDWINDOW = (WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE);
        public const UInt32 WS_EX_PALETTEWINDOW = (WS_EX_WINDOWEDGE | WS_EX_TOOLWINDOW | WS_EX_TOPMOST);
        public const UInt32 WS_EX_LAYERED = 0x00080000;
        public const UInt32 WS_EX_NOINHERITLAYOUT = 0x00100000; // Disable inheritence of mirroring by children
        public const UInt32 WS_EX_LAYOUTRTL = 0x00400000; // Right to left mirroring
        public const UInt32 WS_EX_COMPOSITED = 0x02000000;
        public const UInt32 WS_EX_NOACTIVATE = 0x08000000;

        public const int SW_HIDE = 0;
        public const int SW_SHOWNORMAL = 1;
        public const int SW_NORMAL = 1;
        public const int SW_SHOWMINIMIZED = 2;
        public const int SW_SHOWMAXIMIZED = 3;
        public const int SW_MAXIMIZE = 3;
        public const int SW_SHOWNOACTIVATE = 4;
        public const int SW_SHOW = 5;
        public const int SW_MINIMIZE = 6;
        public const int SW_SHOWMINNOACTIVE = 7;
        public const int SW_SHOWNA = 8;
        public const int SW_RESTORE = 9;
        public const int SW_SHOWDEFAULT = 10;
        public const int SW_FORCEMINIMIZE = 11;
        public const int SW_MAX = 11;

        public const int WM_NULL = 0x00;
        public const int WM_CREATE = 0x01;
        public const int WM_DESTROY = 0x02;
        public const int WM_MOVE = 0x03;
        public const int WM_SIZE = 0x05;
        public const int WM_ACTIVATE = 0x06;
        public const int WM_SETFOCUS = 0x07;
        public const int WM_KILLFOCUS = 0x08;
        public const int WM_ENABLE = 0x0A;
        public const int WM_SETREDRAW = 0x0B;
        public const int WM_SETTEXT = 0x0C;
        public const int WM_GETTEXT = 0x0D;
        public const int WM_GETTEXTLENGTH = 0x0E;
        public const int WM_PAINT = 0x0F;
        public const int WM_CLOSE = 0x10;
        public const int WM_QUERYENDSESSION = 0x11;
        public const int WM_QUIT = 0x12;
        public const int WM_QUERYOPEN = 0x13;
        public const int WM_ERASEBKGND = 0x14;
        public const int WM_SYSCOLORCHANGE = 0x15;
        public const int WM_ENDSESSION = 0x16;
        public const int WM_SYSTEMERROR = 0x17;
        public const int WM_SHOWWINDOW = 0x18;
        public const int WM_CTLCOLOR = 0x19;
        public const int WM_WININICHANGE = 0x1A;
        public const int WM_SETTINGCHANGE = 0x1A;
        public const int WM_DEVMODECHANGE = 0x1B;
        public const int WM_ACTIVATEAPP = 0x1C;
        public const int WM_FONTCHANGE = 0x1D;
        public const int WM_TIMECHANGE = 0x1E;
        public const int WM_CANCELMODE = 0x1F;
        public const int WM_SETCURSOR = 0x20;
        public const int WM_MOUSEACTIVATE = 0x21;
        public const int WM_CHILDACTIVATE = 0x22;
        public const int WM_QUEUESYNC = 0x23;
        public const int WM_GETMINMAXINFO = 0x24;
        public const int WM_PAINTICON = 0x26;
        public const int WM_ICONERASEBKGND = 0x27;
        public const int WM_NEXTDLGCTL = 0x28;
        public const int WM_SPOOLERSTATUS = 0x2A;
        public const int WM_DRAWITEM = 0x2B;
        public const int WM_MEASUREITEM = 0x2C;
        public const int WM_DELETEITEM = 0x2D;
        public const int WM_VKEYTOITEM = 0x2E;
        public const int WM_CHARTOITEM = 0x2F;

        public const int WM_SETFONT = 0x30;
        public const int WM_GETFONT = 0x31;
        public const int WM_SETHOTKEY = 0x32;
        public const int WM_GETHOTKEY = 0x33;
        public const int WM_QUERYDRAGICON = 0x37;
        public const int WM_COMPAREITEM = 0x39;
        public const int WM_COMPACTING = 0x41;
        public const int WM_WINDOWPOSCHANGING = 0x46;
        public const int WM_WINDOWPOSCHANGED = 0x47;
        public const int WM_POWER = 0x48;
        public const int WM_COPYDATA = 0x4A;
        public const int WM_CANCELJOURNAL = 0x4B;
        public const int WM_NOTIFY = 0x4E;
        public const int WM_INPUTLANGCHANGEREQUEST = 0x50;
        public const int WM_INPUTLANGCHANGE = 0x51;
        public const int WM_TCARD = 0x52;
        public const int WM_HELP = 0x53;
        public const int WM_USERCHANGED = 0x54;
        public const int WM_NOTIFYFORMAT = 0x55;
        public const int WM_CONTEXTMENU = 0x7B;
        public const int WM_STYLECHANGING = 0x7C;
        public const int WM_STYLECHANGED = 0x7D;
        public const int WM_DISPLAYCHANGE = 0x7E;
        public const int WM_GETICON = 0x7F;
        public const int WM_SETICON = 0x80;

        public const int WM_NCCREATE = 0x81;
        public const int WM_NCDESTROY = 0x82;
        public const int WM_NCCALCSIZE = 0x83;
        public const int WM_NCHITTEST = 0x84;
        public const int WM_NCPAINT = 0x85;
        public const int WM_NCACTIVATE = 0x86;
        public const int WM_GETDLGCODE = 0x87;
        public const int WM_NCMOUSEMOVE = 0xA0;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int WM_NCLBUTTONUP = 0xA2;
        public const int WM_NCLBUTTONDBLCLK = 0xA3;
        public const int WM_NCRBUTTONDOWN = 0xA4;
        public const int WM_NCRBUTTONUP = 0xA5;
        public const int WM_NCRBUTTONDBLCLK = 0xA6;
        public const int WM_NCMBUTTONDOWN = 0xA7;
        public const int WM_NCMBUTTONUP = 0xA8;
        public const int WM_NCMBUTTONDBLCLK = 0xA9;

        public const int WM_KEYFIRST = 0x100;
        public const int WM_KEYDOWN = 0x100;
        public const int WM_KEYUP = 0x101;
        public const int WM_CHAR = 0x102;
        public const int WM_DEADCHAR = 0x103;
        public const int WM_SYSKEYDOWN = 0x104;
        public const int WM_SYSKEYUP = 0x105;
        public const int WM_SYSCHAR = 0x106;
        public const int WM_SYSDEADCHAR = 0x107;
        public const int WM_KEYLAST = 0x108;

        public const int WM_IME_STARTCOMPOSITION = 0x10D;
        public const int WM_IME_ENDCOMPOSITION = 0x10E;
        public const int WM_IME_COMPOSITION = 0x10F;
        public const int WM_IME_KEYLAST = 0x10F;

        public const int WM_INITDIALOG = 0x110;
        public const int WM_COMMAND = 0x0111;
        public const int WM_SYSCOMMAND = 0x112;
        public const int WM_TIMER = 0x113;
        public const int WM_HSCROLL = 0x114;
        public const int WM_VSCROLL = 0x115;
        public const int WM_INITMENU = 0x116;
        public const int WM_INITMENUPOPUP = 0x117;
        public const int WM_MENUSELECT = 0x11F;
        public const int WM_MENUCHAR = 0x120;
        public const int WM_ENTERIDLE = 0x121;

        public const int WM_CTLCOLORMSGBOX = 0x132;
        public const int WM_CTLCOLOREDIT = 0x133;
        public const int WM_CTLCOLORLISTBOX = 0x134;
        public const int WM_CTLCOLORBTN = 0x135;
        public const int WM_CTLCOLORDLG = 0x136;
        public const int WM_CTLCOLORSCROLLBAR = 0x137;
        public const int WM_CTLCOLORSTATIC = 0x138;

        public const int WM_MOUSEFIRST = 0x200;
        public const int WM_MOUSEMOVE = 0x200;
        public const int WM_LBUTTONDOWN = 0x201;
        public const int WM_LBUTTONUP = 0x202;
        public const int WM_LBUTTONDBLCLK = 0x203;
        public const int WM_RBUTTONDOWN = 0x204;
        public const int WM_RBUTTONUP = 0x205;
        public const int WM_RBUTTONDBLCLK = 0x206;
        public const int WM_MBUTTONDOWN = 0x207;
        public const int WM_MBUTTONUP = 0x208;
        public const int WM_MBUTTONDBLCLK = 0x209;
        public const int WM_MOUSELAST = 0x20A;
        public const int WM_MOUSEWHEEL = 0x20A;

        public const int WM_PARENTNOTIFY = 0x210;
        public const int WM_ENTERMENULOOP = 0x211;
        public const int WM_EXITMENULOOP = 0x212;
        public const int WM_NEXTMENU = 0x213;
        public const int WM_SIZING = 0x214;
        public const int WM_CAPTURECHANGED = 0x215;
        public const int WM_MOVING = 0x216;
        public const int WM_POWERBROADCAST = 0x218;
        public const int WM_DEVICECHANGE = 0x219;

        public const int WM_MDICREATE = 0x220;
        public const int WM_MDIDESTROY = 0x221;
        public const int WM_MDIACTIVATE = 0x222;
        public const int WM_MDIRESTORE = 0x223;
        public const int WM_MDINEXT = 0x224;
        public const int WM_MDIMAXIMIZE = 0x225;
        public const int WM_MDITILE = 0x226;
        public const int WM_MDICASCADE = 0x227;
        public const int WM_MDIICONARRANGE = 0x228;
        public const int WM_MDIGETACTIVE = 0x229;
        public const int WM_MDISETMENU = 0x230;
        public const int WM_ENTERSIZEMOVE = 0x231;
        public const int WM_EXITSIZEMOVE = 0x232;
        public const int WM_DROPFILES = 0x233;
        public const int WM_MDIREFRESHMENU = 0x234;

        public const int WM_IME_SETCONTEXT = 0x281;
        public const int WM_IME_NOTIFY = 0x282;
        public const int WM_IME_CONTROL = 0x283;
        public const int WM_IME_COMPOSITIONFULL = 0x284;
        public const int WM_IME_SELECT = 0x285;
        public const int WM_IME_CHAR = 0x286;
        public const int WM_IME_KEYDOWN = 0x290;
        public const int WM_IME_KEYUP = 0x291;

        public const int WM_MOUSEHOVER = 0x2A1;
        public const int WM_NCMOUSELEAVE = 0x2A2;
        public const int WM_MOUSELEAVE = 0x2A3;

        public const int WM_CUT = 0x300;
        public const int WM_COPY = 0x301;
        public const int WM_PASTE = 0x302;
        public const int WM_CLEAR = 0x303;
        public const int WM_UNDO = 0x304;

        public const int WM_RENDERFORMAT = 0x305;
        public const int WM_RENDERALLFORMATS = 0x306;
        public const int WM_DESTROYCLIPBOARD = 0x307;
        public const int WM_DRAWCLIPBOARD = 0x308;
        public const int WM_PAINTCLIPBOARD = 0x309;
        public const int WM_VSCROLLCLIPBOARD = 0x30A;
        public const int WM_SIZECLIPBOARD = 0x30B;
        public const int WM_ASKCBFORMATNAME = 0x30C;
        public const int WM_CHANGECBCHAIN = 0x30D;
        public const int WM_HSCROLLCLIPBOARD = 0x30E;
        public const int WM_QUERYNEWPALETTE = 0x30F;
        public const int WM_PALETTEISCHANGING = 0x310;
        public const int WM_PALETTECHANGED = 0x311;

        public const int WM_HOTKEY = 0x312;
        public const int WM_PRINT = 0x317;
        public const int WM_PRINTCLIENT = 0x318;

        public const int WM_HANDHELDFIRST = 0x358;
        public const int WM_HANDHELDLAST = 0x35F;
        public const int WM_PENWINFIRST = 0x380;
        public const int WM_PENWINLAST = 0x38F;
        public const int WM_COALESCE_FIRST = 0x390;
        public const int WM_COALESCE_LAST = 0x39F;
        public const int WM_DDE_FIRST = 0x3E0;
        public const int WM_DDE_INITIATE = 0x3E0;
        public const int WM_DDE_TERMINATE = 0x3E1;
        public const int WM_DDE_ADVISE = 0x3E2;
        public const int WM_DDE_UNADVISE = 0x3E3;
        public const int WM_DDE_ACK = 0x3E4;
        public const int WM_DDE_DATA = 0x3E5;
        public const int WM_DDE_REQUEST = 0x3E6;
        public const int WM_DDE_POKE = 0x3E7;
        public const int WM_DDE_EXECUTE = 0x3E8;
        public const int WM_DDE_LAST = 0x3E8;

        public const int WM_USER = 0x400;
        public const int WM_APP = 0x8000;
        
        /// <summary>
        /// Edit Control Messages
        /// </summary>
        public const int EM_GETSEL = 0x00B0;
        public const int EM_SETSEL = 0x00B1;
        public const int EM_GETRECT = 0x00B2;
        public const int EM_SETRECT = 0x00B3;
        public const int EM_SETRECTNP = 0x00B4;
        public const int EM_SCROLL = 0x00B5;
        public const int EM_LINESCROLL = 0x00B6;
        public const int EM_SCROLLCARET = 0x00B7;
        public const int EM_GETMODIFY = 0x00B8;
        public const int EM_SETMODIFY = 0x00B9;
        public const int EM_GETLINECOUNT = 0x00BA;
        public const int EM_LINEINDEX = 0x00BB;
        public const int EM_SETHANDLE = 0x00BC;
        public const int EM_GETHANDLE = 0x00BD;
        public const int EM_GETTHUMB = 0x00BE;
        public const int EM_LINELENGTH = 0x00C1;
        public const int EM_REPLACESEL = 0x00C2;
        public const int EM_GETLINE = 0x00C4;
        public const int EM_LIMITTEXT = 0x00C5;
        public const int EM_CANUNDO = 0x00C6;
        public const int EM_UNDO = 0x00C7;
        public const int EM_FMTLINES = 0x00C8;
        public const int EM_LINEFROMCHAR = 0x00C9;
        public const int EM_SETTABSTOPS = 0x00CB;
        public const int EM_SETPASSWORDCHAR = 0x00CC;
        public const int EM_EMPTYUNDOBUFFER = 0x00CD;
        public const int EM_GETFIRSTVISIBLELINE = 0x00CE;
        public const int EM_SETREADONLY = 0x00CF;
        public const int EM_SETWORDBREAKPROC = 0x00D0;
        public const int EM_GETWORDBREAKPROC = 0x00D1;
        public const int EM_GETPASSWORDCHAR = 0x00D2;
        public const int EM_SETMARGINS = 0x00D3;
        public const int EM_GETMARGINS = 0x00D4;
        public const int EM_SETLIMITTEXT = EM_LIMITTEXT;   /* ;win40 Name change */
        public const int EM_GETLIMITTEXT = 0x00D5;
        public const int EM_POSFROMCHAR = 0x00D6;
        public const int EM_CHARFROMPOS = 0x00D7;
        
        /// <summary>
        /// Tab Control Messages
        /// </summary>
        public const int TCM_FIRST = 0x1300;
        public const int TCM_GETITEMCOUNT = TCM_FIRST + 4;
        public const int TCM_GETCURSEL = TCM_FIRST + 11;
        public const int TCM_SETCURSEL = TCM_FIRST + 12;
        public const int TCM_GETITEM = TCM_FIRST + 60;
        public const int TCIF_TEXT = 0x0001;
        public const int TCIF_IMAGE = 0x0002;
        public const int TCIF_RTLREADING = 0x0004;
        public const int TCIF_PARAM = 0x0008;
        public const int TCIF_STATE = 0x0010;

        /// <summary>
        /// ToolbBar Control Messages
        /// </summary>
        public const int TBN_FIRST = -700;
        public const int TB_GETBUTTON = WM_USER + 23;
        public const int TB_BUTTONCOUNT = WM_USER + 24;
        public const int TB_GETRECT = WM_USER + 51;
        public const int BTNS_BUTTON = 0x0000;
        public const int BTNS_SEP = 0x0001;
        public const int BTNS_DROPDOWN = 0x0090;

        /// <summary>
        /// User Button Notification Codes
        /// </summary>
        public const int BN_CLICKED = 0;
        public const int TBN_DROPDOWN = TBN_FIRST - 10;


        /// <summary>
        /// User Button Control Messages
        /// </summary>
        public const int BM_CLICK = 0x00F5;
        public const int BM_GETCHECK = 0x00F0;
        public const int BM_SETCHECK = 0x00F1;
        public const int BM_GETSTATE = 0x00F2;
        public const int BM_SETSTATE = 0x00F3;
        public const int BST_UNCHECKED = 0x0000;
        public const int BST_CHECKED = 0x0001;
        public const int BST_FOCUS = 0x0008;

        /// <summary>
        /// ComboBox Messages
        /// </summary>
        public const int CB_GETEDITSEL = 0x0140;
        public const int CB_LIMITTEXT = 0x0141;
        public const int CB_SETEDITSEL = 0x0142;
        public const int CB_ADDSTRING = 0x0143;
        public const int CB_DELETESTRING = 0x0144;
        public const int CB_DIR = 0x0145;
        public const int CB_GETCOUNT = 0x0146;
        public const int CB_GETCURSEL = 0x0147;
        public const int CB_GETLBTEXT = 0x0148;
        public const int CB_GETLBTEXTLEN = 0x0149;
        public const int CB_INSERTSTRING = 0x014A;
        public const int CB_RESETCONTENT = 0x014B;
        public const int CB_FINDSTRING = 0x014C;
        public const int CB_SELECTSTRING = 0x014D;
        public const int CB_SETCURSEL = 0x014E;
        public const int CB_SHOWDROPDOWN = 0x014F;
        public const int CB_GETITEMDATA = 0x0150;
        public const int CB_SETITEMDATA = 0x0151;
        public const int CB_GETDROPPEDCONTROLRECT = 0x0152;
        public const int CB_SETITEMHEIGHT = 0x0153;
        public const int CB_GETITEMHEIGHT = 0x0154;
        public const int CB_SETEXTENDEDUI = 0x0155;
        public const int CB_GETEXTENDEDUI = 0x0156;
        public const int CB_GETDROPPEDSTATE = 0x0157;
        public const int CB_FINDSTRINGEXACT = 0x0158;
        public const int CB_SETLOCALE = 345;
        public const int CB_GETLOCALE = 346;
        public const int CB_GETTOPINDEX = 347;
        public const int CB_SETTOPINDEX = 348;
        public const int CB_GETHORIZONTALEXTENT = 349;
        public const int CB_SETHORIZONTALEXTENT = 350;
        public const int CB_GETDROPPEDWIDTH = 351;
        public const int CB_SETDROPPEDWIDTH = 352;
        public const int CB_INITSTORAGE = 353;
        public const int CB_MSGMAX = 354;
        
        /// <summary>
        /// ComboBoxEx Messages
        /// </summary>
        public const int CBEM_GETEDITCONTROL    = WM_USER + 7;
        public const int CBEM_GETITEMW          = WM_USER + 13;
        public const int CBEM_GETITEM           = CBEM_GETITEMW;

        /// <summary>
        /// Listbox messages
        /// </summary>
        public const int LB_ADDSTRING          = 0x0180;
        public const int LB_INSERTSTRING       = 0x0181;
        public const int LB_DELETESTRING       = 0x0182;
        public const int LB_SELITEMRANGEEX     = 0x0183;
        public const int LB_RESETCONTENT       = 0x0184;
        public const int LB_SETSEL             = 0x0185;
        public const int LB_SETCURSEL          = 0x0186;
        public const int LB_GETSEL             = 0x0187;
        public const int LB_GETCURSEL          = 0x0188;
        public const int LB_GETTEXT            = 0x0189;
        public const int LB_GETTEXTLEN         = 0x018A;
        public const int LB_GETCOUNT           = 0x018B;
        public const int LB_SELECTSTRING       = 0x018C;
        public const int LB_DIR                = 0x018D;
        public const int LB_GETTOPINDEX        = 0x018E;
        public const int LB_FINDSTRING         = 0x018F;
        public const int LB_GETSELCOUNT        = 0x0190;
        public const int LB_GETSELITEMS        = 0x0191;
        public const int LB_SETTABSTOPS        = 0x0192;
        public const int LB_GETHORIZONTALEXTENT= 0x0193;
        public const int LB_SETHORIZONTALEXTENT= 0x0194;
        public const int LB_SETCOLUMNWIDTH     = 0x0195;
        public const int LB_ADDFILE            = 0x0196;
        public const int LB_SETTOPINDEX        = 0x0197;
        public const int LB_GETITEMRECT        = 0x0198;
        public const int LB_GETITEMDATA        = 0x0199;
        public const int LB_SETITEMDATA        = 0x019A;
        public const int LB_SELITEMRANGE       = 0x019B;
        public const int LB_SETANCHORINDEX     = 0x019C;
        public const int LB_GETANCHORINDEX     = 0x019D;
        public const int LB_SETCARETINDEX      = 0x019E;
        public const int LB_GETCARETINDEX      = 0x019F;
        public const int LB_SETITEMHEIGHT      = 0x01A0;
        public const int LB_GETITEMHEIGHT      = 0x01A1;
        public const int LB_FINDSTRINGEXACT    = 0x01A2;
        public const int LB_SETLOCALE          = 0x01A5;
        public const int LB_GETLOCALE          = 0x01A6;
        public const int LB_SETCOUNT = 0x01A7;

        public const uint LMI_GRID_SELECTROWS_SETSELECTION = 0;
        public const uint LMI_GRID_SELECTROWS_SELECT = 1;

        #endregion

        #region Data Structures

        [StructLayout(LayoutKind.Sequential)]
        public struct INPUT
        {
            public int type;
            public INPUTUNION union;
        };

        [StructLayout(LayoutKind.Explicit)]
        public struct INPUTUNION
        {
            [FieldOffset(0)]
            public MOUSEINPUT mouseInput;
            [FieldOffset(0)]
            public KEYBDINPUT keyboardInput;
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct MOUSEINPUT
        {
            public int dx;
            public int dy;
            public int mouseData;
            public int dwFlags;
            public int time;
            public IntPtr dwExtraInfo;
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct KEYBDINPUT
        {
            public short wVk;
            public short wScan;
            public int dwFlags;
            public int time;
            public IntPtr dwExtraInfo;
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct HWND
        {
            IntPtr _value;

            public static HWND Cast(IntPtr hwnd)
            {
                HWND temp = new HWND();
                temp._value = hwnd;
                return temp;
            }

            public static implicit operator IntPtr(HWND hwnd)
            {
                return hwnd._value;
            }

            public static HWND NULL
            {
                get
                {
                    HWND temp = new HWND();
                    temp._value = IntPtr.Zero;
                    return temp;
                }
            }

            public static bool operator ==(HWND lhs, HWND rhs)
            {
                return lhs._value == rhs._value;
            }

            public static bool operator !=(HWND lhs, HWND rhs)
            {
                return lhs._value != rhs._value;
            }

            override public bool Equals(object oCompare)
            {
                HWND temp = Cast((HWND)oCompare);
                return _value == temp._value;
            }

            public override int GetHashCode()
            {
                return (int)_value;
            }
        }

        public struct COPYDATASTRUCT
        {
            public IntPtr dwData;
            public int cbData;
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpData;
        }

        public struct LVITEM
        {
            private Int32 mask;
            private Int32 iItem;
            private Int32 iSubItem;
            private Int32 state;
            private Int32 stateMask;
            private string pszText;
            private Int32 cchTextMax;
            private Int32 iImage;
            private Int32 lParam;
            private Int32 iIndent;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct TCITEM
        {
            public int mask;
            public int dwState;
            public int dwStateMask;
            public IntPtr pszText;
            public int cchTextMax;
            public int iImage;
            public int lParam;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct CToolbarButton
        {
            public uint uCommand;
            public int dwState;
            public int dwStateMask;
            public int iImage;
            public IntPtr pszText;
            public int cchTextMax;
            public IntPtr pszTipText;
            public int cchTipTextMax;
            public int lParam;
        }

        [Serializable, StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct NMHDR
        {
            public IntPtr hwndFrom;
            public IntPtr idFrom;
            public int code;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct CNmToolbar
        {
            public NMHDR hdr;
            public int iItem;
            public CToolbarButton tButton;
            public RECT rectButton;
        }        

        [StructLayout(LayoutKind.Sequential)]
        public struct WINDOWPLACEMENT
        {
            public int length;
            public int flags;
            public int showCmd;
            public System.Drawing.Point ptMinPosition;
            public System.Drawing.Point ptMaxPosition;
            public System.Drawing.Rectangle rcNormalPosition;

            public static WINDOWPLACEMENT Default
            {
                get
                {
                    WINDOWPLACEMENT result = new WINDOWPLACEMENT();
                    result.length = Marshal.SizeOf(result);
                    return result;
                }
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct CMenuRawData
        {
            public uint Command;
            public IntPtr Text;
            public int TextMax;
            public uint Flags;
            public int Check;
            public bool HasSubMenu;
            public RECT Rect;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Unicode)]
        public struct CComboboxItemData
        {
            public int Item;            
            //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
            //public char[] Text;
            public IntPtr Text;
            public int Image;
            public int Indent;
            public long Param;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct COMBOBOXINFO
        {
            public Int32 cbSize;
            public RECT rcItem;
            public RECT rcButton;
            public ComboBoxButtonState buttonState;
            public IntPtr hwndCombo;
            public IntPtr hwndEdit;
            public IntPtr hwndList;
        }

        public enum ComboBoxButtonState
        {
            STATE_SYSTEM_NONE = 0,
            STATE_SYSTEM_INVISIBLE = 0x00008000,
            STATE_SYSTEM_PRESSED = 0x00000008
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct CControlRawData
        {
            /// <summary>
            /// The type of the item in following format: CI_xxx
            /// </summary>
            public ControlItemFlags m_uType;
            
            /// <summary>
            /// The bounding rectangle of the item.
            /// </summary>
            public RECT m_rectItem;
            
            /// <summary>
            /// ??? derived LMI::CAlignedWnd::AW_xxx flags
            /// </summary>
            public int m_uDerivedAWFlags;
            
            /// <summary>
            /// Pointer to zero (z) terminated string (s): the text of the control
            /// </summary>
            public IntPtr m_pszText;
            
            /// <summary>
            /// Max length of text. (TextMax) -- cch == count of characters
            /// </summary>
            public int m_cchTextMax;
            
            /// <summary>
            /// Pointer (p) to zero (z) terminated tool tip (TipText) string (s)
            /// </summary>
            public IntPtr m_pszTipText;
            
            /// <summary>
            /// Max length (TextMax) of tool tip (Tip) - cch == count of characters.
            /// </summary>
            public int m_cchTipTextMax;		
            
            /// <summary>
            /// Image list index Unsigned int (u) 
            /// </summary>
            public int m_uImageListIdx;
            
            /// <summary>
            /// Index of image (int). Returns -1 if there's no image
            /// </summary>
            public int m_iImage;

            /// <summary>
            /// ??? state draw info index ???
            /// </summary>
            public int m_uStateIdx;
            
            /// <summary>
            /// Pointer or uint -- user defined custom data.
            /// </summary>
            public UIntPtr m_uUserData;
        }

        public enum ControlItemFlags
        {
            /// <summary>
            /// Invalid item.
            /// </summary>
            CI_INVALID,

            /// <summary>
            /// Static item.
            /// </summary>
            CI_STATIC,
            
            /// <summary>
            /// Click-only button
            /// </summary>
            CI_BUTTON,
            
            /// <summary>
            /// Double-click-only button, see LMI_NM_GROUPLIST_ITEMDBLCLICK
            /// </summary>
            CI_DBLCLICKBUTTON,

            // click XOR double-click button like window's system button
            //// CI_FREECLICKBUTTON,
            
            /// <summary>
            /// Link-style (probably underlined) button with cursor turning to a pointing hand
            /// Clickable item.
            /// </summary>
            CI_LINK,
            
            /// <summary>
            /// Editable item, "I" cursor appears over this item
            /// </summary>
            CI_EDIT,
            
            /// <summary>
            /// Read-only editable item, "I" cursor appears over this item
            /// </summary>
            CI_EDIT_RO,
            
            /// <summary>
            /// Progress bar
            /// </summary>
            CI_PROGRESSBAR,
            
            /// <summary>
            /// Hole (???)
            /// </summary>
            CI_HOLE,
            
            /// <summary>
            /// Group, following items belong to this group until next CI_GROUPEND
            /// </summary>
            CI_GROUPBEGIN,
            
            /// <summary>
            /// Group terminator (should be paired CI_GROUPBEGIN)
            /// </summary>
            CI_GROUPEND,

            /// <summary>
            /// Type mask used for ???
            /// </summary>
            CIMASK_TYPE = 0x000000FF,

            /// <summary>
            /// Drag & drop item
            /// </summary>
            CI_DND = 0x00001000,	
        }

        public class GroupListDataItem
        {
            public ControlItemFlags m_uType;			// item type, CI_xxx
            public Win32.RECT m_rectItem;			// item rect
            public int m_uDerivedAWFlags;	// derived LMI::CAlignedWnd::AW_xxx flags
            public string m_pszText;			// text
            public int m_cchTextMax;			// text max length
            public string m_pszTipText;		// tooltip text
            public int m_cchTipTextMax;		// tooltip text max length
            public int m_uImageListIdx;	// image list index
            public int m_iImage;			// image index or -1 if no image
            public int m_uStateIdx;		// state draw info index            
        }

        #endregion

        #region Methods

        /// <summary>
        /// This static method is required because Win32 does not support GetWindowLongPtr directly.
        /// </summary>        
        public static IntPtr GetWindowLongPtr(IntPtr hWnd, GWL index)
        {
            if (IntPtr.Size == 8)
            {
                return GetWindowLongPtr64(hWnd, (int)index);
            }
            else
            {
                return GetWindowLongPtr32(hWnd, (int)index);
            }
        }
        
        public static int MakeWParam(int loWord, int hiWord)
        {
            return (loWord & 0xFFFF) + ((hiWord & 0xFFFF) << 16);
        }

        /// <summary>
        /// Sends the message which sets the text.
        /// </summary>
        /// <param name="hWnd">The HWND.</param>
        /// <param name="wParam">The wparam.</param>
        /// <param name="msg">The MSG.</param>
        /// <returns></returns>
        public static void SetText(IntPtr hwnd, string text)
        {
            if (hwnd == IntPtr.Zero)
            {
                throw new ArgumentException("The 'hWnd' argument cannot be zero!");
            }

            SendMessage(hwnd, WM_SETTEXT, 0, text);
        }

        /// <summary>
        /// Sends the message which sets the text.
        /// </summary>
        /// <param name="hwnd">The HWND.</param>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        public static string GetText(IntPtr hwnd, int length)
        {
            if (hwnd == IntPtr.Zero) { throw new ArgumentException("The 'hWnd' argument cannot be zero!"); }

            StringBuilder text = new StringBuilder(length + 1);

            SendMessage(hwnd, WM_GETTEXT, length + 1, text);

            return text.ToString();
        }

        /// <summary>
        /// Sends the message which gets the length of text.
        /// </summary>
        /// <param name="hwnd">The HWND.</param>
        /// <returns></returns>
        public static int GetTextLength(IntPtr hwnd)
        {
            if (hwnd == IntPtr.Zero) { throw new ArgumentException("The 'hWnd' argument cannot be zero!"); }

            return SendMessage(hwnd, WM_GETTEXTLENGTH, 0, 0);
        }

        /// <summary>
        /// Sends the message which selects the given range text
        /// </summary>
        /// <param name="hwnd">The HWND.</param>
        /// <param name="start">The start character position</param>
        /// <param name="end">The end character position</param>
        public static void SelectText(IntPtr hwnd, int start, int end)
        {
            if (hwnd == IntPtr.Zero) { throw new ArgumentException("The 'hWnd' argument cannot be zero!"); }

            SendMessage(hwnd, EM_SETSEL, start, end);
        }

        #endregion
    }
}

