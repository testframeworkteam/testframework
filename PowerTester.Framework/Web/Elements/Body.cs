﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Body class
    /// </summary>
    public class Body : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Body"/> class.  
        /// </summary>
        public Body(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Body"/> class.  
        /// </summary>
        public Body(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "Body"; }
        }

        /// <summary>
        /// Gets or sets the Value.
        /// </summary>
        public new string Value
        {
            get { return GetAttribute("value"); }

            set { SetAttribute("value", value); }
        }

        /// <summary>
        /// Types text to textbox
        /// </summary>
        /// <param name="text">
        /// The text to type.
        /// </param>
        public void TypeText(string text)
        {
            SendKeys(text);
        }

        /// <summary>
        /// Types text to textbox
        /// </summary>
        /// <param name="text">
        /// The text to type.
        /// </param>
        /// <param name="clear">
        /// Value inidcating whether the textfield have to be cleared before typing.
        /// </param>
        public void TypeText(string text, bool clear)
        {
            SendKeys(text, clear);
        }
    }

    /// <summary>
    /// Body related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Bodys
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Bodys
        /// </returns>
        public List<Body> Bodys(WebFinderConstraint findBy)
        {
            return FindElements<Body>(findBy);
        }

        /// <summary>
        /// Gets a list of Bodys
        /// </summary>
        /// <returns>
        /// The list of Bodys
        /// </returns>
        public List<Body> Bodys(bool firstChildLevelOnly = false)
        {
            return FindElements<Body>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Body
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result Body.
        /// </returns>
        public Body Body(WebFinderConstraint findBy)
        {
            return FindElement<Body>(findBy);
        }

        /// <summary>
        /// Gets a Body.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the Body
        /// </param>
        /// <returns>
        /// The result Body
        /// </returns>
        public Body Body(string elementId)
        {
            return FindElement<Body>(elementId);
        }
    }
}