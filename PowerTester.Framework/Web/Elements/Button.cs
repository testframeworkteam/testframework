﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Button class.
    /// </summary>
    public class Button : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Button"/> class. 
        /// </summary>
        public Button(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Button"/> class. 
        /// </summary>
        public Button(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "button"; }
        }
    }

    /// <summary>
    /// Button related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets the list of Buttons.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The Buttons list.
        /// </returns>
        public List<Button> Buttons(WebFinderConstraint findBy)
        {
            return FindElements<Button>(findBy);
        }

        /// <summary>
        /// Gets the list of Buttons.
        /// </summary>
        /// <returns>
        /// The Buttons list.
        /// </returns>
        public List<Button> Buttons(bool firstChildLeveleOnly = false)
        {
            return FindElements<Button>(firstChildLeveleOnly);
        }

        /// <summary>
        /// Gets the Button.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The result Button.
        /// </returns>
        public Button Button(WebFinderConstraint findBy)
        {
            return FindElement<Button>(findBy);
        }

        /// <summary>
        /// Gets the Button.
        /// </summary>
        /// <param name="elementId">
        /// The element Id.
        /// </param>
        /// <returns>
        /// The result Button.
        /// </returns>
        public Button Button(string elementId)
        {
            return FindElement<Button>(elementId);
        }
    }
}