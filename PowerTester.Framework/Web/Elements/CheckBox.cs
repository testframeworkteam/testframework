﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The CheckBox class.
    /// </summary>
    public class CheckBox : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Span"/> class. 
        /// </summary>
        public CheckBox(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Span"/> class. 
        /// </summary>
        public CheckBox(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "Input"; }
        }
        
        /// <summary>
        /// Gets or sets the Value.
        /// </summary>
        public new bool Value
        {
            get { return GetAttribute("Checked") == "true"; }

            set
            {
                MouseMove();
                Click(() => (GetAttribute("Checked") == "true") == value, ClickType.Mouse);
            }
        }
        
        /// <summary>
        /// Checks the checkbox.
        /// </summary>
        public void Check()
        {
            Value = true;
        }
        
        /// <summary>
        /// Uncehcks the checkbox.
        /// </summary>
        public void UnCheck()
        {
            Value = false;
        }
    }

    /// <summary>
    /// CheckBox related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets the list of CheckBox.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The CheckBox fields list.
        /// </returns>
        public List<CheckBox> CheckBoxs(WebFinderConstraint findBy)
        {
            return FindElements<CheckBox>(findBy);
        }

        /// <summary>
        /// Gets the list of CheckBox.
        /// </summary>
        /// <returns>
        /// The CheckBox fields list.
        /// </returns>
        public List<CheckBox> CheckBoxs(bool firstChildLevelOnly = false)
        {
            return FindElements<CheckBox>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets the CheckBox.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The CheckBox field.
        /// </returns>
        public CheckBox CheckBox(WebFinderConstraint findBy)
        {
            return FindElement<CheckBox>(findBy);
        }

        /// <summary>
        /// Gets the CheckBox.
        /// </summary>
        /// <param name="elementId">
        /// The element Id.
        /// </param>
        /// <returns>
        /// The CheckBox field.
        /// </returns>
        public CheckBox CheckBox(string elementId)
        {
            return FindElement<CheckBox>(elementId);
        }
    }
}