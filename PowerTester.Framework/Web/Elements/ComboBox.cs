﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Div class
    /// </summary>
    public class Combobox : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Div"/> class.  
        /// </summary>
        public Combobox(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Div"/> class.  
        /// </summary>
        public Combobox(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "select"; }
        }

        public List<Option> Options
        {
            get { return FindElements<Option>(true); }
        }

        public void SelectOption(string option)
        {
            Option optionToCombobox = FindElement<Option>(WebFind.ByValue(option, true));
            optionToCombobox.Click(() => optionToCombobox.Selected);
        }

        public void SelectOption(int index)
        {
            Option optionToCombobox = FindElements<Option>(true)[index];
            optionToCombobox.Click(() => optionToCombobox.Selected);
        }
    }

    /// <summary>
    /// Div related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Comboboxs
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Comboboxs
        /// </returns>
        public List<Combobox> Comboboxs(WebFinderConstraint findBy)
        {
            return FindElements<Combobox>(findBy);
        }

        /// <summary>
        /// Gets a list of Comboboxs
        /// </summary>
        /// <returns>
        /// The list of Comboboxs
        /// </returns>
        public List<Combobox> Comboboxs(bool firstChildLevelOnly = false)
        {
            return FindElements<Combobox>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Combobox
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result Combobox.
        /// </returns>
        public Combobox Combobox(WebFinderConstraint findBy)
        {
            return FindElement<Combobox>(findBy);
        }

        /// <summary>
        /// Gets a Combobox.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the Combobox
        /// </param>
        /// <returns>
        /// The result Combobox
        /// </returns>
        public Combobox Combobox(string elementId)
        {
            return FindElement<Combobox>(elementId);
        }
    }
}