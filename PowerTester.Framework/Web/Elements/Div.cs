﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Div class
    /// </summary>
    public class Div : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Div"/> class.  
        /// </summary>
        public Div(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Div"/> class.  
        /// </summary>
        public Div(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "div"; }
        }
    }

    /// <summary>
    /// Div related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Divs
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Divs
        /// </returns>
        public List<Div> Divs(WebFinderConstraint findBy)
        {
            return FindElements<Div>(findBy);
        }

        /// <summary>
        /// Gets a list of Divs
        /// </summary>
        /// <returns>
        /// The list of Divs
        /// </returns>
        public List<Div> Divs(bool firstChildLevelOnly = false)
        {
            return FindElements<Div>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Div
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result Div.
        /// </returns>
        public Div Div(WebFinderConstraint findBy)
        {
            return FindElement<Div>(findBy);
        }

        /// <summary>
        /// Gets a Div.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the div
        /// </param>
        /// <returns>
        /// The result div
        /// </returns>
        public Div Div(string elementId)
        {
            return FindElement<Div>(elementId);
        }
    }
}