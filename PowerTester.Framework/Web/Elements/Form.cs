﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Form class
    /// </summary>
    public class Form : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Form"/> class.  
        /// </summary>
        public Form(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Form"/> class.  
        /// </summary>
        public Form(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "form"; }
        }
    }

    /// <summary>
    /// Form related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Forms
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Forms
        /// </returns>
        public List<Form> Forms(WebFinderConstraint findBy)
        {
            return FindElements<Form>(findBy);
        }

        /// <summary>
        /// Gets a list of Forms
        /// </summary>
        /// <returns>
        /// The list of Forms
        /// </returns>
        public List<Form> Forms(bool firstChildLevelOnly = false)
        {
            return FindElements<Form>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Form
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result Form.
        /// </returns>
        public Form Form(WebFinderConstraint findBy)
        {
            return FindElement<Form>(findBy);
        }

        /// <summary>
        /// Gets a Form.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the Form
        /// </param>
        /// <returns>
        /// The result Form
        /// </returns>
        public Form Form(string elementId)
        {
            return FindElement<Form>(elementId);
        }
    }
}