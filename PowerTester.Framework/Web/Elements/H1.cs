﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The H1 class
    /// </summary>
    public class H1 : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="H1"/> class.  
        /// </summary>
        public H1(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="H1"/> class.  
        /// </summary>
        public H1(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "H1"; }
        }
    }

    /// <summary>
    /// H1 related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of H1s
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of H1s
        /// </returns>
        public List<H1> H1s(WebFinderConstraint findBy)
        {
            return FindElements<H1>(findBy);
        }

        /// <summary>
        /// Gets a list of H1s
        /// </summary>
        /// <returns>
        /// The list of H1s
        /// </returns>
        public List<H1> H1s(bool firstChildLevelOnly = false)
        {
            return FindElements<H1>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a H1
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result H1.
        /// </returns>
        public H1 H1(WebFinderConstraint findBy)
        {
            return FindElement<H1>(findBy);
        }

        /// <summary>
        /// Gets a H1.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the H1
        /// </param>
        /// <returns>
        /// The result H1
        /// </returns>
        public H1 H1(string elementId)
        {
            return FindElement<H1>(elementId);
        }
    }
}