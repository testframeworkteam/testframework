﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The H2 class
    /// </summary>
    public class H2 : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="H2"/> class.  
        /// </summary>
        public H2(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="H2"/> class.  
        /// </summary>
        public H2(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "H2"; }
        }
    }

    /// <summary>
    /// H2 related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of H2s
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of H2s
        /// </returns>
        public List<H2> H2s(WebFinderConstraint findBy)
        {
            return FindElements<H2>(findBy);
        }

        /// <summary>
        /// Gets a list of H2s
        /// </summary>
        /// <returns>
        /// The list of H2s
        /// </returns>
        public List<H2> H2s(bool firstChildLevelOnly = false)
        {
            return FindElements<H2>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a H2
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result H2.
        /// </returns>
        public H2 H2(WebFinderConstraint findBy)
        {
            return FindElement<H2>(findBy);
        }

        /// <summary>
        /// Gets a H2.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the H2
        /// </param>
        /// <returns>
        /// The result H2
        /// </returns>
        public H2 H2(string elementId)
        {
            return FindElement<H2>(elementId);
        }
    }
}