﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The H3 class
    /// </summary>
    public class H3 : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="H3"/> class.  
        /// </summary>
        public H3(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="H3"/> class.  
        /// </summary>
        public H3(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "H3"; }
        }
    }

    /// <summary>
    /// H3 related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of H3s
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of H3s
        /// </returns>
        public List<H3> H3s(WebFinderConstraint findBy)
        {
            return FindElements<H3>(findBy);
        }

        /// <summary>
        /// Gets a list of H3s
        /// </summary>
        /// <returns>
        /// The list of H3s
        /// </returns>
        public List<H3> H3s(bool firstChildLevelOnly = false)
        {
            return FindElements<H3>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a H3
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result H3.
        /// </returns>
        public H3 H3(WebFinderConstraint findBy)
        {
            return FindElement<H3>(findBy);
        }

        /// <summary>
        /// Gets a H3.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the H3
        /// </param>
        /// <returns>
        /// The result H3
        /// </returns>
        public H3 H3(string elementId)
        {
            return FindElement<H3>(elementId);
        }
    }
}