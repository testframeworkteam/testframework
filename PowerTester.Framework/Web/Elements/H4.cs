﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The H4 class
    /// </summary>
    public class H4 : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="H4"/> class.  
        /// </summary>
        public H4(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="H4"/> class.  
        /// </summary>
        public H4(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "H4"; }
        }
    }

    /// <summary>
    /// H4 related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of H4s
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of H4s
        /// </returns>
        public List<H4> H4s(WebFinderConstraint findBy)
        {
            return FindElements<H4>(findBy);
        }

        /// <summary>
        /// Gets a list of H4s
        /// </summary>
        /// <returns>
        /// The list of H4s
        /// </returns>
        public List<H4> H4s(bool firstChildLevelOnly = false)
        {
            return FindElements<H4>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a H4
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result H4.
        /// </returns>
        public H4 H4(WebFinderConstraint findBy)
        {
            return FindElement<H4>(findBy);
        }

        /// <summary>
        /// Gets a H4.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the H4
        /// </param>
        /// <returns>
        /// The result H4
        /// </returns>
        public H4 H4(string elementId)
        {
            return FindElement<H4>(elementId);
        }
    }
}