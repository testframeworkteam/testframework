﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The H5 class
    /// </summary>
    public class H5 : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="H5"/> class.  
        /// </summary>
        public H5(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="H5"/> class.  
        /// </summary>
        public H5(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "H5"; }
        }
    }

    /// <summary>
    /// H5 related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of H5s
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of H5s
        /// </returns>
        public List<H5> H5s(WebFinderConstraint findBy)
        {
            return FindElements<H5>(findBy);
        }

        /// <summary>
        /// Gets a list of H5s
        /// </summary>
        /// <returns>
        /// The list of H5s
        /// </returns>
        public List<H5> H5s(bool firstChildLevelOnly = false)
        {
            return FindElements<H5>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a H5
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result H5.
        /// </returns>
        public H5 H5(WebFinderConstraint findBy)
        {
            return FindElement<H5>(findBy);
        }

        /// <summary>
        /// Gets a H5.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the H5
        /// </param>
        /// <returns>
        /// The result H5
        /// </returns>
        public H5 H5(string elementId)
        {
            return FindElement<H5>(elementId);
        }
    }
}