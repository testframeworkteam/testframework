﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The H6 class
    /// </summary>
    public class H6 : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="H6"/> class.  
        /// </summary>
        public H6(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="H6"/> class.  
        /// </summary>
        public H6(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "H6"; }
        }
    }

    /// <summary>
    /// H6 related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of H6s
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of H6s
        /// </returns>
        public List<H6> H6s(WebFinderConstraint findBy)
        {
            return FindElements<H6>(findBy);
        }

        /// <summary>
        /// Gets a list of H6s
        /// </summary>
        /// <returns>
        /// The list of H6s
        /// </returns>
        public List<H6> H6s(bool firstChildLevelOnly = false)
        {
            return FindElements<H6>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a H6
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result H6.
        /// </returns>
        public H6 H6(WebFinderConstraint findBy)
        {
            return FindElement<H6>(findBy);
        }

        /// <summary>
        /// Gets a H6.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the H6
        /// </param>
        /// <returns>
        /// The result H6
        /// </returns>
        public H6 H6(string elementId)
        {
            return FindElement<H6>(elementId);
        }
    }
}