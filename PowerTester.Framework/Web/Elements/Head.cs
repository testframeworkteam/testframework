﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Head class
    /// </summary>
    public class Head : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Head"/> class.  
        /// </summary>
        public Head(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Head"/> class.  
        /// </summary>
        public Head(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "Head"; }
        }
    }

    /// <summary>
    /// Head related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Heads
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Heads
        /// </returns>
        public List<Head> Heads(WebFinderConstraint findBy)
        {
            return FindElements<Head>(findBy);
        }

        /// <summary>
        /// Gets a list of Heads
        /// </summary>
        /// <returns>
        /// The list of Heads
        /// </returns>
        public List<Head> Heads(bool firstChildLevelOnly = false)
        {
            return FindElements<Head>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Head
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result Head.
        /// </returns>
        public Head Head(WebFinderConstraint findBy)
        {
            return FindElement<Head>(findBy);
        }

        /// <summary>
        /// Gets a Head.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the Head
        /// </param>
        /// <returns>
        /// The result Head
        /// </returns>
        public Head Head(string elementId)
        {
            return FindElement<Head>(elementId);
        }
    }
}