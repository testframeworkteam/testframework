﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Header class
    /// </summary>
    public class Header : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Header"/> class.  
        /// </summary>
        public Header(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Header"/> class.  
        /// </summary>
        public Header(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "Header"; }
        }
    }

    /// <summary>
    /// Header related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Headers
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Headers
        /// </returns>
        public List<Header> Headers(WebFinderConstraint findBy)
        {
            return FindElements<Header>(findBy);
        }

        /// <summary>
        /// Gets a list of Headers
        /// </summary>
        /// <returns>
        /// The list of Headers
        /// </returns>
        public List<Header> Headers(bool firstChildLevelOnly = false)
        {
            return FindElements<Header>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Header
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result Header.
        /// </returns>
        public Header Header(WebFinderConstraint findBy)
        {
            return FindElement<Header>(findBy);
        }

        /// <summary>
        /// Gets a Header.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the Header
        /// </param>
        /// <returns>
        /// The result Header
        /// </returns>
        public Header Header(string elementId)
        {
            return FindElement<Header>(elementId);
        }
    }
}