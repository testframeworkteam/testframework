﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Div class
    /// </summary>
    public class I : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="I"/> class.  
        /// </summary>
        public I(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="I"/> class.  
        /// </summary>
        public I(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "i"; }
        }
    }

    /// <summary>
    /// Div related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Divs
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Divs
        /// </returns>
        public List<I> Is(WebFinderConstraint findBy)
        {
            return FindElements<I>(findBy);
        }

        /// <summary>
        /// Gets a list of Divs
        /// </summary>
        /// <returns>
        /// The list of Divs
        /// </returns>
        public List<I> Is(bool firstChildLevelOnly = false)
        {
            return FindElements<I>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Div
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result Div.
        /// </returns>
        public I I(WebFinderConstraint findBy)
        {
            return FindElement<I>(findBy);
        }

        /// <summary>
        /// Gets a Div.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the div
        /// </param>
        /// <returns>
        /// The result div
        /// </returns>
        public I I(string elementId)
        {
            return FindElement<I>(elementId);
        }
    }
}