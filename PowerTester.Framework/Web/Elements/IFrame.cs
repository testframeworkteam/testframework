﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Div class
    /// </summary>
    public class IFrame : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Div"/> class.  
        /// </summary>
        public IFrame(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Div"/> class.  
        /// </summary>
        public IFrame(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "iframe"; }
        }
    }

    /// <summary>
    /// Div related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of IFrames
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of IFrames
        /// </returns>
        public List<IFrame> IFrames(WebFinderConstraint findBy)
        {
            return FindElements<IFrame>(findBy);
        }

        /// <summary>
        /// Gets a list of IFrames
        /// </summary>
        /// <returns>
        /// The list of IFrames
        /// </returns>
        public List<IFrame> IFrames(bool firstChildLevelOnly = false)
        {
            return FindElements<IFrame>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a IFrame
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result IFrame.
        /// </returns>
        public IFrame IFrame(WebFinderConstraint findBy)
        {
            return FindElement<IFrame>(findBy);
        }

        /// <summary>
        /// Gets a IFrame.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the IFrame
        /// </param>
        /// <returns>
        /// The result IFrame
        /// </returns>
        public IFrame IFrame(string elementId)
        {
            return FindElement<IFrame>(elementId);
        }
    }
}