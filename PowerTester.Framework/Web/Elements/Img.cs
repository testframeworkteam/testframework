﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Div class
    /// </summary>
    public class Img : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Div"/> class.  
        /// </summary>
        public Img(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Div"/> class.  
        /// </summary>
        public Img(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "img"; }
        }

        public string Src
        {
            get { return GetAttribute("src"); }
        }
    }

    /// <summary>
    /// Div related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Divs
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Divs
        /// </returns>
        public List<Img> Imgs(WebFinderConstraint findBy)
        {
            return FindElements<Img>(findBy);
        }

        /// <summary>
        /// Gets a list of Divs
        /// </summary>
        /// <returns>
        /// The list of Divs
        /// </returns>
        public List<Img> Imgs(bool firstChildLevelOnly = false)
        {
            return FindElements<Img>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Div
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result Div.
        /// </returns>
        public Img Img(WebFinderConstraint findBy)
        {
            return FindElement<Img>(findBy);
        }

        /// <summary>
        /// Gets a Div.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the div
        /// </param>
        /// <returns>
        /// The result div
        /// </returns>
        public Img Img(string elementId)
        {
            return FindElement<Img>(elementId);
        }
    }
}