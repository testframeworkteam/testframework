﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Input class.
    /// </summary>
    public class Input : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Span"/> class. 
        /// </summary>
        public Input(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Span"/> class. 
        /// </summary>
        public Input(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "Input"; }
        }
        
        /// <summary>
        /// Gets or sets the Value.
        /// </summary>
        public new string Value
        {
            get { return GetAttribute("value"); }

            set { SetAttribute("value", value); }
        }

        /// <summary>
        /// Types text to textbox
        /// </summary>
        /// <param name="text">
        /// The text to type.
        /// </param>
        public void TypeText(string text)
        {
            SendKeys(text);
        }

        /// <summary>
        /// Types text to textbox
        /// </summary>
        /// <param name="text">
        /// The text to type.
        /// </param>
        /// <param name="clear">
        /// Value inidcating whether the textfield have to be cleared before typing.
        /// </param>
        public void TypeText(string text, bool clear)
        {
            SendKeys(text, clear);
        }
    }

    /// <summary>
    /// TextField related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets the list of Inputs.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The text fields list.
        /// </returns>
        public List<Input> Inputs(WebFinderConstraint findBy)
        {
            return FindElements<Input>(findBy);
        }

        /// <summary>
        /// Gets the list of Inputs.
        /// </summary>
        /// <returns>
        /// The text fields list.
        /// </returns>
        public List<Input> Inputs(bool firstChildLevelOnly = false)
        {
            return FindElements<Input>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets the Input.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The text field.
        /// </returns>
        public Input Input(WebFinderConstraint findBy)
        {
            return FindElement<Input>(findBy);
        }

        /// <summary>
        /// Gets the Input.
        /// </summary>
        /// <param name="elementId">
        /// The element Id.
        /// </param>
        /// <returns>
        /// The text field.
        /// </returns>
        public Input Input(string elementId)
        {
            return FindElement<Input>(elementId);
        }
    }
}