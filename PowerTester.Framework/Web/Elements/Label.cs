﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Label class. 
    /// </summary>
    public class Label : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Label"/> class.  
        /// </summary>
        public Label(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Label"/> class.  
        /// </summary>
        public Label(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "label"; }
        }
    }

    /// <summary>
    /// Label related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets the list of Labels.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The Labels list.
        /// </returns>
        public List<Label> Labels(WebFinderConstraint findBy)
        {
            return FindElements<Label>(findBy);
        }

        /// <summary>
        /// Gets the list of Labels.
        /// </summary>
        /// <returns>
        /// The Labels list.
        /// </returns>
        public List<Label> Labels(bool firstChildLevelOnly =false)
        {
            return FindElements<Label>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets the Label.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The tesult Label.
        /// </returns>
        public Label Label(WebFinderConstraint findBy)
        {
            return FindElement<Label>(findBy);
        }

        /// <summary>
        /// Gets the Label.
        /// </summary>
        /// <param name="elementId">
        /// The element Id.
        /// </param>
        /// <returns>
        /// The result Label.
        /// </returns>
        public Label Label(string elementId)
        {
            return FindElement<Label>(WebFind.ById(elementId));
        }
    }
}