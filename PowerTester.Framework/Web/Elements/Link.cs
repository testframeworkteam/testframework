﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Link class. 
    /// </summary>
    public class Link : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Link"/> class.  
        /// </summary>
        public Link(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Link"/> class.  
        /// </summary>
        public Link(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "a"; }
        }

        /// <summary>
        /// Gets "href" value of link html-tag [a].
        /// </summary>
        public string Url
        {
            get { return GetAttribute("href"); }
        }
    }

    /// <summary>
    /// Link related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets the list of Links.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The links list.
        /// </returns>
        public List<Link> Links(WebFinderConstraint findBy)
        {
            return FindElements<Link>(findBy);
        }

        /// <summary>
        /// Gets the list of Links.
        /// </summary>
        /// <returns>
        /// The links list.
        /// </returns>
        public List<Link> Links(bool firstChildLevelOnly =false)
        {
            return FindElements<Link>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets the Link.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The tesult link.
        /// </returns>
        public Link Link(WebFinderConstraint findBy)
        {
            return FindElement<Link>(findBy);
        }

        /// <summary>
        /// Gets the Link.
        /// </summary>
        /// <param name="elementId">
        /// The element Id.
        /// </param>
        /// <returns>
        /// The result link.
        /// </returns>
        public Link Link(string elementId)
        {
            return FindElement<Link>(WebFind.ById(elementId));
        }
    }
}