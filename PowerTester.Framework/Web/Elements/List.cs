﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// Definition of [ul] based lists
    /// </summary>
    public class List : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="List"/> class.  
        /// </summary>
        public List(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="List"/> class.  
        /// </summary>
        public List(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "ul"; }
        }

        public List<ListItem> Items
        {
            get { return FindElements<ListItem>(true); }
        }
    }

    /// <summary>
    /// List related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets the List of lists.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The lists list.
        /// </returns>
        public List<List> Lists(WebFinderConstraint findBy)
        {
            return FindElements<List>(findBy);
        }

        /// <summary>
        /// Gets the List of lists.
        /// </summary>
        /// <returns>
        /// The lists list.
        /// </returns>
        public List<List> Lists(bool firstChildLevelOnly = false)
        {
            return FindElements<List>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets the List.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The lists list.
        /// </returns>
        public List List(WebFinderConstraint findBy)
        {
            return FindElement<List>(findBy);
        }

        /// <summary>
        /// Gets the List.
        /// </summary>
        /// <param name="elementId">
        /// The element Id.
        /// </param>
        /// <returns>
        /// The result list.
        /// </returns>
        public List List(string elementId)
        {
            return FindElement<List>(elementId);
        }
    }
}