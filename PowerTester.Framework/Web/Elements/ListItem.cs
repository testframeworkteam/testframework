﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// Defines list items of Select links. One list item is one html [option].
    /// </summary>
    public class ListItem : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ListItem"/> class.  
        /// </summary>
        public ListItem(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ListItem"/> class.  
        /// </summary>
        public ListItem(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "li"; }
        }
    }

    /// <summary>
    /// Link related functions of WebAutomationElement.
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets the List of ListItems.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The list items list.
        /// </returns>
        public List<ListItem> ListItems(WebFinderConstraint findBy)
        {
            return FindElements<ListItem>(findBy);
        }

        /// <summary>
        /// Gets the List of ListItems.
        /// </summary>
        /// <returns>
        /// The list items list.
        /// </returns>
        public List<ListItem> ListItems(bool firstChildLeveleOnly = false)
        {
            return FindElements<ListItem>(firstChildLeveleOnly);
        }

        /// <summary>
        /// Gets ListItem.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The list item.
        /// </returns>
        public ListItem ListItem(WebFinderConstraint findBy)
        {
            return FindElement<ListItem>(findBy);
        }

        /// <summary>
        /// Gets ListItem.
        /// </summary>
        /// <param name="elementId">
        /// The element Id.
        /// </param>
        /// <returns>
        /// The list item.
        /// </returns>
        public ListItem ListItem(string elementId)
        {
            return FindElement<ListItem>(WebFind.ById(elementId));
        }
    }
}