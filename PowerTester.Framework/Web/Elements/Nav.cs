﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Nav class
    /// </summary>
    public class Nav : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Nav"/> class.  
        /// </summary>
        public Nav(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Nav"/> class.  
        /// </summary>
        public Nav(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "Nav"; }
        }
    }

    /// <summary>
    /// Nav related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Navs
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Navs
        /// </returns>
        public List<Nav> Navs(WebFinderConstraint findBy)
        {
            return FindElements<Nav>(findBy);
        }

        /// <summary>
        /// Gets a list of Navs
        /// </summary>
        /// <returns>
        /// The list of Navs
        /// </returns>
        public List<Nav> Navs(bool firstChildLevelOnly = false)
        {
            return FindElements<Nav>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Nav
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result Nav.
        /// </returns>
        public Nav Nav(WebFinderConstraint findBy)
        {
            return FindElement<Nav>(findBy);
        }

        /// <summary>
        /// Gets a Nav.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the Nav
        /// </param>
        /// <returns>
        /// The result Nav
        /// </returns>
        public Nav Nav(string elementId)
        {
            return FindElement<Nav>(elementId);
        }
    }
}