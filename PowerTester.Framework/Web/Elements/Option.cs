﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Div class
    /// </summary>
    public class Option : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Div"/> class.  
        /// </summary>
        public Option(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Div"/> class.  
        /// </summary>
        public Option(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "option"; }
        }

        public bool Selected
        {
            get
            {
                bool returnValue;
                if (bool.TryParse(GetAttribute("selected"), out returnValue))
                {
                    return returnValue;
                }

                return false;
            }
        }
    }

    /// <summary>
    /// Div related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Options
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Options
        /// </returns>
        public List<Option> Options(WebFinderConstraint findBy)
        {
            return FindElements<Option>(findBy);
        }

        /// <summary>
        /// Gets a list of Options
        /// </summary>
        /// <returns>
        /// The list of Options
        /// </returns>
        public List<Option> Options(bool firstChildLevelOnly = false)
        {
            return FindElements<Option>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Option
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result Option.
        /// </returns>
        public Option Option(WebFinderConstraint findBy)
        {
            return FindElement<Option>(findBy);
        }

        /// <summary>
        /// Gets a Option.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the Option
        /// </param>
        /// <returns>
        /// The result Option
        /// </returns>
        public Option Option(string elementId)
        {
            return FindElement<Option>(elementId);
        }
    }
}