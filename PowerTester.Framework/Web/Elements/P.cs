﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The P class.
    /// </summary>
    public class P : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="P"/> class. 
        /// </summary>
        public P(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="P"/> class. 
        /// </summary>
        public P(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "P"; }
        }
    }

    /// <summary>
    /// P related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets the list of Ps.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The Ps list.
        /// </returns>
        public List<P> Ps(WebFinderConstraint findBy)
        {
            return FindElements<P>(findBy);
        }

        /// <summary>
        /// Gets the list of Ps.
        /// </summary>
        /// <returns>
        /// The Ps list.
        /// </returns>
        public List<P> Ps(bool firstChildLeveleOnly = false)
        {
            return FindElements<P>(firstChildLeveleOnly);
        }

        /// <summary>
        /// Gets the P.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The result P.
        /// </returns>
        public P P(WebFinderConstraint findBy)
        {
            return FindElement<P>(findBy);
        }

        /// <summary>
        /// Gets the P.
        /// </summary>
        /// <param name="elementId">
        /// The element Id.
        /// </param>
        /// <returns>
        /// The result P.
        /// </returns>
        public P P(string elementId)
        {
            return FindElement<P>(elementId);
        }
    }
}