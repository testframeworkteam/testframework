﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The RadioButton class.
    /// </summary>
    public class RadioButton : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Span"/> class. 
        /// </summary>
        public RadioButton(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Span"/> class. 
        /// </summary>
        public RadioButton(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "Input"; }
        }

        /// <summary>
        /// Gets or sets the Value.
        /// </summary>
        public new bool Value
        {
            get { return GetAttribute("Checked") == "true"; }

            set 
            {
                MouseMove();
                Click(() =>
                {
                    try
                    {
                        return (GetAttribute("Checked") == "true") == value;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }, ClickType.Mouse); 
            }
        }

        /// <summary>
        /// Checks the checkbox.
        /// </summary>
        public void Check()
        {
            Value = true;
        }
    }

    /// <summary>
    /// TextField related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets the list of RadioButtons.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The text fields list.
        /// </returns>
        public List<RadioButton> RadioButtons(WebFinderConstraint findBy)
        {
            return FindElements<RadioButton>(findBy);
        }

        /// <summary>
        /// Gets the list of RadioButtons.
        /// </summary>
        /// <returns>
        /// The text fields list.
        /// </returns>
        public List<RadioButton> RadioButtons(bool firstChildLevelOnly = false)
        {
            return FindElements<RadioButton>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets the RadioButton.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The text field.
        /// </returns>
        public RadioButton RadioButton(WebFinderConstraint findBy)
        {
            return FindElement<RadioButton>(findBy);
        }

        /// <summary>
        /// Gets the RadioButton.
        /// </summary>
        /// <param name="elementId">
        /// The element Id.
        /// </param>
        /// <returns>
        /// The text field.
        /// </returns>
        public RadioButton RadioButton(string elementId)
        {
            return FindElement<RadioButton>(elementId);
        }
    }
}