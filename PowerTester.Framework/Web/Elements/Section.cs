﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Section class.
    /// </summary>
    public class Section : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Section"/> class. 
        /// </summary>
        public Section(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Section"/> class. 
        /// </summary>
        public Section(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "section"; }
        }
    }

    /// <summary>
    /// Section related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets the list of Sections.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The Sections list.
        /// </returns>
        public List<Section> Sections(WebFinderConstraint findBy)
        {
            return FindElements<Section>(findBy);
        }

        /// <summary>
        /// Gets the list of Sections.
        /// </summary>
        /// <returns>
        /// The Sections list.
        /// </returns>
        public List<Section> Sections(bool firstChildLeveleOnly = false)
        {
            return FindElements<Section>(firstChildLeveleOnly);
        }

        /// <summary>
        /// Gets the Section.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The result Section.
        /// </returns>
        public Section Section(WebFinderConstraint findBy)
        {
            return FindElement<Section>(findBy);
        }

        /// <summary>
        /// Gets the Section.
        /// </summary>
        /// <param name="elementId">
        /// The element Id.
        /// </param>
        /// <returns>
        /// The result Section.
        /// </returns>
        public Section Section(string elementId)
        {
            return FindElement<Section>(elementId);
        }
    }
}