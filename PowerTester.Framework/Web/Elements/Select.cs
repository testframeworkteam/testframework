﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Div class
    /// </summary>
    public class Select : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Div"/> class.  
        /// </summary>
        public Select(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Div"/> class.  
        /// </summary>
        public Select(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "select"; }
        }

        public List<Option> Options
        {
            get { return FindElements<Option>(true); }
        }

        public void SelectOption(string option)
        {
            Option optionToSelect = FindElement<Option>(WebFind.ByValue(option, true));
            optionToSelect.Click(() => optionToSelect.Selected);
        }

        public void SelectOption(int index)
        {
            Option optionToSelect = FindElements<Option>(true)[index];
            optionToSelect.Click(() => optionToSelect.Selected);
        }
    }

    /// <summary>
    /// Div related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Selects
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Selects
        /// </returns>
        public List<Select> Selects(WebFinderConstraint findBy)
        {
            return FindElements<Select>(findBy);
        }

        /// <summary>
        /// Gets a list of Selects
        /// </summary>
        /// <returns>
        /// The list of Selects
        /// </returns>
        public List<Select> Selects(bool firstChildLevelOnly = false)
        {
            return FindElements<Select>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Select
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result Select.
        /// </returns>
        public Select Select(WebFinderConstraint findBy)
        {
            return FindElement<Select>(findBy);
        }

        /// <summary>
        /// Gets a Select.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the Select
        /// </param>
        /// <returns>
        /// The result Select
        /// </returns>
        public Select Select(string elementId)
        {
            return FindElement<Select>(elementId);
        }
    }
}