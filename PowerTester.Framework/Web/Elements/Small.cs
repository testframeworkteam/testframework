﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Small class
    /// </summary>
    public class Small : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Small"/> class.  
        /// </summary>
        public Small(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Small"/> class.  
        /// </summary>
        public Small(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "Small"; }
        }
    }

    /// <summary>
    /// Small related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Smalls
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Smalls
        /// </returns>
        public List<Small> Smalls(WebFinderConstraint findBy)
        {
            return FindElements<Small>(findBy);
        }

        /// <summary>
        /// Gets a list of Smalls
        /// </summary>
        /// <returns>
        /// The list of Smalls
        /// </returns>
        public List<Small> Smalls(bool firstChildLevelOnly = false)
        {
            return FindElements<Small>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Small
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result Small.
        /// </returns>
        public Small Small(WebFinderConstraint findBy)
        {
            return FindElement<Small>(findBy);
        }

        /// <summary>
        /// Gets a Small.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the Small
        /// </param>
        /// <returns>
        /// The result Small
        /// </returns>
        public Small Small(string elementId)
        {
            return FindElement<Small>(elementId);
        }
    }
}