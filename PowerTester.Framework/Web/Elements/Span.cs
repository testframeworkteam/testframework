﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The span class.
    /// </summary>
    public class Span : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Span"/> class. 
        /// </summary>
        public Span(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Span"/> class. 
        /// </summary>
        public Span(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "span"; }
        }
    }

    /// <summary>
    /// Span related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets the list of Spans.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The Spans list.
        /// </returns>
        public List<Span> Spans(WebFinderConstraint findBy)
        {
            return FindElements<Span>(findBy);
        }

        /// <summary>
        /// Gets the list of Spans.
        /// </summary>
        /// <returns>
        /// The Spans list.
        /// </returns>
        public List<Span> Spans(bool firstChildLeveleOnly = false)
        {
            return FindElements<Span>(firstChildLeveleOnly);
        }

        /// <summary>
        /// Gets the Span.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The result Span.
        /// </returns>
        public Span Span(WebFinderConstraint findBy)
        {
            return FindElement<Span>(findBy);
        }

        /// <summary>
        /// Gets the Span.
        /// </summary>
        /// <param name="elementId">
        /// The element Id.
        /// </param>
        /// <returns>
        /// The result Span.
        /// </returns>
        public Span Span(string elementId)
        {
            return FindElement<Span>(elementId);
        }
    }
}