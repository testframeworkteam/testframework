﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Div class
    /// </summary>
    public class Table : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Table"/> class.  
        /// </summary>
        public Table(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Table"/> class.  
        /// </summary>
        public Table(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "table"; }
        }

        public List<Tr> Rows
        {
            get { return Body.Rows; }
        }

        public List<Tr> HeaderRows
        {
            get { return Header.Rows;  }
        }

        public Tbody Body
        {
            get { return Tbodys(true)[0]; }
        }

        public Thead Header
        {
            get { return Theads(true)[0]; }
        }
    }

    /// <summary>
    /// Div related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Tables
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Tables
        /// </returns>
        public List<Table> Tables(WebFinderConstraint findBy)
        {
            return FindElements<Table>(findBy);
        }

        /// <summary>
        /// Gets a list of Tables
        /// </summary>
        /// <returns>
        /// The list of Tables
        /// </returns>
        public List<Table> Tables(bool firstChildLevelOnly = false)
        {
            return FindElements<Table>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Table
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result Table.
        /// </returns>
        public Table Table(WebFinderConstraint findBy)
        {
            return FindElement<Table>(findBy);
        }

        /// <summary>
        /// Gets a Table.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the div
        /// </param>
        /// <returns>
        /// The result Table
        /// </returns>
        public Table Table(string elementId)
        {
            return FindElement<Table>(elementId);
        }
    }
}