﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Div class
    /// </summary>
    public class Tbody : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Tbody"/> class.  
        /// </summary>
        public Tbody(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Tbody"/> class.  
        /// </summary>
        public Tbody(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "tbody"; }
        }

        public List<Tr> Rows
        {
            get { return Trs(true); }
        }
    }

    /// <summary>
    /// Div related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Tbody
        /// </summary>
        /// <param name="findBy">
        /// The FindBy consTbodyaint
        /// </param>
        /// <returns>
        /// The list of Tbody
        /// </returns>
        public List<Tbody> Tbodys(WebFinderConstraint findBy)
        {
            return FindElements<Tbody>(findBy);
        }

        /// <summary>
        /// Gets a list of Tbody
        /// </summary>
        /// <returns>
        /// The list of Tbody
        /// </returns>
        public List<Tbody> Tbodys(bool firstChildLevelOnly = false)
        {
            return FindElements<Tbody>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Tbody
        /// </summary>
        /// <param name="findBy">
        /// The FindBy consTbodyaint
        /// </param>
        /// <returns>
        /// The result Tbody.
        /// </returns>
        public Tbody Tbody(WebFinderConstraint findBy)
        {
            return FindElement<Tbody>(findBy);
        }

        /// <summary>
        /// Gets a Tbody.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the Tbody
        /// </param>
        /// <returns>
        /// The result Tbody
        /// </returns>
        public Tbody Tbody(string elementId)
        {
            return FindElement<Tbody>(elementId);
        }
    }
}