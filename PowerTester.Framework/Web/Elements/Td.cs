﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Div class
    /// </summary>
    public class Td : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Td"/> class.  
        /// </summary>
        public Td(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Td"/> class.  
        /// </summary>
        public Td(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "td"; }
        }
    }

    /// <summary>
    /// Div related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Tds
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Tds
        /// </returns>
        public List<Td> Tds(WebFinderConstraint findBy)
        {
            return FindElements<Td>(findBy);
        }

        /// <summary>
        /// Gets a list of Tds
        /// </summary>
        /// <returns>
        /// The list of Tds
        /// </returns>
        public List<Td> Tds(bool firstChildLevelOnly = false)
        {
            return FindElements<Td>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Td
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result Td.
        /// </returns>
        public Td Td(WebFinderConstraint findBy)
        {
            return FindElement<Td>(findBy);
        }

        /// <summary>
        /// Gets a Td.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the div
        /// </param>
        /// <returns>
        /// The result Td
        /// </returns>
        public Td Td(string elementId)
        {
            return FindElement<Td>(elementId);
        }
    }
}