﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The TextArea class.
    /// </summary>
    public class TextArea : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Span"/> class. 
        /// </summary>
        public TextArea(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Span"/> class. 
        /// </summary>
        public TextArea(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "textarea"; }
        }
        
        /// <summary>
        /// Gets or sets the Value.
        /// </summary>
        public new string Value
        {
            get { return GetAttribute("value"); }

            set { SetAttribute("value", value); }
        }

        /// <summary>
        /// Types text to textbox
        /// </summary>
        /// <param name="text">
        /// The text to type.
        /// </param>
        public void TypeText(string text)
        {
            SendKeys(text);
        }

        /// <summary>
        /// Types text to textbox
        /// </summary>
        /// <param name="text">
        /// The text to type.
        /// </param>
        /// <param name="clear">
        /// Value inidcating whether the textfield have to be cleared before typing.
        /// </param>
        public void TypeText(string text, bool clear)
        {
            SendKeys(text, clear);
        }
    }

    /// <summary>
    /// TextField related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets the list of TextAreas.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The text fields list.
        /// </returns>
        public List<TextArea> TextAreas(WebFinderConstraint findBy)
        {
            return FindElements<TextArea>(findBy);
        }

        /// <summary>
        /// Gets the list of TextAreas.
        /// </summary>
        /// <returns>
        /// The text fields list.
        /// </returns>
        public List<TextArea> TextAreas(bool firstChildLevelOnly = false)
        {
            return FindElements<TextArea>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets the TextArea.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The text field.
        /// </returns>
        public TextArea TextArea(WebFinderConstraint findBy)
        {
            return FindElement<TextArea>(findBy);
        }

        /// <summary>
        /// Gets the TextArea.
        /// </summary>
        /// <param name="elementId">
        /// The element Id.
        /// </param>
        /// <returns>
        /// The text field.
        /// </returns>
        public TextArea TextArea(string elementId)
        {
            return FindElement<TextArea>(elementId);
        }
    }
}