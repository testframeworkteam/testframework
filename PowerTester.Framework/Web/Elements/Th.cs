﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Div class
    /// </summary>
    public class Th : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Th"/> class.  
        /// </summary>
        public Th(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Th"/> class.  
        /// </summary>
        public Th(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "th"; }
        }
    }

    /// <summary>
    /// Div related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Th
        /// </summary>
        /// <param name="findBy">
        /// The FindBy consThaint
        /// </param>
        /// <returns>
        /// The list of Th
        /// </returns>
        public List<Th> Ths(WebFinderConstraint findBy)
        {
            return FindElements<Th>(findBy);
        }

        /// <summary>
        /// Gets a list of Th
        /// </summary>
        /// <returns>
        /// The list of Th
        /// </returns>
        public List<Th> Ths(bool firstChildLevelOnly = false)
        {
            return FindElements<Th>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a TableRow
        /// </summary>
        /// <param name="findBy">
        /// The FindBy consThaint
        /// </param>
        /// <returns>
        /// The result TableRow.
        /// </returns>
        public Th Th(WebFinderConstraint findBy)
        {
            return FindElement<Th>(findBy);
        }

        /// <summary>
        /// Gets a TableRow.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the TableRow
        /// </param>
        /// <returns>
        /// The result TableRow
        /// </returns>
        public Th Th(string elementId)
        {
            return FindElement<Th>(elementId);
        }
    }
}