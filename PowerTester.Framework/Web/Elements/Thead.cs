﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Div class
    /// </summary>
    public class Thead : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Thead"/> class.  
        /// </summary>
        public Thead(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Thead"/> class.  
        /// </summary>
        public Thead(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "thead"; }
        }

        public List<Tr> Rows
        {
            get { return Trs(true); }
        }
    }

    /// <summary>
    /// Div related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Thead
        /// </summary>
        /// <param name="findBy">
        /// The FindBy consTheadaint
        /// </param>
        /// <returns>
        /// The list of Thead
        /// </returns>
        public List<Thead> Theads(WebFinderConstraint findBy)
        {
            return FindElements<Thead>(findBy);
        }

        /// <summary>
        /// Gets a list of Thead
        /// </summary>
        /// <returns>
        /// The list of Thead
        /// </returns>
        public List<Thead> Theads(bool firstChildLevelOnly = false)
        {
            return FindElements<Thead>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a Thead
        /// </summary>
        /// <param name="findBy">
        /// The FindBy consTheadaint
        /// </param>
        /// <returns>
        /// The result Thead.
        /// </returns>
        public Thead Thead(WebFinderConstraint findBy)
        {
            return FindElement<Thead>(findBy);
        }

        /// <summary>
        /// Gets a Thead.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the Thead
        /// </param>
        /// <returns>
        /// The result Thead
        /// </returns>
        public Thead Thead(string elementId)
        {
            return FindElement<Thead>(elementId);
        }
    }
}