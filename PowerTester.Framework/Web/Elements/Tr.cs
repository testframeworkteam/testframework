﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The Div class
    /// </summary>
    public class Tr : WebAutElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Tr"/> class.  
        /// </summary>
        public Tr(string xPath)
            : base(xPath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Tr"/> class.  
        /// </summary>
        public Tr(string xPath, IWebElement rootElement)
            : base(xPath, rootElement)
        {
        }

        public override string HtmlTag
        {
            get { return "tr"; }
        }

        public List<Td> Cells
        {
            get { return Tds(true); }
        }

        public List<Th> HeaderCells
        {
            get { return Ths(true); }
        }
    }

    /// <summary>
    /// Div related functions of WebAutomationElement
    /// </summary>
    public partial class WebAutElement
    {
        /// <summary>
        /// Gets a list of Tr
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The list of Tr
        /// </returns>
        public List<Tr> Trs(WebFinderConstraint findBy)
        {
            return FindElements<Tr>(findBy);
        }

        /// <summary>
        /// Gets a list of Tr
        /// </summary>
        /// <returns>
        /// The list of Tr
        /// </returns>
        public List<Tr> Trs(bool firstChildLevelOnly = false)
        {
            return FindElements<Tr>(firstChildLevelOnly);
        }

        /// <summary>
        /// Gets a TableRow
        /// </summary>
        /// <param name="findBy">
        /// The FindBy constraint
        /// </param>
        /// <returns>
        /// The result TableRow.
        /// </returns>
        public Tr Tr(WebFinderConstraint findBy)
        {
            return FindElement<Tr>(findBy);
        }

        /// <summary>
        /// Gets a TableRow.
        /// </summary>
        /// <param name="elementId">
        /// The ID of the TableRow
        /// </param>
        /// <returns>
        /// The result TableRow
        /// </returns>
        public Tr Tr(string elementId)
        {
            return FindElement<Tr>(elementId);
        }
    }
}