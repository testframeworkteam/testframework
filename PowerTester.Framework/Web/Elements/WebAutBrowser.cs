﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using PowerTester.Framework.UI.Applications;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.UI.Controls;
using PowerTester.Framework.Web.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.PhantomJS;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The WebAutomationBrowser class.
    /// </summary>
    public class WebAutBrowser : WebAutElement
    {
        #region Variables

        private Actions _actions = null;
        private IMouse _mouse = null;
        private IWebDriver _webDriver;
        private int _processID;
        private IJavaScriptExecutor _scriptExecutor = null;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WebAutBrowser"/> class.  
        /// </summary>
        /// <param name="browserType">browser type</param>
        public WebAutBrowser(BrowserType browserType) : base("")
        {
            switch (browserType)
            {
                case BrowserType.Default:
                case BrowserType.InternetExplorer:
                {
                    InternetExplorerOptions options = new InternetExplorerOptions {EnablePersistentHover = false};
                    List<int> processIds = Process.GetProcessesByName("iexplore").Where(p => p.MainWindowHandle.ToInt32() != 0).Select(p => p.Id).ToList();
                    _webDriver = new InternetExplorerDriver(options);

                    foreach (Process proc in Process.GetProcessesByName("iexplore").Where(p => p.MainWindowHandle.ToInt32() != 0).Where(p => !processIds.Contains(p.Id)))
                    {
                        try
                        {
                            UIWindow window = Application.FindApplication(proc.Id).FindUIWindow();
                            _processID = proc.Id;
                            InternetExplorer.SetApplication(_processID);
                            Forward();
                            break;
                        }
                        catch (NullReferenceException)
                        {
                        }
                        catch (UIAutomationException)
                        {
                        }
                    }
                    break;
                }
                case BrowserType.FireFox:
                    {
                        List<int> processIds = Process.GetProcessesByName("firefox").Where(p => p.MainWindowHandle.ToInt32() != 0).Select(p => p.Id).ToList();

                        _webDriver = new FirefoxDriver();

                        _processID = Process.GetProcessesByName("firefox").Where(p => p.MainWindowHandle.ToInt32() != 0).First(p => !processIds.Contains(p.Id)).Id;
                        FireFox.SetApplication(_processID);
                        break;
                    }
                case BrowserType.Chrome:
                    {
                        Directory.CreateDirectory(WebAutEngine.ChromeDownloadFilepath);
                        ChromeOptions options = new ChromeOptions();
                        options.AddLocalStatePreference("download.default_directory", WebAutEngine.ChromeDownloadFilepath);
                        options.AddLocalStatePreference("download.prompt_for_download", false);
                        options.AddUserProfilePreference("download.default_directory", WebAutEngine.ChromeDownloadFilepath);
                        options.AddUserProfilePreference("download.prompt_for_download", false);
                        options.AddArguments("start-maximized");

                        List<int> processIds = Process.GetProcessesByName("chrome").Where(p => p.MainWindowHandle.ToInt32() != 0).Select(p => p.Id).ToList();

                        _webDriver = new ChromeDriver(options);

                        _processID = Process.GetProcessesByName("chrome").Where(p => p.MainWindowHandle.ToInt32() != 0).First(p => !processIds.Contains(p.Id)).Id;
                        Chrome.SetApplication(_processID);
                        break;
                    }
                case BrowserType.None:
                    {
                        _webDriver = new PhantomJSDriver();
                        break;
                    }
                default:
                    throw new ArgumentException("The given type for creating browser is not supported!");
            }
            _actions = new Actions(_webDriver);
            _mouse = ((IHasInputDevices)_webDriver).Mouse;
            _scriptExecutor = (IJavaScriptExecutor)_webDriver;
            UIAutomationSettings.WaitForApplication = 10;
            UIAutomationSettings.WaitForUIControl = 0;
            UIAutomationSettings.WaitForDialog = 0;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the Document.
        /// </summary>
        public WebAutElement Document
        {
            get { return new WebAutElement("/*"); }
        }

        /// <summary>
        /// Gets the ProcessId.
        /// </summary>
        public int ProcessID
        {
            get { return _processID; }
        }

        /// <summary>
        /// Gets the browserType.
        /// </summary>
        public BrowserType Type
        {
            get
            {
                if (_webDriver is InternetExplorerDriver)
                {
                    return BrowserType.InternetExplorer;
                }

                if (_webDriver is FirefoxDriver)
                {
                    return BrowserType.FireFox;
                }

                if (_webDriver is ChromeDriver)
                {
                    return BrowserType.Chrome;
                }

                throw new AutomationException("Add the new browser type here!!!");
            }
        }

        public IWebDriver WebDriver
        {
            get { return _webDriver; }
        }

        /// <summary>
        /// Gets the current window handle
        /// </summary>
        public string CurrentWindowHandle
        {
            get { return _webDriver.CurrentWindowHandle; }
        }

        /// <summary>
        /// Gets the Html.
        /// </summary>
        public string Html
        {
            get { return _webDriver.PageSource; }
        }

        /// <summary>
        /// Gets the title.
        /// </summary>
        public new string Title
        {
            get { return _webDriver.Title; }
        }

        /// <summary>
        /// Gets or sets the DownloadDialog.
        /// </summary>
        public IDownloadDialog DownloadDialog
        {
            get
            {
                switch (Type)
                {
                    case BrowserType.InternetExplorer:
                        return new InternetExplorer.DownloadDialog();
                    case BrowserType.Chrome:
                        return new Chrome.DownloadDialog();
                    case BrowserType.FireFox:
                        return new FireFox.DownloadDialog();
                    default:
                        throw new AutomationException("Unknown browsertype");
                }
            }
        }

        /// <summary>
        /// Gets or sets the DownloadDialog.
        /// </summary>
        public IOpenDialog OpenDialog
        {
            get
            {
                switch (Type)
                {
                    case BrowserType.InternetExplorer:
                        return new InternetExplorer.OpenDialog();
                    case BrowserType.Chrome:
                        return new Chrome.OpenDialog();
                    case BrowserType.FireFox:
                        return new FireFox.OpenDialog();
                    default:
                        throw new AutomationException("Unknown browsertype");
                }
            }
        }

        /// <summary>
        /// Gets or sets the url.
        /// </summary>
        public string Url
        {
            get { return _webDriver.Url; }
            set { _webDriver.Url = value; }
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        public string Version
        {
            get { return Process.GetProcessById(_processID).MainModule.FileVersionInfo.ProductVersion; }
        }

        /// <summary>
        /// Gets or sets the Actions.
        /// </summary>
        public Actions Actions
        {
            get { return _actions; }
            set { _actions = value; }
        }

        /// <summary>
        /// Gets or sets the mouse.
        /// </summary>
        public IMouse Mouse
        {
            get { return _mouse; }
            set { _mouse = value; }
        }


        /// <summary>
        /// Gets or sets the ScriptExecutor.
        /// </summary>
        public IJavaScriptExecutor ScriptExecutor
        {
            get { return _scriptExecutor; }
            set { _scriptExecutor = value; }
        }

        public IAlertDialog AlertDialog
        {
            get
            {
                switch (Type)
                {
                    case BrowserType.InternetExplorer:
                        return new InternetExplorer.AlertDialog();
                    case BrowserType.Chrome:
                        return new Chrome.AlertDialog();
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        public IMessageDialog MessageDialog
        {
            get
            {
                switch (Type)
                {
                    case BrowserType.InternetExplorer:
                        return new InternetExplorer.MessageDialog();
                    case BrowserType.Chrome:
                        return new Chrome.MessageDialog();
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        #endregion

        #region methods
        public void Maximize()
        {
            _webDriver.Manage().Window.Maximize();
        }

        /// <summary>
        /// Closes the webdriver's browser.
        /// </summary>
        public void Close()
        {
            _webDriver.Close();
        }

        /// <summary>
        /// Quit the webdriver.
        /// </summary>
        public void Quit()
        {
            _webDriver.Quit();
        }

        /// <summary>
        /// Navigates browser to url.
        /// </summary>
        /// <param name="url">
        /// The url to go to.
        /// </param>
        public void GoTo(string url)
        {
            _webDriver.Navigate().GoToUrl(url);
        }

        /// <summary>
        /// Calles back on browser.
        /// </summary>
        public void Back()
        {
            _webDriver.Navigate().Back();
        }

        /// <summary>
        /// Calles forward on browser.
        /// </summary>
        public void Forward()
        {
            _webDriver.Navigate().Forward();
        }

        /// <summary>
        /// Calles refresh on browser.
        /// </summary>
        public void Refresh()
        {
            _webDriver.Navigate().Refresh();
        }

        /// <summary>
        /// Attaches to a frame in browser
        /// </summary>
        /// <param name="frameID">
        /// The frame id.
        /// </param>
        public void AttachToFrame(string frameID)
        {
            AttachToFrame(frameID, UIAutomationSettings.WaitForTimeout);
        }

        /// <summary>
        /// Attaches to a frame in browser
        /// </summary>
        /// <param name="frameID">
        /// The frame id.
        /// </param>
        /// <param name="timeOut">Timeout for the dialog to appear.</param>
        public void AttachToFrame(string frameID, int timeOut)
        {
            Logger.Debug(string.Format("Attaching to dialog '{0}'", frameID));
            General.WaitFor(() =>
            {
                foreach (IFrame frame in IFrames())
                {
                    if (frame.Id == frameID && frame.Displayed)
                    {
                        _webDriver.SwitchTo().Frame(frame.Element);
                        return true;
                    }
                }
                return false;
            }, timeOut);
        }
        
        public void AttachToFrame(IFrame frame)
        {
            AttachToFrame(frame, UIAutomationSettings.WaitForTimeout);
        }
        
        public void AttachToFrame(IFrame frame, int timeOut)
        {
            _webDriver.SwitchTo().Frame(frame.Element);
        }
        
        /// <summary>
        /// Detaches from frame.
        /// </summary>
        public void DetachFrame()
        {
            //string hwnd = _webDriver.WindowHandles[0];
            _webDriver.SwitchTo().DefaultContent();
        }

        /// <summary>
        /// Switches to a popup window.
        /// </summary>
        /// <param name="timeout">
        /// The Timeout.
        /// </param>
        public void SwitchToPopupWindow(int timeout)
        {
            int i = 0;

            while (_webDriver.WindowHandles.Count < 2 && i < timeout)
            {
                System.Threading.Thread.Sleep(1000);
                i++;
            }

            if (i >= timeout)
            {
                throw new TimeoutException(string.Format("Popup window was not found in {0} sec", timeout));
            }

            string popupWindowHandle = _webDriver.WindowHandles[1];
            _webDriver.SwitchTo().Window(popupWindowHandle);
        }

        public List<string> WindowHandles
        {
            get { return _webDriver.WindowHandles.ToList(); }
        }

        /// <summary>
        /// Switches to a desired window.
        /// </summary>
        public void SwitchToWindow(string windowhandle)
        {
            _webDriver.SwitchTo().Window(windowhandle);
        }
        
        /// <summary>
        /// Switches to a popup window.
        /// </summary>
        public void SwitchToPopupWindow()
        {
            SwitchToPopupWindow(30);
        }

        /// <summary>
        /// Switches to main window.
        /// </summary>
        public void SwitchToMainWindow()
        {
            string mainWindowHandle = _webDriver.WindowHandles[0];
            _webDriver.SwitchTo().Window(mainWindowHandle);
        }

        /// <summary>
        /// Switches to main window.
        /// </summary>
        public IAlert SwitchToAlert()
        {
            return _webDriver.SwitchTo().Alert();
        }
        
        /// <summary>
        /// Gets a value inidcating whether the url is opened in new window.
        /// </summary>
        /// <param name="url">
        /// The url to look for.
        /// </param>
        /// <param name="closeWindow">
        /// Value inidcating whether the window have to be closed after found.
        /// </param>
        /// <returns>
        /// value inidcating whether the url is opened in new window.
        /// </returns>
        public bool IsUrlOpenedInNewWindow(string url, bool closeWindow = false)
        {
            string parentWindowHandle = _webDriver.CurrentWindowHandle;
            bool result = false;
            foreach (string window in _webDriver.WindowHandles)
            {
                _webDriver.SwitchTo().Window(window);
                if (_webDriver.Url.StartsWith(url, StringComparison.InvariantCultureIgnoreCase))
                {
                    result = true;
                    if (closeWindow)
                    {
                        _webDriver.Close();
                    }
                }
            }

            _webDriver.SwitchTo().Window(parentWindowHandle);
            return result;
        }

        /// <summary>
        /// Gets a value indicating whether the browser contains a text.
        /// </summary>
        /// <param name="text">
        /// The text to look for.
        /// </param>
        /// <returns>
        /// a value indicating whether the browser contains a text.
        /// </returns>
        public bool ContainsText(string text)
        {
            return Html.Contains(text);
        }

        /// <summary>
        /// Executes script
        /// </summary>
        /// <param name="script">
        /// The script to execute.
        /// </param>
        /// <param name="args">
        /// The arguments for the script
        /// </param>
        /// <returns>
        /// The return value of the script.
        /// </returns>
        public object ExecuteScript(string script, params object[] args)
        {
            return ((IJavaScriptExecutor)_webDriver).ExecuteScript(script, args);
        }

        /// <summary>
        /// Gets the WebElement 
        /// </summary>
        /// <returns>
        /// The WebElement
        /// </returns>
        internal IWebElement FindWebElement(string xPath)
        {
            return _webDriver.FindElement(By.XPath(xPath));
        }

        /// <summary>
        /// Gets the WebElement 
        /// </summary>
        /// <returns>
        /// The WebElement
        /// </returns>
        internal List<IWebElement> FindWebElements(string xPath)
        {
            return _webDriver.FindElements(By.XPath(xPath)).ToList();
        }

        public bool Download(WebAutElement elementToClick, string path, ClickType clickType = ClickType.Mouse, int waitfordownload = 15)
        {
            if (_webDriver is InternetExplorerDriver)
            {
                int i = 0;
                do
                {
                    elementToClick.MouseMove();
                    elementToClick.Click(clickType);
                    i++;
                } while (!General.WaitFor(() => WebAutEngine.Browser.DownloadDialog.Exists) && i < 5);

                if (WebAutEngine.Browser.DownloadDialog.Exists)
                {
                    WebAutEngine.Browser.DownloadDialog.Save(path);
                    //// Wait for the file to be saved.
                    Logger.Debug("Wait for the file to be saved.");
                    return General.WaitFor(() => File.Exists(path), waitfordownload);
                }
            }
            else if (_webDriver is ChromeDriver)
            {
                foreach (string file in Directory.GetFiles(WebAutEngine.ChromeDownloadFilepath))
                {
                    File.Delete(file);
                }

                //elementToClick.MouseMove();
                elementToClick.Click(clickType);
                if (!General.WaitFor(() => Directory.GetFiles(WebAutEngine.ChromeDownloadFilepath).Any(), waitfordownload))
                {
                    return false;
                }

                General.WaitFor(() => Directory.GetFiles(WebAutEngine.ChromeDownloadFilepath).Any(a => !a.EndsWith("tmp") && !a.EndsWith("crdownload")), 10);
                File.Copy(Directory.GetFiles(WebAutEngine.ChromeDownloadFilepath).First(), path);
                return true;
            }
            return false;
        }

        public bool Upload(WebAutElement elementToClick, string path, ClickType clickType = ClickType.Mouse)
        {
            int i = 0;
            do
            {
                elementToClick.MouseMove();
                elementToClick.Click(clickType);
                i++;
            } while (!General.WaitFor(() => WebAutEngine.Browser.OpenDialog.Exists) && i < 5);

            if (WebAutEngine.Browser.OpenDialog.Exists)
            {
                WebAutEngine.Browser.OpenDialog.Open(path);
                //// Wait for the file to be saved.
                Logger.Debug("Wait for the file to be saved.");
                return General.WaitFor(() => !WebAutEngine.Browser.OpenDialog.Exists);
            }
            return false;
        }
        #endregion

        #region IDisposable Members

        /// <summary>
        /// Disposes the webdriver.
        /// </summary>
        public void Dispose()
        {
            _webDriver.Dispose();
        }

        #endregion
    }
}
