﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.Web.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;

namespace PowerTester.Framework.Web.Elements
{
    /// <summary>
    /// The main functions and members of WebAutElement
    /// </summary>
    public partial class WebAutElement : IWebAutElement
    {
        ///// <summary>
        ///// The IWebElement.
        ///// </summary>
        //protected IWebElement Element;
        private readonly string _xPath;
        private IWebElement _element;

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WebAutElement"/> class. 
        /// </summary>
        public WebAutElement(string xPath) :this(xPath, null)
        {}

        /// <summary>
        /// Initializes a new instance of the <see cref="WebAutElement"/> class. 
        /// </summary>
        public WebAutElement(string xPath, IWebElement element)
        {
            _xPath = xPath;
            _element = element;
        }

        #endregion

        #region Properties

        public string XPath { get { return _xPath; } }

        public IWebElement Element
        {
            get
            {
                if (_element == null)
                {
                    _element = WebAutEngine.Browser.FindWebElement(_xPath);
                }

                return _element;
            }
        }
      

        public virtual string HtmlTag
        {
            get { return "*"; }
        }

        /// <summary>
        /// Gets the Id.
        /// </summary>
        public string Id
        {
            get { return Element.GetAttribute("id"); }
        }

        /// <summary>
        /// Gets the parent.
        /// </summary>
        public T Parent<T>() where T : IWebAutElement
        {
            return (T)Activator.CreateInstance(typeof(T), string.Concat(_xPath, "/.."));
        }

        /// <summary>
        /// Gets the ClassName.
        /// </summary>
        public string ClassName
        {
            get { return Element.GetAttribute("class").Trim(); }
        }

        /// <summary>
        /// Gets a value indicating whether the element is displayed.
        /// </summary>
        public bool Displayed
        {
            get
            {
                try
                {
                    return Element.Displayed;
                }
                catch (Exception)
                {
                    Logger.Debug(string.Format("Looked for element xpath {0} throw exception", _xPath));
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the element is enabled.
        /// </summary>
        public bool Enabled
        {
            get { return Element.Enabled; }
        }

        /// <summary>
        /// Gets a value indicating whether the element is disabled.
        /// </summary>
        public bool Disabled
        {
            get { return !Enabled; }
        }

        /// <summary>
        /// Gets a value indicating whether the element exists.
        /// </summary>
        public bool Exists
        {
            get
            {
                try
                {
                    return Element != null;
                }
                catch (Exception)
                {
                    Logger.Debug(string.Format("Looked for element xpath {0} throw exception", _xPath));
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets the tagname.
        /// </summary>
        public string TagName
        {
            get { return Element.TagName; }
        }

        /// <summary>
        /// Gets the text of the element.
        /// </summary>
        public string Text
        {
            get { return (Element.Text != null) ? Element.Text.Trim() : Element.GetAttribute("text").Trim(); }
        }

        /// <summary>
        /// Gets the innerText of the element.
        /// </summary>
        public string InnerText
        {
            get { return Element.GetAttribute("innerText").Trim(); }
        }

        /// <summary>
        /// Gets the outerText of the element.
        /// </summary>
        public string OuterText
        {
            get { return Element.GetAttribute("outerText").Trim(); }
        }

        /// <summary>
        /// Gets the innerHTML of the element.
        /// </summary>
        public string InnerHTML
        {
            get { return Element.GetAttribute("innerHTML").Trim(); }
        }

        /// <summary>
        /// Gets the OuterHTML of the element.
        /// </summary>
        public string OuterHTML
        {
            get { return Element.GetAttribute("outerHTML").Trim(); }
        }

        /// <summary>
        /// Gets a value indicating whether the element is read only.
        /// </summary>
        public bool ReadOnly
        {
            get { return !string.IsNullOrEmpty(GetAttribute("readOnly")); }
        }

        /// <summary>
        /// Gets a value indicating whether the element is selected.
        /// </summary>
        public bool Selected
        {
            get { return Element.Selected; }
        }
        
        /// <summary>
        /// Gets the Title.
        /// </summary>
        public string Title
        {
            get { return Element.GetAttribute("title"); }
        }

        /// <summary>
        /// Gets the Name.
        /// </summary>
        public string Name
        {
            get { return Element.GetAttribute("name"); }
        }

        /// <summary>
        /// Gets the Value.
        /// </summary>
        public string Value
        {
            get { return Element.GetAttribute("value"); }
        }
     
        #endregion

        #region public Methods
        #region FindElement

        /// <summary>
        /// Gets the WebAutElement 
        /// </summary>
        /// <param name="id">
        /// The Element Id.
        /// </param>
        /// <returns>
        /// The WebAutElement
        /// </returns>
        public WebAutElement FindElement(string id)
        {
            return FindElement(WebFind.ById(id));
        }

        /// <summary>
        /// Gets the WebAutElement 
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The WebAutElement
        /// </returns>
        public WebAutElement FindElement(WebFinderConstraint findBy)
        {
            return FindElement<WebAutElement>(findBy);
        }

        /// <summary>
        /// Gets the Element.
        /// </summary>
        /// <typeparam name="T">
        /// The Generic.</typeparam>
        /// <param name="id">
        /// The element Id.
        /// </param>
        /// <returns>
        /// The Element.
        /// </returns>
        public T FindElement<T>(string id) where T : WebAutElement
        {
            return FindElement<T>(WebFind.ById(id));
        }

        /// <summary>
        /// Gets the Element.
        /// </summary>
        /// <typeparam name="T">
        /// The Generic.
        /// </typeparam>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <returns>
        /// The Element.
        /// </returns>
        public T FindElement<T>(WebFinderConstraint findBy) where T : WebAutElement
        {
            return (T)Activator.CreateInstance(typeof(T), findBy.ToXPath<T>(_xPath));
        }

        /// <summary>
        /// Gets the Elements.
        /// </summary>
        /// <returns>
        /// The Elements.
        /// </returns>
        public List<WebAutElement> FindElements(bool firstChildOnly = false)
        {
            return FindElements<WebAutElement>(firstChildOnly);
        }

        /// <summary>
        /// Gets the Elements.
        /// </summary>
        /// <returns>
        /// The Elements.
        /// </returns>
        public List<T> FindElements<T>(bool firstChildOnly = false)where T : WebAutElement 
        {
            return FindElements<T>(new WebFinderConstraint(firstChildOnly));
        }
        
        /// <summary>
        /// Gets the Element.
        /// </summary>
        /// <param name="findBy">
        /// The find By.
        /// </param>
        /// <typeparam name="T">
        /// The Generic.
        /// </typeparam>
        /// <returns>
        /// The Element.
        /// </returns>
        public List<T> FindElements<T>(WebFinderConstraint findBy) where T : WebAutElement
        {
            string xPath = findBy.ToXPath<T>(_xPath);
            List<T> elementsList = new List<T>();

            if (_element == null)
            {
                if (WebAutEngine.Browser.FindWebElements(xPath).Any())
                {
                    elementsList.AddRange(WebAutEngine.Browser.FindWebElements(xPath).Select((webElement, i) => (T)Activator.CreateInstance(typeof(T), string.Format("({0})[{1}]", xPath, i + 1), webElement)));
                }
            }
            else
            {
                if (_element.FindElements(By.XPath(xPath)).Any())
                {
                    elementsList.AddRange(_element.FindElements(By.XPath(xPath)).Select((webElement, i) => (T)Activator.CreateInstance(typeof(T), string.Format("({0})[{1}]", xPath, i + 1), webElement)));
                }
            }

            return elementsList;
        }

        #endregion

        #region General

        /// <summary>
        /// Gets the value of attribute
        /// </summary>
        /// <param name="attributeName">
        /// The Attribute name
        /// </param>
        /// <returns>
        /// The value.
        /// </returns>
        public string GetAttributeValue(string attributeName)
        {
            return Element.GetAttribute(attributeName);
        }

        /// <summary>
        /// Clears the element.
        /// </summary>
        public void Clear()
        {
            Element.Clear();
        }

        /// <summary>
        /// Gets the Attribute.
        /// </summary>
        /// <param name="attributeName">
        /// The Attribute name.
        /// </param>
        /// <returns>
        /// The Attribute.
        /// </returns>
        public string GetAttribute(string attributeName)
        {
            return Element.GetAttribute(attributeName);
        }

        /// <summary>
        /// Set the Attribute value
        /// </summary>
        /// <param name="attributeName">
        /// The Attribute name.
        /// </param>
        /// <param name="attributeValue">
        /// The Value.
        /// </param>
        public void SetAttribute(string attributeName, string attributeValue)
        {
            if (WebAutEngine.Browser != null)
            {
                WebAutEngine.Browser.ScriptExecutor.ExecuteScript(
                    "arguments[0].setAttribute(arguments[1],arguments[2])", Element, attributeName, attributeValue);
            }
        }

        /// <summary>
        /// Send keys.
        /// </summary>
        /// <param name="text">
        /// The text to send.
        /// </param>
        public void SendKeys(string text)
        {
            SendKeys(text, true);
        }

        /// <summary>
        /// Send keys.
        /// </summary>
        /// <param name="text">
        /// The text to send.
        /// </param>
        /// <param name="clear">
        /// The clear.
        /// </param>
        public void SendKeys(string text, bool clear)
        {
            if (clear)
            {
                General.WaitFor(() =>
                {
                    Clear();
                    return Text == "";
                });
            }

            Element.SendKeys(text);
        }

        /// <summary>
        /// Submit the element.
        /// </summary>
        public void Submit()
        {
            Element.Submit();
        }

        #endregion

        #region Mouse handling

        /// <summary>
        /// Simple mouse click on the element
        /// </summary>
        public void Click(ClickType clickType = ClickType.Standard)
        {
            //if (Displayed)
            //{
                switch (clickType)
                {
                    case ClickType.Standard:
                        Element.Click();
                        break;
                    case ClickType.Mouse:
                        MouseClick();
                        break;
                    case ClickType.Action:
                        WebAutEngine.Browser.Actions.Click(Element).Perform();
                        break;
                    case ClickType.Java:
                        WebAutEngine.Browser.ScriptExecutor.ExecuteScript("arguments[0].click();", Element);
                        break;
                }
            //}
        }

        /// <summary>
        /// Click on element until the given delegate returns true
        /// </summary>     
        /// <param name="booldelegate">bool delegate</param>
        /// <param name="clickType">The type of clicking</param>
        /// <returns>true if the delegate returned true within timeout</returns>
        public bool Click(BoolDelegate booldelegate, ClickType clickType = ClickType.Standard)
        {
            return Click(booldelegate, UIAutomationSettings.WaitForTimeout, clickType);
        }

        /// <summary>
        /// Click on element until the given delegate returns true
        /// </summary>     
        /// <param name="booldelegate">bool delegate</param>
        /// <param name="timeout">max timeout</param>
        /// <param name="clickType">The type of clicking</param>
        /// <returns>true if the delegate returned true within timeout</returns>
        public bool Click(BoolDelegate booldelegate, int timeout, ClickType clickType = ClickType.Standard)
        {
            Logger.Debug("Click boolDelegate called.");
            DateTime endTime = DateTime.Now.AddSeconds(timeout);
            
            if (WaitFor(booldelegate.Invoke, 0))
            {
                Logger.Debug("Click boolDelegate return true.");
                return true;
            }
            

            Logger.Debug("Click boolDelegate click.");
            do
            {
                try
                {
                    Click(clickType);
                }
                catch (Exception e)
                {
                    Logger.Warning("Element not found to click on.");
                }

                if (WaitFor(booldelegate.Invoke, 1))
                {
                    Logger.Debug("Click boolDelegate return true.");
                    return true;
                }

                Logger.Debug("Click boolDelegate retry.");
            } while (DateTime.Now < endTime);

            Logger.Debug("Click boolDelegate return false.");
            return false;
        }

        /// <summary>
        /// Simple mouse click on the element
        /// </summary>
        public void DoubleClick(ClickType clickType = ClickType.Standard)
        {
            if (Displayed)
            {
                switch (clickType)
                {
                    case ClickType.Standard:

                        Element.Click();
                        Thread.Sleep(250);
                        Element.Click();
                        break;
                    case ClickType.Mouse:
                        MouseClick();
                        Thread.Sleep(250);
                        MouseClick();
                        break;
                    default:
                        WebAutEngine.Browser.Actions.Click(Element).Perform();
                        break;
                }
            }
        }

        /// <summary>
        /// DoubleClick on element until the given delegate returns true
        /// </summary>     
        /// <param name="boolDelegate">bool delegate</param>
        /// <param name="clickType">The type of clicking</param>
        /// <returns>true if the delegate returned true within timeout</returns>
        public bool DoubleClick(BoolDelegate boolDelegate, ClickType clickType = ClickType.Standard)
        {
            return DoubleClick(boolDelegate, 5, clickType);
        }

        /// <summary>
        /// DoubleClick on element until the given delegate returns true
        /// </summary>     
        /// <param name="boolDelegate">bool delegate</param>
        /// <param name="timeout">max timeout</param>
        /// <param name="clickType">The type of clicking</param>
        /// <returns>true if the delegate returned true within timeout</returns>
        public bool DoubleClick(BoolDelegate boolDelegate, int timeout, ClickType clickType = ClickType.Standard)
        {
            Logger.Debug("Click boolDelegate called.");
            DateTime endTime = DateTime.Now.AddSeconds(timeout);

            if (boolDelegate.Invoke())
            {
                Logger.Debug("Click boolDelegate return true.");
                return true;
            }

            Logger.Debug("Click boolDelegate click.");
            do
            {
                DoubleClick(clickType);

                if (WaitFor(boolDelegate.Invoke, 1))
                {
                    Logger.Debug("Click boolDelegate return true.");
                    return true;
                }

                Logger.Debug("Click boolDelegate retry.");
            } while (DateTime.Now < endTime);

            Logger.Debug("Click boolDelegate return false.");
            return false;
        }

        public void DragAndDrop(WebAutElement targetElement)
        {
            WebAutEngine.Browser.Actions.DragAndDrop(Element, targetElement.Element).Build().Perform();
            //ClickAndHold();
            //targetElement.MouseMove();
            //targetElement.Release();
        }

        public void Drag()
        {
            ClickAndHold();
        }

        public void Drop()
        {
            Release();
        }

        /// <summary>
        /// Click on element until a given elem exists
        /// </summary>     
        /// <param name="elementToWaitFor">Element to wait for</param>      
        /// <param name="clickType">The type of clicking</param>
        /// <returns>returns true if element is available</returns>
        public bool Click(WebAutElement elementToWaitFor, ClickType clickType = ClickType.Standard)
        {
            return Click(() => elementToWaitFor.Displayed, UIAutomationSettings.WaitForTimeout, clickType);
        }

        /// <summary>
        /// Click on element until a given elem exists
        /// </summary>     
        /// <param name="elementToWaitFor">Element to wait for</param>      
        /// <param name="timeout">timeout to wait until element is available</param>
        /// <param name="clickType">The type of clicking</param>
        /// <returns>returns true if element is available</returns>
        public bool Click(WebAutElement elementToWaitFor, int timeout, ClickType clickType = ClickType.Standard)
        {
            return Click(() => elementToWaitFor.Displayed, timeout, clickType);
        }

        /// <summary>
        /// Performs a left mouse button (LMB) click action on the current element
        /// </summary>       
        public void MouseClick()
        {
            if (Displayed && WebAutEngine.Browser != null)
            {
                WebAutEngine.Browser.Mouse.Click(((ILocatable)Element).Coordinates);
            }
        }

        /// <summary>
        /// Performs a left mouse button (LMB) click action on the current element
        /// </summary>
        /// <param name="offsetX">
        /// The offset X.
        /// </param>
        /// <param name="offsetY">
        /// The offset Y.
        /// </param>
        public void MouseClick(int offsetX, int offsetY)
        {
            WebAutEngine.Browser.Actions.MoveToElement(Element).MoveByOffset(offsetX, offsetY).Click().Perform();
        }

        /// <summary>
        /// Performs a left mouse button (LMB) click action on the current element
        /// </summary>
        /// <param name="offsetX">
        /// The offset X.
        /// </param>
        /// <param name="offsetY">
        /// The offset Y.
        /// </param>
        public void MouseDoubleClick(int offsetX, int offsetY)
        {
            if (Displayed && WebAutEngine.Browser != null)
            {
                WebAutEngine.Browser.Mouse.DoubleClick(new Cordinates(((ILocatable)Element).Coordinates, offsetX, offsetY));
            }
        }

        /// <summary>
        /// Performs a mouse move action on the current element
        /// </summary>
        /// <param name="offsetX">
        /// The offset X. Default value is 5.
        /// </param>
        /// <param name="offsetY">
        /// The offset Y. Default value is 5.
        /// </param>
        /// <param name="usingJsAtion">
        /// Bool wether java action or Mouse interface should be called
        /// </param>
        public void MouseMove(int offsetX = 5, int offsetY = 5, bool usingJsAtion = true)
        {
            if (Displayed && WebAutEngine.Browser != null)
            {
                if (usingJsAtion)
                {
                    WebAutEngine.Browser.Actions.MoveToElement(Element, offsetX, offsetY).Build().Perform();
                }
                else
                {
                    ICoordinates coordinate = new Cordinates(((ILocatable)Element).Coordinates, offsetX, offsetY);

                    WebAutEngine.Browser.Mouse.MouseMove(coordinate);
                }
            }
        }

        public void ClickAndHold()
        {
            if (Displayed && WebAutEngine.Browser != null)
            {
                WebAutEngine.Browser.Actions.ClickAndHold().Build().Perform();
            }
        }

        public void Release()
        {
            if (Displayed && WebAutEngine.Browser != null)
            {
                WebAutEngine.Browser.Actions.Release().Build().Perform();
            }
        }

        public void ScrollTo()
        {
            WebAutEngine.Browser.ScriptExecutor.ExecuteScript("arguments[0].scrollIntoView(true);", Element);
        }
        #endregion

        #region WaitFor
        /// <summary>
        /// Waits until delegate becomes true
        /// </summary>
        /// <param name="boolDelegate"> bool delegate</param>
        /// <returns>true if delegate returned true within timeout</returns>
        public bool WaitFor(BoolDelegate boolDelegate)
        {
            return WaitFor(boolDelegate, UIAutomationSettings.WaitForTimeout);
        }

        /// <summary>
        /// Waits until delegate becomes true
        /// </summary>
        /// <param name="boolDelegate"> bool delegate</param>
        /// <param name="timeout">max timeout</param>
        /// <returns>true if delegate returned true within timeout</returns>
        public bool WaitFor(BoolDelegate boolDelegate, int timeout)
        {
            DateTime endTime = DateTime.Now.AddSeconds(timeout);

            do
            {
                try
                {
                    if (boolDelegate.Invoke())
                    {
                        Logger.Debug("Click boolDelegate return true.");
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Debug(string.Format("Error executing boolDelegate: {0}.", ex.Message));
                }

                Thread.Sleep(200);
            }
            while (DateTime.Now < endTime);

            return false;
        }

        /// <summary>
        /// Waits until given condition
        /// </summary>  
        /// <returns>returns true if element is available</returns>
        public bool WaitFor()
        {
            return WaitFor(UIAutomationSettings.WaitForTimeout);
        }

        /// <summary>
        /// Waits until given condition
        /// </summary>      
        /// <param name="timeout">timeout to wait until element is available</param>
        /// <returns>returns true if element is available</returns>
        public bool WaitFor(int timeout)
        {
            DateTime endtime = DateTime.Now.AddSeconds(timeout);

            do
            {
                try
                {
                    if (Displayed)
                    {
                        return true;
                    }
                }
                catch 
                {
                    return false;
                }

                Thread.Sleep(200);
            }
            while (DateTime.Now < endtime);

            return false;
        }

        #endregion
        #endregion
        
        private class Cordinates : ICoordinates
        {
            private ICoordinates _cordinate;
            private Point _locationOnScreen;

            public Cordinates(ICoordinates cordinate, int offsetx, int offsety)
            {
                _cordinate = cordinate;
                _locationOnScreen = new Point(cordinate.LocationOnScreen.X + offsetx, cordinate.LocationOnScreen.Y + offsety);
            }

            public object AuxiliaryLocator
            {
                get { return _cordinate.AuxiliaryLocator; }
            }

            public Point LocationInDom
            {
                get { return _cordinate.LocationInDom; }
            }

            public Point LocationInViewport
            {
                get { return _cordinate.LocationInViewport; }
            }

            public Point LocationOnScreen
            {
                get { return _locationOnScreen; }
                set { _locationOnScreen = value; }
            }
        }
    }

    /// <summary>
    /// Tha Style class
    /// </summary>
    public class Style
    {
        /// <summary>
        /// The IWebElement instance
        /// </summary>
        private IWebElement _element;

        /// <summary>
        /// Initializes a new instance of the <see cref="Style"/> class.  
        /// </summary>
        /// <param name="element">
        /// The IWebElement instance
        /// </param>
        public Style(IWebElement element)
        {
            _element = element;
        }

        /// <summary>
        /// Gets the Display 
        /// </summary>
        public string Display
        {
            get { return _element.GetCssValue("display"); }
        }

        /// <summary>
        /// Gets the ZIndex
        /// </summary>
        public string ZIndex
        {
            get { return _element.GetCssValue("z-index"); }
        }

        /// <summary>
        /// Gets the Top
        /// </summary>
        public string Top
        {
            get { return _element.GetCssValue("top"); }
        }

        /// <summary>
        /// Gets the Color
        /// </summary>
        public string Color
        {
            get { return _element.GetCssValue("color"); }
        }

        /// <summary>
        /// Gets the width
        /// </summary>
        public string Width
        {
            get { return _element.GetCssValue("width"); }
        }

        /// <summary>
        /// Gets the CssText
        /// </summary>
        public string CssText
        {
            get
            {
                throw new AutomationException(
                    "Please consider re-implementing the test using more specific CSS properties!");
            }
        }

        /// <summary>
        /// Get Attribute Value
        /// </summary>
        /// <param name="attributeName">
        /// The Attribute name
        /// </param>
        /// <returns>
        /// The Attribute value
        /// </returns>
        public string GetAttributeValue(string attributeName)
        {
            return _element.GetCssValue(attributeName);
        }
    }
}
