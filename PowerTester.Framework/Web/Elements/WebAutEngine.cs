﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;


namespace PowerTester.Framework.Web.Elements
{
    public enum ClickType
    {
        Standard,
        Mouse,
        Action,
        Java
    }

    /// <summary>
    /// The WebAutomationBrowser class.
    /// </summary>
    public static class WebAutEngine
    {
        public static WebAutBrowser Browser { get; set; }

        public static String ChromeDownloadFilepath = "C:/ChromeDownload/";

        /// <summary>
        /// Creates a new instance of Browser class and adds it to the Browsers collection. The object can be reached through the Browser property of this class.
        /// </summary>
        public static void CreateBrowser()
        {
            CreateBrowser(BrowserType.Default);
        }

        /// <summary>
        /// Creates a new instance of Browser class and adds it to the Browsers collection. The object can be reached through the Browser property of this class.
        /// </summary>
        /// <param name="browserType">
        /// Type of the browser.
        /// </param>
        public static void CreateBrowser(BrowserType browserType)
        {
            switch (browserType)
            {
                case BrowserType.Default:
                    CreateBrowser(BrowserType.InternetExplorer);
                    break;

                case BrowserType.InternetExplorer:
                {
                    Browser = new WebAutBrowser(BrowserType.InternetExplorer);
                    Browser.Forward();
                    break;
                }
                case BrowserType.FireFox:
                {
                    Browser = new WebAutBrowser(BrowserType.FireFox);
                    break;
                }
                case BrowserType.Chrome:
                {
                    Browser = new WebAutBrowser(BrowserType.Chrome);
                    break;
                }
                case BrowserType.None:
                {
                    Browser = new WebAutBrowser(BrowserType.None);
                    break;
                }
                default:
                    throw new ArgumentException("The given type for creating browser is not supported!");
            }
        }

        /// <summary>
        /// Closes the browser.
        /// </summary>
        public static void CloseBrowser()
        {
            if (Browser != null)
            {
                try
                {
                    Browser.Close();
                    Browser.Quit();
                }
                catch (Exception)
                {
                }
                
                Browser = null;
            }

            List<Process> processes = new List<Process>();
            switch (General.BrowserType)
            {
                case BrowserType.InternetExplorer:
                {
                    processes = Process.GetProcesses().Where(p => p.ProcessName == "IEDriverServer" || p.ProcessName.ToLower() == "iexplore").ToList();
                    
                    break;
                }
                case BrowserType.Chrome:
                {
                    processes = Process.GetProcesses().Where(p => p.ProcessName == "chromedriver" || p.ProcessName.ToLower() == "chrome").ToList();

                    break;
                }
                default:
                {
                    Logger.Debug("Force close not implemented for browsertype.");
                    break;
                }
            }

            foreach (Process process in processes   )
            {
                try
                {
                    if (process.GetProcessOwner() == Environment.UserDomainName + "/" + Environment.UserName)
                    {
                        process.Kill();
                    }
                }
                catch (Exception)
                {
                    Logger.Debug("Could not kill browser, or driver process.");
                }
            }
        }

        public static string GetProcessOwner(this Process process)
        {
            string query = "Select * From Win32_Process Where ProcessID = " + process.Id;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            ManagementObjectCollection processList = searcher.Get();

            foreach (ManagementObject obj in processList)
            {
                string[] argList = new string[] { string.Empty, string.Empty };
                int returnVal = Convert.ToInt32(obj.InvokeMethod("GetOwner", argList));
                if (returnVal == 0)
                {
                    // return DOMAIN\user
                    return argList[1] + "\\" + argList[0];
                }
            }

            return "NO OWNER";
        }
    }
}
