﻿using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.Web.Interfaces
{
    public abstract class IAlertDialog : IUiMap
    {
        public abstract void Ok();
    }
}
