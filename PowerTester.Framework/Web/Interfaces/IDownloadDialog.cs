﻿using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.Web.Interfaces
{
    public abstract class IDownloadDialog : IUiMap
    {
        public abstract void Save(string fullPath);

        public abstract void Cancel();

        public abstract void Open();
    }
}
