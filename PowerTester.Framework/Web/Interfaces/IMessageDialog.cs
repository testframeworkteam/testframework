﻿using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.Web.Interfaces
{
    public abstract class IMessageDialog : IUiMap
    {
        public abstract void Ok();
        public abstract void Cancel();
        public abstract void Close();
    }
}
