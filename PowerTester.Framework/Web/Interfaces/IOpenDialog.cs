﻿using PowerTester.Framework.UI.Interfaces;

namespace PowerTester.Framework.Web.Interfaces
{
    public abstract class IOpenDialog : IUiMap
    {
        public abstract void Open(string fullPath);

        public abstract void Cancel();

        public abstract void Open();
    }
}
