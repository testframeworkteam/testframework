﻿using PowerTester.Framework.UI.BaseClasses;

namespace PowerTester.Framework.Web.Interfaces
{
    public abstract class IUiMap
    {
        public abstract bool Exists { get; }

        public bool WaitFor()
        {
            return WaitFor(UIAutomationSettings.WaitForTimeout);
        }

        public bool WaitFor(int timeout)
        {
            return General.WaitFor(() => Exists, timeout);
        }
    }
}
