﻿using PowerTester.Framework.UI.BaseClasses;
using PowerTester.Framework.Web.Elements;

namespace PowerTester.Framework.Web.Interfaces
{
    public abstract class IUiObject
    {
        public IUiObject(WebAutElement root)
        {
            _root = root;
        }

        private readonly WebAutElement _root;

        protected WebAutElement Root { get { return _root; } }

        public bool Exists { get { return _root.Displayed; } }

        public bool WaitFor()
        {
            return WaitFor(UIAutomationSettings.WaitForTimeout);
        }

        public bool WaitFor(int timeout)
        {
            return General.WaitFor(() => Exists, timeout);
        }
    }
}
