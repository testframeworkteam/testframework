﻿namespace PowerTester.Framework.Web.Interfaces
{
    /// <summary>
    /// The WebAutomationElement interface.
    /// </summary>
    public interface IWebAutElement
    {
        string HtmlTag { get; }

        /// <summary>
        /// Gets a value indicating whether the element is displayed.
        /// </summary>
        bool Displayed { get; }

        /// <summary>
        /// Gets a value indicating whether the element is enabled.
        /// </summary>
        bool Enabled { get; }

        /// <summary>
        /// Gets a value indicating whether the element is disabled.
        /// </summary>
        bool Disabled { get; }

        /// <summary>
        /// Gets a value indicating whether the element exists.
        /// </summary>
        bool Exists { get; }
        
        /// <summary>
        /// Gets the text of the element.
        /// </summary>
        string Text { get; }

        /// <summary>
        /// Gets a value indicating whether the element is read only.
        /// </summary>
        bool ReadOnly { get; }

        /// <summary>
        /// Gets a value indicating whether the element is selected.
        /// </summary>
        bool Selected { get; }
        
        /// <summary>
        /// Gets the Value.
        /// </summary>
        string Value { get; }

        bool WaitFor();
    }
}