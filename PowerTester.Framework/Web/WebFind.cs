namespace PowerTester.Framework.Web
{
    /// <summary>
    /// The Find class
    /// </summary>
    public static class WebFind
    {
        /// <summary>
        /// ByClass Constraint
        /// </summary>
        /// <param name="className">
        /// The class name
        /// </param>
        /// <param name="firstChild">
        /// Set true to search only first child level
        /// </param>
        /// <returns>
        /// The constraint
        /// </returns>
        public static WebFinderConstraint ByClass(string className, bool firstChild = false)
        {
            return new WebFinderConstraint(className, FindByType.ClassName, firstChild);
        }

        /// <summary>
        /// ByClass Constraint
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="firstChild">
        /// Set true to search only first child level
        /// </param>
        /// <returns>
        /// The constraint
        /// </returns>
        public static WebFinderConstraint ById(string value, bool firstChild = false)
        {
            return new WebFinderConstraint(value, FindByType.Id, firstChild);
        }

        /// <summary>
        /// ByClass Constraint
        /// </summary>
        /// <param name="name">
        /// The name to look for.
        /// </param>
        /// <param name="firstChild">
        /// Set true to search only first child level
        /// </param>
        /// <returns>
        /// The constraint
        /// </returns>
        public static WebFinderConstraint ByName(string name, bool firstChild = false)
        {
            return new WebFinderConstraint(name, FindByType.Name, firstChild);
        }

        /// <summary>
        /// ByClass Constraint
        /// </summary>
        /// <param name="text">
        /// The text to look for.
        /// </param>
        /// <param name="firstChild">
        /// Set true to search only first child level
        /// </param>
        /// <returns>
        /// The constraint
        /// </returns>
        public static WebFinderConstraint ByText(string text, bool firstChild = false)
        {
            return new WebFinderConstraint(text, FindByType.Text, firstChild);
        }

        /// <summary>
        /// ByClass Constraint
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="firstChild">
        /// Set true to search only first child level
        /// </param>
        /// <returns>
        /// The constraint
        /// </returns>
        public static WebFinderConstraint ByTitle(string value, bool firstChild = false)
        {
            return new WebFinderConstraint(value, FindByType.Title, firstChild);
        }

        /// <summary>
        /// ByClass Constraint
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="firstChild">
        /// Set true to search only first child level
        /// </param>
        /// <returns>
        /// The constraint
        /// </returns>
        public static WebFinderConstraint ByValue(string value, bool firstChild = false)
        {
            return new WebFinderConstraint(value, FindByType.Value, firstChild);
        }

        public static WebFinderConstraint ByAttribute(string attributeName, string attributeValue, bool firstChild = false)
        {
            return new WebFinderConstraint(attributeName, attributeValue, FindByType.Attribute, firstChild);
        }
    }
}