using System;
using PowerTester.Framework.Web.Elements;

namespace PowerTester.Framework.Web
{

    /// <summary>
    /// Enum for FindBy  type
    /// </summary>
    public enum FindByType
    {
        None,

        Index,

        /// <summary>
        /// Uses class="" attribute for search
        /// </summary>
        [System.ComponentModel.Description("class")]
        ClassName,

        /// <summary>
        /// Uses id="" attribute for search
        /// </summary>
        [System.ComponentModel.Description("id")]
        Id,

        /// <summary>
        /// Uses name="" attribute for search
        /// </summary>
        [System.ComponentModel.Description("name")]
        Name,

        /// <summary>
        /// Searches in the texts of html nodes.
        /// </summary>
        [System.ComponentModel.Description("text")]
        Text,

        /// <summary>
        /// Uses title="" attribute for search
        /// </summary>
        [System.ComponentModel.Description("title")]
        Title,

        /// <summary>
        /// Uses value="" attribute for search
        /// </summary>
        [System.ComponentModel.Description("value")]
        Value,

        /// <summary>
        /// Seach by custom attribute
        /// </summary>
        [System.ComponentModel.Description("attribute")]
        Attribute
    }

    /// <summary>
    /// The FindByConstraint class
    /// </summary>
    public class WebFinderConstraint
    {
        private FindByType _findBy = FindByType.None;

        public WebFinderConstraint(string value, FindByType findBy, bool firstChild)
        {
            FindBy = findBy;
            Value = value;
            FirstChild = firstChild;
        }

        public WebFinderConstraint(string key, string value, FindByType findBy, bool firstChild)
        {
            FindBy = findBy;
            Key = key;
            Value = value;
            FirstChild = firstChild;
        }

        public WebFinderConstraint(bool firstChild)
        {
            FirstChild = firstChild;
        }
 
        public FindByType FindBy
        {
            get { return _findBy; }
            set { _findBy = value; }
        }

        public string Value { get; set; }

        //public Regex RegExValue { get; set; }

        public bool FirstChild { get; set; }

        public string Key { get; set; }

        public string ToXPath<T>(string rootXPath = "") where T : WebAutElement
        {
            string xpath = rootXPath;

            if (FirstChild)
            {
                xpath = xpath + "/";
            }
            else
            {
                xpath = xpath + "//";
            }

            if (typeof (T) == typeof (WebAutElement))
            {
                xpath = xpath + "*";
            }
            else
            {
                xpath = xpath + ((T) Activator.CreateInstance(typeof (T), "")).HtmlTag;
            }

            if (FindBy == FindByType.Index)
            {
                xpath = string.Format("({0})[{1}]", xpath, Value);
            }
            else
            {
                switch (FindBy)
                {
                    case FindByType.None:
                    {
                        return xpath;
                    }
                    case FindByType.Id:
                    {
                        Key = "@id";
                        break;
                    }
                    case FindByType.ClassName:
                    {
                        Key = "@class";
                        break;
                    }
                    case FindByType.Name:
                    {
                        Key = "@name";
                        break;
                    }
                    case FindByType.Text:
                    {
                        Key = "text()";
                        break;
                    }
                    case FindByType.Value:
                    {
                        Key = ".";
                        break;
                    }
                    case FindByType.Title:
                    {
                        Key = "@title";
                        break;
                    }
                    case FindByType.Attribute:
                    {
                        Key = "@" + Key;
                        break;
                    }
                }

                if (Value.Contains("*"))
                {
                    xpath = string.Format("{0}[contains({1}, '{2}')]", xpath, Key, Value.Replace("*", ""));
                }
                else
                {
                    xpath = string.Format("{0}[{1}='{2}']", xpath, Key, Value);
                }
            }
            return xpath;
        }
    }
}