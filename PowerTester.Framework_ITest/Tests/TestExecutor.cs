﻿using NUnit.Framework;
using PowerTester.Framework;
using PowerTester.Framework.BaseClasses;

namespace PowerTester.Framework_ITest.Tests
{
    public class TestExecutor:TestBase
    {

        public override void OneTimeSetUp()
        {
        }

        [Test]
        [IncludeEnvironment("Local")]
        public void IncludeLocal()
        {
            TestExecuter.Execute(this, ()=> TestExecuter.Assert.AreEqual("Local", General.TestEnvironment, "Test was not executed on the right environment."));
        }

        [Test]
        [IncludeEnvironment("Integration")]
        public void IncludeIntegration()
        {
            TestExecuter.Execute(this, () => TestExecuter.Assert.AreEqual("Integration", General.TestEnvironment, "Test was not executed on the right environment."));
        }

        [Test]
        [ExcludeEnvironment("Local")]
        public void ExcludeLocal()
        {
            TestExecuter.Execute(this, () => TestExecuter.Assert.AreNotEqual("Local", General.TestEnvironment, "Test was not executed on the right environment."));
        }

        [Test]
        [ExcludeEnvironment("Integration")]
        public void ExcludeIntegration()
        {
            TestExecuter.Execute(this, () => TestExecuter.Assert.AreNotEqual("Integration", General.TestEnvironment, "Test was not executed on the right environment."));
        }

    }
}
